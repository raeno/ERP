SimpleCov.start do
  add_group "Actions", "app/actions"
  add_group "Services", "app/services"
  add_group "Presenters", "app/presenters"
  add_group "Long files" do |src_file|
    src_file.lines.count > 100
  end
end
