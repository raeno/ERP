# frozen_string_literal: true

source 'https://rubygems.org'
ruby '2.3.3'

gem 'coffee-rails', '~> 4.2'
gem 'devise'
gem 'hamlit'
gem 'pg'
gem 'rails', '5.0.3'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'

gem 'sidekiq', '< 6'
gem 'sidekiq-unique-jobs'

gem 'aws-sdk', '~> 2'
gem 'barby'  # Barcode generator
gem 'bootstrap_form'  # https://github.com/bootstrap-ruby/rails-bootstrap-forms
gem 'chunky_png'
gem 'jquery-rails'
gem 'nested_form_fields'
gem 'pdf-writer'
gem 'peddler'  # Amazon MWS API in Ruby
gem 'prawn'  # PDF Writer
gem 'sinatra', git: 'https://github.com/sinatra/sinatra.git', require: nil
gem 'therubyracer', platforms: :ruby
gem 'wicked_pdf'

# bulk create activerecord models
gem 'activerecord-import'

gem 'active_model_serializers', '~> 0.10.0'

# memcache integration
gem 'dalli'

# validations made right
gem 'dry-validation', '~> 0.10.7'

# better logging
gem 'awesome_print'

# pagination
gem 'will_paginate', '~> 3.1.0'

# pub/sub implementation
gem 'wisper', '~> 2.0'

# JS stuff
gem 'webpacker', '~> 2.0'

# error tracking
gem 'rollbar'

# multithreaded web-server
gem 'puma'

# form objects
gem "reform", "~> 2.2.0"
gem "reform-rails", "~> 0.1.7"

group :production do
  gem 'rails_12factor'

  # heroku memcachier addon configuration
  gem 'memcachier'
  gem 'newrelic_rpm'
end

group :development, :test do
  gem 'dotenv-rails'
  gem 'pry-nav'

  gem 'factory_girl_rails'
  gem 'minitest-reporters'
  gem 'minitest-spec-rails'
  gem 'mocha'

  gem 'annotate'  # schema info in model/factory files
  gem 'database_cleaner'

  # codestyle & code quality validation
  gem 'flay'
  gem 'pronto'
  gem 'pronto-brakeman', require: false
  gem 'pronto-coffeelint', require: false
  gem 'pronto-flay', require: false
  gem 'pronto-reek', require: false
  gem 'pronto-rubocop', require: false

  gem 'foreman'

  gem 'bullet'
end

group :development do
  gem 'web-console', '~> 2.0'
  gem 'spring'

 # gem 'httplog'

  gem 'rails-erd', require: false # erd diagrams

  gem "better_errors"     # better error pages with a debug console
  gem "binding_of_caller" # better_errors dependency
  gem 'guard'
  gem 'guard-ctags-bundler'
  gem 'guard-minitest'
  gem 'ripper-tags'

  gem 'httplog'

  # debug
  gem 'byebug'
  gem 'pry-byebug'

  gem 'rack-mini-profiler'
end

group :test do
  gem 'simplecov', require: false
  gem 'timecop'

  # prawn PDF testing
  gem 'pdf-inspector', require: "pdf/inspector"

  gem 'capybara'
  gem 'capybara-screenshot'
  gem 'launchy'
  gem 'minitest-capybara'
  gem 'poltergeist'
  gem 'rails-controller-testing'
end
