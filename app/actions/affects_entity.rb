# frozen_string_literal: true
module AffectsEntity
  def call_on(entity, action)
    if entity.send(action)
      StoreResult.new entity: entity, success: true
    else
      StoreResult.new entity: entity, success: false, errors: entity.errors.full_messages
    end
  end

  def call_on_lockable(entity, action)
    return StoreResult.new(entity: entity, success: false, errors: ["Cannot #{action}. The record is read-only."]) if locked?(entity)

    call_on(entity, action)
  end

  private

  def locked?(entity)
    entity.try(:locked?) && record_locking_enabled?
  end

  def record_locking_enabled?
    Rails.configuration.kisoils.allow_record_locking
  end
end
