# frozen_string_literal: true
module AmazonInventories
  class AddHistory
    include AffectsEntity

    def call(amazon_inventory)
      return StoreResult.new(entity: nil, success: false, errors: ['Amazon inventory could not be nil']) unless amazon_inventory

      params = prepare_history_attributes(amazon_inventory)
      inventory_history = AmazonInventoryHistory.new params
      call_on inventory_history, :save
    end

    private

    def prepare_history_attributes(amazon_inventory)
      allowed_attributes = %i(fulfillable reserved inbound_working inbound_shipped)

      params = amazon_inventory.attributes.select { |attr| allowed_attributes.include?(attr.to_sym) }
      params[:reported_at] = Time.now.utc
      params[:amazon_inventory] = amazon_inventory
      params
    end
  end
end
