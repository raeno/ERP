# frozen_string_literal: true
module AmazonInventories
  class UpdateAmazonInventory
    include AffectsEntity

    def self.build
      add_history_action = AddHistory.new
      new(add_history_action)
    end

    def initialize(add_history_action)
      @add_history_action = add_history_action
    end

    def call(amazon_inventory, params)
      return StoreResult.new(entity: amazon_inventory, success: false, errors: 'amazon_inventory is empty') unless amazon_inventory

      # update attributes first
      updated_params = prepare_params(amazon_inventory, params)
      amazon_inventory.attributes = updated_params

      # then create history entry with new values
      add_history_action.call amazon_inventory
      call_on amazon_inventory, :save
    end

    private

    attr_reader :add_history_action

    def prepare_params(amazon_inventory, params)
      if params[:reserved]
        reserved = AmazonInventoryHistory.smooth_reserved(amazon_inventory, params[:reserved])
        params[:reserved] = reserved
      end
      params
    end
  end
end
