module AmazonProcessing

  # First stage of shipment on Amazon. Send CreateInboundShipmentPlan query to
  # MWS API and updates AmazonShipment record with received shipment_id. 
  #
  # Currenly we ignore received InboundShipmentPlanItems since user has already
  # seen allocations and decided on exactly this amount to specific warehouse
  class InitializeShipment < BaseAction
    attr_reader :shipment_processor, :update_shipment, :update_entries

    def self.build
      shipment_address = Setting.instance.shipment_address.to_h
      shipment_processor = AmazonInboundShipmentProcessor.build(shipment_address)
      update_shipment = AmazonShipments::UpdateShipment.build
      update_entries = ShipmentEntries::UpdateFromShipmentPlans.build
      new(shipment_processor, update_shipment, update_entries)
    end

    def initialize(shipment_processor, update_shipment, update_entries)
      @shipment_processor = shipment_processor
      @update_shipment = update_shipment
      @update_entries = update_entries
    end

    def call(amazon_shipment)
      return EntityResult.new(entity: amazon_shipment, success: true) if amazon_shipment.initialized?

      entries = amazon_shipment.shipment_entries.need_sync_with_amazon.includes(:product)

      result = shipment_processor.create_shipment_plan(entries)
      return EntityResult.new(entity: amazon_shipment, success: false, errors: result.errors) unless result.success?

      shipment_id = extract_shipment_id result
      update_entries.call entries, result.entities
      update_shipment.call amazon_shipment, shipment_id: shipment_id, status: 'INITIALIZED'
    end

    private

    def extract_shipment_id(result)
      shipment_plan_item = result.entities.first
      shipment_plan_item&.shipment_id
    end
  end
end

