# frozen_string_literal: true

module AmazonProcessing
  # prepares info to update existing shipment on Amazon
  # and performs update
  class PatchShipment < BaseAction
    attr_reader :shipment_processor, :shipment_address, :update_shipment

    def self.build
      shipment_address = Setting.instance.shipment_address.to_h
      shipment_processor = AmazonInboundShipmentProcessor.build(shipment_address)
      update_shipment = AmazonShipments::UpdateShipment.build
      new(shipment_processor, shipment_address, update_shipment)
    end

    def initialize(shipment_processor, shipment_address, update_shipment)
      @shipment_address = shipment_address
      @shipment_processor = shipment_processor
      @update_shipment = update_shipment
    end

    def call(amazon_shipment)
      return EntityResult.nil_entity_error unless amazon_shipment
      return EntityResult.new(entity: amazon_shipment, success: true) if amazon_shipment.synced?

      shipment_info = ShipmentInfo.from_shipment amazon_shipment, shipment_address

      result = shipment_processor.update_shipment shipment_info
      return EntityResult.new entity: amazon_shipment, success: false, errors: result.errors unless result.success?
      update_shipment.call amazon_shipment, status: 'AMAZON_SYNCED'
    end
  end
end
