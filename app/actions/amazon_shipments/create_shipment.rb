# frozen_string_literal: true

module AmazonShipments
  class CreateShipment < BaseAction
    include AffectsEntity
    attr_reader :create_shipment_entries

    NEW_SHIPMENT_STATUS = 'DRAFT'

    def self.build
      create_shipment_entries = ShipmentEntries::CreateEntries.build
      new(create_shipment_entries)
    end

    def initialize(create_shipment_entries)
      @create_shipment_entries = create_shipment_entries
    end

    def call(user, params)
      shipment = init_shipment user, params

      actions = [
        -> { call_on shipment, :save },
        -> { create_shipment_entries.call shipment, params[:shipment_entries] }
      ]
      result = in_transaction_with_rollback actions
      StoreResult.new entity: parse_result(result), success: result.success?, errors: result.errors
    end

    private

    def init_shipment(user, params)
      attributes = params.except(:shipment_entries).merge(user: user, status: NEW_SHIPMENT_STATUS)
      shipment = AmazonShipment.new attributes
      shipment
    end

    def parse_result(combined_result)
     shipment, *entries = combined_result.entities
     OpenStruct.new shipment: shipment, entries: entries
    end
  end
end
