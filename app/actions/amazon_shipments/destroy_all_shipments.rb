# frozen_string_literal: true

module AmazonShipments
  class DestroyAllShipments < BaseAction
    def call
      AmazonShipment.destroy_all
    end
  end
end
