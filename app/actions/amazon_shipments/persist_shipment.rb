# frozen_string_literal: true
module AmazonShipments
  class PersistShipment
    attr_reader :create_shipment, :update_shipment

    ALLOWED_KEYS = AmazonShipment.attribute_names.map(&:to_sym).freeze

    def self.build
      create_shipment = SimpleActions::Create.new(AmazonShipment)
      update_shipment = SimpleActions::Update.new(AmazonShipment)
      new(create_shipment, update_shipment)
    end

    def initialize(create_shipment, update_shipment)
      @create_shipment = create_shipment
      @update_shipment = update_shipment
    end

    def call(shipment_params)
      params = shipment_params.slice(*ALLOWED_KEYS)
      shipment_id = params[:shipment_id]

      return StoreResult.new(entity: nil, success: false, errors: ['Shipment ID is required to persist shipment']) unless shipment_id

      shipment = AmazonShipment.find_or_initialize_by shipment_id: shipment_id
      if shipment.persisted?
        update_shipment.call shipment, params
      else
        create_shipment.call params
      end
    end
  end
end
