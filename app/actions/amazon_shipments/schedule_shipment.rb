# frozen_string_literal: true

module AmazonShipments
  class ScheduleShipment < BaseAction
    attr_reader :create_shipment

    def self.build
      create_shipment = CreateShipment.build
      new(create_shipment)
    end

    def initialize(create_shipment)
      @create_shipment = create_shipment
    end

    def call(user, params)
      result = create_shipment.call user, params
      return result unless result.success?

      shipment = result.entity.shipment

      job = ShipProductsJob.perform_later shipment
      ShipmentResult.new job: job, shipment_name: shipment.name, success: true, errors: []
    end
  end
end
