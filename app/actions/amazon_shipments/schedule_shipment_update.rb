# frozen_string_literal: true
module AmazonShipments
  class ScheduleShipmentUpdate < BaseAction
    attr_reader :persist_entries, :update_shipment_job

    def self.build
      persist_entries = ShipmentEntries::PersistEntries.build
      update_shipment_job = UpdateExistingShipmentJob
      new(persist_entries, update_shipment_job)
    end

    def initialize(persist_entries, update_shipment_job)
      @persist_entries = persist_entries
      @update_shipment_job = update_shipment_job
    end

    def call(params, user)
      shipment = params.delete :shipment
      result = persist_entries.call shipment, params[:shipment_entries]
      return result unless result.success?

      job = update_shipment_job.perform_later shipment
      ShipmentResult.new job: job, success: true, shipment_name: shipment.name, errors: []
    end
  end
end
