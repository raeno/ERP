module AmazonShipments
  class UpdateShipment < BaseAction
    include AffectsEntity
    attr_reader :update_entry

    def self.build
      update_entry = SimpleActions::Update.new(ShipmentEntry)
      new(update_entry)
    end

    def initialize(update_entry)
      @update_entry = update_entry
    end

    def call(shipment, params)
      return StoreResult.nil_entity_error unless shipment

      shipment.attributes = params
      update_shipment_entries(shipment) if shipment.was_synced_with_amazon?

      call_on shipment, :save
    end

    private

    def update_shipment_entries(shipment)
      entries = shipment.shipment_entries.need_sync_with_amazon

      results = entries.map do |entry|
        update_entry.call entry, status: 'AMAZON_SYNCED'
      end
      CombinedResult.new *results
    end
  end
end

