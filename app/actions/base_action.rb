# frozen_string_literal: true
class BaseAction
  include Logging
  include Wisper::Publisher
  include WithRollback

  # by default .build is aliased to #new
  # descendants should override it to pass preconfigured dependencies to
  # constructor
  def self.build
    new
  end
end
