# frozen_string_literal: true
module Batches
  class CreateBatch < BaseAction
    include FormattedDate
    include AffectsEntity

    attr_reader :update_inventory_action, :track_prime_cost, :finish_task

    def self.build
      inventories_action = Products::MoveProductsInBatch.build
      track_prime_cost = Products::TrackPrimeCost.build
      finish_task = Tasks::FinishTask.build
      new inventories_action, track_prime_cost, finish_task
    end

    def initialize(update_inventory_action, track_prime_cost, finish_task)
      @update_inventory_action = update_inventory_action
      @track_prime_cost = track_prime_cost
      @finish_task = finish_task
    end

    def call(params)
      batch = init_batch params
      actions = [
        -> { call_on batch, :save },
        -> { update_inventory_action.call batch },
        -> { track_prime_cost.call batch },
        -> { finish_related_task batch }
      ]

      in_transaction_with_rollback actions
    end

    private

    def init_batch(params)
      batch = Batch.new params
      if params[:completed_on] && batch.valid?
        batch.completed_on = friendly_string_to_date(params[:completed_on])
      end
      batch
    end

    def finish_related_task(batch)
      return StoreResult.new(entity: nil, success: true) if batch.ship_zone?
      finish_task.call batch.task
    end
  end
end
