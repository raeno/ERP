# frozen_string_literal: true
module Batches
  class CreateShipmentBatch < BaseAction
    class ShipmentBatchParams
      def initialize(user, entry, shipment_id)
        @user = user
        @entry = entry
        @shipment_id = shipment_id
      end

      def product_id
        product = Product.find_by seller_sku: @entry.seller_sku
        product&.id
      end

      def completed_on
        Time.zone.today.strftime("%m/%d/%Y")
      end

      def zone_id
        Zone.ship_zone&.id
      end

      def to_h
        { product_id: product_id, quantity: @entry.items_quantity,
          completed_on: completed_on, zone_id: zone_id,
          amazon_shipment_id: @shipment_id,
          user_ids: [@user.id] }
      end
    end

    attr_reader :create_batch, :track_prime_cost

    def self.build
      create_batch = Batches::CreateBatch.build
      new(create_batch)
    end

    def initialize(create_batch)
      @create_batch = create_batch
    end

    def call(user, shipment_entry, shipment_id)
      batch_params = ShipmentBatchParams.new user, shipment_entry, shipment_id
      create_batch.call batch_params.to_h
    end
  end
end
