# frozen_string_literal: true
module Batches
  class SyncShipmentBatches < BaseAction
    attr_reader :create_batch, :update_batch, :update_entry, :update_shipment

    def self.build
      create_batch = CreateShipmentBatch.build
      update_batch = ChangeBatch.build
      update_entry = ShipmentEntries::UpdateEntry.build
      update_shipment = AmazonShipments::UpdateShipment.build
      new(create_batch, update_batch, update_entry, update_shipment)
    end

    def initialize(create_batch, update_batch, update_entry, update_shipment)
      @create_batch = create_batch
      @update_batch = update_batch
      @update_entry = update_entry
      @update_shipment = update_shipment
    end

    # Fetches amazon shipment entries that don't have batches yet
    # Updates those that already have batch linked, creates batches for orphan ones
    #
    # @return EntityResult with shipment inside #entity
    def call(amazon_shipment)

      entries_to_sync = amazon_shipment.shipment_entries.need_sync_batches.includes(:batch)

      results = entries_to_sync.map do |entry|
        sync_batch entry, amazon_shipment
      end
      sync_result = CombinedResult.new(*results)

      if sync_result.success?
        update_shipment.call amazon_shipment, status: 'BATCHES_SYNCED'
      else
        EntityResult.new entity: amazon_shipment, success: false, errors: sync_result.errors
      end
    end

    private

    def sync_batch(entry, amazon_shipment)
      update_entry_params = { status: 'SYNCED' }
      batch_result = persist_batch entry, amazon_shipment
      update_entry_params[:batch] = batch_result.entity if batch_result.success?
      sync_result = update_entry.call entry, update_entry_params
      CombinedResult.new batch_result, sync_result
    end

    def persist_batch(entry, shipment )
      if entry.batch # we update quantity for existing batches
        update_batch.call entry.batch, quantity: entry.items_quantity
      else # or we create new batch with shipment details
        create_batch.call shipment.user, entry, shipment.shipment_id
      end
    end
  end
end
