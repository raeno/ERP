# frozen_string_literal: true
module Fba
  class CreateFbaAllocation
    include AffectsEntity

    def call(allocation_params)
      fba_allocation = FbaAllocation.new allocation_params
      call_on fba_allocation, :save
    end
  end
end
