# frozen_string_literal: true
module Fba
  class DeleteAllocations

    # accepts both ShipmentEntry and ShipmentItem
    def call(shipment_entry)
      params = { product: shipment_entry.product,
                 quantity_to_allocate: shipment_entry.quantity,
                 shipment_type: shipment_entry.shipment_type }
      allocations = FbaAllocation.where(params)

      if allocations.destroy_all
        TransactionResult.new success: true, errors: []
      else
        TransactionResult.new success: false, errors: ["Failed to destroy some allocations"],
                              failed: allocations
      end
    end
  end
end
