# frozen_string_literal: true
module Ingredients
  class IncrementQuantity < BaseAction
    attr_reader :increment_inventory

    def self.build
      increment_inventory_quantity = Inventories::IncrementQuantity.build
      new increment_inventory_quantity
    end

    def initialize(increment_inventory)
      @increment_inventory = increment_inventory
    end

    # :reek:FeatureEnvy
    def call(ingredient, amount)
      amount *= ingredient.quantity
      increment_inventory.call ingredient.inventory, amount
    end
  end
end
