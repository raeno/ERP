# frozen_string_literal: true
module Ingredients
  class UpdateIngredients < BaseAction
    include WithRollback
    attr_reader :increment_quantity

    def self.build
      increment_quantity = Ingredients::IncrementQuantity.build
      new increment_quantity
    end

    def initialize(increment_quantity)
      @increment_quantity = increment_quantity
      @context = Context.new
    end

    def call(product, zone, amount: 0)
      return TransactionResult.new success: false, errors: 'Product could not be nil' unless product
      return TransactionResult.new success: true if amount == 0

      ingredients = product.ingredients_for_zone(zone)
      update_inventories ingredients, amount
    end

    def rollback
      return if context.empty?
      amount = -context.amount
      update_inventories(context.ingredients, amount)
      log "Rolled back changed ingredients"
    end

    private

    attr_reader :context

    def update_inventories(ingredients, amount)
      actions = ingredients.map do |ingredient|
        -> { increment_quantity.call ingredient, amount }
      end
      result = in_transaction_with_rollback actions
      store_context(ingredients, amount) if result.success?
      result
    end

    def store_context(ingredients, amount)
      context.ingredients = ingredients
      context.amount = amount
    end
  end
end
