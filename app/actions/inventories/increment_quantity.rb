# frozen_string_literal: true
module Inventories
  class IncrementQuantity < BaseAction
    include AffectsEntity

    def self.build
      new.tap do |instance|
        instance.subscribe(InventoryListener.build)
      end
    end

    # increment can be positive or negative
    def call(inventory, increment)
      inventory.quantity += increment
      if inventory.quantity.negative?
        error = "Not enough #{inventory.name}"
        return StoreResult.new(success: false, entity: inventory, errors: [error])
      end

      result = call_on inventory, :save
      publish(:inventory_quantity_changed, inventory, increment) if result.success?
      result
    end
  end
end
