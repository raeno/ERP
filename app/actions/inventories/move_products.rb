# frozen_string_literal: true
module Inventories
  class MoveProducts < BaseAction
    include WithRollback
    attr_reader :increment_quantity

    def self.build
      increment_quantity = Inventories::IncrementQuantity.build
      new(increment_quantity)
    end

    def initialize(increment_quantity)
      @context = Context.new
      @increment_quantity = increment_quantity
    end

    def call(from, to, amount)
      log "Moving from #{from} to #{to} #{amount} of product"
      result = move_products from, to, amount
      store_context from, to, amount
      result
    end

    def rollback
      return if context.empty?
      from = context.destination_inventory
      to = context.source_inventory
      amount = context.amount
      log "Moving from #{from} to #{to} #{amount} of product"
      move_products from, to, amount
      log "Rolled back products moved between inventories"
    end

    private

    attr_reader :context

    def move_products(from, to, amount)
      actions = []
      actions << -> { increment_quantity.call(from, -amount) } if from
      actions << -> { increment_quantity.call(to, amount) } if to
      in_transaction_with_rollback actions
    end

    def store_context(source_inventory, destination_inventory, amount)
      context.source_inventory = source_inventory
      context.destination_inventory = destination_inventory
      context.amount = amount
    end
  end
end
