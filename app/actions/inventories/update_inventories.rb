# frozen_string_literal: true
module Inventories
  class UpdateInventories < BaseAction

    def self.build
      inventory_update = MoveProducts.build
      ingredients_update = Ingredients::UpdateIngredients.build
      new(inventory_update, ingredients_update)
    end

    def initialize(inventory_update, ingredients_update)
      @inventory_update = inventory_update
      @ingredients_update = ingredients_update
    end

    def call(product, source_inventory, destination_inventory, amount)
      inventory_result = inventory_update.call(source_inventory, destination_inventory, amount)

      params = define_zone_and_amount(source_inventory, destination_inventory, amount)
      ingredients_result = ingredients_update.call product, params.zone, amount: params.amount
      CombinedResult.new inventory_result, ingredients_result
    end

    def rollback
      inventory_update.rollback
      ingredients_update.rollback
      log "Rolled back inventories update"
    end

    private

    def define_zone_and_amount(source, destination, amount)
      return OpenStruct.new inventory: source, amount: 0 unless source || destination
      if substract_ingredients?(source, destination)
        OpenStruct.new zone: destination.zone, amount: -amount
      else
        OpenStruct.new zone: source.zone, amount: amount
      end
    end

    def substract_ingredients?(source, destination)
      return false unless destination
      return true unless source
      source.production_order < destination.production_order
    end

    attr_reader :inventory_update, :ingredients_update
  end
end
