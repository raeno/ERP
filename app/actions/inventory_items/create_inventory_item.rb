# frozen_string_literal: true
module InventoryItems
  class CreateInventoryItem
    include PolymorphicParams
    include Logging
    include AffectsEntity

    SKIPPED_INVENTORY_CREATION_ERROR = 'Skipped stock item creation because of errors in inventory_item'

    def self.build
      action = SimpleActions::Create.new(Inventory)
      new(inventory_action: action)
    end

    def initialize(inventory_action:)
      @inventory_action = inventory_action
    end

    def call(inventory_item_params)
      params = prepare_params inventory_item_params
      inventory_item = InventoryItem.new params

      inventory_item_result = save_inventory_item(inventory_item)

      inventory_result = create_inventory(inventory_item_result)
      inventory_result ||= StoreResult.new entity: Inventory.new, success: false,
                                           errors: [SKIPPED_INVENTORY_CREATION_ERROR]

      CombinedResult.new inventory_item_result, inventory_result
    end

    private

    attr_reader :inventory_action

    def save_inventory_item(inventory_item)
      call_on inventory_item, :save
    end

    def create_inventory(inventory_item_result)
      return nil if inventory_item_result.failure?
      inventory_item = inventory_item_result.entity
      inventory_action.call quantity: 0, inventory_item_id: inventory_item.id
    end
  end
end
