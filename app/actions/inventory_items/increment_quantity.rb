# frozen_string_literal: true
module InventoryItems
  class IncrementQuantity < BaseAction
    attr_reader :increment_inventory
    INCREMENT_FAILED_ERROR = 'Cannot increment quantity for product in ship zone'

    def self.build
      increment_inventory = Inventories::IncrementQuantity.build
      new increment_inventory
    end

    def initialize(increment_inventory)
      @increment_inventory = increment_inventory
    end

    # :reek:FeatureEnvy
    def call(inventory_item, quantity)
      if inventory_item.product_in_ship_zone?
        return StoreResult.new(success: false, entity: inventory_item,
                               errors: [INCREMENT_FAILED_ERROR])
      end

      inventory = inventory_item.inventory
      increment_inventory.call inventory, quantity
    end
  end
end
