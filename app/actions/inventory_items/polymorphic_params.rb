# frozen_string_literal: true
module InventoryItems
  module PolymorphicParams
    include Logging

    def prepare_params(params)
      inventoriable_id = params.fetch(:inventoriable_id, '')

      parsed_id = YAML.load(inventoriable_id)
      if parsed_id && parsed_id.is_a?(Array)
        id, type = parsed_id
        params[:inventoriable_id] = id
        params[:inventoriable_type] = type.capitalize
      end
      params
    end
  end
end
