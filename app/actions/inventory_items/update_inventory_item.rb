# frozen_string_literal: true
module InventoryItems
  class UpdateInventoryItem
    include PolymorphicParams
    include AffectsEntity

    def call(inventory_item, inventory_item_params)
      return StoreResult.new(entity: nil, success: false,
                             errors: ['inventory item should not be empty']) unless inventory_item
      params = prepare_params inventory_item_params
      params = sync_production_zone(params) if inventory_item.material?

      inventory_item.attributes = params
      call_on inventory_item, :save
    end

    private

    def sync_production_zone(params)
      zone_id = params[:zone_id]
      params[:production_zone_id] = zone_id if zone_id
      params
    end
  end
end
