# frozen_string_literal: true
module InvoiceItems
  class CreateInvoiceItem < BaseAction
    include AffectsEntity
    attr_reader :create_material_container

    def self.build
      create_material_container = MaterialContainers::CreateMaterialContainer.build
      new(create_material_container)
    end

    def initialize(create_material_container)
      @create_material_container = create_material_container
    end

    def call(params)
      container_params = params.delete(:material_container)
      invoice_item = InvoiceItem.new params
      invoice = invoice_item.invoice

      if invoice.locked?
        return StoreResult.new(entity: invoice_item, success: false, errors: ["Cannot save. The record is read-only."])
      end

      container_result = create_container container_params, invoice

      if container_result.success?
        invoice_item.material_container = container_result.entity
        invoice_item_result = call_on invoice_item, :save
      else
        invoice_item_result = StoreResult.new(entity: invoice_item, success: false, errors: ['Failed to create dependent material container'])
      end

      CombinedResult.new invoice_item_result, container_result
    end

    private

    def create_container(params, invoice)
      params[:lot_number] = LotNumber.generate(invoice) if params[:lot_number].blank?
      create_material_container.call params
    end
  end
end
