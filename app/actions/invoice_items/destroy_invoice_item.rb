# frozen_string_literal: true
module InvoiceItems
  class DestroyInvoiceItem < BaseAction
    include AffectsEntity

    def call(invoice_item)
      call_on_lockable invoice_item, :destroy
    end
  end
end
