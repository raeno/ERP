# frozen_string_literal: true
module InvoiceItems
  class GenerateInvoiceItem < BaseAction
    include AffectsEntity
    attr_reader :generate_material_container

    def self.build
      generate_material_container = MaterialContainers::GenerateMaterialContainer.build
      new(generate_material_container)
    end

    def initialize(generate_material_container)
      @generate_material_container = generate_material_container
    end

    def call(invoice, material)
      params = { invoice_id: invoice.id, quantity: 1 }
      invoice_item = InvoiceItem.new params

      container_result = generate_material_container.call material, LotNumber.generate(invoice)
      return container_result unless container_result.success?

      invoice_item.material_container = container_result.entity
      invoice_item_result = call_on invoice_item, :save

      CombinedResult.new invoice_item_result, container_result
    end
  end
end
