# frozen_string_literal: true
module InvoiceItems
  class InvoiceItemReceived < BaseAction
    include AffectsEntity
    include WithRollback

    attr_reader :update_material_price_action, :increment_inventory_action,
      :refresh_invoice_status

    def self.build
      new Materials::UpdateUnitPrice.build,
        InventoryItems::IncrementQuantity.build,
        Invoices::RefreshReceivingStatus.build
    end

    def initialize(update_material_price_action, increment_inventory_action, refresh_invoice_status)
      @update_material_price_action = update_material_price_action
      @increment_inventory_action = increment_inventory_action
      @refresh_invoice_status = refresh_invoice_status
    end

    def call(invoice_item)
      if invoice_item.received?
        return StoreResult.new(entity: invoice_item, success: false, errors: ["Invoice item is already received."])
      end

      unless invoice_item.invoice_total_price_matching?
        return StoreResult.new(entity: invoice_item, success: false, errors: ["Invoice total price mismatch."])
      end

      invoice_item.become_received

      actions = build_actions invoice_item
      in_transaction_with_rollback actions
    end

    private

    def build_actions(invoice_item)
      material = invoice_item.material_container.material
      inventory_increase = invoice_item.total_material_amount
      price_increase = invoice_item.total_price
      [
        -> { call_on(invoice_item, :save) },
        -> { update_material_price_action.call material, inventory_increase, price_increase },
        -> { update_inventory invoice_item, inventory_increase },
        -> { refresh_invoice_status.call invoice_item.reload.invoice }
      ]
    end

    def update_inventory(invoice_item, inventory_increase)
      container = invoice_item&.material_container
      inventory_item = container&.inventory_item
      increment_inventory_action.call inventory_item, inventory_increase
    end
  end
end
