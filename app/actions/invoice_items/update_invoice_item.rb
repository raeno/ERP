# frozen_string_literal: true
module InvoiceItems
  class UpdateInvoiceItem < BaseAction
    include AffectsEntity
    attr_reader :update_material_container

    def self.build
      update_material_container = MaterialContainers::UpdateMaterialContainer.build
      new(update_material_container)
    end

    def initialize(update_material_container)
      @update_material_container = update_material_container
    end

    def call(invoice_item, params)
      material_container_params = params.delete(:material_container)
      update_material_result = update_material_container.call(invoice_item.material_container, material_container_params) if material_container_params.present?
      update_material_result ||= TransactionResult.new(success: true)

      if update_material_result.success?
        invoice_item.attributes = params
        result = call_on(invoice_item, :save)
      end
      result ||= StoreResult.new(entity: invoice_item, errors: [], success: false)
      CombinedResult.new result, update_material_result
    end
  end
end
