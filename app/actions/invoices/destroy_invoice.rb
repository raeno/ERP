# frozen_string_literal: true
module Invoices
  class DestroyInvoice < BaseAction
    include AffectsEntity

    def call(invoice)
      call_on_lockable invoice, :destroy
    end
  end
end
