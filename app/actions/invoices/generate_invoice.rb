# frozen_string_literal: true
module Invoices
  class GenerateInvoice < BaseAction
    include AffectsEntity
    include WithRollback

    def self.build
      generate_invoice_item_action = InvoiceItems::GenerateInvoiceItem.build
      new generate_invoice_item_action
    end

    def initialize(generate_invoice_item_action)
      @generate_invoice_item_action = generate_invoice_item_action
    end

    def call(params)
      material_ids = params.delete(:material_ids)

      unless material_ids&.any?
        return StoreResult.new(entity: nil, success: false, errors: ["Please select some materials."])
      end

      invoice = Invoice.new params
      actions = build_actions invoice, material_ids
      in_transaction_with_rollback actions
    end

    private

    attr_reader :generate_invoice_item_action

    def build_actions(invoice, material_ids)
      actions = [-> { call_on invoice, :save }]
      material_ids.each do |material_id|
        material = Material.find(material_id)
        actions << -> { generate_invoice_item_action.call(invoice, material) }
      end
      actions << -> { init_invoice_total(invoice) }
      actions
    end

    def init_invoice_total(invoice)
      invoice.total_price = invoice.total_item_price
      call_on invoice, :save
    end
  end
end
