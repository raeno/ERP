# frozen_string_literal: true
module Invoices
  class InvoicePaid < BaseAction
    include AffectsEntity

    def call(invoice)
      if invoice.paid?
        return StoreResult.new(entity: invoice, success: false, errors: ["Invoice is already paid."])
      end

      unless invoice.total_price_matching?
        return StoreResult.new(entity: invoice, success: false, errors: ["Invoice total price mismatch."])
      end

      invoice.become_paid

      call_on invoice, :save
    end
  end
end
