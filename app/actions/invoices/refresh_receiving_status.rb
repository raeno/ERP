# frozen_string_literal: true
module Invoices
  class RefreshReceivingStatus < BaseAction
    include AffectsEntity
    include WithRollback

    attr_reader :refresh_vendor_lead_time

    def self.build
      new Vendors::RefreshLeadTime.build
    end

    def initialize(refresh_vendor_lead_time)
      @refresh_vendor_lead_time = refresh_vendor_lead_time
    end

    def call(invoice)
      unless invoice.all_items_received?
        # leave the status as is
        return StoreResult.new(entity: invoice, success: true)
      end

      invoice.become_received

      actions = build_actions invoice
      in_transaction_with_rollback actions
    end

    private

    def build_actions(invoice)
      [
        -> { call_on(invoice, :save) },
        -> { refresh_vendor_lead_time.call(invoice.vendor) }
      ]
    end
  end
end
