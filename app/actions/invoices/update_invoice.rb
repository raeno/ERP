# frozen_string_literal: true
module Invoices
  class UpdateInvoice < BaseAction
    include AffectsEntity

    def call(invoice, params)
      invoice.attributes = params
      call_on invoice, :save
    end
  end
end
