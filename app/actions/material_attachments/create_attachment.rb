# frozen_string_literal: true
module MaterialAttachments
  class CreateAttachment
    def call(attachment_params)
      material_id = attachment_params.delete :material_id
      material = Material.find_by id: material_id
      return StoreResult.new success: false, entity: nil, errors: ['Material should not be nil'] unless material

      result = create_material_attachment material, attachment_params
      if result
        StoreResult.new success: true, entity: result, errors: []
      else
        StoreResult.new success: false, entity: nil, errors: ['Failed to create material attachment']
      end
    end

    private

    def create_material_attachment(material, attachment_params)
      ActiveRecord::Base.transaction do
        attachment = Attachment.create attachment_params
        raise ActiveRecord::Rollback unless attachment
        MaterialAttachment.create material: material, attachment: attachment
      end
    end
  end
end
