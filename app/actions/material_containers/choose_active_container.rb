# frozen_string_literal: true
module MaterialContainers
  class ChooseActiveContainer < BaseAction
    include AffectsEntity

    def call(material)
      next_container = query.new(material).next_to_open
      return TransactionResult.new(success: false, errors: ['No containers available to choose as active']) unless next_container
      next_container.status = 'active'
      call_on next_container, :save
    end

    private

    def query
      MaterialContainers::SameMaterialQuery
    end
  end
end
