# frozen_string_literal: true
module MaterialContainers
  class CreateMaterialContainer < BaseAction
    include AffectsEntity

    def call(params)
      container = MaterialContainer.new params

      unless container.ordering_size
        return StoreResult.new(entity: container, success: false, errors: ['Please select ordering size'])
      end
      container.copy_amount_and_unit

      container.amount_left = container.amount if container.empty?
      container.status = choose_container_status(params)

      call_on(container, :save)
    end

    private

    def choose_container_status(params)
      material = params[:material] || Material.find_by(id: params[:material_id])
      status = material.material_containers.active.exists? ? 'pending' : 'active'
      MaterialContainer.statuses[status]
    end
  end
end
