# frozen_string_literal: true
module MaterialContainers
  class GenerateMaterialContainer < BaseAction
    include AffectsEntity

    def call(material, lot_number)
      container = MaterialContainer.new(material: material)

      ordering_size = MaterialPurchaseSizeSelector.new(material).best_size(material.production_can_cover)
      unless ordering_size
        error_message = "Cannot find an ordering size for #{material.name}"
        return StoreResult.new(entity: container, success: false, errors: [error_message])
      end

      container.ordering_size = ordering_size
      container.copy_amount_and_unit

      container.price = ordering_size.estimated_price
      container.amount_left = container.amount
      container.lot_number = lot_number

      call_on container, :save
    end
  end
end
