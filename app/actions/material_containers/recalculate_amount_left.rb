# frozen_string_literal: true
module MaterialContainers
  class RecalculateAmountLeft < BaseAction
    attr_reader :track_material_usage, :create_transaction

    def self.build
      track_material_usage = MaterialContainers::TrackMaterialUsage.build
      create_transaction = MaterialTransactions::CreateTransaction.build
      new(track_material_usage, create_transaction)
    end

    def initialize(track_material_usage, create_transaction)
      @track_material_usage = track_material_usage
      @create_transaction = create_transaction
    end

    def call(material, amount, batch, inventory_amount)
      container = active_container(material)
      # containers are optional at the moment, all inventory tracking
      # is done via inventory and there can be problems with sync of
      # their values
      # later we need to add single point for all stock inventory
      # management and fix this problem
      return unless container

      deduct_material(container, amount, batch, inventory_amount)
    end

    def deduct_material(container, amount, batch, inventory_amount)
      if container.enough_material_left?(amount)
        track_usage container, batch, amount, inventory_amount
      else
        amount_in_container = container.amount_left
        to_cover_with_next_container = amount - amount_in_container
        track_usage container, batch, amount_in_container, inventory_amount + to_cover_with_next_container

        next_container = active_container(container.material)
        return unless next_container

        deduct_material next_container, to_cover_with_next_container, batch, inventory_amount
      end
    end

    def active_container(material)
      containers_query = MaterialContainers::SameMaterialQuery.new(material)
      containers_query.active
    end

    def track_usage(container, batch, amount, inventory_amount)
      track_material_usage.call container, amount
      params = { batch: batch, inventory_amount: inventory_amount,
                 quantity: amount,
                 material_container: container }
      create_transaction.call params
    end
  end
end
