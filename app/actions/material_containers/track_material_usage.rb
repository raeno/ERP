# frozen_string_literal: true
module MaterialContainers
  class TrackMaterialUsage < BaseAction
    include AffectsEntity

    attr_reader :choose_active_container

    def self.build
      choose_active_container = MaterialContainers::ChooseActiveContainer.build
      new( choose_active_container)
    end

    def initialize(choose_active_container)
      @choose_active_container = choose_active_container
    end

    def call(container, to_deduct)
      return StoreResult.nil_entity_error unless container
      return StoreResult.new(entity: container, success: false, errors: ['Using more material than container has']) unless container.enough_material_left?(to_deduct)

      container.amount_left -= to_deduct
      container.closed! if container.empty?
      call_on container, :save

      if container.empty?
        choose_active_container.call container.material
      else
        StoreResult.new(success: true, entity: container)
      end
    end
  end
end
