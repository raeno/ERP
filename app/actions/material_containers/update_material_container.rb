# frozen_string_literal: true
module MaterialContainers
  class UpdateMaterialContainer < BaseAction
    include AffectsEntity

    def call(material_container, params)
      return StoreResult.nil_entity_error unless material_container

      material_container.attributes = params

      unless material_container.ordering_size
        return StoreResult.new(entity: material_container, success: false, errors: ['Please select ordering size'])
      end
      material_container.copy_amount_and_unit

      call_on material_container, :save
    end
  end
end
