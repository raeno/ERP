# frozen_string_literal: true
module MaterialTransactions
  class CreateTransaction < BaseAction
    include AffectsEntity

    def call(params)
      transaction = MaterialTransaction.new params
      call_on transaction, :save
    end
  end
end
