# frozen_string_literal: true
module Materials
  class CreateMaterial < BaseAction
    include AffectsEntity
    include WithRollback

    def self.build
      inventory_item_action = InventoryItems::CreateInventoryItem.build
      new(inventory_item_action)
    end

    def initialize(inventory_item_action)
      @inventory_item_action = inventory_item_action
    end

    def call(params)
      material = Material.new params

      actions = [
        -> { call_on(material, :save) },
        -> { create_inventory_item(material) }
      ]
      in_transaction_with_rollback actions
    end

    private

    attr_reader :inventory_item_action

    def create_inventory_item(material)
      zone_id = material.inventory_item_production_zone_id || Zone.default_zone.id
      params = { inventoriable: material, production_zone_id: zone_id }
      inventory_item_action.call params
    end
  end
end
