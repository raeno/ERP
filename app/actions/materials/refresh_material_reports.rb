# frozen_string_literal: true
module Materials
  class RefreshMaterialReports < BaseAction
    attr_reader :refresh_production_report

    def self.build
      refresh_production_report = ProductionReports::RefreshPlanData.build
      new(refresh_production_report)
    end

    def initialize(refresh_production_report)
      @refresh_production_report = refresh_production_report
    end

    def call(material)
      return unless material
      refresh_production_report.call material.inventory_item
    end
  end
end
