# frozen_string_literal: true
module Materials
  class UpdateMaterial < BaseAction
    include AffectsEntity
    include WithRollback

    attr_reader :material

    def self.build
      inventory_item_action = InventoryItems::UpdateInventoryItem.new
      new(inventory_item_action)
    end

    def initialize(inventory_item_action)
      @inventory_item_action = inventory_item_action
    end

    def call(material, params)
      return StoreResult.new(entity: nil, success: false, errors: ["Material should not be empty"]) unless material

      material.attributes = params

      actions = [
        -> { call_on(material, :save) },
        -> { update_inventory_item_zone(material) }
      ]
      in_transaction_with_rollback actions
    end

    private

    attr_reader :inventory_item_action

    def update_inventory_item_zone(material)
      inventory_item = material.inventory_item
      new_zone_id = material.inventory_item_production_zone_id

      return StoreResult.new(entity: inventory_item, success: true) unless new_zone_id

      params = { production_zone_id: new_zone_id }
      inventory_item_action.call inventory_item, params
    end
  end
end
