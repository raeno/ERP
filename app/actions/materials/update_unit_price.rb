# frozen_string_literal: true
module Materials
  class UpdateUnitPrice < BaseAction
    include AffectsEntity
    attr_reader :material

    # Pass negative amount_increment and total_price_increment
    # if material is being deducted, not added.
    def call(material, amount_increment, total_price_increment)
      new_amount = material.inventory_quantity + amount_increment

      current_total_price = material.inventory_total_prime_cost
      new_total_price = current_total_price + total_price_increment

      material.unit_price = new_total_price / new_amount unless new_amount.zero?
      call_on material, :save
    end
  end
end
