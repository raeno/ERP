# frozen_string_literal: true
module ProductAttachments
  class CreateAttachment
    attr_reader :create_attachment_action

    def call(attachment_params)
      product_id = attachment_params.delete :product_id
      product = Product.find_by id: product_id
      return StoreResult.new(success: false, entity: nil, errors: ['Product should not by nil']) unless product

      result = create_product_attachment product, attachment_params

      if result
        StoreResult.new(success: true, entity: result, errors: [])
      else
        StoreResult.new(success: false, entity: nil, errors: ['Failed to create product attachment'])
      end
    end

    private

    def create_product_attachment(product, attachment_params)
      ActiveRecord::Base.transaction do
        attachment = Attachment.create attachment_params
        raise ActiveRecord::Rollback unless attachment
        ProductAttachment.create product: product, attachment: attachment
      end
    end
  end
end
