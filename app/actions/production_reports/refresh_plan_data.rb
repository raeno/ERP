# frozen_string_literal: true

module ProductionReports
  class RefreshPlanData < BaseAction
    include AffectsEntity

    def call(inventory_item)
      production_plan = ProductionPlan::Plan.build inventory_item

      report = fetch_report inventory_item
      update report, production_plan
      call_on report, :save
    end

    private

    def fetch_report(inventory_item)
      report = inventory_item.production_report
      report ||= InventoryItems::ProductionReport.new inventory_item: inventory_item
      report
    end

    def update(report, plan)
      value_names =
        %w[demand demand_coverage supply to_cover can_cover can_cover_in_cases can_cover_optimal]
      value_names.each do |name|
        report.send "#{name}=", plan.send(name)
      end
      report.in_stock = plan.current_inventory

      inventoriable = plan.deficit_inventoriable
      report.previous_zone_deficit = plan.deficit_in_previous_zone?
      report.deficit_inventoriable = inventoriable
      report.deficit_ingredient_name = inventoriable&.name
    end
  end
end
