# frozen_string_literal: true

module ProductionReports
  class RefreshTaskData < BaseAction
    include AffectsEntity

    def call(inventory_item)
      report = inventory_item.production_report

      report.assigned = inventory_item.current_tasks.sum(&:quantity)

      call_on report, :save
    end
  end
end
