# frozen_string_literal: true

module Products
  class AddProductsToShipment < ShipmentAction
    attr_reader :patch_shipment, :sync_batches, :update_shipment

    def self.build
      update_shipment = AmazonShipments::UpdateShipment.build
      patch_shipment = AmazonProcessing::PatchShipment.build
      sync_batches = Batches::SyncShipmentBatches.build
      notification_service = ShipmentNotificationService.new
      new(update_shipment, patch_shipment, sync_batches, notification_service)
    end

    def initialize(update_shipment, patch_shipment, sync_batches, notification_service)
      @patch_shipment = patch_shipment
      @sync_batches = sync_batches
      @update_shipment = update_shipment
      @notification_service = notification_service
    end

    def call(amazon_shipment)
      workflow = create_shipment_update_workflow
      result = run_workflow workflow, amazon_shipment
      broadcast amazon_shipment, status: 'finished', message: 'Shipment update finished'
      result
    end

    private

    def create_shipment_update_workflow
      mark_shipment_as_changed = -> (shipment) { update_shipment.call(shipment, status: 'CHANGED') }
      init_shipment = WorkflowItem.new mark_shipment_as_changed, 'Started processing Amazon shipment', 'Failed to initiate processing of Amazon shipment'

      amazon_update = WorkflowItem.new patch_shipment, 'Updated shipment on Amazon', 'Failed to update shipment on Amazon'

      batch_synchronization = WorkflowItem.new sync_batches, 'Created batches for added to shipment products', 'Updated Amazon but failed to create batches for added to shipment products'
      [init_shipment, amazon_update, batch_synchronization]
    end
  end
end
