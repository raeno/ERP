# frozen_string_literal: true
module Products
  class AssessDependencies < BaseAction
    attr_reader :create_inventory_item, :refresh_production_report,
                :destroy_production_report, :destroy_inventory_item

    def self.build
      create_inventory_item = InventoryItems::CreateInventoryItem.build
      refresh_production_report = Products::RefreshProductReports.build
      destroy_production_report = SimpleActions::Destroy.new(InventoryItems::ProductionReport)
      destroy_inventory_item = SimpleActions::Destroy.new(InventoryItem)
      new(create_inventory_item, refresh_production_report, destroy_production_report, destroy_inventory_item)
    end

    def initialize(create_inventory_item, refresh_production_report, destroy_production_report, destroy_inventory_item)
      @create_inventory_item = create_inventory_item
      @refresh_production_report = refresh_production_report
      @destroy_production_report = destroy_production_report
      @destroy_inventory_item = destroy_inventory_item
    end

    def call(product)
      product_zones = product.zones

      results = Zone.all.flat_map do |zone|
        inventory_item = product.inventory_items.find_by zone: zone
        if product_zones.include?(zone)
          ensure_dependencies_exist(product, inventory_item, zone)
        else
          ensure_dependencies_missing(inventory_item)
        end
      end

      CombinedResult.new(*results)
    end

    private

    def ensure_dependencies_exist(product, inventory_item, zone)
      result = create_inventory_item.call(zone: zone, inventoriable: product) unless inventory_item
      report_result = refresh_production_report.call(product) if result&.success?
      [result, report_result]
    end

    def ensure_dependencies_missing(inventory_item)
      report = inventory_item&.production_report
      report_result = destroy_production_report.call(report) if report
      inventory_item_result = destroy_inventory_item.call(inventory_item) if inventory_item
      [report_result, inventory_item_result]
    end
  end
end
