# frozen_string_literal: true
module Products
  class CreateProduct
    include AffectsEntity

    def self.build
      products_exposer = ExposeProducts.new
      inventory_item_action = InventoryItems::CreateInventoryItem.build
      refresh_reports_action = Products::RefreshProductReports.build
      new(products_exposer, inventory_item_action, refresh_reports_action)
    end

    def initialize(products_exposer, inventory_item_action, refresh_reports_action)
      @products_exposer = products_exposer
      @inventory_item_action = inventory_item_action
      @refresh_reports_action = refresh_reports_action
    end

    def call(params)
      product = Product.new params
      product_result = save_product product

      return product_result unless product_result.success?
      results = [product_result]

      results << create_inventory_item(product_result)
      run_post_creation_actions(product)
      results << refresh_reports_action.call(product)

      CombinedResult.new(*results)
    end

    private

    attr_reader :products_exposer, :inventory_item_action, :refresh_reports_action

    def save_product(product)
      call_on product, :save
    end

    def run_post_creation_actions(product)
      products_exposer.expose_to_all_zones(product)
      create_amazon_inventory(product)
    end

    def create_inventory_item(result)
      product = result.entity
      results = Zone.all.map do |zone|
        params = { inventoriable: product, zone: zone }
        inventory_item_action.call params
      end
      CombinedResult.new(*results)
    end

    def create_amazon_inventory(product)
      AmazonInventory.find_or_create_by product: product
    end
  end
end
