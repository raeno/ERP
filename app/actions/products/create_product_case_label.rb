# frozen_string_literal: true
module Products
  class CreateProductCaseLabel < BaseAction
    def call(product)
      filename = File.join Rails.root, "tmp/#{SecureRandom.urlsafe_base64(16)}.pdf"
      case_label = CaseLabelPdf.new(product)
      case_label.save_as filename
      filename
    end
  end
end
