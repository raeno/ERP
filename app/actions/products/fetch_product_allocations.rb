# frozen_string_literal: true
# Tries to fetch allocations for given amount for the product from database
# If no allocations for that amount in database - calls an action to
# fetch get them from Amazon API and store to DB
module Products
  class FetchProductAllocations
    attr_reader :query_allocations_action

    def self.build
      ship_from_address = Setting.instance.shipment_address.to_h
      query_allocations_action = nil # Products::QueryProductAllocations.build(ship_from_address)
      new(query_allocations_action)
    end

    def initialize(query_allocations_action)
      @query_allocations_action = query_allocations_action
    end

    def call(shipment_item)
      return EntityResult.nil_entity_error unless shipment_item

      allocations = FbaAllocation.shipment_item_allocations shipment_item

      if allocations.empty?
        query_allocations_action.call shipment_item
      else
        EntityResult.new success: true, entity: allocations
      end
    end
  end
end
