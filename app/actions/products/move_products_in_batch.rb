# frozen_string_literal: true
module Products
  class MoveProductsInBatch
    include Logging

    delegate :rollback, to: :update_inventories

    def self.build
      update_inventories = Inventories::UpdateInventories.build
      zones_workflow = ZoneWorkflow
      new zones_workflow, update_inventories
    end

    def initialize(zones_workflow, update_inventories)
      @zones_workflow = zones_workflow
      @update_inventories = update_inventories
    end

    def call(batch, old_batch = nil)
      previous_amount = old_batch&.quantity || 0
      amount = batch.quantity - previous_amount
      return TransactionResult.new(success: true, errors: nil) if amount.zero?
      source_inventory, destination_inventory = define_inventories batch, amount

      # when old_batch is not nil it means that we update or delete batch
      Wisper.subscribe(MaterialAmountChangeListener.new(batch: old_batch || batch)) do
        return update_inventories.call batch.product, source_inventory, destination_inventory, amount.abs
      end
    end

    private

    attr_reader :update_inventories

    def define_inventories(batch, amount)
      product = batch.product
      source_zone, destination_zone = define_zones product, batch.zone, amount

      source_zone_inventory = find_inventory product, source_zone
      destination_zone_inventory = find_inventory product, destination_zone
      [source_zone_inventory, destination_zone_inventory]
    end

    def find_inventory(product, zone)
      inventory_item = InventoryItem.find_by inventoriable: product, zone: zone
      inventory_item&.inventory
    end

    def define_zones(product, current_zone, amount)
      workflow = @zones_workflow.new(product&.zones)
      amount > 0 ? [workflow.previous_zone(current_zone), current_zone]
                 : [current_zone, workflow.previous_zone(current_zone)]
    end
  end
end
