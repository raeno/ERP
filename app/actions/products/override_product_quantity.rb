# frozen_string_literal: true
module Products
  class OverrideProductQuantity
    include Logging
    include AffectsEntity

    def call(product, params)
      return EntityResult.new(entity: product, success: false, errors: 'Product is empty') unless product

      product.attributes = prepare_attributes(params)
      EntityResult.new(entity: product, success: true)
    end

    private

    def prepare_attributes(params)
      only_override_params = params.slice(:to_ship_cases_qty_override, :to_ship_items_qty_override)
      only_override_params.map { |key, value| [key, value.to_i] }.to_h
    end
  end
end
