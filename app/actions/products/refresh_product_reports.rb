# frozen_string_literal: true
module Products
  class RefreshProductReports < BaseAction
    attr_reader :refresh_production_report, :refresh_shipment_report

    def self.build
      refresh_inventory_item = ProductionReports::RefreshPlanData.build
      refresh_shipment_report = RefreshShipmentReport.build
      new(refresh_inventory_item, refresh_shipment_report)
    end

    def initialize(refresh_production_report, refresh_shipment_report)
      @refresh_production_report = refresh_production_report
      @refresh_shipment_report = refresh_shipment_report
    end

    def call(product)
      puts "Refreshing reports for product: #{product.id}"
      return unless product
      inventory_items = product.inventory_items
      results = inventory_items.map do |inventory_item|
        refresh_production_report.call inventory_item
      end

      results << refresh_shipment_report.call(product) if product.has_ship_zone?
      CombinedResult.new(*results)
    end
  end
end
