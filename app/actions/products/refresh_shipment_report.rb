# frozen_string_literal: true
module Products
  class RefreshShipmentReport < BaseAction
    include AffectsEntity
    attr_reader :report_builder

    def self.build
      report_builder = ShipmentReportBuilder
      new(report_builder)
    end

    def initialize(report_builder)
      @report_builder = report_builder
    end

    def call(product)
      builder = report_builder.new product.shipment_report
      builder.with_product_information product
      builder.with_items_allocation default_product_allocations(product, 'items')
      builder.with_cases_allocation default_product_allocations(product, 'cases')
      report = builder.report
      call_on report, :save
    end

    private

    def default_product_allocations(product, allocation_type)
      allocations = FbaAllocation.default_product_allocations product, allocation_type
      AllocationsSerializer.new(allocations).to_json
    end
  end
end
