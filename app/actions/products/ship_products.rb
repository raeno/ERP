module Products
  class ShipProducts < ShipmentAction
    attr_reader :initialize_shipment, :submit_shipment, :sync_batches

    def self.build
      initialize_shipment = AmazonProcessing::InitializeShipment.build
      submit_shipment = AmazonProcessing::PostShipment.build
      sync_batches = Batches::SyncShipmentBatches.build
      notification_service = ShipmentNotificationService.new
      new(initialize_shipment, submit_shipment, sync_batches, notification_service)
    end

    def initialize(initialize_shipment, submit_shipment, sync_batches, notification_service)
      @initialize_shipment = initialize_shipment
      @submit_shipment = submit_shipment
      @sync_batches = sync_batches
      @notification_service = notification_service
    end

    # composes multiple actions on shipment into workflow and executes items of
    # workflow one by one
    # broadcasts status of execution during execution
    def call(shipment)
      workflow = create_shipment_workflow
      result = run_workflow workflow, shipment
      broadcast shipment, status: 'finished', message: 'Shipment process finished'
      result
    end

    private

    def create_shipment_workflow
      initialization = WorkflowItem.new initialize_shipment, 'Initialized shipment', 'Failed to initialize shipment'
      submission = WorkflowItem.new submit_shipment, 'Created shipment on Amazon', 'Failed to create shipment on amazon'
      batch_synchronization = WorkflowItem.new sync_batches, 'Created batches for shipment entries', 'Failed to create batches for shipment entries'
      [initialization, submission, batch_synchronization]
    end
  end
end
