# frozen_string_literal: true
module Products
  class TrackPrimeCost < BaseAction
    include AffectsEntity

    def call(batch)
      product = batch.product
      cogs = product.cumulative_cogs(batch.zone)
      snapshot = ProductCogsSnapshot.new product: product, batch: batch, cogs: cogs, zone: batch.zone

      call_on snapshot, :save
    end
  end
end
