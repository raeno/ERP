# frozen_string_literal: true
module Products
  class UpdateProduct < BaseAction
    include AffectsEntity
    attr_reader :assess_product_dependencies

    def self.build
      assess_product_dependencies = Products::AssessDependencies.build
      new(assess_product_dependencies)
    end

    def initialize(assess_product_dependencies)
      @assess_product_dependencies = assess_product_dependencies
    end

    def call(product, params)
      return StoreResult.new(entity: product, success: false, errors: 'Product is empty') unless product

      params = validate_params(params)

      product.attributes = params
      actions = [
        -> { call_on product, :save },
        -> { assess_product_dependencies.call product }
      ]
      in_transaction_with_rollback actions
    end

    def validate_params(params)
      if params[:zone_ids]
        all_zones_checked_out = params[:zone_ids].reject(&:blank?).blank?
        params[:zone_ids] = [Zone.default_zone.id] if all_zones_checked_out
      end
      params
    end
  end
end
