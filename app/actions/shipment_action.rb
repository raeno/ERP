class ShipmentAction
  class WorkflowItem
    attr_reader :action, :success_message, :error_message

    def initialize(action, success_message = '', error_message  = '')
      @action = action
      @success_message = success_message
      @error_message = error_message
    end

    def call(*args)
      action&.call *args
    end
  end

  attr_reader :notification_service

  def initialize(notification_service)
    @notification_service = notification_service
  end

  protected

  def run_workflow(workflow, shipment)
    results = workflow.map do |workflow_item|
      result = workflow_item.call shipment

      if result.success?
        shipment = result.entity
        broadcast shipment, status: 'working', message: workflow_item.success_message
      else
        broadcast shipment, status: 'failed', message: workflow_item.error_message
        return TransactionResult.new success: false, errors: result.errors
      end
      result
    end
    CombinedResult.new(*results)
  end

  def broadcast(shipment, params)
    notification_service.call shipment.user, shipment.name, params
  end
end
