module ShipmentEntries
  class CreateEntries < BaseAction
    include AffectsEntity

    def call(amazon_shipment, params)
      actions = params.map do |entry_params|
        init_entry entry_params.merge(amazon_shipment: amazon_shipment)
      end

      results = in_transaction_with_rollback actions
      CombinedResult.new *results
    end

    private

    def init_entry(params)
      entry = ShipmentEntry.new params
      -> { call_on entry, :save }
    end
  end
end
