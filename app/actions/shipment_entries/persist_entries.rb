module ShipmentEntries
  class PersistEntries < BaseAction
    attr_reader :create_entry, :update_entry

    def self.build
      create_entry = SimpleActions::Create.new(ShipmentEntry)
      update_entry = ShipmentEntries::UpdateEntry.build
      new(create_entry, update_entry)
    end

    def initialize(create_entry, update_entry)
      @create_entry = create_entry
      @update_entry = update_entry
    end

    # takes array of shipment entries params and amazon shipment
    # creates shipment entry for each product that is not yet in the shipment
    # updates shipment entries for already shipped in this AmazonShipment products, sum quantities
    #
    # @returns CombinedResult of all create/update actions
    def call(shipment, entries)
      return StoreResult.nil_entity_error unless shipment

      results = entries.map do |entry_params|
        persist_entry shipment, entry_params
      end
      CombinedResult.new(*results)
    end

    def persist_entry(shipment, entry_params)
      product_id = entry_params[:product_id]
      entry = ShipmentEntry.find_by amazon_shipment: shipment, product_id: product_id
      if entry
        new_quantity = entry_params[:quantity].to_i + entry.quantity
        update_entry.call entry, { quantity: new_quantity, status: 'CHANGED' }
      else
        create_entry.call entry_params.merge(amazon_shipment: shipment)
      end
    end
  end
end

