module ShipmentEntries
  class UpdateEntry < BaseAction
    include AffectsEntity
    attr_reader :update_shipment

    def self.build
      update_shipment = AmazonShipments::UpdateShipment.build
      new(update_shipment)
    end

    def initialize(update_shipment)
      @update_shipment = update_shipment
    end

    def call(entry, params)
      return StoreResult.nil_entity_error unless entry

      entry.attributes = params
      actions = [ -> { call_on entry, :save } ]

      update_shipment_status = -> { update_shipment.call(entry.amazon_shipment, status: 'CHANGED') }
      actions << update_shipment_status if entry.was_changed?

      in_transaction_with_rollback actions
    end
  end
end

