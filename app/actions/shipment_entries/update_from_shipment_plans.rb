# frozen_string_literal: true

module ShipmentEntries
  class UpdateFromShipmentPlans < BaseAction
    attr_reader :update_entry

    def self.build
      update_entry = ShipmentEntries::UpdateEntry.build
      new(update_entry)
    end

    def initialize(update_entry)
      @update_entry = update_entry
    end

    def call(entries, shipment_plans)
      results = entries.map do |entry|
        matching_plan = find_shipment_plan entry, shipment_plans
        return TransactionResult.new(success: false, errors: "Amazon does not accept #{entry.product.name} to #{entry.fba_warehouse_name} anymore") unless matching_plan

        quantity = extract_quantity matching_plan, entry
        update_entry.call entry, quantity: quantity
      end
      CombinedResult.new *results
    end

    private

    def find_shipment_plan(entry, shipment_plans)
      warehouse = entry.fba_warehouse_name
      same_warehouse_plan = shipment_plans.select do |plan|
        plan.destination_fulfillment_center_id == warehouse
      end.first

      return nil unless same_warehouse_plan

      entry_plans = same_warehouse_plan.items.select do |item|
        item.seller_sku == entry.seller_sku
      end
      entry_plans.first
    end

    def extract_quantity(shipment_plan_item, entry)
      quantity = shipment_plan_item.quantity.to_i
      if entry.quantity_in_case
        quantity_in_case = entry.quantity_in_case.zero? ? 1 : entry.quantity_in_case
        quantity /= quantity_in_case
      end
      quantity
    end
  end
end
