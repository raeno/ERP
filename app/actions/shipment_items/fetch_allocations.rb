module ShipmentItems
  class FetchAllocations < BaseAction
    attr_reader :retrieve_allocations

    def self.build
      ship_from_address = Setting.instance.shipment_address.to_h
      retrieve_allocations = RetrieveAllocations.build(ship_from_address)
      new(retrieve_allocations)
    end

    def initialize(retrieve_allocations)
      @retrieve_allocations = retrieve_allocations
    end

    def call(shipment_item)
      return EntityResult.nil_entity_error unless shipment_item

      allocations = FbaAllocation.shipment_item_allocations shipment_item

      if allocations.empty?
        retrieve_allocations.call shipment_item
      else
        EntityResult.new success: true, entity: allocations
      end
    end
  end
end

