module ShipmentItems
  class RetrieveAllocations < BaseAction

    attr_reader :shipment_processor, :create_allocation_action, :delete_allocations_action

    def self.build(ship_from_address)
      shipment_processor = AmazonInboundShipmentProcessor.build(ship_from_address)
      create_allocation_action = Fba::CreateFbaAllocation.new
      delete_allocations_action = Fba::DeleteAllocations.new
      new(shipment_processor, create_allocation_action, delete_allocations_action)
    end

    def initialize(shipment_processor, create_allocation_action, delete_allocations_action)
      @shipment_processor = shipment_processor
      @create_allocation_action = create_allocation_action
      @delete_allocations_action = delete_allocations_action 
    end

    def call(shipment_item)
      return TransactionResult.new success: false, errors: ['Product should not be empty'] unless shipment_item

      shipment_plan_result = shipment_processor.create_shipment_plan [shipment_item]
      return shipment_plan_result unless shipment_plan_result.success?

      allocations = prepare_allocations(shipment_plan_result.entities, shipment_item)
      EntityResult.new success: true, entity: allocations
    end

    private

    def prepare_allocations(shipment_plans, shipment_item)
      delete_allocations_action.call shipment_item
      allocation_results = shipment_plans.flat_map do |shipment_plan|
        prepare_allocation(shipment_plan, shipment_item)
      end
      allocation_results.select(&:success?).map(&:entity)
    end

    def prepare_allocation(shipment_plan, shipment_item)
      warehouse = FbaWarehouse.retrieve_by_name shipment_plan.destination_fulfillment_center_id

      shipment_plan.items.map do |item|
        create_allocation item.quantity.to_i, warehouse, shipment_item
      end
    end

    def create_allocation(quantity, warehouse, shipment_item)
      product = shipment_item.product

      # shipment plan item always has quantity in items. 
      # That's why for cases we need to divide 
      quantity /= product.items_per_case if shipment_item.shipment_in_cases?
      params = { fba_warehouse: warehouse, product: product,
                 quantity: quantity, 
                 quantity_to_allocate: shipment_item.quantity,
                 shipment_type: shipment_item.shipment_type }
      create_allocation_action.call(params)
    end
  end
end

