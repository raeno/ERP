# frozen_string_literal: true
module SimpleActions
  class Base < BaseAction
    include AffectsEntity
    attr_reader :model

    def self.build(model)
      new(model)
    end

    # :reek:FeatureEnvy is ok here
    def initialize(model)
      @model = model.is_a?(String) ? model.constantize : model
    end
  end
end
