# frozen_string_literal: true
module SimpleActions
  class Create < SimpleActions::Base
    def call(params)
      instance = @model.new params
      call_on instance, :save
    end
  end
end
