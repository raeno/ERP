# frozen_string_literal: true
module SimpleActions
  class Destroy < SimpleActions::Base
    def call(instance)
      call_on instance, :destroy
    end
  end
end
