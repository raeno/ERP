# frozen_string_literal: true
module SimpleActions
  class Update < SimpleActions::Base
    def call(instance, params)
      return StoreResult.new(entity: nil, success: false, errors: ["#{model_name} should not be empty"]) unless instance
      instance.attributes = params
      call_on instance, :save
    end

    private

    def model_name
      @model.name
    end
  end
end
