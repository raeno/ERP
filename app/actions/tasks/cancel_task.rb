# frozen_string_literal: true

module Tasks
  class CancelTask < BaseAction
    include AffectsEntity

    def call(task)
      return StoreResult.new(entity: nil, success: false, errors: ['Task not found']) unless task
      task.started_at = nil
      call_on task, :save
    end
  end
end
