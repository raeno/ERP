# frozen_string_literal: true

module Tasks
  class CreateTask < BaseAction
    include AffectsEntity

    def self.build
      refresh_report = ProductionReports::RefreshTaskData.new
      new(refresh_report: refresh_report)
    end

    def initialize(refresh_report:)
      @refresh_report = refresh_report
    end

    def call(task_form, params)
      unless task_form.validate params
        return StoreResult.new entity: task_form.model, success: false, errors: ['Record is invalid']
      end

      task = task_form.sync
      call_on_task task
    end

    def call_on_task(task)
      ii = task.inventory_item
      task.product_items_per_case = ii.product_items_per_case
      task.duration_estimate = ProductionEstimate.new(ii).duration_for task.quantity

      actions = [
        -> { call_on task, :save },
        -> { refresh_report.call ii }
      ]
      in_transaction_with_rollback actions
    end

    private

    attr_reader :refresh_report
  end
end
