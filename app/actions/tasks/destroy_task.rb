# frozen_string_literal: true
module Tasks
  class DestroyTask < BaseAction
    include AffectsEntity

    def self.build
      refresh_report = ProductionReports::RefreshTaskData.new
      new(refresh_report: refresh_report)
    end

    def initialize(refresh_report:)
      @refresh_report = refresh_report
    end

    def call(task)
      return StoreResult.new(entity: nil, success: false, errors: ['Task not found']) unless task
      ii = task.inventory_item

      actions = [
        -> { call_on task, :destroy },
        -> { refresh_report.call ii }
      ]
      in_transaction_with_rollback actions
    end

    private

    attr_reader :refresh_report
  end
end
