# frozen_string_literal: true
module Tasks
  class FinishTask < BaseAction
    include AffectsEntity

    def self.build
      create_task = Tasks::CreateTask.build
      refresh_report = ProductionReports::RefreshTaskData.new
      new create_task: create_task, refresh_report: refresh_report
    end

    def initialize(create_task:, refresh_report:)
      @create_task = create_task
      @refresh_report = refresh_report
    end

    def call(task)
      return StoreResult.new(entity: nil, success: false, errors: ['Task not found']) unless task

      actions = [
        -> { finish task },
        -> { create_remainder_task_for task },
        -> { refresh_report.call task.inventory_item }
      ]

      in_transaction_with_rollback actions
    end

    private

    attr_reader :create_task, :refresh_report

    def finish(task)
      task.finished_at = Time.zone.now
      task.set_labour_cost
      call_on task, :save
    end

    def create_remainder_task_for(task)
      unless task.quantity_remainder.positive?
        return StoreResult.new(entity: nil, success: true)
      end

      new_task = Task.new(task.remainder_task_attributes)
      create_task.call_on_task new_task
    end
  end
end
