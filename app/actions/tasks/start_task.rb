# frozen_string_literal: true
module Tasks
  class StartTask < BaseAction
    include AffectsEntity

    def call(task, user)
      return StoreResult.new(entity: nil, success: false, errors: ['Task not found']) unless task

      task.started_at = Time.zone.now
      task.user_id = user.id
      call_on task, :save
    end
  end
end
