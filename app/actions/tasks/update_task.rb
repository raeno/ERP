# frozen_string_literal: true

module Tasks
  class UpdateTask < BaseAction
    include AffectsEntity

    def self.build
      refresh_report = ProductionReports::RefreshTaskData.new
      new(refresh_report: refresh_report)
    end

    def initialize(refresh_report:)
      @refresh_report = refresh_report
    end

    def call(task_form, params)
      unless task_form.validate params
        return StoreResult.new entity: task_form.model, success: false, errors: task_form.errors.messages
      end

      task = task_form.sync
      task.duration_estimate = ProductionEstimate.new(task.inventory_item).duration_for task.quantity

      actions = [
        -> { call_on task, :save },
        -> { refresh_report.call task.inventory_item }
      ]
      in_transaction_with_rollback actions
    end

    private

    attr_reader :refresh_report
  end
end
