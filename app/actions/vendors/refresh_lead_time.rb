# frozen_string_literal: true
module Vendors
  class RefreshLeadTime < BaseAction
    include AffectsEntity

    def call(vendor)
      invoices = vendor.invoices.received.reorder(date: :desc).limit(5)
      return StoreResult.new(entity: vendor, success: true) if invoices.empty?

      lead_times = invoices.map(&:lead_time_days)
      vendor.lead_time_days = (lead_times.sum.to_f / lead_times.count).round

      call_on vendor, :save
    end
  end
end
