# frozen_string_literal: true
module WithRollback
  # Accepts array of lambdas or procs.
  def in_transaction_with_rollback(actions)
    results = []
    ActiveRecord::Base.transaction do
      actions.each do |action|
        results << action.call
        rollback_if_failed(results)
      end
    end

    CombinedResult.new(*results)
  end

  private

  def rollback_if_failed(results)
    raise ActiveRecord::Rollback unless results.last.success?
  end
end
