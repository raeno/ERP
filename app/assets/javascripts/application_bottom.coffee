# This is a manifest file that'll be compiled into application.js, which will include all the files
# listed below.
#
# Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
# or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
#
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# compiled file.
#
# Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
# about supported directives.
#
#= require action_cable
#= require action_cable_init
#
#= require_directory ./invoices
#= require_directory ./materials_purchase
#= require_directory ./components
#= require_directory ./channels
#= require common
#= require in_place_editor
#= require table_checkboxes
#= require messagebox
#= require back_button
#= require reports_pagination
#= require batch
#= require batches
#= require material
#= require router

window.Kisoils ?= {}
window.SPINNER_HTML = "<i class='fa fa-spinner fa-spin spinner'/>"

Kisoils.Router.baseModules = ['Common', 'InPlaceEditor', 'Messagebox', 'BackButton']

Kisoils.Router.routes =
  product_reports:
    matcher: /product_reports/,
    matcherType: 'path',
    modules: ['ReportsPagination', 'TableCheckboxes']
  batches:
    matcher: /batches/,
    matcherType: 'path',
    modules: ['Batch', 'Batches']
  materials:
    matcher: /materials/,
    matcherType: 'path',
    modules: ['Material', 'TableCheckboxes', 'VendorFilter', 'PurchaseButton']
  invoices:
    matcher: /invoices/,
    matcherType: 'path',
    modules: ['Invoices.Attachment', 'Invoices.Form', 'Invoices.Show', 'Invoices.Index', 'Invoices.Charts']

Kisoils.Router.init()
