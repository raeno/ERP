window.Kisoils ?= {}
selectors = null

Kisoils.BackButton =
  selectors:
    backButton: -> $('.back-btn')

  init: ->
    selectors = @selectors
    @bindUIActions()

  bindUIActions: ->
    selectors.backButton().bind('click', -> history.back())
