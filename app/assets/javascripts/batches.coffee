window.Kisoils ?= {}
selectors = null

Kisoils.Batches =
  selectors:
    batchesViewTabs : -> $('.js-batchs-view-tab')
    activeTab: -> $('.nav.nav-tabs li.active a')
    specificViewTab: (view) -> $(".js-batchs-view-tab[data-view=#{view}]")
    batchesViewTabContent: (view) ->$(".js-tab-batches-#{view}")
    chartContainer: 'chart-container'
    dateRangeSelect: -> $('#date-range')

  init: ->
    selectors = @selectors
    @bindUIActions()

    @setTabFromHash()
    @loadDefaultTab()

  bindUIActions: ->
    selectors.batchesViewTabs().on 'show.bs.tab', (event) => @updateHash(event.target.hash)
    selectors.batchesViewTabs().on 'click', (event) => @updateTab(event.currentTarget)

    selectors.dateRangeSelect().on 'change', (event) => @updateTab(selectors.activeTab())

  setTabFromHash: ->
    hash = document.location.hash
    if hash
      viewName = hash.substring(1)
      selectors.specificViewTab(viewName).tab('show')

  loadDefaultTab: ->
    tab = selectors.activeTab()
    @updateTab(tab)

  updateTab: (element) ->
    @loadChart() if @needToLoadChart(element)
    $(element).tab('show')
    return false

  needToLoadChart: (element)  ->
    $(element).data('view') == 'graph-view'

  loadChart: (element) ->
    dateRange = selectors.dateRangeSelect().val()
    url = "/batches/chart?date_range=#{dateRange}"
    $.get(url, (data) =>
      color_series = ['#d9534f', '#f0ad4e', '#5bc0de']
      zone_subtitles = @prepareSubtitles(data.chart_data, color_series)
      
      Highcharts.chart(selectors.chartContainer, {
        colors: color_series,
        stacked: true,
        chart: {
          type: 'column'
        },
        title: {
          text: "Production (Last #{dateRange} Days)"
        },
        subtitle: {
          text: zone_subtitles.join(', ')
        },
        xAxis: {
          categories: data.days,
          labels: {
            formatter: ->
              return this.value.substring(1, 8)
          }
          crosshair: true
        },
        yAxis: {
          min: 0
        },
        tooltip: {
          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
          footerFormat: '</table>',
          shared: true,
          useHTML: true
        },
        plotOptions: {
          column: {
            pointPadding: 0.2,
            borderWidth: 0
          }
        },
        series: data.chart_data
      })
    )

  prepareSubtitles: (chart_data, color_series)->
    chart_data.map (zone, index) ->
      total = $(zone.data).reduce (x,y) -> x + y
      '<span style="color: ' + color_series[index] + '; font-size: 14px;">' + zone.name + ': ' + total + '</span>'

  updateHash: (newHash) ->
    window.location.hash = newHash
    window.scrollTo(0,0)
