window.Kisoils ?= {}
Kisoils.shipment = Kisoils.cable.subscriptions.create "ShipmentChannel",
  received: (data) ->
    name = data['shipment_name']
    status = data['status']
    message = data['message']
    fullMessage = "Shipment #{name}: #{message}"
    method = @messageType status
    Kisoils.Vue.toasted[method](fullMessage, { duration: 5000 })
    
  connected: ->

  messageType: (status) ->
    switch status
      when 'working' then 'info'
      when 'failed' then 'error'
      when 'finished' then 'success'
