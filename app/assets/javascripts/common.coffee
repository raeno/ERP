window.Kisoils ?= {}

Kisoils.Common =

  init: ->
    @commonInitializers()

  commonInitializers: ->
    $('.datepicker').datepicker(todayHighlight: true)
    $('[data-toggle="tooltip"]').tooltip()
    @initPopover()

    $('textarea.wysiwyg').trumbowyg()

    $('body').on 'click', '.not-implemented', (e) ->
      alert "This action is not implemented yet."
      e.preventDefault()

  initPopover: ->
    popoverSettings =
      placement: 'bottom'
      trigger: 'focus'
      container: 'body'
      html: true
      selector: '[data-toggle="popover"]'
    $('body').popover(popoverSettings)
