window.Kisoils ?= {}

class Kisoils.TabSwitcher
  constructor: (parentElement, tabs) ->
    @parentElement = parentElement
    @tabs = tabs

  init: ->
    new Kisoils.Vue({
      el: @parentElement,
      data:
        tabs: @tabs,
        activeTab: ''

      mounted: ->
        hash = window.location.hash
        tab = if hash
                (tab for id, tab of @tabs when tab.href == hash)[0]
              else
                first = Object.keys(@tabs)[0]
                @tabs[first]

        @activeTab = tab.name
        tab.activated()

      props: ['tab'],

      methods:
        setActive: (tab) ->
          @activeTab = tab.name
          window.location.hash = tab.href
          tab.activated()
    })
  
