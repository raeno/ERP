window.Kisoils ?= {}
Kisoils.Invoices ?= {}
selectors = null

Kisoils.Invoices.Attachment =
  selectors:
    uploadAttachment: $('.js-upload-attachment')
    changeAttachment: $('.js-change-attachment')
    deleteAttachment: $('.js-delete-attachment')
    attachmentPreview: $('.js-attachment-preview')
    uploaderInit: $('.js-uploader-init')
    filepickerViewer: -> $('.js-filepicker-preview')

  init: ->
    selectors = @selectors
    apiKey = selectors.uploaderInit.data('apiKey')
    @attachmentsHandler = new Kisoils.Models.FilestackAttachment apiKey
    @bindUIActions()
    @loadAttachmentPreview()

  bindUIActions: ->
    selectors.uploadAttachment.on 'click', (event) => @showUploadPopup(event)
    selectors.changeAttachment.on 'click',  => @changeAttachment()
    selectors.deleteAttachment.on 'click', => @deleteAttachment()

  invoiceId: ->
    selectors.uploaderInit.data('invoiceId')

  attachmentUrl: ->
    selectors.attachmentPreview.data('attachmentUrl')

  showUploadPopup: ->
    @attachmentsHandler.uploadSingleFilePopup( (uploadedFiles) =>
      url = uploadedFiles[0].url
      @updateInvoiceAttachment(url)
    )

  changeAttachment: ->
    @removeFromFilestack(@showUploadPopup)

  deleteAttachment: ->
    @removeFromFilestack(=> @updateInvoiceAttachment(null))

  removeFromFilestack: (onSuccess) ->
    @attachmentsHandler.removeFromFilestack(@attachmentUrl(),
      onSuccess,
      -> noty(text: "Failed to remove attachment from Filestack")
    )

  loadAttachmentPreview: ->
    url = selectors.attachmentPreview.data('attachmentUrl')
    return unless !!url
    @attachmentsHandler.loadAttachmentPreview(url, selectors.attachmentPreview)

  updateInvoiceAttachment: (url) ->
    updateAttachmentUrl = "/invoices/#{@invoiceId()}"
    $.ajax(
      type: 'PATCH',
      url: updateAttachmentUrl,
      data: { invoice: { attachment_url: url}},
      dataType: 'json')
        .done( -> window.location.reload())
        .fail( (data) ->
          noty(text: "Failed to update invoice attachment: #{data['error']}", type: 'alert', timeout: 3000)
        )
