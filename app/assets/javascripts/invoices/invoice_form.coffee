window.Kisoils ?= {}
Kisoils.Invoices ?= {}
selectors = null

Kisoils.Invoices.Form =
  selectors:
    materialSelect: $('.js-material-select')
    sizeSelect: $('.js-ordering-sizes')
    step2: $('.js-step-2')

  init: ->
    selectors = @selectors
    @bindUIActions()

  bindUIActions: ->
    selectors.materialSelect.on 'change', (event) => @showStep2(event.target)

  showStep2: (materialSelect) ->
    return unless @materialSelected()
    materialId = @selectedMaterialId()

    $.getJSON "/materials/" + materialId + "/ordering_sizes",
      (data) =>
        @populateOrderingSizes(data)
        selectors.step2.fadeIn().removeClass('hidden')

  selectedMaterialId: ->
    selectors.materialSelect.val()

  materialSelected: ->
    @selectedMaterialId().length > 0

  populateOrderingSizes: (data) ->
    addOptionFn = @addOptionToSelect

    selectors.sizeSelect.empty()
    $.each data, (index, item) ->
      addOptionFn(selectors.sizeSelect, item.id, item.name)

  addOptionToSelect: (theSelect, optionValue, optionText) ->
    theSelect.append $('<option />').val(optionValue).text(optionText)
