window.Kisoils ?= {}
Kisoils.Invoices ?= {}
selectors = null

Kisoils.Invoices.Show =
  selectors:
    receivedLink: -> $('.js-item-received')
    invoiceStatus: -> $('.js-status-badge')

  init: ->
    selectors = @selectors
    @bindUIActions()

  bindUIActions: ->
    selectors.receivedLink().on 'click', (event) => @markAsReceived(event)

  markAsReceived: (event) ->
    event.preventDefault()
    return unless confirm('Are you sure?')

    theLink = $(event.target)
    td = theLink.parents('td')

    invoice_id = theLink.data('invoice-id')
    item_id = theLink.data('invoice-item-id')
    url = '/invoices/' + invoice_id + '/invoice_items/' + item_id + '/item_received'

    $.ajax
      type: 'patch'
      url: url
      dataType: 'json'
      beforeSend: ->
        td.html("<i class='fa fa-spinner fa-spin'/>")
      success: (json) ->
        td.html("<span class='fa fa-check-circle green'/>")
        if json.invoice_received
          location.reload()
      error: (response) ->
        td.html("<span class='red'>Error</span>")
        alert 'Error! ' + response.responseText
