window.Kisoils ?= {}
Kisoils.Invoices ?= {}
selectors = null

Kisoils.Invoices.Charts =
  selectors:
    seriesChartContainer: 'invoices-series-chart'
    pieChartContainer: 'invoices-vendors-pie-chart'
    dateRangeSelect: -> $('.js-date-range-selector')

  init: ->
    selectors = @selectors
    @bindUIActions()

  bindUIActions: ->
    selectors.dateRangeSelect().on 'change', =>
      months = @selectedDateRange() || 12
      @loadSeriesChart months
      @loadVendorsPieChart months

  selectedDateRange: ->
    selectors.dateRangeSelect().val()

  loadCharts: ->
    @loadSeriesChart(12)
    @loadVendorsPieChart(12)

  loadSeriesChart: (months) ->
    container = @selectors.seriesChartContainer
    Kisoils.Invoices.SeriesChart.loadChart container, months

  loadVendorsPieChart: (months) ->
    container = @selectors.pieChartContainer
    Kisoils.Invoices.VendorsPieChart.loadChart container, months


