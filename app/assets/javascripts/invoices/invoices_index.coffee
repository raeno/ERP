window.Kisoils ?= {}
Kisoils.Invoices ?= {}
selectors = null

Kisoils.Invoices.Index =
  selectors:
    invoicesContentSelector: '.js-invoices-content'
    invoicesContent: -> $(selectors.invoicesContentSelector)

  init: ->
    selectors = @selectors
    @loadCharts() if @isInvoicesPage()

  isInvoicesPage: ->
    selectors.invoicesContent().length > 0

  loadCharts: ->
    Kisoils.Invoices.Charts.loadCharts()
