window.Kisoils ?= {}
Kisoils.Invoices ?= {}

Kisoils.Invoices.SeriesChart =

  loadChart: (containerId, interval) ->
    months = interval || 12
    url = "/invoices/chart?months=#{months}"
    $.get(url, (data) =>@buildChart(data, containerId))

  buildChart: (data, containerId) ->
    color_series = ['#d9534f', '#f0ad4e', '#5bc0de']
    subtitles = @prepareSubtitles(data.series, color_series)
    highchartsOptions = @buildHighchartsOptions(data, color_series, subtitles)
    Highcharts.chart containerId, highchartsOptions

  buildHighchartsOptions: (data, color_series, subtitles) ->
    colors: color_series
    stacked: true
    chart:
      type: 'column'
    title:
      text: "Invoices"
    subtitle:
      text: subtitles
    xAxis:
      categories: data.months,
      labels:
        formatter: ->
          return @value.substring(0, 10)
      crosshair: true
    yAxis:
      min: 0
      labels:
        formatter: ->
          "$#{@value}"
    tooltip: @prepareTooltip()

    plotOptions:
      column:
        pointPadding: 0.2,
        borderWidth: 0
    series: data.series

  prepareTooltip: ->
    headerFormat: '<span style="font-size:10px">{point.key}</span>
                     <table>'
    pointFormat: '<tr>
                    <td style="color:{series.color};padding:0">{series.name}:
                    </td>
                    <td style="padding:0">
                      <b>${point.y:.0f}</b>
                    </td>
                  </tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true

  prepareSubtitles: (series, color_series)->
    subtitles = series.map (status, index) =>
      total = $(status.data).reduce (x,y) -> x + y
      @subtitleText status.name.capitalize(), total, color_series[index]
    subtitles.push @subtitleText("Total", @totalSpent(series), 'black')
    subtitles.join('  |  ')

  totalSpent: (series) ->
    series.sum (entry) -> entry.data.reduce((x,y) -> x + y)

  subtitleText: (name, value, color) ->
    '<span style="color: ' + color + '; font-size: 14px;">' +
      "#{name}: $#{value.toLocaleString()}</span>"
