window.Kisoils ?= {}
Kisoils.Invoices ?= {}

Kisoils.Invoices.VendorsPieChart =
  loadChart: (containerId, interval) ->
    months = interval || 12
    url = "/invoices/vendors_chart?months=#{months}"
    $.get(url, (data) => @buildChart(data, containerId))

  buildChart: (data, containerId) ->
    series = [{ name: 'Expenses', colorByPoint: true, data: data }]
    highchartsOptions = @buildHighchartsOptions series
    Highcharts.setOptions
      lang:
        decimalPoint: ','
        thousandsSep: ','
    Highcharts.chart containerId, highchartsOptions

  buildHighchartsOptions: (series) ->
    chart:
      type: 'pie'
    title:
      text: 'Expenses by Vendor'
    tooltip:
      pointFormat: '{series.name}: <b>${point.y:.0f}</b>'
    plotOptions:
      pie:
        allowPointSelect: true
        cursor: 'pointer'
        dataLabels:
          format: '<b>{point.name}</b>: ${point.y:,.0f}'
          style:
            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
    series: series
