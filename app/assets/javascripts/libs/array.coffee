Array::chunk = (chunkSize) ->
  return this if chunkSize == 0
  array = this
  [].concat.apply [], array.map((elem, i) ->
    (if i % chunkSize then [] else [array.slice(i, i + chunkSize)])
  )

Array::sum = (fn = (x) -> x) ->
  @reduce ((a, b) -> a + fn b), 0

Array::present = () ->
  @.length > 0

Array::empty = () ->
  @.length == 0

Array::merge = (other) ->
  Array::push.apply @, other

Array::any ?= (f) ->
  (return true if f x) for x in @
  return false

Array::every ?= (f) ->
  (return false if not f x) for x in @
  return true

Array::unique = ->
  output = {}
  output[@[key]] = @[key] for key in [0...@length]
  value for key, value of output
