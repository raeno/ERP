extend = window.extend = (object, properties) ->
  for key, val of properties
    object[key] = val
  object

Object.resolve = (path, obj) ->
  path.split('.').reduce( ((prev, curr) ->
    if prev then prev[curr] else undefined
   ), obj || self)
