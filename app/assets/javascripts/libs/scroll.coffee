# detect end of scroll
window.Kisoils ?= {}

Kisoils.endOfScroll = (element) ->
  endZoneSize = 20
  scrollHeight = document.documentElement.scrollHeight
  endZone = scrollHeight - element.outerHeight() - endZoneSize
  position = element.scrollTop()
  position >= endZone

Kisoils.onScroll = (callback) ->
  $(window).on 'scroll', ->
    if Kisoils.endOfScroll($(window))
      $(window).off('scroll')
      callback()
