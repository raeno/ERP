window.Kisoils ?= {}
selectors = null

Kisoils.Material =
  selectors:
    uploaderInit: $('.js-uploader-init')
    materialInfo: $('.js-material-info')
    addAttachmentButton: $('.js-add-attachment')
    deleteAttachmentButton: $('.js-delete-attachment')
    showFullSize: $('.js-show-full-size')

  init: ->
    selectors = @selectors
    apiKey = selectors.uploaderInit.data('apiKey')
    @attachmentsHandler = new Kisoils.Models.FilestackAttachment apiKey
    @bindUIActions()

  bindUIActions: ->
    selectors.addAttachmentButton.on 'click', => @addAttachment()
    selectors.deleteAttachmentButton.on 'click', (event) => @deleteAttachment(event.target)
    selectors.showFullSize.on 'click', (event) => @showFullSizeViewer(event.target)

  materialId: ->
    selectors.materialInfo.data('materialId')

  addAttachment: ->
    @attachmentsHandler.uploadManyFilesPopup((uploadedFiles) =>
      urls = uploadedFiles.map (file) -> file.url
      @saveAttachmentsToMaterial(urls)
    )

  deleteAttachment: (element) ->
    attachmentId = $(element).parent().data('attachmentId')
    url = $(element).parent().data('url')
    @removeFromFilestack(url, => @deleteAttachmentForMaterial(attachmentId))

  showFullSizeViewer: (element) ->
    url = $(element).data('viewerUrl')
    lity(url)

  removeFromFilestack: (url, onSuccess) ->
    @attachmentsHandler.removeFromFilestack(url,
      onSuccess,
      -> noty(text: "Failed to remove attachment from Filestack")
    )

  deleteAttachmentForMaterial: (attachmentId) ->
    deleteAttachmentUrl = "/materials/#{@materialId()}/attachments/#{attachmentId}"
    $.ajax(
      type: 'DELETE',
      url: deleteAttachmentUrl,
      dataType: 'json')
      .done( -> window.location.reload())
      .fail((data) ->
        noty(text: "Failed to delete attachment: #{data['error']}", type: 'alert', timeout: 3000)
    )

  saveAttachmentsToMaterial: (urls) ->
    requests = urls.map (url) => @saveAttachment(url)
    $.when(requests)
      .done( -> window.location.reload())
      .fail((data) ->
        noty(text: "Failed to add attachments to material", type: 'alert', timeout: 3000)
      )

  saveAttachment: (url) ->
    materialId = @materialId()
    addAttachmentUrl = "/materials/#{materialId}/attachments"
    $.ajax(
      type: 'POST',
      url: addAttachmentUrl,
      data: { attachment: { url: url, material_id: materialId}},
      dataType: 'json')
