window.Kisoils ?= {}
selectors = null

Kisoils.PurchaseButton =
  selectors:
    vendorSelect: -> $('.js-vendor')
    purchaseButton: -> $('.js-create-invoice')
    selectedTrs: -> $('tbody input[type="checkbox"]:checked:visible').parents('tr')

  init: ->
    selectors = @selectors
    @bindUIActions()

  bindUIActions: ->
    selectors.purchaseButton().on 'click', (event) => @submitInvoice(event)

  selectedVendorId: -> selectors.vendorSelect().val()

  submitInvoice: (event) ->
    event.preventDefault()
    vendorId = @selectedVendorId()
    materialIds = $.map(selectors.selectedTrs(), (tr) -> $(tr).data('material-id'))

    if materialIds.length == 0
      Kisoils.Messagebox.show("Please select some materials.")
      return

    $.ajax
      method: 'PUT'
      dataType: 'JSON'
      url: '/invoices/generate'
      data: { invoice: { vendor_id: vendorId, material_ids: materialIds } }
      beforeSend: ->
        selectors.purchaseButton().replaceWith(SPINNER_HTML)
      success: (json) ->
        location.href = json.path
      error: (response) ->
        Kisoils.Messagebox.show('Error: ' + response.responseJSON.error_message)
