window.Kisoils ?= {}
selectors = null

Kisoils.VendorFilter =
  selectors:
    vendorSelect: -> $('.js-vendor')
    purchaseButtonPanel: -> $('.js-purchase-panel')
    allTrs: -> $('table.materials-purchase tbody tr')

  init: ->
    selectors = @selectors
    @bindUIActions()

  bindUIActions: ->
    selectors.vendorSelect().on 'change', (event) => @switchVendor()

  switchVendor: ->
    vendorId = @selectedVendorId()
    if vendorId == 'all'
      selectors.allTrs().show()
      selectors.purchaseButtonPanel().hide()
    else
      selectors.allTrs().hide()
      @vendorTrs(vendorId).show()
      selectors.purchaseButtonPanel().show()

  selectedVendorId: -> selectors.vendorSelect().val()

  vendorTrs: (vendorId) ->
    selectors.allTrs().filter -> parseInt(vendorId) in $(this).data('vendor-ids')
