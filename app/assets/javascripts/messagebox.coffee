window.Kisoils ?= {}
selectors = null

Kisoils.Messagebox =
  selectors:
    messagebox: -> $(".js-messagebox")
    container: -> $( ".js-message-boxes" )

  init: ->
    selectors = @selectors
    @bindUIActions()

  bindUIActions: ->

  show: (message) ->
    template = @messageTemplate('info', message)
    selectors.container().append(template)

  showError: (message) ->
    template = @messageTemplate('danger', message)
    selectors.container().append(template)
  
  showWarning: (message) ->
    template = @messageTemplate('warning', message)
    selectors.container().append(template)

  messageTemplate: (type, message) ->
    "<div class='alert alert-#{type}' style='margin-bottom: 10px;'>
      <button class='close' data-dismiss='alert'>×</button>
      <span>#{message}</span>
    </div"

