window.Kisoils ?= {}
Kisoils.Models ?= {}

class Kisoils.Models.FilestackAttachment

  constructor: (apiKey) ->
    filepicker.setKey apiKey
    @_self = @

  uploadSingleFilePopup: (onSuccess) ->
    @uploadPopup({ multiple: false }, onSuccess)

  uploadManyFilesPopup: (onSuccess) ->
    @uploadPopup({multiple: true}, onSuccess)

  uploadPopup: (pickerOptions, onSuccess) ->
    options = { services: ['COMPUTER'], mimetypes: ['application/pdf', 'image/*'] }
    filepicker.pickAndStore(
      $.extend(options, pickerOptions),
      {
        location: 'dropbox'
      },
      onSuccess)

  removeFromFilestack: (url, onSuccess, onFail) ->
    blob = { url: url }
    filepicker.remove(blob, onSuccess, onFail)

  loadAttachmentPreview: (url, parent) ->
    blob = { url: url }
    filepicker.stat(blob, (info) =>
      isImage = info.mimetype.match(/image/)
      if isImage
      then @renderAttachmentAsImage(url, parent)
      else @renderViewer(url, parent)
    )

  renderViewer: (url, parent) ->
    viewerBody = "<div id='filepicker-preview' class='js-filepicker-preview filepicker-preview' data-fp-url=#{url} type='filepicker-preview'/>"
    parent.append viewerBody
    filepicker.constructWidget $('.js-filepicker-preview')

  renderAttachmentAsImage: (url, parent) ->
    previewBody = "<img class='filepicker-preview' target='_blank' src=#{url} />"
    parent.append previewBody
