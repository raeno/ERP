window.Kisoils ?= {}
Kisoils.Products ?= {}
selectors = null
pageComponent = null

Kisoils.Products.Properties =
  selectors:
    productAvailabilitiesForm: -> $('.js-product-availabilities-form')
    internalTitle: -> $('#internal-title')

  init: ->
    selectors = @selectors
    pageVM = @initViewModel()

  initViewModel: ->
    new Vue({
      el: "#product-details",
      data:
        showFormConfirmation: false,
        internalTitle: selectors.internalTitle().data('title'),
        productId: selectors.internalTitle().data('productId'),
      methods:
        showConfirmation: ->
          @showFormConfirmation = true
        submitCheckboxesForm: ->
          @showFormConfirmation = false
          selectors.productAvailabilitiesForm().submit()
        cancelCheckboxesForm: ->
          @showFormConfirmation = false
        saveTitle: ->
          @showSpinner = true
          data = { product: { internal_title: @internalTitle } }
          $.ajax("/products/#{@productId}",
            method: 'PATCH',
            data: data,
            dataType: 'json')
    })
