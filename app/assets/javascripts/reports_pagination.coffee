window.Kisoils ?= {}
selectors = null

Kisoils.ReportsPagination =
  selectors:
    nextLink: -> $('.js-pagination a.next_page')
    reportsLoader: -> $('.js-reports-loader')

  init: ->
    selectors = @selectors
    @loadOnScroll(selectors.reportsLoader, selectors.nextLink)

  loadOnScroll: (loader, nextLinkSelector) ->
    Kisoils.onScroll =>
      url = nextLinkSelector().attr('href')
      unless url
        @loadOnScroll(loader, nextLinkSelector)
        return

      loader().toggleClass 'hidden', false
      $.ajax(url: url, dataType: 'script')
        .done(=> @loadOnScroll(loader, nextLinkSelector))
        .fail((xhr, status, error) ->
          console.log("Status: " + status + " Error: " + error)
        )
        .always(-> loader().toggleClass 'hidden', true)

