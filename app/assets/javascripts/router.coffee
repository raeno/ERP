window.Kisoils ?= {}

Kisoils.Router =

  baseModules: []
  loadedModules: []
  routes: {}

  init: ->
    @reloadModules()

  reloadModules: ->
    dynamicModules = []
    @loadMatchingModules dynamicModules, @routes

    modulesToLoad = @baseModules.concat dynamicModules
    modulesToLoad.forEach (module) ->
      try
        Object.resolve("Kisoils.#{module}").init()
      catch e
        console.log "Failed to load module #{module}"
        console.log e

    @loadedModules.merge modulesToLoad

  loadMatchingModules: (dynamicModules, routes)->
    $.each routes, (routeName, route) =>
      dynamicModules.merge route.modules if @routeMatch(route, document.location)
    dynamicModules

  routeMatch: (route, url) ->
    result = false
    switch route.matcherType
      when 'path'
        result = route.matcher.test url.pathname
      when 'hash'
        result = route.matcher.test url.hash
      else
        result = false

    if route.parentRoute
      parent = @routes[route.parentRoute]
      result && @routeMatch(parent, url)
    else
      result


