window.Kisoils ?= {}
selectors = null

Kisoils.TableCheckboxes =
  selectors:
    selectAllCheckbox: -> $('.js-select-all')
    allVisibleCheckboxes: -> $('tbody input[type="checkbox"]:visible')

  init: ->
    selectors = @selectors
    @bindUIActions()

  bindUIActions: ->
    selectors.selectAllCheckbox().on 'change', (event) => @toggleCheckboxes(event)

  toggleCheckboxes: (event) ->
    selectAllCheckbox = $(event.target)
    checked = selectAllCheckbox.prop("checked")
    @setCheckboxes(checked)

  setCheckboxes: (checked) ->
    selectors.selectAllCheckbox().prop("checked", checked)
    selectors.allVisibleCheckboxes().prop("checked", checked)

