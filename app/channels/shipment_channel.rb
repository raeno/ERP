# frozen_string_literal: true
class ShipmentChannel < ApplicationCable::Channel
  def subscribed
    stream_from "shipment_channel_#{current_user.id}"
  end
end
