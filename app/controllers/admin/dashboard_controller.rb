# frozen_string_literal: true
module Admin
  class DashboardController < ApplicationController
    def index
    end

    def fetch_products
      FetchProductsJob.perform_later
      UpdateAmazonInventoryJob.perform_later
      redirect_to admin_dashboard_index_path, notice: 'Started background products and inventory update'
    end

    def refresh_fba_allocations
      FetchFbaAllocationsJob.perform_later
      redirect_to admin_dashboard_index_path, notice: 'Started background fba allocations updated'
    end
  end
end
