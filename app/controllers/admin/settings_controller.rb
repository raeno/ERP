class Admin::SettingsController < ApplicationController
  def show
    @setting = Setting.first
  end

  def edit
    @setting = Setting.first
    render_edit
  end

  def update
    @setting = Setting.first
    if @setting.update(settings_params)
      redirect_to admin_settings_path, notice: 'Settings were successfully updated'
    else
      render_edit
    end
  end

  def settings_params
    permitted_attrs =
      [:aws_access_key_id, :aws_secret_key, :mws_marketplace_id, :mws_merchant_id,
       :address_name, :address_line1, :address_line2, :address_city,
       :address_state, :address_zip_code, :address_country,
       :red_urgency_level_days, :yellow_urgency_level_days]
    params.require(:setting).permit(permitted_attrs)
  end

  protected

  def render_edit
    if params[:amazon_credentials]
      render 'edit_amazon_credentials'
    elsif params[:address]
      render 'edit_address'
    elsif params[:urgency_levels]
      render 'edit_urgency_levels'
    end
  end
end
