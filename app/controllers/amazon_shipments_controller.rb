# frozen_string_literal: true
class AmazonShipmentsController < ApplicationController
  def index
    shipments = AmazonShipment.filtered(params[:filter])
                  .includes(:fba_warehouse)
                  .order('created_at DESC', 'status ASC')
    shipments = shipments.where(fba_warehouse_id: params[:warehouse_id]) if params[:warehouse_id]
    @presenter = AmazonShipmentsPresenter.new shipments, params[:filter]

    respond_to do |format|
      format.html
      format.json  { render json: @presenter.shipments }
    end
  end

  def show
    amazon_shipment = AmazonShipment.find params[:id]
    @presenter = ShipmentPresenter.new amazon_shipment
  end

  def create
    result = actions[:create_shipment].call current_user, amazon_shipment_params

    if result.success?
      respond_to do |format|
        format.json { render json: { job_id: result.job_id, name: result.shipment_name }, status: :ok }
      end
    else
      respond_to do |format|
        format.json { render json: { error: result.error_message }, status: :unprocessable_entity }
      end
    end
  end

  def update
    shipment = AmazonShipment.find params[:id]
    options = amazon_shipment_params.merge(shipment: shipment)
    result = actions[:update_shipment].call options, current_user

    if result.success?
      respond_to do |format|
        format.json { render json: { job_id: result.job_id }, status: :ok }
      end
    else
      respond_to do |format|
        format.json { render json: { error: result.error_message }, status: :error }
      end
    end
  end

  private

  def amazon_shipment_params
    params.require(:amazon_shipment)
      .permit(:name, :shipment_id, :status, :cases_required, :fba_warehouse_id,
              shipment_entries: [:product_id, :quantity])
  end

  def actions
    { create_shipment: AmazonShipments::ScheduleShipment.build,
      update_shipment: AmazonShipments::ScheduleShipmentUpdate.build }
  end
end
