# frozen_string_literal: true
class BatchesController < ApplicationController
  include FormattedDate
  include Logging

  def index
    batches = Batch.fresh_first
                   .includes(:zone, :product, :users)
    presenters = batches.map { |batch| BatchPresenter.new(batch) }
    render locals: { presenters: presenters }
  end

  def show
    batch = Batch.find params[:id]
    presenter = BatchPresenter.new(batch)
    render locals: { presenter: presenter }
  end

  def chart
    start_date = params[:date_range].to_i.days.ago
    chart_presenter = BatchChartPresenter.new start_date: start_date, end_date: Time.zone.today, zones: Zone.all

    respond_to do |format|
      format.json { render json: chart_presenter.chart_data, status: :ok }
    end
  end

  def new
    params[:batch] ||= { quantity: 0 }
    batch = Batch.new(batch_params)
    batch.completed_on = Time.zone.today
    batch.user_ids << current_user.id

    presenter = BatchPresenter.new batch
    render locals: { presenter: presenter }
  end

  def edit
    batch = Batch.find_by id: params[:id]
    presenter = BatchPresenter.new batch
    render locals: { presenter: presenter }
  end

  def create
    result = actions[:create_batch].call(batch_params.to_h)
    if result.success?
      redirect_to result.entity, notice: "Batch successfully created"
    else
      presenter = BatchPresenter.new result.entity
      flash[:error] = result.error_message
      render :new, locals: { presenter: presenter }
    end
  end

  def update
    batch = Batch.find_by id: params[:id]
    result = actions[:change_batch].call(batch, batch_params.to_h)

    if result.success?
      redirect_to result.entity, notice: "Batch successfully updated"
    else
      presenter = BatchPresenter.new result.entity
      flash[:error] = result.error_message
      render :edit, locals: { presenter: presenter }
    end
  end

  def destroy
    batch = Batch.find_by id: params[:id]
    result = actions[:destroy_batch].call batch
    if result.success?
      redirect_to batches_path, notice: "Batch successfully deleted."
    else
      flash[:error] = result.error_message
      redirect_to batches_path
    end
  end

  private

  def actions
    { create_batch: Batches::CreateBatch.build,
      change_batch: Batches::ChangeBatch.build,
      destroy_batch: Batches::DestroyBatch.build
    }
  end

  def batch_params
    params.require(:batch)
          .permit(:product_id, :quantity, :completed_on, :zone_id, :task_id,
                  :notes, :auxillary_info, :user_ids => [])
  end
end
