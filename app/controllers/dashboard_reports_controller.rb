# frozen_string_literal: true
class DashboardReportsController < ApplicationController
  def inventory_health
    @presenters =
      Product.active.ordered_by_title.includes(:shipment_report)
             .map { |product| ProductPresenter.new(product) }
  end

  def profit_and_loss
    products = Product.active.ordered_by_title.includes(:shipment_report, :amazon_inventory)
    @totals_presenter = ProductTotalsPresenter.new(products)
  end
end
