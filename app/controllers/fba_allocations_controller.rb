# frozen_string_literal: true
class FbaAllocationsController < ApplicationController
  respond_to :json

  def index
    result = fetch_allocations params
    if result.success?
      allocations = result.entity.reject(&:empty?)
      presenter = AllocationsSerializer.new allocations
      render json: presenter, status: :ok
    else
      render json: { error: result.error_message, status: :error }, status: 500
    end
  end

  private

  def actions
    { fetch_product_allocations: ShipmentItems::FetchAllocations.build }
  end

  def fetch_allocations(params)
    shipment_item = ShipmentItem.from_params params
    actions[:fetch_product_allocations].call shipment_item
  end
  
  def ship_from_address
    Setting.instance.shipment_address.to_h
  end
end
