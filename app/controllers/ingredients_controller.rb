# frozen_string_literal: true
class IngredientsController < ApplicationController
  def edit
    @product = Product.find(params[:id])
    @ingredients = @product.ingredients
                           .map { |ingredient| IngredientPresenter.new ingredient }
    @new_ingredient_presenter = IngredientPresenter.new Ingredient.new(product: @product)
  end

  # Parameters: {
  #   "ingredients"=>{
  #      "0"=>{"inventory_item_id"=>"1", "quantity"=>"5"},
  #      "1"=>{"inventory_item_id"=>"2", "quantity"=>"2"}
  #   }
  # }
  def update
    product = Product.find(params[:id])

    respond_to do |format|
      format.json do
        ingredients_hash = params[:ingredients]&.values || {}
        if product.replace_ingredients(ingredients_hash)
          flash[:notice] = "Ingredients were successfully saved."
          render json: { redirect_to: product_inventory_url(product) }, status: :ok
        else
          render json: { error_message: product.errors.full_messages.first }, status: :unprocessable_entity
        end
      end
    end
  end
end
