# frozen_string_literal: true
class InventoriesController < ApplicationController
  def create
    result = actions[:create].call inventory_params
    if result.success?
      respond_with_success
    else
      respond_with_failure result.error_message
    end
  end

  def update
    inventory = Inventory.find params[:id]
    result = actions[:update].call inventory, inventory_params
    if result.success?
      respond_with_success
    else
      respond_with_failure result.error_message
    end
  end

  def actions
    { create: SimpleActions::Create.new(Inventory),
      update: SimpleActions::Update.new(Inventory) }
  end

  def inventory_params
    params.require(:inventory).permit(:quantity)
  end

  private

  def respond_with_success
    respond_to do |format|
      format.json { head :no_content, status: :ok }
    end
  end

  def respond_with_failure(message)
    respond_to do |format|
      format.json { render json: { error: message }, status: :unprocessable_entity }
    end
  end
end
