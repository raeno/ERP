# frozen_string_literal: true
class InventoryItemsController < ApplicationController
  def update
    inventory_item = InventoryItem.find(params[:id])
    result = actions[:update_inventory_item].call(inventory_item, inventory_item_params)

    if result.success?
      respond_with_update_success
    else
      respond_with_update_error(inventory_item, result)
    end
  end

  def destroy
    @inventory_item = InventoryItem.find(params[:id])

    if @inventory_item.destroy
      flash[:notice] = "InventoryItem was deleted."
    else
      flash[:error] = "Cannot delete. #{@inventory_item.errors.full_messages.join(' ')}"
    end
    redirect_to inventory_items_path
  end

  def actions
    { create_inventory_item: InventoryItems::CreateInventoryItem.build,
      update_inventory_item: InventoryItems::UpdateInventoryItem.new }
  end

  def inventory_item_params
    params.require(:inventory_item)
          .permit(:zone_id, :production_zone_id, :inventoriable_id, :inventoriable_type,
                  :production_units_per_minute, :production_setup_minutes,
                  inventoriable_attributes: [:name, :unit_id, :unit_price],
                  inventory_attributes: [:quantity])
  end

  private

  def respond_with_update_success
    respond_to do |format|
      format.html { redirect_to inventory_items_path, notice: 'InventoryItem was successfully updated.' }
      format.json { head :no_content, status: :ok }
    end
  end

  def respond_with_update_error(inventory_item, result)
    @presenter = InventoryItemPresenter.new inventory_item
    respond_to do |format|
      format.html { render action: :edit }
      format.json { render json: { error: result.error_message }, status: :unprocessable_entity }
    end
  end
end
