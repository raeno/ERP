# frozen_string_literal: true
class InvoiceItemsController < ApplicationController
  before_action do
    @invoice = Invoice.find(params[:invoice_id])
  end

  def show
    item = @invoice.items.find params[:id]
    init_presenter(item)
  end

  def new
    item = @invoice.items.build
    item.build_material_container
    init_presenter(item)
  end

  def edit
    item = @invoice.items.find params[:id]
    init_presenter(item)
  end

  def create
    result = actions[:create_item].call invoice_item_params.to_h

    if result.success?
      redirect_to @invoice, notice: 'Line item was successfully created.'
    else
      flash[:error] = result.error_message
      invoice_item = result.entity
      invoice_item.build_material_container
      init_presenter(invoice_item)
      render action: :new
    end
  end

  def update
    item = @invoice.items.find params[:id]
    result = actions[:update_item].call item, invoice_item_params(item).to_h

    if result.success?
      redirect_to @invoice, notice: 'Line item was successfully updated.'
    else
      flash.now[:error] = result.error_message
      init_presenter(result.entity)
      render action: :edit
    end
  end

  def item_received
    item = @invoice.items.find params[:id]
    result = actions[:item_received].call item

    respond_to do |format|
      format.json do
        if result.success?
          render json: { invoice_received: @invoice.received? }, status: :ok
        else
          render json: result.error_message, status: :unprocessable_entity
        end
      end
    end
  end

  def destroy
    item = @invoice.items.find params[:id]
    result = actions[:destroy_item].call item

    if result.success?
      flash[:notice] = 'Line item was deleted'
    else
      flash[:error] = "Failed to destroy line item: #{result.error_message}"
    end
    redirect_to @invoice
  end

  def actions
    { create_item:  InvoiceItems::CreateInvoiceItem.build,
      update_item:  InvoiceItems::UpdateInvoiceItem.build,
      destroy_item: InvoiceItems::DestroyInvoiceItem.build,
      item_received: InvoiceItems::InvoiceItemReceived.build }
  end

  def invoice_item_params(item = nil)
    insensitive_params = [
      material_container: [:external_name, :lot_number, :expiration_date_str]
    ]

    sensitive_params = [
      :invoice_id, :quantity,
      material_container: [:material_id, :external_name,
                           :ordering_size_id, :price,
                           :lot_number, :expiration_date_str]
    ]

    return params.require(:invoice_item).permit(insensitive_params) if item&.locked?
    return params.require(:invoice_item).permit(sensitive_params)
  end

  private

  def init_presenter(invoice_item)
    @presenter = InvoiceItemPresenter.new(invoice_item)
  end
end
