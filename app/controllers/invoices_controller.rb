# frozen_string_literal: true
class InvoicesController < ApplicationController
  DEFAULT_CHART_RANGE = 12

  def index
    invoices = Invoice.includes(:vendor, items: [material_container: :material]).ordered
    @presenters = invoices.map { |invoice| InvoicePresenter.new(invoice) }
  end

  def chart
    months = params[:months]&.to_i || DEFAULT_CHART_RANGE
    invoices = Invoice.for_last_n_months(months).order('created_at ASC')
    invoices_chart_presenter = InvoiceChartPresenter.new invoices
    respond_to do |format|
      format.json do
        render json: invoices_chart_presenter.chart_data, status: :ok
      end
    end
  end

  def vendors_chart
    months = params[:months]&.to_i || DEFAULT_CHART_RANGE
    invoices = Invoice.for_last_n_months(months).includes(:vendor)
    vendors_chart_presenter = InvoiceVendorsChartPresenter.new invoices
    respond_to do |format|
      format.json do
        render json: vendors_chart_presenter.chart_data, status: :ok
      end
    end
  end

  def show
    invoice = Invoice.includes(:items => [material_container: [:unit, material: :unit]]).find(params[:id])
    init_presenter(invoice)
  end

  def new
    invoice = Invoice.new
    init_presenter(invoice)
  end

  def edit
    invoice = Invoice.find params[:id]
    init_presenter(invoice)
  end

  def create
    result = actions[:create_invoice].call invoice_params.to_h
    if result.success?
      redirect_to result.entity, notice: 'Invoice was successfully created.'
    else
      init_presenter result.entity
      render action: :new
    end
  end

  def generate
    result = actions[:generate_invoice].call invoice_params.to_h
    respond_to do |format|
      format.json do
        if result.success?
          json = { path: url_for(controller: :invoices, action: :show, id: result.entity, only_path: true) }
          render json: json, status: :ok
        else
          render json: { error_message: result.error_message }, status: :unprocessable_entity
        end
      end
    end
  end

  def update
    @invoice = Invoice.find params[:id]
    result = actions[:update_invoice].call @invoice, invoice_params(@invoice).to_h

    respond_to do |format|
      format.html do
        if result.success?
          redirect_to result.entity, notice: 'Invoice was successfully updated.'
        else
          init_presenter(result.entity)
          flash.now[:error] = result.error_message
          render action: :edit
        end
      end

      format.json do
        json_body = result.success? ? { status: :success } : { status: :error, error: result.error_message }
        status = result.success? ? 200 : 500
        render json: json_body, status: status
      end
    end
  end

  def invoice_paid
    invoice = Invoice.find params[:id]
    result = actions[:invoice_paid].call invoice

    if result.success?
      redirect_to invoice, notice: 'Invoice was marked as paid'
    else
      flash[:error] = "Cannot mark invoice as paid. #{result.error_message}"
      redirect_to invoice
    end
  end

  def destroy
    invoice = Invoice.find params[:id]
    result = actions[:destroy_invoice].call invoice

    if result.success?
      flash[:notice] = 'Invoice was deleted'
    else
      flash[:error] = "Failed to destroy invoice: #{result.error_message}"
    end
    redirect_to invoices_path
  end

  def actions
    { create_invoice:   SimpleActions::Create.new(Invoice),
      generate_invoice: Invoices::GenerateInvoice.build,
      update_invoice:   Invoices::UpdateInvoice.build,
      invoice_paid:     Invoices::InvoicePaid.build,
      destroy_invoice:  Invoices::DestroyInvoice.build }
  end

  def invoice_params(invoice = nil)
    generic_params = [:date, :date_str, :number, :notes, :attachment_url]
    sensitive_params = [:vendor_id, :total_price, material_ids: [], fees_attributes: [:id, :name, :price, :_destroy]]

    return params.require(:invoice).permit(generic_params) if invoice&.locked?
    return params.require(:invoice).permit(generic_params + sensitive_params)
  end

  private

  def init_presenter(invoice)
    @presenter = InvoicePresenter.new(invoice)
  end
end
