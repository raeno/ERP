# frozen_string_literal: true
class MaterialCategoriesController < ApplicationController
  def index
    @categories = MaterialCategory.ordered
  end

  def new
    @category = MaterialCategory.new
  end

  def create
    @category = MaterialCategory.new material_category_params

    if @category.save
      redirect_to material_categories_path, notice: 'Category was successfully created.'
    else
      render action: :new
    end
  end

  def edit
    @category = MaterialCategory.find params[:id]
  end

  def update
    @category = MaterialCategory.find params[:id]

    if @category.update material_category_params
      redirect_to material_categories_path, notice: 'Category was updated'
    else
      render :edit
    end
  end

  def destroy
    @category = MaterialCategory.find params[:id]

    if @category.destroy
      flash[:notice] = 'Category was deleted'
    else
      flash[:error] = 'Error deleting category'
    end

    redirect_to material_categories_path
  end

  def material_category_params
    params.require(:material_category).permit(:name)
  end
end
