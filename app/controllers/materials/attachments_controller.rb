# frozen_string_literal: true
module Materials
  class AttachmentsController < ApplicationController
    def index
      material = Material.find params[:material_id]
      @presenter = MaterialPresenter.new material
    end

    def create
      result = actions[:create_attachment].call attachment_params
      respond_to do |format|
        format.json do
          if result.success?
            attachment = result.entity
            render json: { status: :success, preview_url: attachment.preview_url }, status: 200
          else
            render json: { status: :error, error: result.error_message }, status: 500
          end
        end
      end
    end

    def destroy
      material = Material.find params[:material_id]
      attachment = material.attachments.find params[:id]
      result = actions[:destroy_attachment].call attachment
      respond_to do |format|
        format.json do
          if result.success?
            render json: { status: :success }, status: 200
          else
            render json: { status: :error, error: result.error_message }, status: 500
          end
        end
      end
    end

    private

    def actions
      { create_attachment: MaterialAttachments::CreateAttachment.new,
        destroy_attachment: SimpleActions::Destroy.new(Attachment) }
    end

    def attachment_params
      params.require(:attachment).permit(:url, :material_id, :mimetype)
    end
  end
end
