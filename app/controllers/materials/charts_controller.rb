# frozen_string_literal: true
module Materials
  class ChartsController < ApplicationController
    def index
      material = Material.find params[:material_id]
      @presenter = MaterialPresenter.new material
    end

    def cost_over_time
      material = Material.find params[:material_id]
      presenter = MaterialCostChartPresenter.new material
      respond_to do |format|
        format.json { render json: presenter }
      end

    end
  end
end
