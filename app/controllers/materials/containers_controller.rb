# frozen_string_literal: true
module Materials
  class ContainersController < ApplicationController
    def index
      material = Material.find params[:material_id]
      @presenter = MaterialPresenter.new material
    end

    def edit
      material = Material.find params[:material_id]
      container = material.material_containers.find params[:id]
      @material_presenter = MaterialPresenter.new material
      @container_presenter = MaterialContainerPresenter.new(container)
    end

    def update
      material = Material.find params[:material_id]
      container = material.material_containers.find params[:id]
      result = actions[:update_container].call container, material_container_params
      if result.success?
        redirect_to material_containers_path(material), notice: 'Successfully updated container'
      else
        render :edit, alert: 'Failed to update container'
      end
    end

    def actions
      { update_container: MaterialContainers::UpdateMaterialContainer.build }
    end

    def material_container_params
      params.require(:material_container)
            .permit(:expiration_date_str, :amount_left, :status)
    end
  end
end
