# frozen_string_literal: true
module Materials
  class ForecastsController < ApplicationController
    def index
      material = Material.find params[:material_id]
      @presenter = MaterialPresenter.new material
    end
  end
end
