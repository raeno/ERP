# frozen_string_literal: true
module Materials
  class InvoicesController < ApplicationController
    def index
      materials = Material.all.includes(invoice_items: [material_container: :material, invoice: :vendor])
      material = materials.find params[:material_id]
      @presenter = MaterialPresenter.new material
    end
  end
end
