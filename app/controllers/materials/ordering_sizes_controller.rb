# frozen_string_literal: true
module Materials
  class OrderingSizesController < ApplicationController
    def index
      material = Material.find params[:material_id]
      respond_to do |format|
        format.json do
          sizes = material.ordering_sizes.ordered.map { |size| { id: size.id, name: size.to_s } }
          render json: sizes
        end
      end
    end
  end
end
