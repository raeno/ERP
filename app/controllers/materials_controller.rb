class MaterialsController < ApplicationController
  def index
    if params[:purchase]
      materials = Material.includes(:unit,
        :inventory_item => [:inventory, :production_report, :inventoriable])
      @presenters = materials.map { |material| MaterialPresenter.new(material) }
      @presenters = @presenters.sort_by(&:demand_coverage)
      render 'index_purchase'
    else  # inventory
      materials = Material.ordered_by_zone.ordered_by_name.includes(:unit,
        :material_vendors => :vendor, :inventory_item => :inventory)

      category_id = params[:category_id]
      materials = materials.in_category(category_id) if category_id

      zone_id = params[:zone_id]
      materials = materials.for_zone(zone_id) if zone_id

      @presenter = MaterialsPresenter.new(materials, category_id, zone_id)
      render 'index_inventory'
    end
  end

  def show
    init_presenter
  end

  def new
    material = Material.new
    @presenter = MaterialPresenter.new(material)
  end

  def create
    result = actions[:create_material].call material_params
    material = result.entity

    if result.success?
      redirect_to material, notice: 'Material was successfully created.'
    else
      @presenter = MaterialPresenter.new(material)
      render :new
    end
  end

  def edit
    init_presenter
  end

  def edit_sizes
    material = Material.find params[:material_id]
    @presenter = MaterialPresenter.new(material)
  end

  def update
    material = Material.find params[:id]
    result = actions[:update_material].call(material, material_params)

    if result.success?
      redirect_to material, notice: 'Material was updated'
    else
      @presenter = MaterialPresenter.new(material)
      template = params[:page] == "edit_sizes" ? :edit_sizes : :edit
      render template
    end
  end

  def destroy
    material = Material.find params[:id]
    if material.destroy
      redirect_to materials_path, notice: 'Material was deleted'
    else
      @presenter = MaterialPresenter.new(material)
      render :edit
    end
  end

  def actions
    { create_material: Materials::CreateMaterial.build,
      update_material: Materials::UpdateMaterial.build }
  end

  def material_params
    params.require(:material)
          .permit(:name, :unit_id, :ingredient_unit_id, :unit_conversion_rate,
                  :image_url, :category_id,
                  :inventory_item_production_zone_id,
                  ordering_sizes_attributes: [:id, :name, :amount, :unit_id, :_destroy],
                  material_vendors_attributes: [:id, :vendor_id, :url, :_destroy])
  end

  private

  def init_presenter
    material = Material.find params[:id]
    @presenter = MaterialPresenter.new(material)
  end
end
