# frozen_string_literal: true

class PlanningController < ApplicationController
  def make
    render 'index', locals: {
      presenter: planning_presenter(Zone.make_zone, params)
    }
  end

  def pack
    render 'index', locals: {
      presenter: planning_presenter(Zone.pack_zone, params)
    }
  end

  def workload
    render 'workload', locals: { workload: UserWorkload.new }
  end

  private

  def planning_presenter(zone, params)
    status = params[:status]&.to_sym || :unassigned
    inventory_items = fetch_inventory_items zone, status
    ProductionPlanningPresenter.new inventory_items, zone, status: status
  end

  def fetch_inventory_items(zone, status)
    reports = InventoryItems::ProductionReport.with_status(status)
    InventoryItem.in_zone(zone).active_products
      .joins(:production_report).merge(reports)
      .includes(:zone, :production_report, product: :shipment_report)
      .ordered_by_days_of_cover
  end
end
