# frozen_string_literal: true
class ProductReportsController < ApplicationController
  def ship
    page = params[:page]
    @reports = Products::ShipmentReport.active.ordered.includes(:product).paginate(page: page)

    @presenter = ShipmentReportsPresenter.new @reports, helpers
    respond_to do |format|
      format.html
      format.json { render json: @presenter, status: 200 }
    end
  end
end
