# frozen_string_literal: true
module Products
  class ArchivesController < ApplicationController
    def index
      products = Product.inactive.order(:internal_title).includes(:zones)
      @presenters = products.map { |product| ProductPresenter.new product }
    end
  end
end
