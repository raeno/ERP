# frozen_string_literal: true
module Products
  class AttachmentsController < ApplicationController
    def index
      product = Product.find params[:product_id]
      @attachments = product.attachments
      @presenter = ProductPresenter.new product
    end

    def create
      result = actions[:create_attachment].call attachment_params
      respond_to do |format|
        format.json do
          if result.success?
            attachment = result.entity
            render json: { status: :success, preview_url: attachment.preview_url }, status: 200
          else
            render json: { status: :error, error: result.error_message }, status: 500
          end
        end
      end
    end

    def destroy
      product = Product.find params[:product_id]
      attachment = product.attachments.find params[:id]
      result = actions[:destroy_attachment].call attachment
      respond_to do |format|
        format.json do
          if result.success?
            render json: { status: :success }, status: 200
          else
            render json: { status: :error, error: result.error_message }, status: 500
          end
        end
      end
    end

    def actions
      { create_attachment: ProductAttachments::CreateAttachment.new,
        destroy_attachment: SimpleActions::Destroy.new(Attachment) }
    end

    def attachment_params
      params.require(:attachment).permit(:url, :product_id, :mimetype)
    end
  end
end
