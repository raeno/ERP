# frozen_string_literal: true
module Products
  class BarcodesController < ApplicationController
    def show
      product = Product.find params[:product_id]
      generator = BarcodeGenerator.new
      generator.create_barcode product.title, product.fnsku, "tmp/barcode.pdf"

      respond_to do |format|
        format.pdf do
          pdf_filename = File.join(Rails.root, "tmp/barcode.pdf")
          send_file(pdf_filename, filename: "my.pdf", disposition: 'inline', type: "application/pdf")
        end
      end
    end
  end
end
