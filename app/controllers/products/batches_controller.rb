# frozen_string_literal: true
module Products
  class BatchesController < ApplicationController
    def index
      product = Product.includes(:batches => [:zone, :users]).find(params[:product_id])
      @presenter = ProductPresenter.new(product)
    end
  end
end
