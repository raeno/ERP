# frozen_string_literal: true
module Products
  class CaseLabelsController < ApplicationController
    def show
      product = Product.find params[:product_id]

      filename = actions[:create_label].call product
      respond_to do |format|
        format.pdf do
          downloaded_file_name = "#{product.name.underscore}_label.pdf"
          send_file(filename, filename: downloaded_file_name, disposition: 'inline', type: 'application/pdf')
        end
      end
    end

    def actions
      { create_label: Products::CreateProductCaseLabel.build }
    end
  end
end
