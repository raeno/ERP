# frozen_string_literal: true
module Products
  class InventoriesController < ApplicationController
    def show
      product = Product.includes(
        :inventory_items => [:zone, :production_report, :product, :inventoriable],
        :ingredients => {
          :inventory_item => [
            :production_report, :inventoriable, :inventory,
            :material => [:unit, :ingredient_unit, :category]
          ]
        }
      ).find(params[:product_id])
      @presenter = ProductPresenter.new product
    end

    def index
      zone_id = params[:zone_id]
      @zone = Zone.fetch_or_default zone_id

      products = InventoryItem.active_products.in_zone(@zone).includes(
        :zone, :inventory, :inventoriable, :product => [:shipment_report]
      ).order('products.internal_title')
      @presenters = products.map { |product| InventoryItemPresenter.new(product) }
    end
  end
end
