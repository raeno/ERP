# frozen_string_literal: true
module Products
  class MediaController < ApplicationController
    def index
      product = Product.find params[:product_id]
      @presenter = ProductPresenter.new product
    end
  end
end
