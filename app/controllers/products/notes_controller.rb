# frozen_string_literal: true
module Products
  class NotesController < ApplicationController
    def show
      init_presenter
    end

    def edit
      init_presenter
    end

    def update
      product = Product.find params[:product_id]
      product.notes = params[:product][:notes]
      if product.save
        redirect_to product_note_path(product), notice: "Notes were updated."
      else
        @presenter = ProductPresenter.new product
        render :edit
      end
    end

    private

    def init_presenter
      product = Product.find params[:product_id]
      @presenter = ProductPresenter.new product
    end
  end
end
