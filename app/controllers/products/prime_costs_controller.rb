# frozen_string_literal: true
module Products
  class PrimeCostsController < ApplicationController
    def index
      product = Product.find_by id: params[:product_id]
      render(json: { error: 'Product not found' }, status: 404) && return unless product

      snapshots = product.product_cogs_snapshots
                         .final_only
                         .ordered

      chart_presenter = PrimeCostChartPresenter.new product, snapshots
      render json: chart_presenter.chart_data, status: :ok
    end
  end
end
