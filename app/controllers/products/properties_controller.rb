# frozen_string_literal: true
module Products
  class PropertiesController < ApplicationController
    def show
      product = Product.find params[:product_id]
      @presenter = ProductPresenter.new(product)
    end
  end
end
