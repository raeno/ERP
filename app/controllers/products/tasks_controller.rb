# frozen_string_literal: true
module Products
  class TasksController < ApplicationController
    def index
      product = Product.find(params[:product_id])
      @product_presenter = ProductPresenter.new(product)

      tasks = Task.for_product(product).ordered.includes(:user,
        :batch => [:product], :inventory_item => [:zone]).limit(500)
      @task_presenters = tasks.map { |task| TaskPresenter.new(task) }
    end
  end
end
