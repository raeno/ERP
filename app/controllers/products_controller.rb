# frozen_string_literal: true
class ProductsController < ApplicationController
  before_action :find_product, only: [:update, :override_quantity]

  # PUT
  def update
    result = actions[:update_product].call @product, request_params
    respond_to do |format|
      format.html { redirect_to product_properties_path(@product) }
      format.json do
        if result.success?
          head :no_content, status: :ok
        else
          render json: { error: result.error_message }, status: :unprocessable_entity
        end
      end
    end
  end

  # PUT
  def override_quantity
    quantity, shipment_type = parse_override_quantity_params params[:product]
    shipment_item = ShipmentItem.build @product, quantity, shipment_type
    result = actions[:fetch_allocations].call shipment_item
    respond_to do |format|
      format.json do
        if result.success?
          allocations = result.entity.reject(&:empty?)
          presenter = AllocationsSerializer.new allocations
          render json: presenter, status: :ok
        else
          render json: [], status: 500
        end
      end
    end
  end

  def show
    redirect_to product_inventory_path(params[:id])
  end

  def actions
    { update_product: Products::UpdateProduct.build,
      override_product_quantity: Products::OverrideProductQuantity.new,
      fetch_allocations: Products::FetchProductAllocations.build }
  end

  private

  def request_params
    params.require(:product)
          .permit(:to_ship_cases_qty_override, :to_ship_items_qty_override,
                  :internal_title,
                  :items_per_case, :production_buffer_days,
                  :batch_min_quantity, :batch_max_quantity, :fee,
                  :is_active, :shipping_type, zone_ids: [])
  end

  def find_product
    @product = Product.find params[:id]
  end

  def parse_override_quantity_params(product_params)
    quantity_param = product_params.keys.select { |param| param =~ /to_ship_.+_qty_override/ }.first
    quantity = product_params[quantity_param].to_i
    shipment_type = quantity_param.match(/to_ship_(.+)_qty_override/)[1]
    [quantity, shipment_type]
  end
end
