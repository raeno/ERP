# frozen_string_literal: true
class TasksController < ApplicationController
  include ZonesHelper

  def current
    respond_to_index
  end

  def make
    respond_to_index Zone.make_zone
  end

  def pack
    respond_to_index Zone.pack_zone
  end

  def finished
    @presenters = init_finished_task_presenters
    render 'index_finished'
  end

  def show
    task = Task.find params[:id]
    @presenter = TaskPresenter.new(task)
  end

  def edit
    task = Task.find params[:id]
    task_form = TaskForm.new(task)
    presenter = TaskPresenter.new(task, task_form)
    render 'edit', locals: { presenter: presenter }
  end

  def create
    respond_to do |format|
      inventory_item = InventoryItem.find(params[:task][:inventory_item_id])
      task = Task.new(inventory_item_id: inventory_item.id)
      task_form = TaskForm.new(task)
      result = actions[:create].call task_form, params[:task]

      format.html do
        if result.success?
          ii_presenter = InventoryItemPresenter.new(inventory_item.reload)
          render partial: 'planning/record', locals: { ii_presenter: ii_presenter }
        else
          render plain: "Cannot create task. #{result.error_message}", status: :unprocessable_entity
        end
      end
    end
  end

  def update
    task = Task.find params[:id]
    task_form = TaskForm.new(task)
    result = actions[:update].call task_form, params[:task]

    if result.success?
      redirect_to task, notice: 'Task was updated'
    else
      presenter = TaskPresenter.new(task, task_form)
      render 'edit', locals: { presenter: presenter }
    end
  end

  def destroy
    task = Task.unfinished.find params[:id]
    success_path = zone_planning_path(task.zone)
    result = actions[:destroy].call task

    if result.success?
      redirect_to success_path, notice: 'You successfully deleted the task.'
    else
      flash[:error] = "Could not delete the task. #{result.error_message}"
      redirect_to task_path(task)
    end
  end

  def start
    task = Task.pending.find params[:id]
    result = actions[:start].call task, current_user

    if request.referer.in? [make_tasks_url, pack_tasks_url, current_tasks_url]
      if result.success?
        task_presenter = TaskPresenter.new(result.entity)
        render partial: 'tasks/task_cards/card_front_in_progress', locals: { task_presenter: task_presenter }
      else
        render partial: 'shared/error', locals: { error: "Error: #{result.error_message}" }
      end
    else
      unless result.success?
        flash[:error] = "Cannot not start the task. #{result.error_message}"
      end
      redirect_to task
    end
  end

  def cancel
    task = Task.in_progress.for_user(current_user).find params[:id]
    result = actions[:cancel].call task

    if result.success?
      redirect_back fallback_location: task_path(task), notice: 'Work on the task was cancelled.'
    else
      flash[:error] = "Could not cancel the task. #{result.error_message}"
      redirect_to task_path(task)
    end
  end

  def actions
    { create:  Tasks::CreateTask.build,
      start:   Tasks::StartTask.build,
      cancel:  Tasks::CancelTask.build,
      finish:  Tasks::FinishTask.build,
      update:  Tasks::UpdateTask.build,
      destroy: Tasks::DestroyTask.build }
  end

  private

  def respond_to_index(zone = nil)
    if request.xhr?
      date_range = DateRange.new(params[:date_range])
      render partial: 'tasks/task_cards/days',
        locals: {
          daily_presenters: daily_tasks_presenters(zone, date_range),
          date_range: date_range
        }
    else
      date_range = DateRange.new(end_date: 14.days.from_now.to_date)
      render 'index_cards',
        locals: {
          daily_presenters: daily_tasks_presenters(zone, date_range),
          date_range: date_range,
          zone: zone
        }
    end
  end

  def daily_tasks_presenters(zone, date_range)
    tasks = fetch_current_tasks(zone, date_range)
    tasks_by_day = tasks.to_a.group_by(&:date)

    presenters = []
    tasks_by_day.each do |date, daily_tasks|
      presenters << DailyTasksPresenter.new(daily_tasks, date)
    end
    presenters
  end

  def fetch_current_tasks(zone, date_range)
    tasks = current_user.admin ? Task.all : current_user.tasks
    tasks = tasks.in_zone(zone.id) if zone
    tasks.unfinished.within_date_range(date_range).ordered_by_priority
      .includes(:user, :inventory_item => [:zone, :production_report, :product => :shipment_report])
  end

  def init_finished_task_presenters
    tasks = current_user.admin ? Task.all : current_user.tasks
    tasks = tasks.finished.ordered_by_finished_at.limit(500).includes(
      :user,
      :batch => [:product],
      :inventory_item => [:zone, :product]
    )
    tasks.map { |task| TaskPresenter.new(task) }
  end
end
