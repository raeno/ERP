# frozen_string_literal: true
class UnitsController < ApplicationController
  def index
    @units = Unit.all
  end

  def new
    @unit = Unit.new
  end

  def edit
    @unit = Unit.find params[:id]
  end

  def create
    result = actions[:create_unit].call unit_params

    if result.success?
      redirect_to units_path, notice: 'Unit was successfully created.'
    else
      @unit = result.entity
      render action: :new
    end
  end

  def update
    @unit = Unit.find params[:id]
    result = actions[:update_unit].call @unit, unit_params

    if result.success?
      redirect_to units_path, notice: 'Unit was successfully updated.'
    else
      render action: :edit
    end
  end

  def destroy
    @unit = Unit.find params[:id]
    result = actions[:destroy_unit].call @unit

    if result.success?
      flash[:notice] = 'Unit was deleted'
    else
      flash[:error] = "Failed to destroy unit: #{@unit.errors.full_messages.join(', ')}"
    end
    redirect_to units_path
  end

  def actions
    { create_unit: SimpleActions::Create.new(Unit),
      update_unit: SimpleActions::Update.new(Unit),
      destroy_unit: SimpleActions::Destroy.new(Unit) }
  end

  def unit_params
    params.require(:unit).permit(:name, :full_name)
  end
end
