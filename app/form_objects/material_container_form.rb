# frozen_string_literal: true

class MaterialContainerForm < Reform::Form
  property :inventory_item_id
  property :user_id
  property :quantity_in_zone_units
  property :date
  # to access the task, call #model

  validation do
    required(:user_id).filled
    required(:quantity_in_zone_units).filled(:int?, gt?: 0)
  end

  def date
    super&.to_s(:short)
  end

  def date=(string)
    date = Date.strptime(string, Date::DATE_FORMATS[:short]) if string.present?
    super(date)
  end
end
