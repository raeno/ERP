# frozen_string_literal: true
module Generic
  module ApplicationHelper
    # Ouputs percent in green or red and with an arrow
    def dynamics(percent, precision: 1)
      return unless percent
      percent = percent.round(precision)

      content_tag :span, class: "text-nowrap #{color_css_class(percent)}" do
        concat content_tag :i, '', class: arrow_css_class(percent)
        concat "&nbsp;".html_safe
        concat content_tag :span, "#{percent.abs}%"
      end
    end

    private

    def arrow_css_class(dynamics_value)
      return '' if dynamics_value.zero?
      return 'fa fa-long-arrow-up' if dynamics_value.positive?
      'fa fa-long-arrow-down' # negative
    end

    def color_css_class(dynamics_value)
      dynamics_value.positive? ? 'red' : 'green'
    end
  end
end
