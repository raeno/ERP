module Generic::CssHelper
  def bootstrap_class_for flash_type
    case flash_type.to_sym
      when :success
        'alert-success'
      when :error
        'alert-danger'
      when :alert
        'alert-warning'
      when :notice
        'alert-info'
      else
        flash_type.to_s
    end
  end

  def active_if(condition)
    condition ? 'active' : ''
  end

  def hidden_if(condition)
    condition ? 'hidden' : ''
  end
end
