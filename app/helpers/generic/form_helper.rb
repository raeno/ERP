# frozen_string_literal: true

module Generic
  module FormHelper
    def price_field(f, field, attrs = {})
      f.number_field field, attrs.merge(step: 0.01, prepend: "$")
    end

    def form_actions(f)
      content_tag :div, class: 'form-actions' do
        submit_button(f) + cancel_button
      end
    end

    def cancel_button
      link_to "Cancel", "#", class: "btn btn-default pull-right back-btn"
    end

    def submit_button(f)
      f.submit "Save", class: "btn btn-primary pull-right save"
    end
  end
end
