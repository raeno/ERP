# frozen_string_literal: true
module Generic
  module FormattingHelper
    include ActionView::Helpers::OutputSafetyHelper
    include ActionView::Helpers::NumberHelper

    # 15000 to "15,000 ML"
    def with_unit(number, unit_name, convert_to_integer: true)
      return unless number

      unless number.is_a?(String) # not formatted yet
        number = number.round if (number % 1).zero? || convert_to_integer
        number = number_with_delimiter(number)
      end

      # rubocop:disable Rails/OutputSafety
      safe_join [number, '&nbsp;'.html_safe, unit_name]
    end

    # 111222333.999 to 111,222,334
    def integer_with_delimiter(number)
      number_with_precision number, delimiter: ',', precision: 0
    end

    def duration(total_seconds, include_seconds: true)
      return unless total_seconds

      seconds = total_seconds % 60
      minutes = (total_seconds / 60) % 60
      hours   = total_seconds / (60 * 60)

      return format("%02d:%02d:%02d", hours, minutes, seconds) if include_seconds
      return format("%02d:%02d", hours, minutes)
    end

    # 0.54321 to "54.32%"
    def percent(val)
      return unless val
      "#{(val * 100).round(2)}%"
    end

    def markdown(text)
      return unless text
      sanitize(text).html_safe
    end
  end
end
