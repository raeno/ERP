# frozen_string_literal: true
module Generic
  module InPlaceEditableHelper
    # These helpers provide in-place editable links.
    # Usage example:
    # editable_field product, :production_buffer_days, input_options: { min: 0, class: 'xsmall' }

    # Common options:
    # options: { reload: true } - produces page reload after successful editing
    # options: { link_text: "text" } - the editable link text

    def editable_price(model, method, options: {}, input_options: {})
      options[:precision] = 2
      options[:prepend] = '$'
      input_options[:prepend] = '$'
      input_options[:min] = 0
      input_options[:step] = 0.01
      editable_decimal(model, method, options: options, input_options: input_options)
    end

    def editable_integer(model, method, options: {}, input_options: {})
      value = model.send(method)&.round
      options[:link_text] ||= link_text(value)
      input_options[:value] ||= value
      input_options[:step] = 1

      editable_field(model, method, options: options, input_options: input_options)
    end

    # Additional options:
    # options: { precision: 4 }
    def editable_decimal(model, method, options: {}, input_options: {})
      options[:link_text] ||= decimal_link_text(model.send(method), options[:precision])
      editable_field(model, method, options: options, input_options: input_options)
    end

    def editable_field(model, method, options: {}, input_options: {})
      link_text = options[:link_text] || link_text(model.send(method))
      link_text = options[:prepend] + link_text if options[:prepend]

      content_tag(:div, class: "js-in-place-editable", data: container_data(options)) do
        concat content_tag(:span, link_text, class: 'js-edit-link')
        concat content_tag(:div, nil, class: 'error-box')
        form = bootstrap_form_for model do |f|
          f.number_field method, input_options
        end
        concat form
      end
    end

    private

    def decimal_link_text(value, precision)
      value = value&.round(precision) if precision
      link_text(value)
    end

    def link_text(value)
      value&.to_s || "N/A"
    end

    def container_data(options)
      options[:reload] ? { reload: true } : {}
    end
  end
end
