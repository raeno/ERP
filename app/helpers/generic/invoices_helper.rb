# frozen_string_literal: true
module Generic
  module InvoicesHelper
    def invoice_status(invoice)
      invoice_status_internal(*badge_args(invoice.status))
    end

    private

    def badge_args(status)
      case status
      when :error    then ['error', 'danger', 'question']
      when :pending  then ['pending', 'info inverted', 'clock-o']
      when :paid     then ['paid', 'info', 'usd']
      when :received then ['received', 'info', 'home']
      when :complete then ['paid', 'success', 'check']
      end
    end

    def invoice_status_internal(badge_text, color_class, icon_class)
      content_tag :span, class: "label status-badge label-#{color_class}" do
        concat content_tag :i, nil, class: "fa fa-#{icon_class}"
        concat "&nbsp;".html_safe
        concat badge_text
      end
    end
  end
end
