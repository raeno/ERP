# frozen_string_literal: true

module Generic
  module LayoutHelper
    # opts: { icon: 'music' }
    def page_header(text = nil, icon: nil)
      content_tag :div, class: "row page-header" do
        content_tag :div, class: "col-lg-12" do
          if text
            content_tag :h1 do
              if icon
                concat content_tag(:i, '', class: "fa fa-#{icon}")
                concat ' '
              end
              concat text
            end
          else
            yield
          end
        end
      end
    end
  end
end
