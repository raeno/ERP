# frozen_string_literal: true
module Generic
  module LinksHelper
    # Returns a link to the model if current user is permitted to view it;
    # Returns a text otherwise.
    # Can accept a block instead of link text.
    def link_or_text(model, is_admin, link_text = nil)
      link_text ||= model.to_s
      if block_given?
        is_admin ? link_to(model) { yield } : capture { yield }
      else
        is_admin ? link_to(link_text, model) : link_text
      end
    end

    def icon_link_to(text, path, icon_type, opts = {})
      link_to path, opts do
        concat content_tag(:i, "", class: "fa fa-#{icon_type}")
        concat ' '
        concat text
      end
    end
  end
end
