# frozen_string_literal: true
module Generic
  module NavigationHelper
    def sidebar_menu_parent(text, icon, force_active = false)
      li_css_class = active_if force_active
      inner_ul_css_class = force_active ? 'in' : nil

      content_tag(:li, class: li_css_class) do
        link = link_to '#' do
          concat content_tag(:i, '', class: "fa fa-w fa-#{icon}") if icon
          concat ' ' + text + ' '
          concat content_tag(:span, '', class: "fa arrow")
        end
        inner_content = content_tag(:ul, '', class: "nav nav-second-level collapse #{inner_ul_css_class}") do
          yield
        end
        safe_join [link, inner_content]
      end
    end

    # The menu item will be active for the link_path given,
    # and also for any of the extra_active_paths.
    def sidebar_menu_item(text, icon, link_path, extra_active_paths: [])
      link_css_class = active_if current_path?(link_path, *extra_active_paths)

      content_tag(:li) do
        link_to link_path, class: link_css_class do
          concat content_tag(:i, '', class: "fa fa-w fa-#{icon}") if icon
          concat ' ' + text
        end
      end
    end

    # The tab will be active for the path given, and also for any of the extra_active_paths.
    def tab(title, path, link_options: {}, extra_active_paths: [])
      link_css_class = active_if current_path?(path, *extra_active_paths)

      content_tag :li, class: link_css_class do
        link_to title, path, link_options
      end
    end

    # Detects if the current_page path is one of the given paths.
    def current_path?(*paths)
      paths.detect { |p| current_page?(p) }
    end

    def planning_section?
      params[:controller] == 'planning'
    end

    def materials_section?
      params[:controller] == 'materials'
    end
  end
end
