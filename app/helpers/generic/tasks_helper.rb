# frozen_string_literal: true
module Generic
  module TasksHelper
    def task_in_progress_badge
      content_tag :span, 'in progress', class: "label status-badge label-info"
    end
  end
end
