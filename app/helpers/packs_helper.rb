# frozen_string_literal: true
module PacksHelper
  def page_specific_javascript_pack_tag
    packs = PacksLoader.lookup_javascript request.fullpath
    tags = packs.map do |pack|
      javascript_pack_tag pack.name, media: 'all'
    end
    tags.join("\n").html_safe
  end

  def page_specific_stylesheet_pack_tag
    packs = PacksLoader.lookup_stylesheet request.fullpath
    tags = packs.map do |pack|
      stylesheet_pack_tag pack.name, media: 'all'
    end
    tags.join("\n").html_safe
  end
end
