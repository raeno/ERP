# frozen_string_literal: true
module ZonesHelper
  def zone_label(zone)
    return nil unless zone
    content_tag :span, class: "zone-label zone-#{zone.slug}" do
      zone.name
    end
  end

  def active_if_zone(zone)
    return unless zone
    active_zone?(zone) ? 'active' : ''
  end

  def active_zone?(zone)
    path = request.fullpath

    zone.make? && path == root_path ||
      zone.ship?  && path =~ %r{#{ship_product_reports_path}} ||
      !zone.ship? && path =~ %r{#{zone_tasks_path(zone)}}
  end

  def zone_planning_path(zone)
    return '' unless zone
    send "planning_#{zone.slug}_path"
  end

  def zone_tasks_path(zone)
    send "#{zone.slug}_tasks_path"
  end

  def zone_finished_tasks_path(zone)
    send "#{zone.slug}_finished_tasks_path"
  end

  def zone_icon(zone)
    icon = if    zone.make? then 'cog'
           elsif zone.pack? then 'gift'
           elsif zone.ship? then 'truck'
           end

    content_tag(:i, '', class: "fa fa-w fa-#{icon}")
  end
end
