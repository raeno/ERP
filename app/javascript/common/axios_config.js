import axios from 'axios';
let token = document.getElementsByName('csrf-token');
if (token && token.length > 0) {
  token = token[0].getAttribute('content')
}

axios.defaults.headers.common['X-CSRF-Token'] = token
axios.defaults.headers.common['Accept'] = 'application/json'

Kisoils.Vue.prototype.$http = axios;

export default axios;
