window.Kisoils ?= {}
selectors = null
state = null

export default Kisoils.ViewToggler =
  selectors:
    toggler:         -> $(".js-view-toggler")
    togglerSelected: -> @toggler().find(".js-current")
    togglerOption:   -> @toggler().find(".js-option")
    toggledContent:  -> $(".js-toggled")

  init: ->
    selectors = @selectors
    @bindUIActions()

  bindUIActions: ->
    selectors.togglerOption().on 'click', @toggleView

  toggleView: (event) ->
    toggleClass = $(this).data('toggle')
    togglerTitle = $(this).text()

    selectors.toggledContent().addClass('hidden')
    selectors.toggledContent().filter('.' + toggleClass).removeClass('hidden')
    selectors.togglerSelected().text(togglerTitle)
    event.preventDefault()
