import jquery from 'jquery'
window.$ = window.jQuery = jquery

import 'babel-polyfill';
import Bootstrap from 'bootstrap'
import startboostrap from 'startbootstrap-sb-admin-2/dist/js/sb-admin-2'
import MetisMenu from 'metismenu'
import BootstrapDatepicker from 'bootstrap-datepicker'

import Lity from 'lity'

import Highcharts from 'highcharts'
import highchartsMore from 'highcharts-more'
highchartsMore(Highcharts)
window.Highcharts = Highcharts

import Trumbowyg from 'trumbowyg'

import FormConfirmation from '../common/components/form_confirmation.vue'

import 'bootstrap/dist/css/bootstrap'
import 'bootstrap-datepicker/dist/css/bootstrap-datepicker3.css'
import 'startbootstrap-sb-admin-2/dist/css/sb-admin-2.css'
import 'trumbowyg/dist/ui/trumbowyg.css'
import TrumbowygIcons from 'trumbowyg/dist/ui/icons.svg'
$.trumbowyg.svgPath = TrumbowygIcons

import 'jquery-countdown'


import Vue from 'vue'
import Toasted from 'vue-toasted'
import VueHighcharts from 'vue-highcharts'

Vue.config.productionTip = false
Vue.component('form-confirmation', FormConfirmation)
Vue.use(Toasted)
Vue.use(VueHighcharts)
Kisoils.Vue = Vue

Kisoils.vueBus = new Vue()
