import '../common/axios_config'

import CostOverTimeChart from '../materials/cost_over_time_chart'

const App = new Kisoils.Vue({
  data: {
    materialId: 1
  },
  components: {
    'cost-over-time-chart': CostOverTimeChart
  }
})

App.materialId = $('#materials-app').data('materialId')


if ($('#materials-app').length != 0) {
  App.$mount('#materials-app')
}
