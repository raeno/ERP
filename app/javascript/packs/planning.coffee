import NewTaskForm from '../planning/new_task_form'

appOptions =
  el: '#production-planning'
  data:
    newTaskForm: NewTaskForm
  selectors:
    inventoryItemTr: (inventoryItemId)-> $("table tr[data-ii-id='#{inventoryItemId}']")
  methods:
    updateRow: (inventoryItemId, newHtmlSnippet) ->
      theTr = this.$options.selectors.inventoryItemTr(inventoryItemId)
      indexTd = theTr.find('td.index')
      indexTd.nextAll('td').not('td.assign').remove()
      indexTd.after(newHtmlSnippet)
      theTr.fadeOut(400).fadeIn(800)
      Kisoils.Vue.toasted.success('Assigned', { duration: 1000, position: 'top-center' })

planningApp = new Kisoils.Vue(appOptions)

planningApp.$on 'row-outdated', (inventoryItemId, newHtmlSnippet) ->
  planningApp.updateRow(inventoryItemId, newHtmlSnippet)

Kisoils.PlanningApp = planningApp
