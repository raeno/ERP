import ViewToggler from '../common/modules/view_toggler'
import Product from '../products/product'
import PrimeCostHistory from '../products/prime_cost_history'
import ProductProperties from '../products/components/properties.vue'
import ProductIngredients from '../products/ingredients'

Kisoils.Vue.component('product-properties', ProductProperties)

modulesToLoad = [ ViewToggler, Product, PrimeCostHistory,
                  ProductIngredients ]

app = new Kisoils.Vue(
  el: '#product-page',
  data: {},
  mounted: ->
    module.init() for module in modulesToLoad
)

