import '../common/axios_config'
import App from '../shipping/app'

App.shipmentsUrl = $('#shipment-reports').data('shipmentsUrl')
App.reportsUrl = $('#shipment-reports').data('reportsUrl')
App.$mount("#shipment-reports");

Kisoils.ShipmentApp = App;
