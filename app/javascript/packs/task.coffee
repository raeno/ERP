import TaskTimer from '../tasks/task_timer'

modulesToLoad = [ TaskTimer ]

appOptions =
  el: '#task-details'
  mounted: ->
    module.init() for module in modulesToLoad

taskApp = new Kisoils.Vue(appOptions)
Kisoils.TaskApp = taskApp
