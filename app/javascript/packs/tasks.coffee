import TaskTimer from '../tasks/task_timer'
import ProductCard from '../tasks/product_card'
import ProductCardStartTask from '../tasks/product_card_start_task'
import RefreshCards from '../tasks/refresh_cards'

modulesToLoad = [ TaskTimer, ProductCard, ProductCardStartTask, RefreshCards ]

appOptions =
  el: '#task-cards'
  data:
    startDate: null
    endDate: null
    refreshAllowed: false

  selectors:
    dateRangePicker: -> $('.js-date-range')
    startDatepicker: -> $('.js-start-date')
    endDatepicker: -> $('.js-end-date')

  methods:
    applyDateFilter: ->
      return unless @refreshAllowed
      RefreshCards.refreshTasks()

    # This is a fix for bootstrap-datepicker integration to VueJS.
    # VueJS cannot sync data automatically when user picks a date.
    # Details: https://github.com/vuejs/vue/issues/4231
    syncDatepickers: ->
      @startDate = this.$options.selectors.startDatepicker().val()
      @endDate   = this.$options.selectors.endDatepicker().val()

    initDatepickers: ->
      this.$options.selectors.dateRangePicker().datepicker(
        format: 'M d, yyyy',
        todayHighlight: true,
        autoclose: true,
        clearBtn: true,
        keepEmptyValues: true
      ).on "changeDate", => @syncDatepickers()

      appContainer = $(this.$el)
      startDate = new Date(appContainer.data("start-date"))
      endDate   = new Date(appContainer.data("end-date"))
      # Keep it in this order: set end date first, then start date, so on initial load
      # the start date remains blank (is not autopopulated by the datepicker).
      this.$options.selectors.endDatepicker().datepicker('setDate', endDate)
      this.$options.selectors.startDatepicker().datepicker('setDate', startDate)
      # avoid multiple page refreshes when initializing startDate/endDate
      setTimeout (=> @refreshAllowed = true), 1

  watch:
    startDate: -> @applyDateFilter()
    endDate: -> @applyDateFilter()

  mounted: ->
    @initDatepickers()
    module.init() for module in modulesToLoad

taskCardsApp = new Kisoils.Vue(appOptions)

taskCardsApp.$on 'cards-refreshed', ->
  TaskTimer.init() # run timers again

Kisoils.TaskCardsApp = taskCardsApp
