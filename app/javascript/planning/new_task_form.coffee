selectors = null

export default NewTaskForm =
  inventoryItemId: null
  quantity: 0
  error: null

  selectors:
    theModal: -> $('.js-new-task-form')
    theForm:  -> @theModal().find('form')

  show: (inventoryItemId, quantity) ->
    @inventoryItemId = inventoryItemId
    @quantity = quantity
    @error = null
    @selectors.theModal().modal('show')

  submit: ->
    formData = @selectors.theForm().serialize()
    $.ajax '/tasks',
      type: 'post'
      data: formData
      dataType: 'html'
      success: (htmlSnippet) =>
        @selectors.theModal().modal('hide')
        Kisoils.PlanningApp.$emit('row-outdated', @inventoryItemId, htmlSnippet)
      error: (data) =>
        @error = data.responseText
