selectors = null

export default ProductIngredients =
  selectors:
    container: -> $('.js-edit-product-ingredients')
    addButton: -> $('.js-add-ingredient')
    saveButton: -> $('.js-save')

    remove: '.js-remove'
    removeIcon: -> $(selectors.remove)

    inventoryItem: '.js-inventory-item'
    unit: '.js-unit'
    ingredientTemplate: '.js-ingredient-template'
    inventoryItemSelect: -> $(selectors.inventoryItem)

    ingredientTrTemplate: -> $(selectors.ingredientTemplate)
    allIngredientTrs: -> $('tr.js-ingredient').not(selectors.ingredientTemplate)
    trOf: (innerElement) -> $(innerElement).parents('tr')

    productId: -> selectors.container().data('product-id')
    trInventoryItemId:   (tr) -> $(tr).find(selectors.inventoryItem).val()
    trInventoryItemName: (tr) -> $(tr).find(selectors.inventoryItem).find("option:selected").text()
    trQuantity:     (tr) -> $.trim( $(tr).find('.js-quantity').val() )

  init: ->
    selectors = @selectors
    if @isRelevantPage()
      @bindUIActions()

  isRelevantPage: ->
    return selectors.container().length > 0

  bindUIActions: ->
    selectors.container().on 'change', selectors.inventoryItem, (event) => @updateUnitInfo(event.target)
    selectors.container().on 'click', selectors.remove, (event) => @removeIngredient(event.target)
    selectors.addButton().on 'click', => @addIngredient()
    selectors.saveButton().on 'click', => @saveIngredients()

  updateUnitInfo: (inventoryItemSelect) ->
    inventoryItemSelect = $(inventoryItemSelect)
    selectedInventoryItemUnit = inventoryItemSelect.find("option:selected").data('unit') || ''
    inventoryItemTr = selectors.trOf(inventoryItemSelect)
    unitLabel = inventoryItemTr.find(selectors.unit)
    unitLabel.text(selectedInventoryItemUnit)

  addIngredient: ->
    template = selectors.ingredientTrTemplate()
    newTr = template.clone()
    newTr.removeClass('js-ingredient-template')
    template.before(newTr)

  removeIngredient: (removeIcon) ->
    removeIcon = $(removeIcon)
    inventoryItemRow = selectors.trOf(removeIcon)
    inventoryItemRow.remove()

  saveIngredients: ->
    productId = selectors.productId()

    ingredients = []
    for tr in selectors.allIngredientTrs()
      inventoryItemId = selectors.trInventoryItemId(tr)
      continue if ( inventoryItemId == '0')
      quantity = selectors.trQuantity(tr)
      if (quantity == '')
        alert "Please select quantity for " + selectors.trInventoryItemName(tr)
        return
      ingredients.push({inventory_item_id: inventoryItemId, quantity_in_small_units: quantity})

    $.ajax '/products/' + productId + '/ingredients',
      method: 'post'
      data: { ingredients: ingredients }
      dataType: 'json'
      timeout: 8000
      success: (json) ->
        window.location.href = json["redirect_to"]
      error: (result) ->
        error_message = result.responseJSON["error_message"]
        if error_message && (error_message.length > 0)
          alert "ERROR! " + error_message
        else
          alert "Sorry, but something went wrong. Please check your input and try again."
