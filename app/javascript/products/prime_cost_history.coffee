selectors = null

export default PrimeCostHistory =
  selectors:
    chartContainer: $('.js-prime-cost-history-graph')
    chartContainerId: 'prime-cost-chart-container'

  init: ->
    selectors = @selectors
    @bindUIActions()
    @loadChart()

  bindUIActions: ->

  loadChart: ->
    #dateRange = selectors.dateRangeSelect().val()
    product_id = selectors.chartContainer.data('productId')
    url = "/products/#{product_id}/prime_costs"
    $.get(url, (data) => @build_chart(data) )

  build_chart: (data)->
    Highcharts.chart(selectors.chartContainerId, {
      chart:
        type: 'line'
      title:
        text: 'COGS history'
      xAxis:
        categories: data['days']
      yAxis:
        title:
          text: 'COGS'
      plotOptions:
        line:
          dataLabels:
            enabled: true
        enableMouseTracking: false
      series: [{
        name: data['name'],
        data: data['prices'].map(Number)
      }]
    })
