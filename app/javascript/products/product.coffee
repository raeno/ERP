selectors = null
pageComponent = null

export default Product =
  selectors:
    productAvailabilitiesForm: -> $('.js-product-availabilities-form')
    internalTitleInput: -> $('input#internal-title')
    idInput: -> $('input#id')
    uploaderInit: $('.js-uploader-init')
    productInfo: $('.js-product-info')
    addAttachmentButton: $('.js-add-attachment')
    deleteAttachmentButton: $('.js-delete-attachment')
    showFullSize: $('.js-show-full-size')

  init: ->
    selectors = @selectors
    apiKey = selectors.uploaderInit.data('apiKey')
    @attachmentsHandler = new Kisoils.Models.FilestackAttachment apiKey
    @bindUIActions()


  bindUIActions: ->
    selectors.addAttachmentButton.on 'click', => @addAttachment()
    selectors.deleteAttachmentButton.on 'click', (event) => @deleteAttachment(event.target)
    selectors.showFullSize.on 'click', (event) => @showFullSizeViewer(event.target)

  productId: ->
    selectors.productInfo.data('productId')

  addAttachment: ->
    @attachmentsHandler.uploadManyFilesPopup((uploadedFiles) =>
      urls = uploadedFiles.map (file) -> file.url
      @saveAllAttachmentsToProduct(urls)
    )

  deleteAttachment: (element) ->
    attachmentId = $(element).parent().data('attachmentId')
    url = $(element).parent().data('url')
    @removeFromFilestack(url, => @deleteAttachmentForProduct(attachmentId))

  showFullSizeViewer: (element) ->
    url = $(element).data('viewerUrl')
    lity(url)

  removeFromFilestack: (url, onSuccess) ->
    @attachmentsHandler.removeFromFilestack(url,
        onSuccess,
        -> noty(text: 'Failed to remove attachment from Filestack')
    )

  deleteAttachmentForProduct: (attachmentId) ->
    deleteAttachmentUrl = "/products/#{@productId()}/attachments/#{attachmentId}"
    $.ajax(
      type: 'DELETE',
      url: deleteAttachmentUrl,
      dataType: 'json')
      .done( -> window.location.reload())
      .fail((data) ->
        noty(text: "Failed to delete attachment #{data['error']}", type: 'alert', timeout: 3000))

  saveAllAttachmentsToProduct: (urls) ->
    requests = urls.map (url) => @saveAttachmentToProduct(url)
    $.when(requests)
      .done( -> window.location.reload())
      .fail((data) ->
        noty(text: 'Failed to add attachments to product', type: 'alert', timeout: 3000))

  saveAttachmentToProduct: (url) ->
    productId = @productId()
    addAttachmentUrl = "/products/#{productId}/attachments"
    $.ajax(
      type: 'POST',
      url: addAttachmentUrl,
      data: { attachment: { url: url, product_id: productId } },
      dataType: 'json')
