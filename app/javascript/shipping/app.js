import ViewToggler from '../common/modules/view_toggler';
import ShipTab from './products_ship_tab';
import ShippingState from './product_shipping_state';

import ShipmentPanel from './components/shipment_panel.vue';
import ItemsCasesSelector from './components/items_cases_selector.vue';
import ReportsTable from './components/reports_table.vue';

const App = new Kisoils.Vue({
  data: {
    selectedWarehouse: {
      id: null,
      name: 'all'
    },
    shipmentPanelVisible: false,
    warehouseShipments: [],
    shipmentsUrl: '',
    reportsUrl: '',
    shippingState: ShippingState,
    shipmentType: 'cases',
    reports: [],
    currentPage: 0,
    totalPages: null,
    message: {
      visible: false,
      text: '',
      type: 'info'
    },
    isAdmin: false
  },
  components: {
    'shipment-panel': ShipmentPanel,
    'items-cases-selector': ItemsCasesSelector,
    'reports-table': ReportsTable
  },

  computed: {
    reportsData() {
      return {
        reports: this.warehouseReports,
        couldFetchMore: this.couldFetchMore
      };
    },
    couldFetchMore() {
      return !(this.totalPages && this.currentPage >= this.totalPages);
    },
    allocationType() {
      return `${this.shipmentType}_fba_allocation`;
    },
    warehouseReports: function() {
      if (this.selectedWarehouse.name == 'all') {
        return this.reports;
      } else {
        let self = this;
        return this.reports.filter(function(report) {
          let allocations = report[self.allocationType];
          return allocations.some(function(allocation) {
            return allocation.warehouse_name == self.selectedWarehouse.name;
          });
        });
      }
    }
  },

  watch: {
    selectedWarehouse: function(warehouse) {
      this.shipmentPanelVisible = true;
      this.fetchShipments(warehouse);
    }
  },

  mounted: function() {
    ShipTab.init(ShippingState);
  },

  methods: {
    findSelectedWarehouseAllocation(allocation) {
      return allocation.warehouse_name == this.selectedWarehouse.name;
    },
    buildShipmentEntry(product) {
      const methodName = `to_allocate_${this.shipmentType}_quantity`;
      const quantity = product[methodName];
      return {
        product_id: product.product_id,
        quantity: quantity
      };
    },
    ship: function(shipmentInfo) {
      const selected = this.reports.filter(rep => rep.selected);
      if (selected.length == 0) {
        this.showMessage('No products selected to ship!');
        return;
      }

      const hasEnoughToShip = this.hasEnoughInInventory(selected);

      if (!hasEnoughToShip) {
        this.showMessage('Some products do not have enough amount in packed inventory.', 'danger');
        return;
      }

      const shipmentEntries = selected.map(this.buildShipmentEntry);

      this.showMessage('Shipping is in progress. Please wait for a while');
      const amazonShipment = {
        fba_warehouse_id: this.selectedWarehouse.id,
        cases_required: this.shipmentType == "cases",
        name: shipmentInfo.name,
        shipment_entries: shipmentEntries
      };

      if (shipmentInfo.new) {
        this.createNewShipment({
          amazon_shipment: amazonShipment
        });
      } else {
        this.updateExistingShipment(shipmentInfo.id, {
          amazon_shipment: amazonShipment
        });
      }
    },

    createNewShipment: function(shipmentData) {
      this.$http.post('/amazon_shipments', shipmentData)
        .then((response) => {
          this.showMessage("Added shipment for background processing!");
          this.resetState();
        })
        .catch((exception) => {
          this.showMessage('Shipping fails. Check and try again');
        });
    },

    updateExistingShipment: function(id, shipmentData) {
      const url = `/amazon_shipments/${id}`;
      this.$http.put(url, shipmentData)

        .then((response) => {
          this.showMessage("Changes to shipment added for background processing!");
          this.resetState();
        })
        .catch((exception) => {
        });
    },

    resetState: function() {
      this.selectedWarehouse = {
        id: null,
        name: 'all'
      };
      this.reports.forEach(report => report.selected = false);
    },

    changeShipmentType: function(newType) {
      this.shipmentType = newType;
    },

    fetchReports: function() {
      let self = this;
      if (this.totalPages && this.currentPage >= this.totalPages) {
        return;
      }

      let url = `${this.reportsUrl}?page=${this.currentPage+1}`;
      this.$http.get(url)
        .then(response => {
          self.reports = [...self.reports, ...response.data.data];
          self.isAdmin = response.data.meta.admin;
          self.currentPage = response.data.meta.current_page;
          self.totalPages = response.data.meta.total_pages;
        })
        .catch(e => {
          self.showMessage(e);
          self.reports = [];
        });
    },

    hasEnoughInInventory: function(selectedProducts) {
      const methodName = `${this.shipmentType}_fba_allocation`;
      return selectedProducts.every(product => {
        const allocations = product[methodName];
        const warehouseAllocations = allocations.filter(this.findSelectedWarehouseAllocation);
        return warehouseAllocations.every(allocation => allocation.inventory_enough);
      });
    },

    fetchShipments: function(warehouse) {
      let self = this;
      if (warehouse.id === null) {
        return;
      }
      let url = `${this.shipmentsUrl}?filter=amazon_processing&warehouse_id=${warehouse.id}`;
      this.$http.get(url)
        .then(response => {
          self.warehouseShipments = response.data;
        })
        .catch(e => {
          self.showMessage(e);
          self.warehouseShipments = [];
        });
    },

    showMessage: function(message, type = 'info') {
      this.message.visible = true;
      this.message.text = message;
      this.message.type = type;
    }
  }
});

export default App;
