window.Kisoils ?= {}
Kisoils.States ?= {}

Kisoils.States.ProductsShippingState =

  state: {}
  subscribers: {}

  onChange: (key, fn) ->
    @subscribers[key] ?= []
    @subscribers[key].push fn

  unsubscribe: (key, fn) ->
    @subscribers[key] ?= []
    @subscribers[key] = @subscribers[key].filter((subscription) -> subscription == fn)

  fire: (key) ->
    @subscribers[key] ?= []
    for subscriber in @subscribers[key]
      subscriber.call @, @state[key]

  setState: (key, value) ->
    stateElement = {}
    stateElement[key] = value
    extend(@state, stateElement)
    @fire(key)

  getState: (key) ->
    @state[key]

  removeKey: (key) ->
    delete @state(key)
    @fire(key)

module.exports = Kisoils.States.ProductsShippingState
