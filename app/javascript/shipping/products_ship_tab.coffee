window.Kisoils ?= {}
selectors = null
state = null
Kisoils.ProductsShipTab =
  selectors:
    container: -> $('.js-products-ship')

    shipButtonPanel: -> $('.js-ship-panel')
    warehouseSelect: -> $(".js-filter .js-warehouse")
    selectedWarehouseId: -> selectors.warehouseSelect().val()
    selectedWarehouseName: -> selectors.warehouseSelect().find("option:selected").text()
    shipButton: -> @shipButtonPanel().find('.js-ship-button')
    shipmentNameInput: -> @shipButtonPanel().find('.js-shipment-name')
    shipmentName: -> @shipmentNameInput().val()

    closestTr: (element) -> $(element).closest('tr')
    productId: (trOrInnerElement) -> @closestTr(trOrInnerElement).data('product-id')

    shipmentReports: -> $('.js-shipment-reports')

    productFbaAllocationTd: (productId) ->
      $('tr[data-product-id="' + productId + '"] td.js-fba-allocation')
    allWarehouseRecords: -> $('td.js-fba-allocation .warehouse')
    warehouseRecords: (warehouseId) ->
      $('td.js-fba-allocation .warehouse[data-warehouse-id="' + warehouseId + '"]')

    allProductTrs: -> $('tbody tr')
    warehouseProductTrs: (warehouseId) -> @warehouseRecords(warehouseId).parents('tr')
    allVisibleCheckedProductCheckboxes: -> $('tbody input[type="checkbox"]:checked:visible')
    selectedProductTrs: -> @allVisibleCheckedProductCheckboxes().parents('tr')

    toShipQantityOverrideInputs:  -> $('.js-to-ship-qty-override')
    toShipQantityOverrideLinks:   -> $('.js-in-place-editable .js-to-ship-qty-override-link')
    elementsForItemsModeOnly: -> $('.js-viewmode-items')
    elementsForCasesModeOnly: -> $('.js-viewmode-cases')
    packedItemsQuantities: -> $('.js-items-quantity')
    packedCasesQuantities: -> $('.js-cases-quantity')
    refreshAllocationsButton: -> $('.js-refresh-allocations')

  init: (shippingState) ->
    selectors = @selectors
    state = shippingState

    if @isRelevantPage()
      @bindUIActions()
      @bindStateActions()
      @populateAllFbaAllocations()

  isRelevantPage: ->
    return selectors.container().length > 0

  bindUIActions: ->
    selectors.toShipQantityOverrideLinks().on 'successfulUpdate', (event, fbaAllocations) =>
      productId = selectors.productId(event.target)
      shipmentType = state.getState('shipmentType')
      @updateProductFbaAllocation(productId, fbaAllocations, shipmentType)

    selectors.container().on 'click', '.js-refresh-allocations', (event) =>
      productId = $(event.target).data('productId')
      @refreshProductAllocations(productId)

  rebindUIActions: ->
    selectors.toShipQantityOverrideLinks().off 'successfulUpdate'
    selectors.container().off 'click'
    @bindUIActions()

  bindStateActions: ->
    state.setState 'products.loading', false
    state.onChange 'shipmentType', (newShippingType) =>
      @switchProductsDisplayType newShippingType
    state.onChange 'selectedWarehouse', (warehouseId) =>
      setTimeout(=>
        @filterWarehouseProducts(warehouseId)
      , 0)
      @filterWarehouseProducts(warehouseId)

  # --- --- --- FBA Warehouse/Allocation processing --- --- ---
  filterWarehouseProducts: (warehouseId) ->
    if warehouseId == null
      selectors.allProductTrs().show()
      @populateAllFbaAllocations()
    else
      selectors.allProductTrs().hide()
      selectors.warehouseProductTrs(warehouseId).show()
      @showShipButtonPanel()
    @highlightCurrentWarehouse()

  highlightCurrentWarehouse: ->
    warehouseId = state.getState('selectedWarehouse')
    if warehouseId == undefined || warehouseId == 'all'
      selectors.allWarehouseRecords().show()
    else
      selectors.allWarehouseRecords().hide()
      selectors.warehouseRecords(warehouseId).show()

  refreshProductAllocations: (productId) ->
    @toggleAllocationsSpinner(productId)
    shipmentType = state.getState('shipmentType')
    @fetchFbaAllocations(productId, shipmentType)
      .done((allocations) =>
        @updateProductFbaAllocation(productId,allocations, shipmentType))
      .fail( ->
        noty(text: "Failed to refresh allocations for product", type: 'alert', timeout: 2000))
      .always( =>
        @toggleAllocationsSpinner(productId)
      )

  showRefreshAllocationsButton: (productId) ->
    button = selectors.productFbaAllocationTd(productId).find(selectors.refreshAllocationsButton())
    button.removeClass 'hidden'

  toggleAllocationsSpinner: (productId) ->
    spinner = selectors.productFbaAllocationTd(productId).find(selectors.refreshAllocationsButton())
    spinner.toggleClass 'fa-spin'

  fetchFbaAllocations: (productId, shipmentType) ->
    quantity = @allocationColumn(productId, shipmentType).find(selectors.toShipQantityOverrideLinks()).html()
    $.ajax({
      method: 'GET',
      dataType: 'json',
      url: "/products/#{productId}/fba_allocations?shipment_type=#{shipmentType}&quantity=#{quantity}"
    })

  # Populates all FBA allocations
  populateAllFbaAllocations: ->
    for tr in selectors.allProductTrs()
      productId = $(tr).data('product-id')
      allocationColumns = selectors.productFbaAllocationTd(productId)
      for allocationColumn in allocationColumns
        @renderAllocationData(allocationColumn)

  updateProductFbaAllocation: (productId, fbaAllocations, shipmentType) ->
    allocationColumn = @allocationColumn productId, shipmentType
    allocationColumn.data('fba-allocations', fbaAllocations)
    @renderAllocationData allocationColumn
    @showRefreshAllocationsButton productId

  allocationColumn: (productId, shipmentType) ->
    allColumns = selectors.productFbaAllocationTd(productId)
    selector = if shipmentType == 'items'
                 selectors.elementsForItemsModeOnly()
               else
                 selectors.elementsForCasesModeOnly()
    allColumns.filter(selector)

  # Displays product FBA allocation information from td's "data-fba-allocations" attribute.
  renderAllocationData: (allocationColumn) ->
    allocationColumn = $(allocationColumn)
    allocationColumn.find('.warehouse').remove()
    fbaAllocations = allocationColumn.data('fba-allocations')
    packedInventoryQuantity = parseInt(allocationColumn.data('packed-inventory-quantity'))

    for allocation in fbaAllocations
      allocation = $.parseJSON(allocation)
      quantity = allocation['quantity']
      warehouseDiv = $('<div>', {class: 'warehouse'})
      warehouseDiv.data('warehouse-id', allocation['warehouse_id'])
      warehouseDiv.attr('data-warehouse-id', allocation['warehouse_id'])
      warehouseDiv.text("#{allocation['warehouse_name']}:\xa0#{quantity}")
      if parseInt(quantity) > packedInventoryQuantity
        warehouseDiv.addClass('error-overflow')
      allocationColumn.append(warehouseDiv)
    Kisoils.ProductsShipTab.highlightCurrentWarehouse()


  # --- --- --- "Ship!" button processing --- --- ---
  showShipButtonPanel: ->
    selectors.shipButton().text("Ship Selected to " + selectors.selectedWarehouseName())
    currentDate = new Date()
    shipmentName = currentDate.getFullYear() + '/' + (currentDate.getMonth()+1).toString() +
      '/' + currentDate.getDate() + ' ' + selectors.selectedWarehouseName()
    selectors.shipmentNameInput().val(shipmentName)
    selectors.shipButtonPanel().removeClass('hidden')

  validateQuantityOverride: ->
    isValid = true
    warehouseId = parseInt(selectors.selectedWarehouseId())
    selectors.selectedProductTrs().each( ->
      productId = $(this).data('product-id')
      allocationTd = selectors.productFbaAllocationTd(productId)
      packedInventoryQuantity = parseInt(allocationTd.data('packed-inventory-quantity'))
      fbaAllocations = allocationTd.data('fba-allocations')

      for allocation in fbaAllocations
        allocation = $.parseJSON(allocation)
        if parseInt(allocation.warehouse_id) == warehouseId && parseInt(allocation.quantity) > packedInventoryQuantity
          isValid = false

    )
    return isValid

  switchProductsDisplayType: (shipmentType) ->
    selectors.elementsForCasesModeOnly().toggleClass('hidden', shipmentType == 'items')
    selectors.packedCasesQuantities().toggleClass('hidden', shipmentType == 'items')
    selectors.elementsForItemsModeOnly().toggleClass('hidden', shipmentType == 'cases')
    selectors.packedItemsQuantities().toggleClass('hidden', shipmentType == 'cases')

  shipSelectedProducts: (e) ->
    e.preventDefault()

    if Kisoils.ProductsShipTab.validateQuantityOverride() == false
      Kisoils.Messagebox.show("FBA allocation should be less than packed inventory quantity! (marked as red color)")
      return

    shipmentType = state.getState('shipmentType')

    productsToShip = selectors.selectedProductTrs().map(->
      product_id: $(this).data('product-id'),
      quantity: $(this).find("#product_to_ship_#{shipmentType}_qty_override").val(),
    ).get()

    warehouseId = selectors.selectedWarehouseId()
    shipmentName = selectors.shipmentName()

    if productsToShip.length == 0
      Kisoils.Messagebox.show("No products selected to ship!")
    else
      $.ajax({
        method: 'POST',
        dataType: 'JSON',
        url: '/batches/create_with_shipment',
        data: {productsToShip: JSON.stringify(productsToShip), shipmentName: shipmentName, warehouseId: warehouseId, shipmentType: shipmentType},
        beforeSend: ->
          Kisoils.Messagebox.show("Shipping is in progress. Please wait for a while!")
        success: (result) ->
          Kisoils.Messagebox.show("Added shipment for background processing!")
          location.href = "/product_reports/ship"
        error: (data) ->
          if data.responseJSON.error.code == 'err_overflow_inventory'
            Kisoils.Messagebox.show("Shipping fails. Overflow inventory quantity. Reloading!")
            location.reload()
            return
          Kisoils.Messagebox.show("Shipping fails. Check and try again!") })

module.exports = Kisoils.ProductsShipTab
