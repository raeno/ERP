selectors = null

export default ProductCard =
  selectors:
    container: -> $(".js-task-cards")

    flipTogglersSelector: ".js-product-card .js-flip-icon"

    cardOf: (innerElement) -> $(innerElement).parents('.js-product-card')
    cardFrontOf: (innerElement) -> selectors.cardOf(innerElement).find('.js-product-card-front')
    cardBackOf:  (innerElement) -> selectors.cardOf(innerElement).find('.js-product-card-back')

  init: ->
    selectors = @selectors
    @bindUIActions()

  bindUIActions: ->
    selectors.container().on 'click', selectors.flipTogglersSelector, (event) => @toggleCard(event)

  toggleCard: (event) ->
    selectors.cardFrontOf(event.target).toggle()
    selectors.cardBackOf(event.target).toggle()
