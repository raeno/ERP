selectors = null

export default ProductCardStartTask =
  selectors:
    container: -> $(".js-task-cards")

    startButtonSelector: ".js-product-card .js-start-task"

    cardOf: (innerElement) -> $(innerElement).parents('.js-product-card')
    pendingTaskBlock: (card) -> $(card).find('.js-task-pending')

  init: ->
    selectors = @selectors
    @bindUIActions()

  bindUIActions: ->
    selectors.container().on 'click', selectors.startButtonSelector, (event) => @startTask(event)

  startTask: (event) ->
    event.preventDefault()
    theCard = selectors.cardOf(event.target)
    task_id = theCard.data('task-id')
    path = "/tasks/" + task_id + "/start"

    $.ajax path,
      type: 'put'
      dataType: 'html',
      success: (data) ->
        selectors.pendingTaskBlock(theCard).replaceWith(data)
        theCard.trigger('timerAdded')
