selectors = null

export default RefreshCards =
  selectors:
    container: -> $('.js-task-cards')

  init: ->
    selectors = @selectors
    setInterval(@refreshTasks.bind(this), 300000) # refresh every 5 min

  refreshTasks: ->
    startDate = Kisoils.TaskCardsApp.startDate
    endDate = Kisoils.TaskCardsApp.endDate
    daysContainer = selectors.container()
    $.ajax window.location.pathname,
      type: 'get'
      dataType: 'html',
      data: { date_range: { start_date: startDate, end_date: endDate } }
      success: (data) ->
        daysContainer.html(data)
        Kisoils.TaskCardsApp.$emit('cards-refreshed')
