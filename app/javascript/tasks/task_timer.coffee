selectors = null

export default TaskTimer =
  selectors:
    timerContainer: -> $(".js-timer")
    innerTimer: (container) -> $(container).find(".js-timer")
    productCard: -> $('.js-product-card')

  init: ->
    selectors = @selectors
    self = this
    selectors.timerContainer().each ->
      self.startTimer($(this))
    selectors.productCard().on 'timerAdded', ->
      self.startTimer(selectors.innerTimer(this))

  startTimer: (timerDiv) ->
    startSeconds = timerDiv.data('seconds-left')
    targetTime = (new Date).getTime() + startSeconds * 1000

    timerDiv.countdown(targetTime, elapse: true).on 'update.countdown', (event) ->
      if event.elapsed
        template = '<span class="overdue">-%H:%M:%S</span>'
      else
        template = '<span>%H:%M:%S</span>'
      $(this).html event.strftime(template)
