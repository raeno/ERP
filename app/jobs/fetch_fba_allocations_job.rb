# frozen_string_literal: true
class FetchFbaAllocationsJob < ApplicationJob
  queue_as :default

  def perform
    address = Setting.instance.shipment_address.to_h
    AmazonFbaAllocationsFetcher.build(address).run
  end
end
