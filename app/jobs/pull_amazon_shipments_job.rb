# frozen_string_literal: true
class PullAmazonShipmentsJob < ApplicationJob
  queue_as :default

  # fetches active Amazon shipments for last 6 months
  def perform(days_to_fetch: 60)
    start_date = days_to_fetch.days.ago
    shipments_fetcher.call start_date: start_date
  end

  def shipments_fetcher
    AmazonShipmentsFetcher.build
  end
end
