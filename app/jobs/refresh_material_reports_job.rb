# frozen_string_literal: true
class RefreshMaterialReportsJob < ApplicationJob
  queue_as :default

  def perform(product)
    refresh_reports_action.call product
  end

  def refresh_reports_action
    @action ||= Materials::RefreshMaterialReports.build
  end
end
