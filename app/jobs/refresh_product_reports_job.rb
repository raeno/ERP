# frozen_string_literal: true
class RefreshProductReportsJob < ApplicationJob
  queue_as :default

  def perform(product)
    refresh_reports_action.call product
  end

  def refresh_reports_action
    @action ||= Products::RefreshProductReports.build
  end
end
