# frozen_string_literal: true
class RefreshProductionPlanReportsJob < ApplicationJob
  queue_as :default

  def perform(inventory_item)
    if inventories_changed_recently?
      # re-schedule job until we sure that all inventories weren't changed
      # for some time ( 5 seconds )
      self.class.set(wait: 1.second).perform_later inventory_item
    else
      refresh_dependent_reports inventory_item
    end
  end

  private

  def refresh_dependent_reports(inventory_item)
    # we schedule a job to refresh each product reports
    # due to uniquenes limitation enabled, sidekiq will allow only
    # one job with same name and arguments to be scheduled
    dependent_inventoriables(inventory_item).each do |inventoriable|
      if inventoriable.is_a?(Product)
        RefreshProductReportsJob.perform_later inventoriable
      else
        RefreshMaterialReportsJob.perform_later inventoriable
      end
    end
  end

  def dependent_inventoriables(inventory_item)
    collection = []

    # self
    collection << inventory_item.inventoriable

    # "parent" products - products for materials, gift boxes for products
    collection += inventory_item.products_as_ingredient

    # "child" components for products
    collection += inventory_item.all_inventoriable_components.map(&:inventoriable)
  end

  def inventories_changed_recently?
    Inventory.where(updated_at: 5.seconds.ago..Time.zone.now).count.positive?
  end
end
