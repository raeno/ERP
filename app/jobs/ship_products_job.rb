# frozen_string_literal: true
class ShipProductsJob < ApplicationJob
  queue_as :default

  def perform(shipment)
    ship_action.call shipment

    # refresh immediately shipments statuses on Amazon for last two days
    refresh_shipments_job.perform_later days_to_fetch: 2
  end

  def ship_action
    @ship_action ||= Products::ShipProducts.build
  end

  def refresh_shipments_job
    @refresh_shipments_job ||= PullAmazonShipmentsJob
  end
end
