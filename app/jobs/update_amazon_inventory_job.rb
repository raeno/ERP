# frozen_string_literal: true
class UpdateAmazonInventoryJob < ApplicationJob
  queue_as :default

  def perform
    AmazonInventoryUpdater.build.run
  end
end
