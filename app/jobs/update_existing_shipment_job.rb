# frozen_string_literal: true
class UpdateExistingShipmentJob < ApplicationJob
  queue_as :default

  def perform(amazon_shipment)
    update_shipment_action.call amazon_shipment
    refresh_shipments_job.perform_later days_to_fetch: 2
  end

  def update_shipment_action
    @update_shipment_action ||= Products::AddProductsToShipment.build
  end

  def refresh_shipments_job
    @refresh_shipments_job ||= PullAmazonShipmentsJob
  end
end
