# frozen_string_literal: true
module Amazon
  class InboundShipmentApi
    include Logging
    include WithThrottling

    ACTIVE_SHIPMENT_STATUSES = %w( WORKING SHIPPED IN_TRANSIT DELIVERED CHECKED_IN RECEIVING CLOSED CANCELLED DELETED ERROR).freeze
    DEFAULT_SHIPMENTS_RANGE = 24.months.ago

    def self.build(amazon_config, options = {})
      inbound_shipment_client = MWS.fulfillment_inbound_shipment(amazon_config)
      new(inbound_shipment_client, options)
    end

    def initialize(inbound_shipment_client, options = {})
      @inbound_shipment_client = inbound_shipment_client
      @ship_from_address = options.fetch(:ship_from_address, {})
    end

    def get_prep_instructions_for_sku(*skus)
      response = inbound_shipment_client.get_prep_instructions_for_sku('US', skus)
      response.parse
    rescue => e
      log(e.message, :error)
      nil
    end

    def create_shipment_plan(items)
      shipment_plans = inbound_shipment_client.create_inbound_shipment_plan(ship_from_address, items)
      shipment_plans.parse
    rescue => e
      log(e.message, :error)
      nil
    end

    def create_shipment(shipment_id, header, items_to_ship)
      response = inbound_shipment_client.create_inbound_shipment(shipment_id, header, inbound_shipment_items: items_to_ship)
      ShipmentApiResult.new response: response, success: true
    rescue => e
      ShipmentApiResult.new response: e.response, success: false, errors: [e.message, e.response.body]
    end

    def update_shipment(shipment_id, header, items_to_ship)
      response = inbound_shipment_client.update_inbound_shipment(shipment_id, header, inbound_shipment_items: items_to_ship)
      ShipmentApiResult.new(response: response, success: true)
    rescue => e
      ShipmentApiResult.new(response: e.response, success: false, errors: [e.message, e.response.body])
    end

    def fetch_shipments(statuses: ACTIVE_SHIPMENT_STATUSES,
                        date_from: DEFAULT_SHIPMENTS_RANGE,
                        date_to: Time.zone.now)
      params = {
        shipment_status_list: statuses,
        last_updated_before: date_to.to_s(:iso8601),
        last_updated_after: date_from.to_s(:iso8601)
      }
      shipments_response = inbound_shipment_client.list_inbound_shipments(params).parse
      shipments = [shipments_response]

      while shipments_response['NextToken']
        throttle 30, per: :minute do
          token = shipments_response['NextToken']
          shipments_response = inbound_shipment_client.list_inbound_shipments_by_next_token(token).parse
          shipments << shipments_response
        end
      end
      shipments
    end

    private

    attr_reader :inbound_shipment_client, :ship_from_address
  end
end
