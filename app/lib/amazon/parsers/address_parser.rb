# frozen_string_literal: true
module Amazon
  module Parsers
    class AddressParser
      attr_reader :address_hash

      ADDRESS_KEYS = [:name, :address_line_1, :city, :state_or_province_code, :postal_code, :country_code].freeze

      def parse(address_hash)
        params = ADDRESS_KEYS.map do |key|
          [key, send(key, address_hash)]
        end.to_h
        Structs::Address.new params
      end

      private

      def address_line_1(address_hash)
        address_hash.dig('AddressLine1')
      end

      (ADDRESS_KEYS - [:address_line_1]).each do |name|
        define_method(name) do |address_hash|
          address_hash.dig name.to_s.camelcase
        end
      end
    end
  end
end
