# frozen_string_literal: true
module Amazon
  module Parsers
    class PrepInstructionsParser
      def self.build
        new
      end

      def parse(instructions_response)
        return {} unless instructions_response
        prep_instructions = instructions_response.dig('SKUPrepInstructionsList', 'SKUPrepInstructions')
        return {} unless prep_instructions

        instructions_array = Array.wrap(prep_instructions).map do |instruction|
          sku = instruction.dig 'SellerSKU'
          instructions = prepare_instructions_for_shipment instruction
          [sku, instructions]
        end
        instructions_array.to_h
      end

      private

      def prepare_instructions_for_shipment(instructions_body)
        instruction_data = instructions_body.dig 'PrepInstructionList', 'PrepInstruction'
        result = {}
        Array.wrap(instruction_data).each_with_index do |instruction, index|
          result["PrepDetails.#{index + 1}.PrepInstruction"] = instruction
          result["PrepDetails.#{index + 1}.PrepOwner"] = 'SELLER'
        end
        result
      end
    end
  end
end
