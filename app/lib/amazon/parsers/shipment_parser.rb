# frozen_string_literal: true
module Amazon
  module Parsers
    class ShipmentParser
      KEYS_MAPPING = {
        shipment_id: 'ShipmentId',
        cases_required: 'AreCasesRequired',
        name: 'ShipmentName',
        fba_warehouse: 'DestinationFulfillmentCenterId',
        label_prep_type: 'LabelPrepType',
        status: 'ShipmentStatus',
        box_content_source: 'BoxContentSource'
      }.freeze

      def self.build
        new
      end

      def parse(shipment_data)
        shipments = Array.wrap(shipment_data).flat_map do |data|
          data.dig('ShipmentData', 'member')
        end

        shipments.compact.map { |shipment| parse_shipment(shipment) }
      end

      private

      def parse_shipment(shipment)
        params = {}
        KEYS_MAPPING.each do |key, value|
          params[key] = shipment.dig value
        end
        params
      end
    end
  end
end
