# frozen_string_literal: true
module Amazon
  module Parsers
    class ShipmentPlansParser
      attr_reader :address_parser, :prep_instructions_parser

      def self.build
        address_parser = Amazon::Parsers::AddressParser.new
        new(address_parser)
      end

      def initialize(address_parser)
        @address_parser = address_parser
      end

      def parse(response)
        return ShipmentPlansResult.new([], false, ['Invalid response in creating shipment plan']) unless response

        plans = response.dig 'InboundShipmentPlans', 'member'
        result = Array.wrap(plans).map do |plan_body|
          build_shipment_plan(plan_body)
        end

        ShipmentPlansResult.new(result, true, [])
      rescue => e
        ShipmentPlansResult.new(result, false, [e.message])
      end

      private

      def build_shipment_plan(plan)
        shipment_plan = OpenStruct.new
        shipment_plan.destination_fulfillment_center_id = plan.dig 'DestinationFulfillmentCenterId'
        shipment_plan.label_prep_type = plan.dig 'LabelPrepType'
        shipment_plan.shipment_id = plan.dig 'ShipmentId'
        shipment_plan.ship_to_address = parse_address plan

        items = plan.dig 'Items', 'member'
        shipment_plan.items = parse_items_on_shipment_plan items

        shipment_plan
      end

      def parse_items_on_shipment_plan(items)
        return [] unless items

        Array.wrap(items).map do |item|
          prep_details = item.dig 'PrepDetailsList', 'PrepDetails'

          OpenStruct.new(
            fulfillment_network_sku: item.dig('FulfillmentNetworkSKU'),
            quantity: item.dig('Quantity'),
            seller_sku: item.dig('SellerSKU'),
            prep_details_list: parse_preparation_details(prep_details)
          )
        end
      end

      def parse_preparation_details(preparation_details)
        return [] unless preparation_details

        Array.wrap(preparation_details).map do |details|
          owner = details.dig 'PrepOwner'
          instruction = details.dig 'PrepInstruction'
          OpenStruct.new prep_owner: owner, prep_instruction: instruction
        end
      end

      def parse_address(response)
        address_response = response.dig 'ShipToAddress'
        raise ArgumentError, 'Invalid shipping address' unless address_response
        address_parser.parse(address_response)
      end
    end
  end
end
