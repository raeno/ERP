# frozen_string_literal: true
module Amazon
  module Structs
    class Address < BaseStruct
      ATTRIBUTES = [:name, :address_line_1, :city, :state_or_province_code,
                    :postal_code, :country_code].freeze
    end
  end
end
