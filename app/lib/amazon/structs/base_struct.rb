# frozen_string_literal: true
module Amazon
  module Structs
    class BaseStruct < OpenStruct
      ATTRIBUTES = [].freeze

      def initialize(params = {})
        valid_params = params.slice(*self.class::ATTRIBUTES)
        super(valid_params)
      end

      def attributes
        self.class::ATTRIBUTES
      end

      def to_h
        attributes.map { |method| [method, send(method)] }
                  .reject { |el| el[1].nil? }
                  .to_h
      end
    end
  end
end
