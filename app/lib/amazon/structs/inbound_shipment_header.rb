# frozen_string_literal: true
module Amazon
  module Structs
    class InboundShipmentHeader < BaseStruct
      ATTRIBUTES = [:shipment_name, :ship_from_address, :destination_fulfillment_center_id, :shipment_status,
                    :label_prep_type, :are_cases_required, :box_content_source].freeze

      def self.build(amazon_shipment, shipment_address)
        params = { shipment_name: amazon_shipment.name,
                   ship_from_address: shipment_address,
                   shipment_status: 'WORKING',
                   label_prep_type: 'SELLER_LABEL' }
        params[:destination_fulfillment_center_id] =  amazon_shipment.fba_warehouse_name
        params[:are_cases_required] = amazon_shipment.cases_required?
        new params
      end
    end
  end
end
