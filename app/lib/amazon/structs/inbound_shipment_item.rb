# frozen_string_literal: true
module Amazon
  module Structs
    class InboundShipmentItem
      ATTRIBUTES = [:seller_sku, :quantity_shipped, :prep_details_list].freeze
    end
  end
end
