# frozen_string_literal: true
module Amazon
  module Structs
    class InboundShipmentPlanItem < BaseStruct
      ATTRIBUTES = [:seller_sku, :quantity, :quantity_in_case, :prep_details_list].freeze
    end
  end
end
