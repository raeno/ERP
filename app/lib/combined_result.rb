# frozen_string_literal: true
class CombinedResult < Result
  attr_reader :results

  def initialize(*results)
    @results = results.compact
  end

  def success?
    results.all? { |res| res && res.success? }
  end

  def entity
    results.find { |res| !res.entity.nil? }&.entity
  end

  def entities
    results.map(&:entity)
  end

  def errors
    results.flat_map(&:errors).compact
  end
end
