# frozen_string_literal: true
class Context < OpenStruct
  def empty?
    eql?(OpenStruct.new)
  end
end
