# frozen_string_literal: true
# Same as OpenStruct but doesn't have top-level 'table' key
# when dumped to json
class DataStruct < OpenStruct
  def as_json(options = {})
    @table.as_json(options)
  end
end
