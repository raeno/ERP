# frozen_string_literal: true
class EntityResult < Result
  attr_reader :entity, :errors

  def initialize(entity:, success:, errors: [])
    @entity = entity
    @success = success
    @errors = errors
  end

  def success?
    success
  end

  def self.nil_entity_error
    new(entity: nil, success: false, errors: ['Passed entity should not be nil'])
  end

  private

  attr_reader :success
end
