# frozen_string_literal: true
class FilestackApi
  BASE_URL = 'https://www.filestackapi.com/api'

  attr_reader :url

  def initialize(url)
    @url = url
  end

  def preview_url(height: 150)
    return nil unless url.present?
    "#{BASE_URL}/file/#{file_handler}/convert?format=jpg&h=#{height}"
  end

  def viewer_url
    return nil unless url.present?
    "#{BASE_URL}/preview/#{file_handler}"
  end

  def self.api_key
    ENV['FILESTACK_API_KEY']
  end

  private

  def file_handler
    return nil unless url.present?
    url.match(/\w+$/)[0]
  end
end
