# frozen_string_literal: true
module MwsStructs

  Item = Struct.new(:seller_sku, :quantity, :prep_details_list)
  ItemInCase = Struct.new(:seller_sku, :quantity, :quantity_in_case, :prep_details_list)

  ItemShipped = Struct.new(:seller_sku, :quantity_shipped, :prep_details_list)

end
