class PacksLoader

 def self.lookup_javascript(route)
    packs = lookup route
    packs.select(&:javascript?)
  end

  def self.lookup_stylesheet(route)
    packs = lookup route
    packs.select(&:stylesheet?)
  end

  def self.lookup(route)
    matched_routes = matchers.select { |matcher, pack| route =~ matcher }
    matched_routes.values
  end

  def self.matchers
    @matchers ||= {
      /products/ => PacksRegistry.products,
      /product_reports\/ship/ => PacksRegistry.shipping,
      /planning\/(make|pack)/ => PacksRegistry.planning,
      /tasks\/(make|pack|current)/ => PacksRegistry.tasks,
      /tasks\/\d/ => PacksRegistry.task,
      /materials/ => PacksRegistry.material
    }
  end
end
