# frozen_string_literal: true
class PacksRegistry
  class AssetsPack
    attr_reader :name, :js, :css

    def initialize(name, js: true, css: true)
      @name = name
      @js = js
      @css = css
    end

    def stylesheet?
      css
    end

    def javascript?
      js
    end
  end

  def self.method_missing(method_name)
    registry[method_name] || super
  end

  def self.respond_to_missing?(method_name, include_private = false)
    registry[method_name] || super
  end

  def self.registry
    @registry ||= OpenStruct.new(
      shipping: AssetsPack.new('shipping', css: false),
      products: AssetsPack.new('products', css: false),
      planning: AssetsPack.new('planning', css: false),
      tasks:    AssetsPack.new('tasks', css: false),
      task:     AssetsPack.new('task', css: false),
      material: AssetsPack.new('materials', css: false)
    )
  end
end
