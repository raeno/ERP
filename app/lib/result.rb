# frozen_string_literal: true
class Result
  def success?
    defined?(@success) ? @success : false
  end

  def failure?
    !success?
  end

  def entity
    nil
  end

  def errors
    []
  end

  def error=(new_value)
    errors << new_value
  end

  def results
    [self]
  end

  def error_message
    errors.join '. '
  end

  def to_s
    { success?: success?, entity: entity, errors: errors.to_s }.to_s
  end
end
