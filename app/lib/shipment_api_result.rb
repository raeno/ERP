# frozen_string_literal: true
class ShipmentApiResult < Result
  attr_reader :response

  def initialize(response:, success:, errors: nil)
    @response = response
    @success = success
    @errors = Array.wrap(errors)
  end

  def parsed_response
    response&.parse
  end

  def errors
    return [] if @success
    amazon_error = parsed_response&.fetch('Message', nil)
    (@errors << amazon_error).compact
  end
end
