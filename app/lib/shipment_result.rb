class ShipmentResult < Result
  attr_reader :job, :errors, :shipment_name

  def initialize(job:, shipment_name:, success:, errors:)
    @job = job
    @success = success
    @shipment_name = shipment_name
    @errors = errors
  end

  def success?
    success
  end

  def job_id
    job&.job_id
  end

  private

  attr_reader :success
end
