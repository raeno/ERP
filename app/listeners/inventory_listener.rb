# frozen_string_literal: true
# listens to events related to products and their amount in inventory
class InventoryListener
  attr_reader :refresh_reports

  def self.build
    new RefreshProductionPlanReportsJob
  end

  def initialize(refresh_reports)
    @refresh_reports = refresh_reports
  end

  def inventory_quantity_changed(inventory, _)
    refresh_reports.perform_later inventory.inventory_item
  end
end
