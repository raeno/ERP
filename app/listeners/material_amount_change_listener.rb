# frozen_string_literal: true
class MaterialAmountChangeListener
  attr_reader :batch, :invoice

  def initialize(batch: nil, invoice: nil)
    @batch = batch
    @invoice = invoice
  end

  def inventory_quantity_changed(inventory, delta)
    inventory_item = inventory&.inventory_item
    return unless inventory_item&.material?

    material = inventory.inventory_item.material
    material_spent = -delta # delta comes as negative when spending
    recalculate_amount_left.call material, material_spent, batch, inventory.quantity
  end

  def recalculate_amount_left
    MaterialContainers::RecalculateAmountLeft.build
  end

  private

  def ordered_in_invoice_container(invoice, material)
    return nil unless invoice
    invoice.material_containers.where(material: material).first
  end
end
