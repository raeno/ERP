# frozen_string_literal: true
# == Schema Information
#
# Table name: amazon_inventories
#
#  id              :integer          not null, primary key
#  fulfillable     :integer          default(0)
#  reserved        :integer          default(0)
#  inbound_working :integer          default(0)
#  inbound_shipped :integer          default(0)
#  product_id      :integer          not null
#
# Foreign Keys
#
#  fk_rails_...  (product_id => products.id)
#

class AmazonInventory < ApplicationRecord
  belongs_to :product
  has_many :amazon_inventory_histories, dependent: :destroy

  def total_amazon_inventory
    inbound_shipped + fulfillable
  end

  def days_of_cover
    return 0.0 if total_amazon_inventory.zero?
    ProductAmount.new(total_amazon_inventory).days(reserved)
  end

  def days_to_cover
    days = product.production_buffer_days - days_of_cover
    [0, days].max
  end

  def coverage_ratio
    buffer = product.production_buffer_days

    # we don't have to send to Amazon anything if no demand on the product or
    # no need to have buffer
    return 100 if buffer.zero? || reserved.zero?
    (100 * days_of_cover / buffer).to_i
  end
end
