# frozen_string_literal: true
# == Schema Information
#
# Table name: amazon_inventory_histories
#
#  id                  :integer          not null, primary key
#  fulfillable         :integer
#  reserved            :integer
#  inbound_working     :integer
#  inbound_shipped     :integer
#  reported_at         :datetime
#  amazon_inventory_id :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class AmazonInventoryHistory < ApplicationRecord
  belongs_to :amazon_inventory

  def self.smooth_reserved(amazon_inventory, new_value)
    histories = where(amazon_inventory: amazon_inventory).last(2)
    reserved_values = histories.map(&:reserved) << new_value.to_f
    reserved_values.sum / reserved_values.size.to_f
  end

  def self.old_histories
    old_history_ids = all.group(:amazon_inventory_id)
                         .having('count(id) > 2')
                         .pluck('array_agg(id)')
                         .flat_map { |ids_array| ids_array[0..-3] }
    where(id: old_history_ids)
  end
end
