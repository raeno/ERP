# frozen_string_literal: true
# == Schema Information
#
# Table name: amazon_shipments
#
#  id               :integer          not null, primary key
#  shipment_id      :text
#  name             :text
#  status           :string
#  cases_required   :boolean
#  fba_warehouse_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  user_id          :integer
#

class AmazonShipment < ApplicationRecord
  belongs_to :fba_warehouse
  belongs_to :user
  has_many :shipment_entries, dependent: :destroy

  delegate :name, to: :fba_warehouse, prefix: true

  scope :processing, -> { where(status: %w(INITIALIZED AMAZON_SYNCED)) }
  scope :processing_by_amazon, -> { where(status: 'WORKING') }

  def self.filtered(filter)
    case filter
    when 'processing'
      self.processing
    when 'amazon_processing'
      self.processing_by_amazon
    else
      all
    end
  end

  def initialized?
    status == 'INITIALIZED'
  end

  def synced?
    status == 'AMAZON_SYNCED'
  end

  def was_synced_with_amazon?
    status_changed? && synced?
  end
end
