# frozen_string_literal: true
# == Schema Information
#
# Table name: attachments
#
#  id         :integer          not null, primary key
#  url        :text             not null
#  mimetype   :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Attachment < ApplicationRecord
  has_many :material_attachments, dependent: :destroy
  has_many :product_attachments, dependent: :destroy

  def preview_url
    @preview_url ||= filestack_api.preview_url
  end

  def viewer_url
    @viewer_url ||= filestack_api.viewer_url
  end

  def pdf?
    return false unless mimetype
    mimetype.match(/pdf/)
  end

  def image?
    return false unless mimetype
    mimetype.match(/image/)
  end

  private

  def filestack_api
    FilestackApi.new(url)
  end
end
