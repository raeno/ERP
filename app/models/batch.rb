# frozen_string_literal: true
# == Schema Information
#
# Table name: batches
#
#  id                 :integer          not null, primary key
#  product_id         :integer          not null
#  quantity           :integer          not null
#  notes              :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  completed_on       :date
#  zone_id            :integer          not null
#  auxillary_info     :hstore
#  task_id            :integer
#  amazon_shipment_id :integer
#

# The main purpose of a batch (vs task) is to keep record of product inventory changes.
class Batch < ApplicationRecord
  self.per_page = 50

  belongs_to :product
  belongs_to :zone
  belongs_to :amazon_shipment
  belongs_to :task

  has_and_belongs_to_many :users
  has_one :product_cogs_snapshot, dependent: :destroy
  has_many :material_transactions, dependent: :destroy

  store_accessor :auxillary_info

  scope :fresh_first, -> { order(completed_on: :desc, created_at: :desc) }

  validates :quantity, presence: true
  validates :user_ids, presence: { message: " must be selected" }
  validates :task_id, presence: true, unless: :ship_zone?

  scope :for_interval, ->(start_date, end_date) { where(completed_on: start_date..end_date) }
  scope :ordered_by_completion, -> { order(completed_on: :asc) }

  delegate :items_per_case, to: :product, prefix: true

  def make_zone?
    zone&.make?
  end

  def pack_zone?
    zone&.pack?
  end

  def ship_zone?
    zone&.ship?
  end

  def shipment_id
    amazon_shipment_id || auxillary_info&.fetch('shipment_id', nil)
  end

  def prime_cost
    product_cogs_snapshot&.cogs&.round(3) || 'N/A'
  end

  def case_quantity
    quantity / product_items_per_case
  end
end
