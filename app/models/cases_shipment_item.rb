class CasesShipmentItem < ShipmentItem
  def shipment_type
    'cases'
  end

  def shipment_in_cases?
    true
  end

  def quantity_in_case
    product.items_per_case
  end

  def to_h
    quantity_in_items = quantity * quantity_in_case
    { seller_sku: seller_sku, quantity: quantity_in_items, quantity_in_case: quantity_in_case }
  end
end
