# frozen_string_literal: true
module Products
  module Financials
    extend ActiveSupport::Concern

    included do
      has_many :product_cogs_snapshots, dependent: :destroy
    end

    def profit
      return unless selling_price
      selling_price - fee - prime_cost
    end

    def gross_margin
      return unless selling_price&.nonzero? && profit
      (profit * 100 / selling_price).round(2)
    end

    def roi
      return unless prime_cost&.nonzero? && profit
      (profit / prime_cost).round(2)
    end

    def labour_cost
      Rails.cache.fetch ['products', self, 'labour_cost'], expires: 1.hour do
        inventory_items.map(&:labour_cost).compact.sum
      end
    end

    def make_labour_cost
      make_inventory_item&.labour_cost
    end

    def pack_labour_cost
      pack_inventory_item&.labour_cost
    end

    def prime_cost
      Rails.cache.fetch ingredients_cache_key('prime_cost') do
        ingredients.map(&:cost).sum
      end
    end
    alias :cogs :prime_cost

    # For pack zone, includes pack zone ingredients only.
    def zone_cogs(zone)
      Rails.cache.fetch ingredients_cache_key(zone, 'zone_cogs') do
        ingredients_for_zone(zone).map(&:cost).sum
      end
    end

    # For pack zone, includes pack and all previous zones' (make) ingredients.
    def cumulative_cogs(zone)
      Rails.cache.fetch ingredients_cache_key(zone, 'cumulative_cogs') do
        ingredients_up_to_zone(zone).map(&:cost).sum
      end
    end
  end
end
