# frozen_string_literal: true

# Product is an inventoriable that can be an ingredient of another product.
# Ingredients can (in general) be input in tiny units of measure (e.g. ML) -
# not the main ones (Lbs). Each inventoriable knows how to convert back and forth
# between the units.
# For products that are always in 'Each', this conversion is trivial.
# This module implements the required inventoriable methods for ingredients to work.

module Products
  module UnitsOfMeasure
    extend ActiveSupport::Concern

    def main_unit_name
      Unit.each_piece.name
    end
    alias ingredient_unit_name main_unit_name

    def ingredient_to_main_units(amount)
      amount
    end
  end
end
