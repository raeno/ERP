# frozen_string_literal: true
module ReceivingStatus
  extend ActiveSupport::Concern

  included do
    scope :received, -> { where.not(received_date: nil) }
  end

  def received?
    received_date.present?
  end

  def become_received
    self.received_date = Time.zone.today
  end
end
