# frozen_string_literal: true

module Tasks
  module StatusAndDuration
    extend ActiveSupport::Concern

    included do
      scope :pending,     -> { where(started_at: nil) }
      scope :in_progress, -> { where.not(started_at: nil).where(finished_at: nil) }
      scope :finished,    -> { where.not(finished_at: nil) }
      scope :unfinished,  -> { where(finished_at: nil) } # pending and in progress
    end

    def pending?
      started_at.blank?
    end

    def started?
      started_at.present?
    end

    def in_progress?
      started? && !finished?
    end

    def in_progress_by?(user)
      in_progress? && (self.user == user)
    end

    def finished?
      finished_at.present?
    end

    def duration # in seconds
      return 0 unless started?
      end_time = finished_at || Time.zone.now
      (end_time - started_at).to_i
    end

    def duration_hours
      duration.to_f / 3600
    end

    def duration_estimate_hours
      return unless duration_estimate
      duration_estimate.to_f / 3600
    end

    def seconds_left
      return unless duration_estimate
      duration_estimate - duration
    end
  end
end
