# frozen_string_literal: true
# == Schema Information
#
# Table name: conversion_rates
#
#  id           :integer          not null, primary key
#  from_unit_id :integer          not null
#  to_unit_id   :integer          not null
#  rate         :decimal(, )      not null
#

class ConversionRate < ApplicationRecord
  belongs_to :from_unit, class_name: "Unit"
  belongs_to :to_unit, class_name: "Unit"

  scope :from_unit, ->(unit) { where(from_unit_id: unit.id) }
  scope :to_unit, ->(unit) { where(to_unit_id: unit.id) }
end
