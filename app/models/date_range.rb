# frozen_string_literal: true

class DateRange
  attr_reader :start_date, :end_date

  # Params example: { "start_date": "Sep 13, 2017", "end_date": "Sep 27, 2017" }
  # Both start and end dates are optional.
  # For the date, it accepts a string or a date/time.
  def initialize(params = {})
    @start_date = parse_date params[:start_date]
    @end_date   = parse_date params[:end_date]
  end

  def start?
    start_date.present?
  end

  def end?
    end_date.present?
  end

  private

  # value can be a date, time or string
  def parse_date(value)
    return nil if value.blank?
    return Date.strptime(value, Date::DATE_FORMATS[:default]) if value.is_a?(String)
    value.to_date
  end
end
