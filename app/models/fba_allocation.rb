# frozen_string_literal: true
# == Schema Information
#
# Table name: fba_allocations
#
#  id                   :integer          not null, primary key
#  fba_warehouse_id     :integer          not null
#  product_id           :integer          not null
#  quantity             :integer          not null
#  quantity_to_allocate :integer
#  created_at           :datetime
#  updated_at           :datetime
#  shipment_type        :text
#

class FbaAllocation < ApplicationRecord
  belongs_to :fba_warehouse
  belongs_to :product

  FRESHNESS_INTERVAL = 1.day

  validates :quantity, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :quantity_to_allocate, presence: true, numericality: { greater_than_or_equal_to: 0 }

  scope :fresh, -> { where('updated_at > ?', Time.zone.now - FRESHNESS_INTERVAL) }
  scope :stale, -> { where('updated_at < ?', Time.zone.now - FRESHNESS_INTERVAL) }
  scope :positive, -> { where('quantity > ?', 0) }

  def self.clear_for_products(products)
    where(product_id: products).destroy_all
  end

  def empty?
    quantity.zero?
  end

  def could_be_fulfilled?
    quantity <= available_product_quantity
  end

  def self.default_product_allocations(product, shipment_type)
    quantity = shipment_type == 'cases' ? product.to_allocate_case_quantity : product.to_allocate_item_quantity
    fetch_allocations product, quantity, shipment_type
  end

  def self.shipment_item_allocations(shipment_item)
    fetch_allocations shipment_item.product, shipment_item.quantity, shipment_item.shipment_type
  end

  def self.fetch_allocations(product, quantity, shipment_type)
    fresh.where product: product,
                quantity_to_allocate: quantity,
                shipment_type: shipment_type
  end

  def product_stats
    @product_stats ||= Products::ProductInventoryStats.new(product)
  end

  private

  def available_product_quantity
    quantity = product_stats.packed_inventory_quantity
    quantity = in_cases(quantity) if cases_shipment?
    quantity
  end

  def in_cases(quantity)
    per_case = product.items_per_case
    ProductAmount.new(quantity).cases_floor(per_case)
  end

  def cases_shipment?
    shipment_type == 'cases'
  end
end
