# frozen_string_literal: true
# == Schema Information
#
# Table name: fba_warehouses
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  created_at :datetime
#  updated_at :datetime
#

# FbaWarehouse is a warehouse in Amazon.
# Amazon distributes products by FBA warehouses automatically, we do not manage this.
# All we do is ask Amazon API to distribute a certain amount of product;
# Then Amazon responds with how many items should go where. And we save this information.
class FbaWarehouse < ApplicationRecord
  has_many :fba_allocations
  has_many :products

  validates :name, presence: true

  # Amazon assigns names for warehouses based on their locations
  # we order warehouse by proximity to production based on their names
  PRIORITIES = { "P" => 5, "O" => 4, "R" => 3, "S" => 2, "B" => 1 }.freeze

  def priority
    PRIORITIES.fetch(name[0].upcase, 0)
  end

  def self.retrieve_by_name(name)
    find_or_create_by name: name
  end
end
