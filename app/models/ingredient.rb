# == Schema Information
#
# Table name: ingredients
#
#  id                      :integer          not null, primary key
#  product_id              :integer          not null
#  quantity_obsolete       :decimal(, )      default(1.0), not null
#  created_at              :datetime
#  updated_at              :datetime
#  inventory_item_id       :integer          not null
#  quantity_in_small_units :decimal(, )      default(1.0), not null
#
# Foreign Keys
#
#  fk_rails_...  (product_id => products.id)
#

class Ingredient < ApplicationRecord
  include Logging
  belongs_to :inventory_item, touch: true
  belongs_to :product, touch: true

  validates :product_id, presence: true
  validates :quantity_in_small_units, presence: true, numericality: { greater_than: 0 }
  validates :inventory_item_id, presence: true, uniqueness: { scope: :product_id, message: "is a duplicate" }

  scope :ordered, -> { order(:id) }

  scope :for_zone, ->(zone) { joins(inventory_item: :production_zone).where('zones.id' => zone.id) }
  scope :up_to_zone, lambda { |zone|
    joins(inventory_item: :production_zone).where('zones.production_order <= ?', zone.production_order)
  }

  delegate :zone, to: :inventory_item, prefix: true
  delegate :name, to: :inventory_item, prefix: true, allow_nil: true
  delegate :inventory, :inventory_quantity, to: :inventory_item
  delegate :inventoriable, :product?, :material?, to: :inventory_item

  def zones_available
    return Zone.all unless product
    product.zones.ordered
  end

  def cost
    quantity_price inventory_item.prime_cost
  end

  def cost_percent
    product_prime_cost = product.prime_cost
    return 0 if product_prime_cost.zero?
    (cost * 100 / product_prime_cost).round(1)
  end

  def supply
    (inventory_quantity / quantity).to_i
  end

  concerning :UnitsOfMeasure do
    delegate :main_unit_name, to: :inventory_item

    def small_unit_name
      inventory_item&.ingredient_unit_name
    end

    # quantity in main material units (lbs)
    def quantity
      return nil unless inventory_item
      inventory_item.ingredient_to_main_units(quantity_in_small_units)
    end
  end

  private

  def quantity_price(unit_price)
    return nil unless unit_price
    unit_price * quantity
  end
end
