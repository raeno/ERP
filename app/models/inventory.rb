# frozen_string_literal: true
# == Schema Information
#
# Table name: inventories
#
#  id                :integer          not null, primary key
#  inventory_item_id :integer          not null
#  quantity          :decimal(, )      not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Foreign Keys
#
#  fk_rails_...  (inventory_item_id => inventory_items.id)
#

class Inventory < ApplicationRecord
  include Logging
  belongs_to :inventory_item

  has_one :product, through: :inventory_item, source: :inventoriable, source_type: 'Product'
  has_one :material, through: :inventory_item, source: :inventoriable, source_type: 'Material'

  has_one :zone, through: :inventory_item
  validates :quantity, presence: true, numericality: { greater_than_or_equal_to: 0 }

  scope :positive, -> { where('inventories.quantity > 0') }

  delegate :production_order, to: :zone

  delegate :name, to: :inventory_item

  def debit(amount)
    raise ArgumentError, 'Only positive amount is allowed' unless amount > 0
    change_quantity(-amount)
  end

  def credit(amount)
    raise ArgumentError, 'Only positive amount is allowed' unless amount > 0
    change_quantity amount
  end

  private

  def change_quantity(amount)
    with_lock do
      self.quantity += amount
      save!
    end
  end
end
