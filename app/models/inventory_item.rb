# frozen_string_literal: true
# == Schema Information
#
# Table name: inventory_items
#
#  id                          :integer          not null, primary key
#  zone_id                     :integer
#  inventoriable_id            :integer
#  inventoriable_type          :string
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  production_zone_id          :integer
#  production_setup_minutes    :integer          default(0), not null
#  production_units_per_minute :integer
#

class InventoryItem < ApplicationRecord
  INVENTORIABLE_TYPES = %w(Material Product).freeze

  self.per_page = 24

  belongs_to :inventoriable, polymorphic: true, touch: true
  belongs_to :product, foreign_key: :inventoriable_id, foreign_type: 'Product'
  belongs_to :material, foreign_key: :inventoriable_id, foreign_type: 'Material'

  belongs_to :zone, touch: true
  belongs_to :production_zone, touch: true, class_name: 'Zone'

  validates :inventoriable_type, inclusion: { in: INVENTORIABLE_TYPES }

  delegate :name, :image_url, to: :inventoriable
  delegate :name, to: :zone, prefix: true
  delegate :items_per_case, :reserved, to: :product, prefix: true
  delegate :slug, :name, to: :zone, prefix: true

  scope :materials, -> { where(inventoriable_type: 'Material') }
  scope :products, -> { where(inventoriable_type: 'Product') }
  scope :active_products, -> { where(inventoriable_type: 'Product').joins(:product).merge(Product.active) }
  scope :in_zone,  -> (zone) { where(zone: zone) }
  scope :for_zone, -> (zone) { where(production_zone: zone) }

  before_create :set_production_zone

  def build_inventoriable(params)
    raise "Unknown inventoriable_type: #{inventoriable_type}" unless INVENTORIABLE_TYPES.include?(inventoriable_type)
    self.inventoriable = inventoriable_type.constantize.new(params)
  end

  def nature
    return nil unless inventoriable_type
    inventoriable_type.downcase.to_sym
  end

  def material?
    nature == :material
  end

  def product?
    nature == :product
  end

  def active_product?
    product? && inventoriable.is_active?
  end

  def in_make_zone?
    zone.make?
  end

  def in_pack_zone?
    zone.pack?
  end

  def in_ship_zone?
    zone.ship?
  end

  def product_in_ship_zone?
    product? && in_ship_zone?
  end

  def set_production_zone
    return if production_zone_id
    zone_workflow = ZoneWorkflow.new
    if material?
      self.production_zone_id = zone_id
    else
      next_zone = zone_workflow.next_zone(zone)
      self.production_zone_id = next_zone&.id || zone_id
    end
  end

  concerning :Ingredients do
    included do
      # "Parent" ingredients where this II is a component
      has_many :ingredients, dependent: :destroy
      # Products this II is a component of.
      has_many :products_as_ingredient, through: :ingredients, source: :product
    end

    # "Child" ingredients of this II, i.e. ingredients this II consists of.
    # Only applicable to product IIs.
    def contained_ingredients
      return Ingredient.none unless product?
      inventoriable.ingredients_for_zone(zone)
    end

    # Product ingredients go first.
    # Then go material ingredients, sorted by category.
    def contained_ingredients_ordered
      contained_ingredients.includes(:inventory_item => { :material => :category })
                           .order('inventory_items.inventoriable_type DESC', 'material_categories.name')
    end

    # Components of the inventoriable in all(!) zones.
    def all_inventoriable_components
      return InventoryItem.none unless product?
      inventoriable.components
    end
  end

  concerning :Inventories do
    included do
      has_one :inventory, dependent: :destroy, autosave: true
      scope :with_positive_amount, -> { joins(:inventory).merge(Inventory.positive) }
    end

    def inventory_quantity
      # For product ship zone inventory items, inventory.quantity value is inacurate (not aimed to be).
      # Correct value is fetched from Amazon and stored in AmazonInventory model.
      if product_in_ship_zone?
        product_stats = Products::ProductInventoryStats.new(product)
        return product_stats.total_amazon_inventory
      end

      inventory&.quantity || 0
    end

    def zero_inventory?
      inventory_quantity.zero?
    end
  end

  concerning :Costs do
    def prime_cost
      product? ? inventoriable.cumulative_cogs(zone) : inventoriable.prime_cost
    end

    def latest_container_unit_price
      material&.latest_container_unit_price
    end

    def previous_prime_cost
      return material.previous_container_unit_price if material?
      ProductCogsHistory.new(product, zone).previous_prime_cost
    end

    def prime_cost_change_percent
      return material.price_change_percent if material?
      ProductCogsHistory.new(product, zone).prime_cost_change_percent
    end

    def total_prime_cost
      inventory_quantity * prime_cost
    end

    # For products, only counts costs of ingredients involved in this zone.
    #   (for pack IIs, only includes pack ingredients)
    # For materials, it is the same as prime_cost.
    def zone_prime_cost
      product? ? inventoriable.zone_cogs(zone) : prime_cost
    end

    def labour_cost
      last_finished_task&.unit_labour_cost
    end
  end

  # Inventory item production process concern - tasks and plans.
  # Only relevant to products (not materials).
  concerning :Production do
    included do
      has_many :tasks, dependent: :destroy
      has_one :production_report, class_name: 'InventoryItems::ProductionReport', dependent: :destroy

      delegate :can_cover, :production_can_cover_cases, :can_cover_optimal,
        :fully_assigned?,
        to: :production_report, prefix: :production
    end

    class_methods do
      def ordered_by_days_of_cover
        # sort by title when days of cover are equal to produce the same order between queries
        joins(product: :shipment_report)
          .order('products_shipment_reports.days_of_cover ASC, products.title ASC')
      end
    end

    def current_tasks
      tasks.unfinished.ordered_by_id
    end

    def last_finished_task
      tasks.finished.ordered_by_id.first
    end

    def production_seconds_per_unit
      return 0 unless production_units_per_minute&.nonzero?
      60.0 / production_units_per_minute
    end
  end

  concerning :UnitsOfMeasure do
    # Materials:
    # main_unit_name: e.g. lbs
    # ingredient_unit_name: e.g. ml
    # zone_unit_name: lbs (using main_unit for all zones)

    # Products:
    # main_unit_name: each
    # ingredient_unit_name: each
    # zone_unit_name in Make zone: unit
    # zone_unit_name in Pack/Ship zones: case

    delegate :main_unit_name, :ingredient_unit_name, :ingredient_to_main_units,
      to: :inventoriable

    def zone_unit_name
      return main_unit_name if material?
      return 'units' if in_make_zone?
      'cases'
    end
  end
end
