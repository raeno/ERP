# frozen_string_literal: true
# == Schema Information
#
# Table name: inventory_items_production_reports
#
#  id                         :integer          not null, primary key
#  inventory_item_id          :integer          not null
#  in_stock                   :decimal(, )      not null
#  demand                     :decimal(, )      not null
#  demand_coverage            :decimal(, )      not null
#  supply                     :decimal(, )      not null
#  to_cover                   :decimal(, )      not null
#  can_cover                  :decimal(, )      not null
#  can_cover_in_cases         :decimal(, )
#  previous_zone_deficit      :boolean
#  deficit_inventoriable_id   :integer
#  deficit_inventoriable_type :string
#  deficit_ingredient_name    :string
#  can_cover_optimal          :decimal(, )      not null
#  assigned                   :decimal(, )      default(0.0), not null
#

module InventoryItems
  # define prefix here while we have only one class under namespace
  def self.table_name_prefix
    'inventory_items_'
  end

  # Accumulates all production info ( supply, demand, coverage )
  # should be refreshed each time when inventory item quantity or
  # it's ingredients quantities are changed
  class ProductionReport < ApplicationRecord
    belongs_to :inventory_item
    belongs_to :deficit_inventoriable, polymorphic: true
    belongs_to :product, foreign_key: :deficit_inventoriable_id, foreign_type: 'Product'
    belongs_to :material, foreign_key: :deficit_inventoriable_id, foreign_type: 'Material'

    delegate :product?, :in_pack_zone?, :product_items_per_case,
      to: :inventory_item, allow_nil: true

    def in_stock_cases
      return nil unless product?
      ProductAmount.new(in_stock).cases_floor(product_items_per_case)
    end

    # How many units could be covered but have no task to be.
    def unassigned
      [0, can_cover_optimal - assigned].max.to_i
    end

    # in seconds
    def time_estimate
      @time_estimate ||= ProductionEstimate.new(inventory_item).duration_for can_cover_optimal
    end

    def time_estimate_hours
      time_estimate.to_f / 3600
    end

    concerning :Status do
      included do
        scope :low_supply, -> { where('can_cover < to_cover') }
        scope :unassigned, -> { where('assigned < can_cover_optimal') }
        scope :managed,    -> { where('(can_cover >= to_cover) AND (assigned >= can_cover_optimal)') }
      end

      class_methods do
        def with_status(status)
          return all unless status

          case status
          when :unassigned then unassigned
          when :low_supply then low_supply
          when :managed then managed
          when :all then all
          end
        end
      end

      def fully_supplied?
        to_cover <= supply
      end

      def fully_assigned?
        unassigned.zero?
      end
    end
  end
end
