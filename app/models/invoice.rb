# == Schema Information
#
# Table name: invoices
#
#  id             :integer          not null, primary key
#  vendor_id      :integer
#  created_at     :datetime
#  updated_at     :datetime
#  number         :string           default(""), not null
#  attachment_url :text
#  date           :date             not null
#  total_price    :decimal(, )      not null
#  received_date  :date
#  notes          :text
#  paid_date      :date
#

class Invoice < ApplicationRecord
  belongs_to :vendor
  has_many :items, class_name: "InvoiceItem", dependent: :destroy
  has_many :material_containers, through: :items

  validates :vendor_id, :total_price, presence: true
  after_initialize :set_default_fields

  scope :ordered, -> { order(date: :desc, id: :desc) }

  delegate :name, to: :vendor, prefix: true, allow_nil: true
  delegate :count, to: :items, prefix: true

  def self.for_last_n_months(n)
    where('invoices.created_at > ?', n.months.ago)
  end

  def to_s
    number.to_s
  end

  def date_str
    date.to_s(:short)
  end

  def date_str=(val)
    self.date = Date.strptime(val, Date::DATE_FORMATS[:short])
  end

  concerning :Prices do
    included do
      has_many :fees, -> { ordered }, class_name: "InvoiceFee", dependent: :destroy

      accepts_nested_attributes_for :fees, allow_destroy: true
    end

    def total_price_matching?
      total_price == total_item_price
    end

    def total_item_price
      @total_item_price ||= items.sum(:total_price) + fees.sum(:price)
    end
  end

  concerning :Status do
    included do
      include ReceivingStatus #  #received?, #become_received
    end

    def status
      return :error unless total_price_matching?

      return :complete if received? && paid?
      return :received if received?
      return :paid if paid?

      :pending
    end

    def paid?
      paid_date.present?
    end

    def become_paid
      self.paid_date = Time.zone.today
    end

    def all_items_received?
      items.pending.empty?
    end

    def lead_time_days
      return unless received_date
      (received_date - date).to_i
    end

    def locked?
      paid? || received?
    end
  end

  private

  def set_default_fields
    return unless new_record?
    self.date ||= Time.zone.today
    self.total_price ||= 0
    self.number = next_invoice_number if number.blank?
    init_fees
  end

  def init_fees
    return unless new_record? && fees.empty?
    %w(Freight Tax).each { |name| fees.build(name: name, price: 0) }
  end

  def next_invoice_number
    (Invoice.count + 1).to_s.rjust(6, '0')
  end
end
