# frozen_string_literal: true
# == Schema Information
#
# Table name: invoice_fees
#
#  id         :integer          not null, primary key
#  invoice_id :integer          not null
#  name       :string           not null
#  price      :decimal(, )      not null
#  created_at :datetime
#  updated_at :datetime
#
# Foreign Keys
#
#  fk_rails_...  (invoice_id => invoices.id)
#

class InvoiceFee < ApplicationRecord
  belongs_to :invoice

  validates :name, presence: true
  validates :price, presence: true, numericality: { greater_than_or_equal_to: 0 }

  scope :ordered, -> { order(:id) }
end
