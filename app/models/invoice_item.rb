# frozen_string_literal: true
# == Schema Information
#
# Table name: invoice_items
#
#  id                    :integer          not null, primary key
#  invoice_id            :integer          not null
#  material_id           :integer
#  pkg_external_name     :string
#  pkg_form              :string
#  pkg_unit_id           :integer
#  pkg_amount            :float
#  pkg_price             :decimal(, )
#  quantity              :integer          default(1), not null
#  total_material_amount :decimal(, )      not null
#  total_price           :decimal(, )      not null
#  batch_number          :string
#  created_at            :datetime
#  updated_at            :datetime
#  received_date         :date
#  expiration_date       :date
#  material_container_id :integer
#
# Foreign Keys
#
#  fk_rails_...  (invoice_id => invoices.id)
#  fk_rails_...  (material_id => materials.id)
#

class InvoiceItem < ApplicationRecord
  belongs_to :invoice
  belongs_to :material
  belongs_to :material_container, dependent: :destroy

  validates :invoice_id, :quantity, presence: true

  scope :ordered_by_invoice_date, -> { includes(:invoice).order('invoices.date DESC') }
  scope :before, ->(date) { ordered_by_invoice_date.where('invoices.date < ?', date) }

  delegate :vendor, :vendor_name, to: :invoice
  delegate :date, :number, to: :invoice, prefix: true
  delegate :total_price_matching?, to: :invoice, prefix: true
  delegate :material_name, :material_unit, :material_unit_id, to: :material_container
  delegate :unit, :unit_id, to: :material_container, prefix: 'container'

  def default_lot_number
    invoice.next_lot_number
  end

  concerning :Prices do
    included do
      before_save :set_totals
    end

    private

    def set_totals
      self.total_material_amount = material_container.amount_in_material_units * quantity
      self.total_price = material_container.price * quantity
    end
  end

  concerning :Status do
    included do
      include ReceivingStatus  #  #received?, #become_received

      scope :pending, -> { where(received_date: nil) }
    end

    def locked?
      received? || invoice.locked?
    end
  end
end
