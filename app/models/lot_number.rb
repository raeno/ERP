# frozen_string_literal: true
class LotNumber
  def self.generate(invoice)
    return unless invoice
    "#{invoice.number}/#{invoice.items_count.next}"
  end
end
