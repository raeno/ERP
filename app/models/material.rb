# frozen_string_literal: true
# == Schema Information
#
# Table name: materials
#
#  id                   :integer          not null, primary key
#  name                 :string           not null
#  unit_price           :decimal(, )      default(0.0), not null
#  created_at           :datetime
#  updated_at           :datetime
#  ingredient_unit_id   :integer          not null
#  image_url            :string
#  category_id          :integer          default(1), not null
#  unit_id              :integer          not null
#  unit_conversion_rate :decimal(, )
#

class Material < ApplicationRecord
  belongs_to :category, class_name: "MaterialCategory"
  has_one :inventory_item, as: :inventoriable, dependent: :destroy
  has_one :inventory, through: :inventory_item

  has_many :material_containers, dependent: :destroy
  has_many :invoice_items, through: :material_containers
  has_many :material_transactions, through: :material_containers

  has_many :material_attachments
  has_many :attachments, through: :material_attachments
  has_many :media, class_name: 'MaterialMedia'

  scope :ordered_by_id, -> { order(:id) }
  scope :ordered_by_zone, -> { includes(:inventory_item => :production_zone).order('zones.production_order') }
  scope :ordered_by_name, -> { order(:name) }
  scope :in_category, ->(category_id) { where(category_id: category_id) }
  scope :for_zone, ->(zone_id) { joins(:inventory_item).where('inventory_items.production_zone_id' => zone_id) }

  validates :unit_price, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :name, :category_id, presence: true
  validates :unit_conversion_rate, numericality: { greater_than: 0, allow_nil: true }

  delegate :inventory_quantity, :production_zone, to: :inventory_item
  delegate :production_zone_id, to: :inventory_item, allow_nil: true
  alias_attribute :prime_cost, :unit_price
  delegate :production_can_cover, to: :inventory_item

  attr_accessor :inventory_item_production_zone_id  # used to set/edit II zone via Material form.

  def inventory_total_prime_cost
    inventory_item.total_prime_cost
  end

  concerning :Vendors do
    included do
      has_many :material_vendors
      has_many :vendors, through: :material_vendors

      accepts_nested_attributes_for :material_vendors, allow_destroy: true
    end
  end

  concerning :MaterialContainers do
    def active_container
      ::MaterialContainers::SameMaterialQuery.new(self).active
    end

    def price_change_percent
      latest_container&.material_price_change_percent
    end

    def latest_container
      ::MaterialContainers::SameMaterialQuery.new(self).latest
    end

    def latest_container_unit_price
      latest_container&.material_unit_price || unit_price
    end

    def previous_container_unit_price
      latest_container&.previous_container_material_unit_price
    end
  end

  concerning :UnitsOfMeasure do
    included do
      belongs_to :unit
      belongs_to :ingredient_unit, class_name: "Unit"
      alias_method :main_unit, :unit

      delegate :name, to: :unit, prefix: true  # unit_name
      delegate :name, to: :main_unit, prefix: true  # main_unit_name
      delegate :name, to: :ingredient_unit, prefix: true  # ingredient_unit_name

      validate :units_must_be_compatible
    end

    def ingredient_to_main_units(amount)
      MaterialUnitConversion.new(self).ingredient_to_main(amount)
    end

    private

    def units_must_be_compatible
      unless UnitConversion.new(ingredient_unit, main_unit).valid?
        errors.add(:ingredient_unit_id, "is incompatible with the main unit")
      end
    end
  end

  concerning :OrderingSizes do
    included do
      has_many :ordering_sizes, dependent: :destroy
      accepts_nested_attributes_for :ordering_sizes, allow_destroy: true
    end
  end
end
