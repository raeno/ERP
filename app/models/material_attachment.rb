# frozen_string_literal: true
# == Schema Information
#
# Table name: material_attachments
#
#  id            :integer          not null, primary key
#  material_id   :integer          not null
#  attachment_id :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class MaterialAttachment < ApplicationRecord
  belongs_to :material
  belongs_to :attachment
end
