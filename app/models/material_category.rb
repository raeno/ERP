# frozen_string_literal: true
# == Schema Information
#
# Table name: material_categories
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  created_at :datetime
#  updated_at :datetime
#

class MaterialCategory < ApplicationRecord
  has_many :materials, foreign_key: :category_id

  validates :name, presence: true

  scope :ordered, -> { order(:name) }
end
