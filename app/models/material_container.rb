# frozen_string_literal: true
# == Schema Information
#
# Table name: material_containers
#
#  id               :integer          not null, primary key
#  material_id      :integer          not null
#  external_name    :text
#  unit_id          :integer          not null
#  amount           :decimal(, )      default(1.0), not null
#  price            :decimal(, )      not null
#  lot_number       :text
#  expiration_date  :date
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  amount_left      :decimal(, )      default(0.0), not null
#  old_status       :integer          default(0)
#  ordering_size_id :integer
#  status           :enum             default("pending")
#
# Foreign Keys
#
#  fk_rails_...  (material_id => materials.id)
#

class MaterialContainer < ApplicationRecord
  belongs_to :material
  belongs_to :unit
  belongs_to :ordering_size
  has_one :invoice_item
  has_many :material_transactions

  enum status: {
    active: 'active',
    closed: 'closed',
    pending: 'pending',
    expired: 'expired'
  }

  delegate :unit, :unit_id, :name, to: :material, prefix: true
  delegate :inventory_item, to: :material

  scope :ordered, -> { order('created_at ASC') }
  scope :active, -> { where(status: 'active') }
  scope :pending, -> { where(status: 'pending') }

  validates :material_id, :unit_id, :amount, :price, presence: true
  validates :amount, numericality: { greater_than: 0 }
  validates :price, numericality: { greater_than_or_equal_to: 0 }
  validate :unit_must_be_compatible_with_material

  EMPTY_EPSILON = 0.00001

  def previous
    return nil unless invoice_item
    invoice_date = invoice_item.invoice_date
    MaterialContainers::SameMaterialQuery.new(material).first_before(invoice_date)
  end

  def amount_in_material_units
    unit_conversion.convert(amount)
  end

  def amount_left_in_material_units
    unit_conversion.convert(amount_left)
  end

  def empty?
    amount_left < EMPTY_EPSILON
  end

  def enough_material_left?(amount)
    amount_left >= amount
  end

  def container_form
    ordering_size&.name
  end

  def copy_amount_and_unit
    self.unit = ordering_size&.unit
    self.amount = ordering_size&.amount
  end

  concerning :Prices do
    def unit_price
      return nil unless amount&.nonzero?
      price / amount
    end

    def material_unit_price
      return nil unless amount_in_material_units&.nonzero?
      price / amount_in_material_units
    end

    def previous_container_material_unit_price
      previous&.material_unit_price
    end

    # FIXME: extract this, doesn't look pretty
    def material_price_change_percent
      return nil unless previous_container_material_unit_price&.nonzero? && material_unit_price&.nonzero?
      (material_unit_price - previous_container_material_unit_price) * 100 / previous_container_material_unit_price
    end
  end

  concerning :Dates do
    def expiration_date_str
      expiration_date&.to_s(:short)
    end

    # TODO: move to form
    def expiration_date_str=(val)
      return if val.blank?
      self.expiration_date = Date.strptime(val, Date::DATE_FORMATS[:short])
    end
  end

  private

  # TODO: extract to validator
  def unit_must_be_compatible_with_material
    unless unit_conversion.valid?
      errors.add(:unit_id, "is incompatible with material unit of measure")
    end
  end

  def unit_conversion
    @unit_conversion ||= UnitConversion.new(unit, material_unit)
  end
end
