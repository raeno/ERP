# frozen_string_literal: true
# == Schema Information
#
# Table name: material_media
#
#  id          :integer          not null, primary key
#  url         :text
#  material_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  mimetype    :text
#

class MaterialMedia < ApplicationRecord
  belongs_to :material

  def preview_url
    @preview_url ||= FilestackApi.new(url).preview_url
  end

  def viewer_url
    @viewer_url ||= FilestackApi.new(url).viewer_url
  end

  def pdf?
    return false unless mimetype
    mimetype.match(/pdf/)
  end

  def image?
    return false unless mimetype
    mimetype.match(/image/)
  end
end
