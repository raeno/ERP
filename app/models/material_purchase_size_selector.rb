# frozen_string_literal: true
class MaterialPurchaseSizeSelector
  attr_reader :material

  def initialize(material)
    @material = material
  end

  # Returns the best purchase size (barrel, can) to cover given the material demand.
  def best_size(demand_amount)
    sizes = material.ordering_sizes.ordered
    sizes.find_by('amount_in_main_material_units >= ?', demand_amount) || sizes.last
  end
end
