# frozen_string_literal: true
# == Schema Information
#
# Table name: material_transactions
#
#  id                    :integer          not null, primary key
#  quantity              :float
#  inventory_amount      :float
#  batch_id              :integer
#  material_container_id :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class MaterialTransaction < ApplicationRecord
  belongs_to :material_container
  belongs_to :batch

  scope :ordered, -> { order('created_at DESC') }
end
