# frozen_string_literal: true
class MaterialUnitConversion
  attr_reader :material

  def initialize(material)
    @material = material
  end

  def main_to_ingredient_rate
    material.unit_conversion_rate || main_to_ingredient_conversion.rate
  end

  def ingredient_to_main_rate
    material_rate = material.unit_conversion_rate
    return 1 / material_rate if material_rate&.nonzero?
    ingredient_to_main_conversion.rate
  end

  def main_to_ingredient(main_unit_amount)
    main_to_ingredient_rate * main_unit_amount
  end

  def ingredient_to_main(ingredient_unit_amount)
    ingredient_to_main_rate * ingredient_unit_amount
  end

  private

  def main_to_ingredient_conversion
    @main_to_ingredient_conversion ||= UnitConversion.new(material.unit, material.ingredient_unit)
  end

  def ingredient_to_main_conversion
    @ingredient_to_main_conversion ||= UnitConversion.new(material.ingredient_unit, material.unit)
  end
end
