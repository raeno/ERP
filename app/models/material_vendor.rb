# frozen_string_literal: true
# == Schema Information
#
# Table name: material_vendors
#
#  id          :integer          not null, primary key
#  material_id :integer          not null
#  vendor_id   :integer          not null
#  url         :string
#

class MaterialVendor < ActiveRecord::Base
  belongs_to :vendor
  belongs_to :material

  delegate :name, to: :vendor, prefix: true

  validates :vendor_id, uniqueness: { scope: :material_id }
end
