# frozen_string_literal: true
# == Schema Information
#
# Table name: ordering_sizes
#
#  id                            :integer          not null, primary key
#  material_id                   :integer          not null
#  unit_id                       :integer          not null
#  amount                        :float            not null
#  amount_in_main_material_units :float            not null
#  name                          :string
#
# Foreign Keys
#
#  fk_rails_...  (material_id => materials.id)
#  fk_rails_...  (unit_id => units.id)
#

class OrderingSize < ApplicationRecord
  include ActionView::Helpers::NumberHelper

  belongs_to :unit
  belongs_to :material
  has_many :material_containers

  scope :ordered, -> { order(:amount_in_main_material_units) }

  validates :unit_id, :material_id, presence: true
  validates :amount, presence: true, numericality: { greater_than: 0 }
  validate :units_must_be_compatible

  delegate :name, to: :unit, prefix: true
  delegate :unit, :unit_id, to: :material, prefix: true

  before_save :update_amount_in_main_material_units

  def to_s
    amount_and_unit = "#{number_with_delimiter amount} #{unit_name}"
    return amount_and_unit if name.blank?
    "#{name} (#{amount_and_unit})"
  end

  def estimated_price
    last_purchased_price || (material.unit_price * amount_in_main_material_units).round(2)
  end

  def last_purchased_price
    material_containers.order(:id).last&.price
  end

  private

  def units_must_be_compatible
    unless unit_conversion.valid?
      errors.add(:unit_id, "is incompatible with the main unit")
    end
  end

  def update_amount_in_main_material_units
    self.amount_in_main_material_units = unit_conversion.convert(amount)
  end

  def unit_conversion
    UnitConversion.new(unit, material_unit)
  end
end
