# frozen_string_literal: true
# == Schema Information
#
# Table name: products
#
#  id                       :integer          not null, primary key
#  title                    :string
#  seller_sku               :string
#  asin                     :text
#  fnsku                    :string
#  size                     :string
#  list_price_amount        :string
#  list_price_currency      :string
#  total_supply_quantity    :integer
#  in_stock_supply_quantity :integer
#  small_image_url          :string
#  is_active                :boolean          default(TRUE)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  inbound_qty              :float            default(0.0)
#  sold_last_24_hours       :float            default(0.0)
#  weeks_of_cover           :float            default(0.0)
#  sellable_qty             :float            default(0.0)
#  internal_title           :string
#  sales_rank               :integer
#  selling_price            :float
#  selling_price_currency   :string
#  items_per_case           :integer          default(0), not null
#  production_buffer_days   :integer          default(35), not null
#  batch_min_quantity       :integer          default(0), not null
#  batch_max_quantity       :integer
#  fee                      :decimal(, )      default(0.0), not null
#  notes                    :text
#

class Product < ApplicationRecord
  include Products::Financials  # profit, gross_margin, roi, cogs, labour_cost
  include Products::UnitsOfMeasure  # main_unit_name

  self.per_page = 24

  has_many :batches, dependent: :destroy

  has_many :product_attachments
  has_many :attachments, through: :product_attachments

  scope :active, -> { where(is_active: true) }
  scope :inactive, -> { where(is_active: false) }
  scope :ordered_by_title, -> { order(:internal_title) }

  validates :production_buffer_days, :items_per_case, :batch_min_quantity, :fee,
    presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :batch_max_quantity, numericality: { greater_than_or_equal_to: 0, allow_nil: true }

  def name
    internal_title.presence || title.presence || 'Untitled'
  end

  def image_url
    small_image_url || ''
  end

  concerning :ZonesAvailable do
    included do
      has_many :product_zone_availabilities, dependent: :destroy
      has_many :zones, through: :product_zone_availabilities
    end

    # rubocop:disable Style/PredicateName
    def has_zones_available?
      product_zone_availabilities.count.positive?
    end
    # rubocop:enable Style/PredicateName

    # rubocop:disable Style/PredicateName
    def has_zone?(zone)
      product_zone_availabilities.where(zone: zone).any?
    end
    # rubocop:enable Style/PredicateName

    # rubocop:disable Style/PredicateName
    def has_make_zone?
      has_zone?(Zone.make_zone)
    end
    # rubocop:enable Style/PredicateName

    # rubocop:disable Style/PredicateName
    def has_pack_zone?
      has_zone?(Zone.pack_zone)
    end
    # rubocop:enable Style/PredicateName

    # rubocop:disable Style/PredicateName
    def has_ship_zone?
      has_zone?(Zone.ship_zone)
    end
    # rubocop:enable Style/PredicateName
  end

  concerning :Inventories do
    included do
      has_one :amazon_inventory, dependent: :destroy
      has_many :inventory_items, as: :inventoriable, dependent: :destroy
      has_many :inventories, through: :inventory_items
    end

    def inventories
      super.joins(:zone)
    end

    def zone_inventory(zone)
      inventories.find_by('zones.id' => zone)
    end

    def zone_inventory_item(zone)
      inventory_items.in_zone(zone).first
    end

    def make_inventory_item
      zone_inventory_item(Zone.make_zone)
    end

    def pack_inventory_item
      zone_inventory_item(Zone.pack_zone)
    end

    def ship_inventory_item
      zone_inventory_item(Zone.ship_zone)
    end
  end

  concerning :Production do
    included do
    end

    def production_urgency
      setting = Setting.instance
      red_limit    = setting.red_urgency_level_days
      yellow_limit = setting.yellow_urgency_level_days

      covered_days = shipment_report&.days_of_cover || days_of_cover

      case covered_days
      when 0..red_limit then :red
      when red_limit..yellow_limit then :yellow
      else :green
      end
    end

    def days_to_cover
      amazon_inventory&.days_to_cover || 0
    end

    # If a product is involved in a giftbox with a lesser days_of_cover,
    # the product's days_of_cover should be the giftbox's value.
    def days_of_cover
      self_days = amazon_inventory&.days_of_cover || 0.0
      all_days = gift_boxes.map(&:days_of_cover) << self_days
      all_days.min
    end

    def amazon_coverage_ratio
      amazon_inventory&.coverage_ratio || 0
    end

    def reserved
      amazon_inventory&.reserved || 0
    end
  end

  concerning :Shipment do
    included do
      has_one :shipment_report, class_name: 'Products::ShipmentReport', dependent: :destroy
    end

    def shipping_priority
      ShippingPriority.new(self).priority
    end

    def need_to_allocate?
      ShippingPriority.new(self).allocate?
    end
  end

  concerning :FbaAllocations do
    included do
      has_many :fba_allocations
      has_many :fba_warehouses, through: :fba_allocations

      attr_accessor :to_ship_cases_qty_override
      attr_accessor :to_ship_items_qty_override
    end

    def to_allocate_item_quantity
      return 0 unless ship_inventory_item
      to_ship_items_qty_override || ProductionPlan::Plan.build(ship_inventory_item).can_cover
    end

    def to_allocate_case_quantity
      return 0 unless ship_inventory_item
      to_ship_cases_qty_override || ProductionPlan::Plan.build(ship_inventory_item).can_cover_in_cases
    end
  end

  concerning :Ingredients do
    included do
      has_many :ingredients, dependent: :destroy
      has_many :components, through: :ingredients, source: :inventory_item
    end

    # For pack zone, includes pack zone ingredients only.
    def ingredients_for_zone(zone)
      Rails.cache.fetch ingredients_cache_key(zone, 'ingredients_for_zone') do
        ingredients.for_zone(zone).includes(:inventory_item)
      end
    end

    # For pack zone, includes pack and all previous zones' (make) ingredients.
    def ingredients_up_to_zone(zone)
      Rails.cache.fetch ingredients_cache_key(zone, 'ingredients_up_to_zone')  do
        ingredients.up_to_zone(zone).includes(:inventory_item)
      end
    end

    def product_components
      components.includes(:inventoriable, :inventory, :zone).products
    end

    # TODO: move to actions, replace with bulk import
    # new_ingredient_attributes format:
    # [ {"inventory_item_id"=>"1", "quantity"=>"30"},
    #   {"inventory_item_id"=>"2", "quantity"=>"1"}, ... ]
    def replace_ingredients(new_ingredient_attributes)
      return true unless new_ingredient_attributes
      result = true

      ActiveRecord::Base.transaction do
        # A replacement of ingredients.destroy_all :
        Ingredient.where(id: ingredient_ids).delete_all

        new_ingredient_attributes.each do |attributes_hash|
          ingredient = ingredients.build(attributes_hash)
          next if ingredient.save
          message = ingredient.errors.full_messages.first
          errors.add(:base, "Ingredient \"#{ingredient.inventory_item_name}\" cannot be saved: #{message}")
          result = false
          raise ActiveRecord::Rollback
        end
      end
      result
    end

    private

    def ingredients_cache_key(*additional_keys)
      @cache_key_query ||= ProductIngredientsCacheQuery.new(self)
      @cache_key_query.cache_key additional_keys
    end
  end

  concerning :GiftBoxes do
    def gift_boxes
      @gift_boxes ||= begin
                        boxes = make_inventory_item&.products_as_ingredient || Product.none
                        boxes - [self] # ensure that we don't have circular reference
                      end
    end

    def gift_box?
      product_components.any?
    end
  end
end
