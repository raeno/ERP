# frozen_string_literal: true
class ProductAmount
  attr_reader :quantity

  def initialize(quantity)
    @quantity = quantity
  end

  # per_day = "reserved"
  def days(per_day)
    return Float::INFINITY if per_day.zero?
    (quantity.to_f / per_day).round(1)
  end

  def cases_floor(per_case)
    return 0 if per_case.nil? || per_case.zero?
    quantity.to_i / per_case
  end

  def cases_ceil(per_case, quantity_limit = nil)
    return 0 if quantity.nil? || per_case.zero?

    cases = (quantity.to_f / per_case).ceil
    return cases unless quantity_limit

    cases_limit = quantity_limit / per_case
    [cases, cases_limit].min
  end

  def cost(unit_price)
    return 0 unless unit_price
    quantity * unit_price
  end
end
