# frozen_string_literal: true
class ProductCogsHistory
  attr_reader :product, :zone

  def initialize(product, zone = Zone.ship_zone)
    @product = product
    @zone = zone
  end

  def latest_history_record
    Rails.cache.fetch [product, 'cogs_history', 'latest', zone] do
      product.product_cogs_snapshots.for_zone(zone).ordered.last
    end
  end

  def previous_prime_cost
    latest_history_record&.cogs
  end

  def prime_cost_change_percent
    current_prime_cost = zone.ship? ? product.prime_cost : product.cumulative_cogs(zone)
    cost_change_percent(previous_prime_cost, current_prime_cost)
  end

  def a_year_ago_history_record
    Rails.cache.fetch [product, 'cogs_history', 'a_year_ago'] do
      product.product_cogs_snapshots.final_only.past_year.ordered.first
    end
  end

  def a_year_ago_prime_cost
    a_year_ago_history_record&.cogs
  end

  def prime_cost_annual_change_percent
    cost_change_percent(a_year_ago_prime_cost, product.prime_cost)
  end

  private

  def cost_change_percent(previous_cost, new_cost)
    return nil unless previous_cost && new_cost
    change = (new_cost - previous_cost) / previous_cost
    change * 100
  end
end
