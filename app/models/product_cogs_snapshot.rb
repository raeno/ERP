# frozen_string_literal: true
# == Schema Information
#
# Table name: product_cogs_snapshots
#
#  id         :integer          not null, primary key
#  product_id :integer
#  batch_id   :integer
#  cogs       :decimal(, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  zone_id    :integer
#

class ProductCogsSnapshot < ApplicationRecord
  belongs_to :product, touch: true
  belongs_to :batch
  belongs_to :zone

  scope :for_zone, ->(zone) { where(zone: zone) }
  scope :shipment_only, -> { for_zone(Zone.ship_zone) }
  # Final product cogs that include all materials right to the shipment boxes.
  scope :final_only, -> { shipment_only }
  scope :ordered, -> { order(:created_at) }
  scope :past_year, -> { where('created_at > ?', 1.year.ago) }
end
