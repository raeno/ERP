# frozen_string_literal: true
class ProductIngredientsCacheQuery
  attr_reader :product

  def initialize(product)
    @product = product
  end

  def cache_key(additional_keys = [])
    [product, 'ingredients', latest_ingredients_change, *additional_keys]
  end

  private

  def latest_ingredients_change
    [latest_product_ingredients_change, latest_material_ingredients_change].compact.max
  end

  def latest_product_ingredients_change
    product.ingredients.joins(inventory_item: :product)
           .order('products.updated_at DESC')
           .pluck('products.updated_at')
           .first
  end

  def latest_material_ingredients_change
    product.ingredients.joins(inventory_item: :material)
           .order('materials.updated_at DESC')
           .pluck('materials.updated_at')
           .first
  end
end
