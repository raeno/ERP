# frozen_string_literal: true

class ProductionEstimate
  attr_reader :inventory_item

  def initialize(inventory_item)
    @inventory_item = inventory_item
  end

  delegate :production_seconds_per_unit, :production_setup_minutes,
    to: :inventory_item,  allow_nil: true

  # in seconds
  def duration_for(quantity)
    return unless production_seconds_per_unit && quantity && production_setup_minutes
    (production_setup_minutes * 60 + production_seconds_per_unit * quantity).to_i
  end
end
