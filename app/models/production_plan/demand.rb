# frozen_string_literal: true
module ProductionPlan
  class Demand
    attr_reader :ingredient_demand, :next_zone_demand

    def self.build(inventory_item)
      ingredient_demand = IngredientDemand.new(inventory_item)
      next_zone_demand  = NextZoneDemand.new(inventory_item)
      new(ingredient_demand, next_zone_demand)
    end

    def initialize(ingredient_demand, next_zone_demand)
      @ingredient_demand = ingredient_demand
      @next_zone_demand = next_zone_demand
    end

    # Tells how many inventory items (material or product) we ideally need
    # to cover the production buffer of all products on Amazon FBA.
    def value
      @value ||= ingredient_demand.value + next_zone_demand.value
    end

    # Returns hash: { product: demand, ... }, where
    # product is the product that includes the inventory item as a component;
    # demand is the demand of the inventory item for this particular product.
    def breakdown
      @breakdown ||= {
        as_ingredient: as_ingredient_breakdown,
        for_next_zone: next_zone_breakdown
      }
    end

    def as_ingredient_breakdown
      ingredient_demand.breakdown
    end

    def next_zone_breakdown
      next_zone_demand.breakdown
    end
  end
end
