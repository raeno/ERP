# frozen_string_literal: true
module ProductionPlan
  class IngredientDemand
    attr_reader :inventory_item

    def initialize(inventory_item)
      @inventory_item = inventory_item
    end

    def value
      @value ||= breakdown.values.sum
    end

    # Returns hash: { product: demand, ... }, where
    # product is a product that includes the inventory item as a component;
    # demand is the demand of the inventory item for this particular product, based on the produtc's demand.
    # E.g. how many caps are required to cover all products' demand;
    # OR how many made bottles we need for giftboxes.
    # Relevant to all materials, and also to products that are part of gift boxes.
    def breakdown
      @breakdown ||=
        begin
          inventory_item.ingredients.includes(:product).map do |ingredient|
            product = ingredient.product
            demand = ingredient.quantity * product_demand(product, inventory_item.production_zone)
            [product, demand]
          end.to_h
        end
    end

    private

    # Product demand in the given zone.
    def product_demand(product, zone)
      product_inventory_item = product.zone_inventory_item(zone)
      return 0 unless product_inventory_item
      ProductionPlan::Plan.build(product_inventory_item).to_cover
    end
  end
end
