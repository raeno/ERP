# frozen_string_literal: true
module ProductionPlan
  class Labour
    attr_reader :inventory_item

    def initialize(inventory_item)
      @inventory_item = inventory_item
    end
  end
end
