# frozen_string_literal: true
module ProductionPlan
  class NextZoneDemand
    attr_reader :inventory_item

    def initialize(product_inventory_item)
      @inventory_item = product_inventory_item
    end

    def value
      @value ||= breakdown.values.sum
    end

    # Returns a hash like: { "Pack": 500 }, where
    # "Pack" is the next zone, and 500 is the demand for this zone.
    def breakdown
      return {} unless inventory_item.product?
      @breakdown ||= next_zone_demand_breakdown_for_ship if inventory_item.in_ship_zone?
      @breakdown ||= next_zone_demand_breakdown_for_make_pack
    end

    private

    def next_zone_demand_breakdown_for_ship
      product = inventory_item.product
      demand = product.reserved * product.production_buffer_days

      # If there is no inventory on Amazon, ship at least one case.
      demand = [demand, product.items_per_case].max if inventory_item.zero_inventory?

      { "FBA" => demand }
    end

    def next_zone_demand_breakdown_for_make_pack
      product = inventory_item.product
      next_zone = inventory_item.production_zone
      next_inventory_item = product.zone_inventory_item(next_zone)
      return {} unless next_inventory_item
      demand = ProductionPlan::Plan.build(next_inventory_item).to_cover
      { next_zone.name => demand }
    end
  end
end
