# frozen_string_literal: true
module ProductionPlan
  class Plan
    attr_reader :inventory_item
    attr_reader :demand_calculator, :supply_calculator, :labour_calculator

    delegate :in_make_zone?, to: :inventory_item

    def self.build(inventory_item)
      demand_calculator = ProductionPlan::Demand.build(inventory_item)

      supply_calculator =
        ProductionPlan::Supply.new(inventory_item.product, inventory_item.zone) if inventory_item.product?

      labour_calculator = ProductionPlan::Labour.new(inventory_item)

      new(inventory_item, demand_calculator, supply_calculator, labour_calculator)
    end

    def initialize(inventory_item, demand_calculator, supply_calculator, labour_calculator)
      @inventory_item = inventory_item
      @demand_calculator = demand_calculator
      @supply_calculator = supply_calculator
      @labour_calculator = labour_calculator
    end

    # Inventory quantity - how many product/material items we have in stock at the moment.
    def current_inventory
      @current_inventory ||= inventory_item.inventory_quantity
    end

    concerning :Demand do
      # Pure demand - how many product/material items we ideally want to have at the moment
      # to cover all related production buffers.
      def demand
        demand_calculator.value
      end

      def demand_breakdown
        demand_calculator.breakdown
      end

      def as_ingredient_demand_breakdown
        demand_calculator.as_ingredient_breakdown
      end

      def next_zone_demand_breakdown
        demand_calculator.next_zone_breakdown
      end

      def demand_coverage
        return 100 if demand.zero?
        (100 * current_inventory / demand).to_i
      end
    end

    concerning :Supply do
      # The maximum amount of product we could technically produce from the inventory we have at the moment.
      # The maximum number of material we could buy (returns "infinity" for materials).
      def supply
        supply_calculator&.value || 2**64
      end

      def supply_breakdown
        supply_calculator&.breakdown
      end
    end

    # How much product we need to add to the current inventory (produce) to cover the demand.
    # How much material we need to buy on top of the current inventory to cover the demand.
    def to_cover
      [0, demand - current_inventory].max
    end

    # How many of the "to_cover" product we can produce from the current materials/inventory.
    # For materials, returns "to_cover" value.
    def can_cover
      return nil unless inventory_item
      [to_cover, supply].min
    end

    # Rounds "can_cover" to full cases:
    # up if there is enough inventory to produce more, down otherwise.
    # Only applicable to products, but for materials there may be purchase barrels in future, too.
    def can_cover_in_cases
      return nil unless inventory_item&.product?
      @can_cover_in_cases ||=
        ProductAmount.new(can_cover).cases_ceil(inventory_item.product_items_per_case, supply)
    end

    # For materials, the same as :can_cover
    # For products in make zone, the same as :can_cover, too.
    # For products in other zones, returns :can_cover_in_cases converted back
    # to items (generally not equal to :can_cover).
    def can_cover_optimal
      return can_cover if inventory_item.material? || in_make_zone? || !can_cover_in_cases
      can_cover_in_cases * inventory_item.product_items_per_case
    end

    def fully_supplied?
      to_cover <= supply
    end

    def deficit_in_previous_zone?
      return false if fully_supplied?
      supply_calculator&.previous_zone_is_least_supplied?
    end

    def deficit_inventoriable
      return nil if fully_supplied? || deficit_in_previous_zone?
      ingredient_id = supply_calculator&.least_supplied_ingredient_id
      ingredient = Ingredient.find_by id: ingredient_id

      # we're doing bad query here but this one used only for reports
      ingredient&.inventory_item&.inventoriable
    end
  end
end
