# frozen_string_literal: true
module ProductionPlan
  class Supply
    attr_reader :product, :zone, :product_stats

    def initialize(product, zone)
      @product = product
      @zone    = zone
      @product_stats = Products::ProductInventoryStats.new(product)
    end

    # Tells how many product we can produce now
    # from the current material/product inventory.
    def value
      @value ||= breakdown.values.map(&:values).flatten.min
    end

    def value_for_ingredient(ingredient)
      breakdown.dig(:ingredients, ingredient.id)
    end

    def breakdown
      @breakdown ||=
        {
          ingredients: ingredient_supply_breakdown,
          previous_zone: previous_zone_supply_breakdown
        }
    end

    # Returns true if the previous zone inventory is the bottleneck
    # that determines the supply value.
    def previous_zone_is_least_supplied?
      value == previous_zone_supply_breakdown.values.first
    end

    # If supply bottleneck is an ingredient, returns its id.
    def least_supplied_ingredient_id
      return nil if previous_zone_is_least_supplied?
      ingredient_supply_breakdown.rassoc(value).first
    end

    # Returns { ingredient_id => supply, ... }, where
    # ingredient_id stands for the product ingredient,
    # supply is how many product we could produce if it only consisted of this single ingredient.
    def ingredient_supply_breakdown
      @ingredient_breakdown ||=
        begin
          ingredients = product.ingredients_for_zone(zone).includes(inventory_item: :inventory)
          ingredients.map { |ing| [ing.id, ing.supply] }.to_h
        end
    end

    # Returns something like { "Make" => 500 },
    # meaning we have 500pc of the product as made inventory.
    def previous_zone_supply_breakdown
      @previous_zone_breakdown ||=
        begin
          zone_workflow = ZoneWorkflow.new(product.zones)
          previous_zone = zone_workflow.previous_zone(zone)
          zone_supply_breakdown previous_zone
        end
    end

    def zone_supply_breakdown(zone)
      return {} unless zone
      supply = product_stats.zone_inventory_quantity(zone).to_i
      { zone.name => supply }
    end
  end
end
