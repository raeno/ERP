# frozen_string_literal: true

# Inventory totals among the given set of products.
module Products
  class AggregatedStats
    attr_reader :products
    attr_reader :stats

    def initialize(products)
      @products = products
      @stats = products.map { |product| Products::ProductInventoryStats.new(product) }
    end

    def average_selling_price
      products.average(:selling_price)
    end

    def total_inventory
      stats.sum(&:total_inventory)
    end

    def total_reserved
      stats.sum(&:reserved)
    end

    def total_inventory_price
      stats.sum(&:total_inventory_price)
    end

    def total_inventory_cogs
      stats.sum(&:total_inventory_cogs)
    end

    def total_inventory_profit
      stats.sum(&:total_inventory_profit)
    end

    def total_reserved_quantity_profit
      stats.sum(&:reserved_quantity_profit)
    end
  end
end
