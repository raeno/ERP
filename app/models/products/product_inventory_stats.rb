# frozen_string_literal: true
module Products
  class ProductInventoryStats
    attr_reader :product

    def initialize(product)
      @product = product
    end

    concerning :AmazonInventoryDelegation do
      included do
        delegate :amazon_inventory, :reserved, to: :product
      end

      def total_amazon_inventory
        amazon_inventory&.total_amazon_inventory || 0
      end

      def inbound_working
        amazon_inventory&.inbound_working || 0
      end

      def inbound_shipped
        amazon_inventory&.inbound_shipped || 0
      end

      def fulfillable
        amazon_inventory&.fulfillable || 0
      end
    end

    concerning :AmazonInventories do
      def fba_total
        inbound_shipped + fulfillable
      end
    end

    concerning :LocalInventoryDelegation do
      included do
        delegate :inventories, :zone_inventory, :zone_inventory_item,
          :make_inventory_item, :pack_inventory_item, :ship_inventory_item,
          to: :product
      end

      def zone_inventory_quantity(zone)
        zone_inventory(zone)&.quantity || 0
      end

      def made_inventory_quantity
        zone_inventory_quantity(Zone.make_zone)
      end

      def packed_inventory_quantity
        zone_inventory_quantity(Zone.pack_zone)
      end

      def made_plus_packed_quantity
        made_inventory_quantity + packed_inventory_quantity
      end
    end

    concerning :LocalInventories do
      def packed_case_quantity
        cache_under_product 'packed_case_quantity' do
          ProductAmount.new(packed_inventory_quantity).cases_floor(product.items_per_case)
        end
      end
    end

    concerning :CommonInventories do
      def total_inventory
        cache_under_product 'total_inventory' do
          [total_amazon_inventory, packed_inventory_quantity].sum
        end
      end
    end

    concerning :Costs do
      def made_inventory_total_cogs
        make_inventory_item&.total_prime_cost || 0
      end

      def packed_inventory_total_cogs
        pack_inventory_item&.total_prime_cost || 0
      end

      def made_plus_packed_inventory_total_cogs
        made_inventory_total_cogs + packed_inventory_total_cogs
      end

      def total_inventory_cogs
        cache_under_product 'total_inventory_cogs' do
          ProductAmount.new(total_inventory).cost(product.cogs)
        end
      end

      def inbound_shipped_cost_total
        cache_under_product 'inbound_shipped_cost_total' do
          ProductAmount.new(inbound_shipped).cost(product.selling_price)
        end
      end

      def fulfillable_cost_total
        cache_under_product 'fulfillable_cost_total' do
          ProductAmount.new(fulfillable).cost(product.selling_price)
        end
      end

      def fba_cost_total
        inbound_shipped_cost_total + fulfillable_cost_total
      end

      def total_inventory_price
        cache_under_product 'total_inventory_price' do
          ProductAmount.new(total_inventory).cost(product.selling_price)
        end
      end

      def total_inventory_profit
        cache_under_product 'total_inventory_profit' do
          ProductAmount.new(total_inventory).cost(product.profit)
        end
      end

      def reserved_quantity_profit
        cache_under_product 'reserved_quantity_profit' do
          ProductAmount.new(reserved).cost(product.profit)
        end
      end
    end

    private

    def cache_under_product(key)
      Rails.cache.fetch ['products', product, key], expires: 24.hours do
        yield
      end
    end
  end
end
