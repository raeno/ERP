# frozen_string_literal: true
# == Schema Information
#
# Table name: products_shipment_reports
#
#  id                        :integer          not null, primary key
#  product_id                :integer
#  packed_quantity           :integer
#  packed_cases_quantity     :integer
#  packed_quantity_in_days   :float
#  days_of_cover             :float
#  total_amazon_inventory    :integer
#  amazon_coverage_ratio     :decimal(, )
#  can_ship                  :integer
#  can_ship_in_cases         :integer
#  can_ship_in_days          :float
#  to_allocate_item_quantity :integer
#  to_allocate_case_quantity :integer
#  items_fba_allocation      :json
#  cases_fba_allocation      :json
#  items_shipment_priority   :integer          default(0)
#  cases_shipment_priority   :integer          default(0)
#

module Products
  # define prefix for module here while we have only one class under namespace
  def self.table_name_prefix
    'products_'
  end

  class ShipmentReport < ApplicationRecord
    belongs_to :product
    self.per_page = 30

    delegate :items_per_case, to: :product

    scope :ordered, -> { order('cases_shipment_priority DESC, packed_cases_quantity DESC, products_shipment_reports.product_id ASC') }
    scope :active, -> { joins(:product).merge(Zone.ship_zone.products.active) }

    def serializable_hash
      Products::ShipmentReportSerializer.new(self).serializable_hash
    end

    def allocate_items?
      allocate? items_shipment_priority
    end

    def allocate_cases?
      allocate? cases_shipment_priority
    end

    private

    def allocate?(priority)
      priority >= ShippingPriority::ALLOCATION_LOWER_LIMIT
    end
  end
end
