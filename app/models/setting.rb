# frozen_string_literal: true
# This is model holding global application settings.
# When there are too many setting, migrate to an alternative solution:
# https://github.com/huacnlee/rails-settings-cached
# == Schema Information
#
# Table name: settings
#
#  id                        :integer          not null, primary key
#  aws_access_key_id         :string
#  aws_secret_key            :string
#  mws_marketplace_id        :string
#  mws_merchant_id           :string
#  address_name              :string
#  address_line1             :string
#  address_line2             :string
#  address_city              :string
#  address_state             :string
#  address_zip_code          :string
#  address_country           :string
#  created_at                :datetime
#  updated_at                :datetime
#  red_urgency_level_days    :integer          default(10), not null
#  yellow_urgency_level_days :integer          default(20), not null
#

class Setting < ApplicationRecord
  validates :red_urgency_level_days, :yellow_urgency_level_days,
            numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  after_update :expire_cache

  def self.instance
    Rails.cache.fetch 'setting', expires: 1.hour do
      Setting.first || Setting.new
    end
  end

  def amazon_config
    { aws_access_key_id: aws_access_key_id, aws_secret_access_key: aws_secret_key,
      merchant_id: mws_merchant_id, primary_marketplace_id: mws_marketplace_id }
  end

  def shipment_address
    params = { name: address_name,
               address_line_1: address_line1,
               city: address_city,
               state_or_province_code: address_state,
               postal_code: address_zip_code,
               country_code: address_country }
    Amazon::Structs::Address.new params
  end

  protected

  def expire_cache
    Rails.cache.delete("setting")
  end
end
