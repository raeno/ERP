# == Schema Information
#
# Table name: shipment_entries
#
#  id                 :integer          not null, primary key
#  amazon_shipment_id :integer
#  product_id         :integer
#  quantity           :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  status             :string           default("NEW")
#  batch_id           :integer
#

# ShipmentEntry has both batch and product entry as associations but
# product is always present and batch is created only on last stage of
# processing
class ShipmentEntry < ApplicationRecord
  belongs_to :product
  belongs_to :amazon_shipment
  belongs_to :batch

  delegate :cases_required?, :fba_warehouse_name, to: :amazon_shipment
  delegate :seller_sku, to: :product

  scope :need_sync_with_amazon, -> { where(status: ['NEW', 'CHANGED'])}
  scope :need_sync_batches, -> { where(status: 'AMAZON_SYNCED')}
  scope :synced, -> { where(status: 'SYNCED')}

  # TODO: extract to validaiton schema
  validates :product_id, presence: true
  validates :quantity, presence: true, numericality: { greater_than: 0 }

  def was_changed?
    status_changed? && status == 'CHANGED'
  end

  def quantity_in_case
    return nil unless cases_required?
    product.items_per_case
  end

  def shipment_type
    cases_required? ? 'cases' : 'items'
  end

  def in_cases(quantity)
    ProductAmount.new(quantity).cases_floor(quantity_in_case)
  end

  def to_h
    { seller_sku: seller_sku, quantity: items_quantity, quantity_in_case: quantity_in_case }
  end

  def items_quantity
    return quantity unless cases_required?
    quantity * quantity_in_case
  end

end
