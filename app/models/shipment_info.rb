class ShipmentInfo
  attr_reader :shipment_id, :header, :items

  def initialize(shipment_id, header, items)
    @shipment_id = shipment_id
    @header = header
    @items = items
  end

  def self.from_shipment(amazon_shipment, shipment_address)
    entries = amazon_shipment.shipment_entries.need_sync_with_amazon
    items = build_shipment_items entries

    shipment_id = amazon_shipment.shipment_id
    header = Amazon::Structs::InboundShipmentHeader.build(amazon_shipment, shipment_address)

    new(shipment_id, header, items)
  end

  private

  def self.build_shipment_items(entries)
      items = entries.map(&:to_h)
      items.each do |item|
        item[:quantity_shipped] = item.delete(:quantity)
      end
      items
    end
end
