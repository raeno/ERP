class ShipmentItem
  attr_reader :product, :quantity

  delegate :seller_sku, to: :product

  def self.build(product, quantity, shipment_type)
    klass = shipment_type == 'cases' ? CasesShipmentItem : self
    klass.new product, quantity
  end

  def self.from_params(params)
    product = Product.find_by id: params[:product_id]
    return nil unless product

    quantity = params[:quantity]&.to_i || 0
    shipment_type = params[:shipment_type] || 'cases'
    build product, quantity, shipment_type
  end

  def initialize(product, quantity)
    @product = product
    @quantity = quantity
  end

  def shipment_type
    'items'
  end

  def shipment_in_cases?
    false
  end

  def to_h
    { seller_sku: seller_sku, quantity: quantity }
  end
end
