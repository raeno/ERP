# frozen_string_literal: true
class ShipmentReportBuilder
  attr_reader :product, :report

  PRODUCT_FIELDS = [
    :to_allocate_item_quantity, :to_allocate_case_quantity,
    :days_of_cover, :amazon_coverage_ratio
  ].freeze

  STATS_FIELDS = {
    packed_quantity: :packed_inventory_quantity,
    packed_cases_quantity: :packed_case_quantity
  }.freeze

  def initialize(existing_report = nil)
    @report = existing_report || Products::ShipmentReport.new
  end

  def with_product_information(product)
    @product = product
    report.product = product
    copy_product_fields
    calculate_coverage
    calculate_shipment_priority
  end

  def with_items_allocation(allocation)
    report.items_fba_allocation = allocation
  end

  def with_cases_allocation(allocation)
    report.cases_fba_allocation = allocation
  end

  private

  def inventory_item
    return nil unless product
    @inventory_item ||= product.ship_inventory_item
  end

  def production_report
    return nil unless inventory_item
    @production_report ||= inventory_item.production_report
  end

  def copy_product_fields
    report.product = product

    product_stats = Products::ProductInventoryStats.new(product)

    PRODUCT_FIELDS.each do |method_name|
      report.send "#{method_name}=", product.send(method_name)
    end

    report.total_amazon_inventory = product_stats.total_amazon_inventory

    STATS_FIELDS.each do |report_method, product_method|
      report.send "#{report_method}=", product_stats.send(product_method)
    end
  end

  def calculate_shipment_priority
    report.items_shipment_priority = shipment_priority product, :items
    report.cases_shipment_priority = shipment_priority product, :cases
  end

  def calculate_coverage
    report.can_ship = production_report.can_cover
    report.can_ship_in_cases = production_report.can_cover_in_cases
  end

  def shipment_priority(product, shipment_type)
    ShippingPriority.new(product, shipment_type: shipment_type).priority
  end
end
