# frozen_string_literal: true
class ShippingPriority
  attr_reader :product, :shipment_type

  # numbers here dont have any hidden meaning
  # they are used just to project many [float, boolean] pairs on linear function
  # so we can sort products by this only number
  MAX_COVERAGE_PRIORITY = 1000
  ALLOCATION_LOWER_LIMIT = 10_000
  ALLOCATION_UPPER_LIMIT = 1_000_000

  def initialize(product, shipment_type: :cases)
    @product = product
    @shipment_type = shipment_type
  end

  def priority
    coverage_priority * available_amount_coefficient
  end

  def allocate?
    priority >= ALLOCATION_LOWER_LIMIT
  end

  private

  def available_amount_coefficient
    return 0 unless product.ship_inventory_item
    production_plan = ProductionPlan::Plan.build(product.ship_inventory_item)

    can_cover = in_cases? ? production_plan.can_cover_in_cases : production_plan.can_cover
    can_cover.positive? ? 1000 : 1
  end

  def in_cases?
    shipment_type == :cases
  end

  def coverage_priority
    coverage = [product.amazon_coverage_ratio.to_f, 0.0].max
    priority = [1000 / coverage, MAX_COVERAGE_PRIORITY].min
    [1, priority].max
  end
end
