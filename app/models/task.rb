# frozen_string_literal: true
# == Schema Information
#
# Table name: tasks
#
#  id                      :integer          not null, primary key
#  inventory_item_id       :integer          not null
#  started_at              :datetime
#  finished_at             :datetime
#  quantity                :integer
#  created_at              :datetime
#  updated_at              :datetime
#  quantity_cases_obsolete :integer
#  labour_cost             :decimal(, )
#  user_id                 :integer          not null
#  duration_estimate       :integer
#  date                    :date
#  product_items_per_case  :integer
#
# Foreign Keys
#
#  fk_rails_...  (inventory_item_id => inventory_items.id)
#

# The main purpose of a task (vs batch) is to track time and labour
class Task < ApplicationRecord
  include Tasks::StatusAndDuration

  belongs_to :inventory_item
  has_one :batch
  belongs_to :user

  validates :user_id, :inventory_item_id, presence: true

  scope :ordered, -> { ordered_by_id }
  scope :ordered_by_id, -> { order(id: :desc) }
  scope :ordered_by_finished_at, -> { order(finished_at: :desc) }
  scope :for_inventory_item, ->(ii_id) { where(inventory_item_id: ii_id) }
  scope :for_product, ->(product) { where(inventory_item_id: product.inventory_item_ids) }
  scope :for_user, ->(user_id) { where(user_id: user_id) }
  scope :for_date,   ->(date) { where(date: date) }
  scope :till_date,  ->(date) { where('date <= ?', date) }
  scope :after_date, ->(date) { where('date >= ?', date) }
  scope :in_zone, ->(zone_id) { includes(:inventory_item).where(inventory_items: { zone_id: zone_id }) }

  delegate :zone, :zone_id, :product, to: :inventory_item, allow_nil: true
  delegate :id, to: :product, prefix: true, allow_nil: true
  delegate :image_url, to: :product, prefix: true
  delegate :make?, :pack?, to: :zone, allow_nil: true
  delegate :name, to: :zone, prefix: true, allow_nil: true

  # Sort by task date, then by product days_of_cover within the same date,
  # and then by product title to produce the same order between queries
  def self.ordered_by_priority
    joins(inventory_item: { product: :shipment_report })
      .order('date ASC, products_shipment_reports.days_of_cover ASC, products.title ASC')
  end

  def self.within_date_range(date_range)
    tasks = all
    return tasks unless date_range
    tasks = tasks.after_date(date_range.start_date) if date_range.start?
    tasks = tasks.till_date(date_range.end_date) if date_range.end?
    tasks
  end

  def product_name
    inventory_item.name
  end

  concerning :ItemsAndCases do
    # All task quantities ares stored in items - because this way we can estimate
    # task duration and II items_per second, compare employees' performance
    # (in bottles it is easier than in various-sized cases), etc.
    # Still, for pack zone, users want to see and input everything in cases.
    # So we are keeping :quantity_cases as a virtual attribute
    # that is calculated based on :quantity (in items) and :items_per_case.

    # Items per case is copied from Product to each task for a record.
    # When product.items_per_case number suddenly changes, all previous tasks
    # will still convert quantity to cases correctly (will not have 7.1253999 cases).

    # 'unit' for make zone, 'case' for other zones
    delegate :zone_unit_name, to: :inventory_item

    # In items for make, in cases for pack
    def quantity_in_zone_units
      pack? ? quantity_cases : quantity
    end

    def quantity_in_zone_units=(val)
      if pack?
        self.quantity_cases = val
      else
        self.quantity = val
      end
    end

    def quantity_cases
      return quantity_cases_obsolete if quantity_cases_obsolete&.nonzero?
      @quantity_cases ||= ProductAmount.new(quantity).cases_floor(items_per_case_guaranteed)
    end

    def quantity_cases=(val)
      self.quantity = val.to_i * items_per_case_guaranteed
    end

    # self.product_items_per_case is usually non-nil.
    # But for new task records, for quantity_cases setter, we sometimes need this.
    def items_per_case_guaranteed
      product_items_per_case || inventory_item.product_items_per_case
    end
  end

  concerning :Batches do
    def batch_quantity
      @batch_quantity ||= batch&.quantity || 0
    end

    def batch_quantity_cases
      @batch_quantity_cases ||= batch&.case_quantity || 0
    end

    def quantity_remainder
      return unless quantity
      quantity - batch_quantity
    end

    def coverage
      return unless batch_quantity && quantity&.nonzero?
      batch_quantity.to_f / quantity
    end

    def batch_default_attributes
      {
        product_id: product.id,
        zone_id: zone_id,
        quantity: quantity,
        task_id: id,
        user_ids: [user_id]
      }
    end

    # Used to init another task after this one is finished.
    def remainder_task_attributes
      {
        quantity: quantity_remainder,
        date: date,
        inventory_item_id: inventory_item_id,
        user_id: user_id
      }
    end
  end

  concerning :Labour do
    included do
      delegate :labour_rate, to: :user
    end

    def unit_labour_cost
      return unless labour_cost && batch_quantity&.nonzero?
      labour_cost / batch_quantity
    end

    def set_labour_cost
      return unless labour_rate && duration_hours
      self.labour_cost = labour_rate * duration_hours
    end
  end
end
