# frozen_string_literal: true
# == Schema Information
#
# Table name: units
#
#  id        :integer          not null, primary key
#  name      :text
#  full_name :text
#

class Unit < ApplicationRecord
  has_many :materials
  has_many :from_unit_conversion_rates, class_name: "ConversionRate", foreign_key: :from_unit_id
  has_many :to_unit_conversion_rates,   class_name: "ConversionRate", foreign_key: :to_unit_id

  def self.each_piece
    Rails.cache.fetch 'units/each_piece_unit', expires: 1.hour do
      Unit.find_by name: 'Each'
    end
  end
end
