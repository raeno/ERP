# frozen_string_literal: true

class UnitConversion
  def self.units_convertable_to(unit)
    ConversionRate.to_unit(unit).map(&:from_unit) +
      ConversionRate.from_unit(unit).map(&:to_unit) +
      [unit]
  end

  attr_reader :from_unit, :to_unit

  def initialize(from_unit, to_unit)
    @from_unit = from_unit
    @to_unit = to_unit
  end

  def rate
    @rate ||= find_conversion_rate
  end

  def valid?
    rate.present?
  end

  def identic?
    from_unit == to_unit
  end

  def convert(amount)
    raise "Incompatible units of measure" unless valid?
    amount * rate
  end

  private

  def find_conversion_rate
    return 1 if identic?

    rate = find_conversion_record(from_unit, to_unit)&.rate
    return rate if rate

    inverse_rate = find_conversion_record(to_unit, from_unit)&.rate
    return nil unless inverse_rate&.nonzero?
    1 / inverse_rate
  end

  def find_conversion_record(from, to)
    Rails.cache.fetch "conversion_rates/#{from.id}/#{to.id}", expires: 1.hour do
      ConversionRate.from_unit(from).to_unit(to).first
    end
  end
end
