# frozen_string_literal: true

class UserWorkload
  attr_reader :data

  def initialize
    @data = init_data
  end

  # Returns total unfinished tasks' duration (in seconds) for the given user, for the given date.
  def workload_for(date, user_id)
    return nil unless data[date]
    workload = data[date][user_id]
    # ignoring zero values (less than 30 seconds)
    return nil unless workload&.nonzero? && workload >= 30
    workload
  end

  def dates
    data.keys.sort
  end

  def users
    User.active.ordered
  end

  private

  def init_data
    data = {}

    Task.unfinished.find_each do |task|
      date = task.date
      user_id = task.user_id
      data[date] ||= {}
      data[date][user_id] ||= 0
      data[date][user_id] += task.duration_estimate
    end

    data
  end
end
