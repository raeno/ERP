# frozen_string_literal: true
# == Schema Information
#
# Table name: vendors
#
#  id                      :integer          not null, primary key
#  name                    :string           not null
#  contact_name            :string
#  phone                   :string
#  contact_email           :string
#  contact_fax             :string
#  order_email             :string
#  order_fax               :string
#  tags                    :string
#  address                 :string
#  notes                   :text
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  website                 :string
#  lead_time_days_obsolete :integer          default(0), not null
#  lead_time_days          :integer
#

class Vendor < ApplicationRecord
  has_many :invoices
  has_many :material_vendors
  has_many :materials, through: :material_vendors

  validates_presence_of :name
  validates :website, :format => /(^$)|(^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?$)/ix

  scope :ordered, -> { order(:id) }
end
