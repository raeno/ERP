# frozen_string_literal: true
# == Schema Information
#
# Table name: zones
#
#  id               :integer          not null, primary key
#  name             :string
#  production_order :integer
#  created_at       :datetime
#  updated_at       :datetime
#

class Zone < ApplicationRecord
  include Comparable

  has_many :product_zone_availabilities, dependent: :destroy
  has_many :products, through: :product_zone_availabilities
  has_many :inventory_items, dependent: :destroy
  has_many :batches, dependent: :destroy

  has_many :components, class_name: 'InventoryItem', foreign_key: :production_zone_id

  default_scope { order(:name) }
  scope :ordered, -> { order(production_order: :asc) }
  scope :starting_from, -> (zone) { where('production_order >= ?', zone.production_order).ordered }

  delegate :with_positive_amount, to: :components, prefix: true

  def slug
    name.downcase
  end

  def active_products
    products.active.includes(:amazon_inventory, :zones)
  end

  def inactive_products
    products.inactive.includes(:amazon_inventory)
  end

  def self.latest_change
    maximum(:updated_at)
  end

  def self.default_zone
    Zone.make_zone
  end

  def self.make_zone
    Zone.find_by name: 'Make'
  end

  def self.pack_zone
    Zone.find_by name: 'Pack'
  end

  def self.ship_zone
    Zone.find_by name: 'Ship'
  end

  def self.fetch_or_default(zone_id)
    find_by(id: zone_id) || default_zone
  end

  def make?
    name == 'Make'
  end

  def pack?
    name == 'Pack'
  end

  def ship?
    name == 'Ship'
  end

  def <=>(other)
    production_order <=> other.production_order
  end
end
