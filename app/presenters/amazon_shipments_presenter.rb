class AmazonShipmentsPresenter
  attr_reader :filter

  def initialize(shipments, filter)
    @shipments = shipments
    @filter = filter || 'all'
  end

  def shipments
    @presenters ||= @shipments.map { |shipment| ShipmentPresenter.new(shipment) }
  end

  def filter_css_class(filter_name)
    filter_name == filter ? 'active' : ''
  end
end
