# frozen_string_literal: true
class ApplicationPresenter
  include ActiveModel::Serializers::JSON
end
