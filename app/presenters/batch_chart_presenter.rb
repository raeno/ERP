# frozen_string_literal: true
class BatchChartPresenter
  attr_reader :start_date, :end_date, :zones

  def initialize(start_date:, end_date:, zones:)
    @start_date = start_date
    @end_date = end_date
    @zones = zones
  end

  def chart_data
    batches = fetch_batches
    days = batches.keys.map(&:to_s)
    chart_data = prepare_data(batches)
    DataStruct.new days: days, chart_data: chart_data
  end

  def as_json(_options = {})
    chart_data.to_h
  end

  private

  def fetch_batches
    Batch.for_interval(start_date, end_date)
         .ordered_by_completion
         .group_by { |record| [record.completed_on] }
  end

  def prepare_data(batches)
    zones.map do |zone|
      data = batches.map do |_one_day, records|
        records.select { |record| record.zone_id == zone.id }.sum(&:quantity)
      end
      { name: zone.name, data: data }
    end
  end
end
