# frozen_string_literal: true
class BatchPresenter
  attr_reader :batch

  delegate :product, :product_id, :task, :task_id,
    :zone, :zone_id, :make_zone?, :pack_zone?,  :ship_zone?,
    :users,
    :quantity, :case_quantity, :notes, :completed_on, :shipment_id,
    to: :batch

  delegate :name, to: :zone, prefix: true

  def initialize(batch)
    @batch = batch
  end

  def product_image_url
    batch.product&.small_image_url || ''
  end

  def product_title
    batch.product&.internal_title
  end

  def active_users
    User.active
  end

  def main_quantity
    return quantity if make_zone?
    case_quantity
  end

  def main_quantity_unit
    unit = make_zone? ? 'unit' : 'case'
    unit.pluralize(main_quantity)
  end
end
