# frozen_string_literal: true
class DailyTasksPresenter
  attr_reader :tasks, :date

  def initialize(tasks, date)
    @tasks = tasks
    @date = date
  end

  def task_presenters
    tasks.map { |task| TaskPresenter.new(task) }
  end

  def total_hours
    tasks.sum(&:duration_estimate_hours).round
  end

  def overdue?
    date < Time.zone.today
  end

  def date_title
    title = date.to_s(:date_month)
    title = "Overdue: #{title}" if overdue?
    title = "Today: #{title}" if date&.today?
    title
  end

  def title_css_class
    overdue? ? 'overdue' : ''
  end
end
