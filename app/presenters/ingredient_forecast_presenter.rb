# frozen_string_literal: true
class IngredientForecastPresenter
  include Generic::FormattingHelper

  attr_reader :ingredient, :ingredient_presenter
  attr_reader :product_production_report

  delegate :inventoriable, :product?, :material?,
    :inventory_item, :inventory_item_name,
    :small_unit_name, :main_unit_name, :quantity_with_unit,
    :cost, :cost_percent, :prime_cost_change_percent,
    :material_presenter,
    to: :ingredient_presenter

  delegate :quantity, to: :ingredient, prefix: true
  delegate :inventory_quantity, to: :ingredient

  def initialize(ingredient, product_production_report)
    @ingredient = ingredient
    @product_production_report = product_production_report
    @ingredient_presenter = IngredientPresenter.new(ingredient)
  end

  def previous_cost
    unit_cost = inventory_item.previous_prime_cost
    return nil unless unit_cost
    ingredient_quantity * unit_cost
  end

  def current_inventory_with_unit
    with_unit_name inventory_quantity
  end

  def current_inventory_in_products
    integer_with_delimiter to_products inventory_quantity
  end

  # Only relevant to gift boxes at the moment.
  # How many bottles of oil can be made soon.
  # Future: for materials, it can be pending material amount
  #   that is ordered but not yet received.
  def in_process
    return 0 unless inventory_item.product?
    inventory_item_presenter.can_cover
  end

  def in_process_with_unit
    return nil if in_process.zero?
    with_unit_name in_process
  end

  def in_process_products
    integer_with_delimiter to_products in_process
  end

  def to_cover
    product_production_report.to_cover * ingredient_quantity
  end

  def to_cover_with_unit
    with_unit_name to_cover
  end

  def to_cover_products
    integer_with_delimiter product_production_report.to_cover
  end

  def can_cover
    [inventory_quantity, to_cover].min
  end

  def can_cover_with_unit
    with_unit_name can_cover
  end

  def can_cover_products
    integer_with_delimiter to_products can_cover
  end

  def shortfall
    inventory_quantity + in_process - to_cover
  end

  def shortfall_with_unit
    shortfall < 0 ? with_unit_name(shortfall) : ''
  end

  def shortfall_products
    result = to_products shortfall
    return '' unless result < 0
    integer_with_delimiter result
  end

  def inventory_item_presenter
    InventoryItemPresenter.new(inventory_item)
  end

  private

  def with_unit_name(number)
    with_unit number, main_unit_name
  end

  def to_products(material_amount)
    return 0 if ingredient_quantity.zero?
    (material_amount / ingredient_quantity).to_i
  end
end
