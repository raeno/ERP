# frozen_string_literal: true
class IngredientPresenter
  include Generic::FormattingHelper

  attr_reader :ingredient, :zones

  delegate :inventory_item_zone, :small_unit_name, :main_unit_name,
           :inventory_item_name, :inventory_item, :cost, :quantity_in_small_units,
           :inventoriable, :product?, :material?,
           to: :ingredient

  delegate :prime_cost_change_percent, to: :inventory_item

  def initialize(ingredient)
    @ingredient = ingredient
    @zones = @ingredient.zones_available.includes(components: { material: :unit })
                        .includes(components: { product: { ingredients: :inventory_item } })
                        .includes(components: :inventoriable)
  end

  def components?(zone)
    components = zone_materials(zone) + zone_products(zone)
    components.any?
  end

  def zone_materials(zone)
    Rails.cache.fetch components_cache_key(zone, 'materials') do
      zone_components zone, -> (component) { component.material? }
    end
  end

  def zone_products(zone)
    Rails.cache.fetch components_cache_key(zone, 'products') do
      return [] unless zone.pack?
      zone_components zone, -> (component) { component.active_product? }
    end
  end

  def quantity_with_unit
    with_unit ingredient.quantity_in_small_units.round(4),
      ingredient.small_unit_name, convert_to_integer: false
  end

  def cost_percent
    "#{ingredient.cost_percent}%"
  end

  def material_presenter
    return nil unless inventory_item.material?
    @material_presenter ||= MaterialPresenter.new(inventory_item.material)
  end

  private

  def components_cache_key(zone, type)
    "edit_ingredients/#{type}/#{zone.name}-#{Zone.latest_change}"
  end

  def zone_components(zone, predicate)
    zone.components.select(&predicate).sort_by(&:name)
  end
end
