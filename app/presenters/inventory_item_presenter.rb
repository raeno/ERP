# frozen_string_literal: true
class InventoryItemPresenter
  include ActionView::Helpers
  include Rails.application.routes.url_helpers
  include Generic::FormattingHelper
  include Generic::LinksHelper

  delegate :inventoriable, :inventoriable_id,
           :material, :material?, :product, :product?,
           :inventory, :image_url,
           :zone, :zone_id, :zone_slug, :zone_name, :production_zone,
           :in_make_zone?, :in_pack_zone?, :in_ship_zone?,
           :name, :main_unit_name, :ingredient_unit_name, :zone_unit_name,
           :inventory_quantity,
           :prime_cost, :total_prime_cost, :zone_prime_cost, :labour_cost,
           :production_report, :production_seconds_per_unit, :production_setup_minutes,
           to: :inventory_item
  delegate :id, to: :inventory_item, prefix: true

  attr_reader :inventory_item

  def initialize(inventory_item)
    @inventory_item = inventory_item
  end

  def product_presenter
    return nil unless product?
    ProductPresenter.new(inventoriable)
  end

  def ingredient_forecast_presenters
    inventory_item.contained_ingredients_ordered.map do |ingredient|
      IngredientForecastPresenter.new(ingredient, production_report)
    end
  end

  def zones
    @zones ||= Zone.ordered
  end

  def units
    @units ||= Unit.all
  end

  def inventory_item_type
    inventory_item.nature
  end

  def current_type?(inventory_item_type)
    self.inventory_item_type == inventory_item_type
  end

  concerning :ProductionPlanning do
    delegate :production_urgency, :days_of_cover, :production_buffer_days,
      to: :product

    delegate :days_of_cover, to: 'product.shipment_report'

    delegate :demand_breakdown,
      :as_ingredient_demand_breakdown, :next_zone_demand_breakdown,
      to: :production_plan

    delegate :demand, :demand_coverage,
      :to_cover, :can_cover, :can_cover_in_cases, :assigned, :unassigned,
      :seconds_per_unit,
      to: :production_report

    delegate :time_estimate,
      to: :production_report, prefix: 'production'

    delegate :production_fully_assigned?, :current_tasks,
      to: :inventory_item

    def current_tasks_presenters
      current_tasks.includes(:user).map { |task| TaskPresenter.new(task) }
    end

    def production_plan
      @production_plan ||= ProductionPlan::Plan.build(inventory_item)
    end

    def current_inventory
      production_report.in_stock
    end

    def current_inventory_cases
      production_report.in_stock_cases
    end

    def current_inventory_in_main_units
      quantity_in_main_units current_inventory, current_inventory_cases
    end

    def production_plan_fully_supplied?
      production_report.fully_supplied?
    end

    def low_ingredients?
      !production_plan_fully_supplied?
    end

    def production_plan_supplied_css_class
      production_plan_fully_supplied? ? 'plan-supplied' : 'plan-unsupplied'
    end

    def demand_with_unit
      quantity_with_unit demand, nil
    end

    def demand_coverage_limited
      [demand_coverage, 100].min
    end

    def demand_coverage_color
      return 'red' if demand_coverage < 50
      return 'yellow' if demand_coverage < 100
      'green'
    end

    def product_days_of_cover_hint
      return unless product? && days_of_cover
      return 'infinite' if days_of_cover.infinite? # Float::INFINITY is incompatible with .to_i
      "#{days_of_cover.to_i} of #{production_buffer_days}"
    end

    def to_cover_cases
      return nil unless product?
      ProductAmount.new(to_cover).cases_ceil(product.items_per_case)
    end

    def to_cover_with_unit
      quantity_with_unit(to_cover, to_cover_cases)
    end

    def to_cover_in_main_units
      quantity_in_main_units to_cover, to_cover_cases
    end

    def to_cover_cost
      to_cover * inventory_item.prime_cost
    end

    def can_cover_in_main_units
      quantity_in_main_units can_cover, can_cover_in_cases
    end

    def assigned_cases
      return nil unless product?
      ProductAmount.new(assigned).cases_ceil(product.items_per_case)
    end

    def assigned_in_main_units
      quantity_in_main_units assigned, assigned_cases
    end

    def unassigned_cases
      return nil unless product?
      ProductAmount.new(unassigned).cases_ceil(product.items_per_case)
    end

    def unassigned_in_main_units
      quantity_in_main_units unassigned, unassigned_cases
    end

    def anything_to_cover?
      to_cover.positive?
    end

    def compound_demand?
      as_ingredient_demand_breakdown.any?
    end

    def deficit_ingredient_link(user)
      return if !product? || production_plan_fully_supplied?

      exclamation = content_tag :i, '', class: 'fa fa-exclamation-triangle'
      link_text = "#{exclamation} #{deficit_ingredient_name}".html_safe

      return link_text unless user.admin?
      link_to link_text, product, title: "Low material"
    end

    def deficit_ingredient_hint
      return if !product? || production_plan_fully_supplied?
      "Low on #{deficit_ingredient_name}"
    end

    def deficit_ingredient_name
      return 'previous zone' if production_report.previous_zone_deficit?
      production_report.deficit_ingredient_name
    end

    def production_time_estimate_hours
      production_report.time_estimate_hours.round
    end

    def urgency_css_class
      ["urgency-#{production_urgency}", production_plan_supplied_css_class].join(' ')
    end

    private

    def quantity_with_unit(qty_units, qty_cases)
      return with_unit(qty_units, main_unit_name) if material?
      return with_unit(qty_cases, zone_unit_name) if product? && in_pack_zone?
      return with_unit(qty_units, zone_unit_name)
    end

    def quantity_in_main_units(qty_units, qty_cases)
      quantity = product? && in_pack_zone? ? qty_cases : qty_units
      integer_with_delimiter quantity
    end
  end
end
