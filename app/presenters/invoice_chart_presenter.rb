# frozen_string_literal: true
class InvoiceChartPresenter
  def initialize(invoices)
    @invoices = invoices
  end

  def chart_data
    Rails.cache.fetch [@invoices, 'series_chart_data'] do
      grouped = invoices_by_month_by_status
      months = grouped.keys.map { |key| key.to_s(:month_year) }
      series = chart_series(grouped)
      DataStruct.new months: months, series: series
    end
  end

  def as_json(_ = {})
    chart_data.to_h
  end

  private

  # prepares series for highcharts
  # highcharts expects list of object each with 'name' and 'data' properties
  def chart_series(grouped_by_status)
    return {} if grouped_by_status.empty?
    data = grouped_by_status.values.first.keys.map do |status|
      [status, []]
    end.to_h

    grouped_by_status.values.each do |hash|
      hash.each { |status, price| data[status] << price.to_i }
    end
    data[:pending] = accumulate_total_to_latest_month(data[:pending])
    data = data.map { |status, values| { name: status, data: values } }
  end

  # group invoices by month first and then by status inside
  # uses a bit simplified "status" definition than original model
  # since status is based on business logic inside model,
  # we can't move it to DB and have to group in memory
  def invoices_by_month_by_status
    grouped_by_month = @invoices.group_by { |i| i.created_at.to_date.change(day: 1) }

    price_data = grouped_by_month.map do |month, invoices|
      initial_hash = { pending: 0, unpaid: 0, paid: 0 }

      sum_by_status = invoices.each_with_object(initial_hash) do |invoice, memo|
        status = simplified_invoice_status(invoice)
        memo[status] += invoice.total_price
      end
      [month, sum_by_status.to_h]
    end
    price_data.to_h
  end

  def accumulate_total_to_latest_month(data)
    array = Array.new(data.size - 1, 0)
    array << data.sum
  end

  # acts as invoice.status but does not distinguish
  # between :completed and :paid
  # also does not check total price matching
  def simplified_invoice_status(invoice)
    return :paid if invoice.paid?
    return :unpaid if invoice.received?
    :pending
  end
end
