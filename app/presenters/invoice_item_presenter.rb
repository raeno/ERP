# frozen_string_literal: true
class InvoiceItemPresenter
  include ActionView::Helpers::NumberHelper
  include Generic::FormattingHelper

  attr_reader :invoice_item, :material_container_presenter
  alias item invoice_item

  delegate :invoice, :invoice_id, :invoice_date,
    :vendor, :vendor_name,
    :quantity, :total_price,
    :received_date, :received?, :locked?,
    :container_unit_id, :material_unit_id, :material_unit,
    to: :invoice_item

  delegate  :unit_price, :material_unit_price,
            :material_name, :material, :external_name,
            :container_form,
            :pkg_amount_in_pkg_units, :amount_in_material_units,
            :price, :unit_price_explained,
            :lot_number, :expiration_date,
            to: :material_container_presenter

  delegate :id, to: :invoice_item, prefix: true

  def initialize(invoice_item)
    @invoice_item = invoice_item
    @material_container_presenter = MaterialContainerPresenter.new(invoice_item.material_container)
  end

  def materials_for_selection
    vendor ? vendor.materials.ordered_by_name : Material.none
  end

  def sizes_for_selection
    material ? material.ordering_sizes.ordered : OrderingSize.none
  end

  def price_change_percent
    material_container_presenter.material_price_change_percent
  end

  def pkg_unit_price
    material_container_presenter.unit_price
  end

  def in_material_units?
    container_unit_id == material_unit_id
  end

  def total_material_amount
    with_unit invoice_item.total_material_amount, material_unit.name
  end

  concerning :Form do
    def creating?
      item.new_record?
    end

    def editing?
      !creating?
    end

    def step_1_disabled?
      editing?
    end

    def step_2_hidden?
      creating?
    end
  end
end
