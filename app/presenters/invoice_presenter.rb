# frozen_string_literal: true
class InvoicePresenter
  attr_reader :invoice

  delegate :vendor, :vendor_name, :attachment_url, :date, :items,
    :total_price, :total_item_price, :total_price_matching?, :fees, :notes,
    :status, :paid_date, :paid?, :received_date, :received?, :locked?,
    to: :invoice
  delegate :id, to: :invoice, prefix: true

  def initialize(invoice)
    @invoice = invoice
  end

  def invoice_item_presenters
    items.order(:id).map { |item| InvoiceItemPresenter.new(item) }
  end

  def materials_summary
    items.map(&:material_name).join(', ').truncate(70)
  end

  def vendors_for_selection
    Vendor.all
  end

  def attachment?
    invoice.attachment_url.present?
  end

  def preview_url
    FilestackApi.new(attachment_url).viewer_url
  end

  def filestack_api_key
    FilestackApi.api_key
  end

  def attachment_button_text
    attachment? ? 'Change' : 'Upload'
  end

  def invoice_number
    inovice_number = invoice.number
    return "N/A" if inovice_number.blank?
    inovice_number
  end

  def display_paid_button?
    !paid? && total_price_matching?
  end

  def status_tip
    tips = []
    tips << "Total prices are not matching" unless total_price_matching?
    tips << "Paid on #{paid_date}" if paid?
    tips << "Received on #{received_date}" if received?
    tips.join('. ')
  end
end
