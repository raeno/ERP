class InvoiceVendorsChartPresenter
  def initialize(invoices)
    @invoices = invoices
  end

  def chart_data
    Rails.cache.fetch [@invoices, 'vendors_pie_chart_data'] do
      data = @invoices.group('vendors.name')
        .pluck('vendors.name AS vendor_name,
               SUM(invoices.total_price) AS spent')
      data.map { |entry| { name: entry[0], y: entry[1].to_f}} 
    end
  end
end
