# frozen_string_literal: true
class MaterialContainerPresenter
  include Generic::FormattingHelper

  attr_reader :material_container

  delegate :expiration_date, :lot_number, :material, :external_name,
    :container_form, :unit, :amount, :price, :unit_price,
    :status,
    :material_unit_price, :previous_container_material_unit_price,
    :material_price_change_percent,
    to: :material_container

  delegate :id, to: :material_container, prefix: true

  def initialize(material_container)
    @material_container = material_container
  end

  def unit_price_explained
    "1 #{material_unit.name} = #{number_to_currency material_unit_price}"
  end

  def amount_left
    number_with_precision(material_container.amount_left, precision: 2)
  end

  def amount_in_material_units
    with_unit material_container.amount_in_material_units, material_unit.name, convert_to_integer: false
  end

  def amount_left_in_material_units
    with_unit material_container.amount_left_in_material_units, material_unit.name
  end

  def pkg_amount_in_pkg_units
    with_unit material_container.amount.round(2), unit.name, convert_to_integer: false
  end

  def invoice
    material_container.invoice_item&.invoice
  end

  def highlight?
    material_container.active?
  end

  concerning :Material do
    delegate :material_name,
      to: :material_presenter

    def material_presenter
      @material_presenter ||= MaterialPresenter.new(material)
    end

    def material_unit
      material_presenter.unit
    end
  end
end
