class MaterialCostChartPresenter
  attr_reader :material

  def initialize(material)
    @material = material
  end

  def chart_data
    containers = containers_query.already_received_ordered

    days = containers.map(&:invoice_item).map(&:received_date)
                     .map { |data| data.to_s(:short_no_century) }
    costs = containers.map { |container| container.material_unit_price.to_f.round(3)}

    material_data = { name: material.name, data: costs }
    DataStruct.new days: days, series: [material_data]
  end

  def containers_query
    MaterialContainers::SameMaterialQuery.new(material)
  end

  def as_json(_options = {})
    chart_data.to_h
  end
end
