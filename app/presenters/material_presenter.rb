# frozen_string_literal: true
class MaterialPresenter
  include ActionView::Helpers::NumberHelper
  include ActionView::Helpers::TextHelper
  include Generic::FormattingHelper

  delegate  :material_vendors, :vendors, :vendor_ids, :category,
            :production_zone, :production_zone_id,
            :inventory_item, :inventory, :inventory_quantity,
            :unit, :unit_name, :ingredient_unit, :ingredient_unit_name, :unit_conversion_rate,
            :unit_price, :prime_cost, :price_change_percent,
            :attachments,
            to: :material

  delegate :name, :id, to: :material, prefix: true
  delegate :name, to: :category, prefix: true
  delegate :id, to: :inventory_item, prefix: true

  attr_reader :material

  def initialize(material)
    @material = material
  end

  def inventory_item_presenter
    @ii_presenter ||= InventoryItemPresenter.new(inventory_item)
  end

  def invoice_item_presenters
    material.invoice_items.ordered_by_invoice_date.map { |item| InvoiceItemPresenter.new(item) }
  end

  def image_url
    material&.image_url || ''
  end

  def material_container_presenters
    containers = material.material_containers.includes(:unit, :ordering_size, invoice_item: :invoice)
    ordered = MaterialContainers::OrderedByStatusQuery.new(containers).all
    ordered.map { |container| MaterialContainerPresenter.new(container) }
  end

  def transaction_presenters
    transactions = material.material_transactions
                           .ordered
                           .includes(:material_container, batch: [:zone, :product])
    transactions.map { |transaction| MaterialTransactionPresenter.new(material, transaction) }
  end

  def vendor_presenters
    vendors.ordered.map { |vendor| VendorPresenter.new(vendor) }
  end

  def vendors_comma_separated
    vendors.ordered.map(&:name).join(', ')
  end

  def inventory_quantity_with_unit
    with_unit inventory_quantity, unit_name
  end

  def vendors_for_selection
    @vendors ||= Vendor.ordered
  end

  def units_for_selection
    @units ||= Unit.all
  end

  def units_compatible
    @units_compatible ||= UnitConversion.units_convertable_to(unit)
  end

  def default_unit_conversion_rate
    return unless unit && ingredient_unit
    UnitConversion.new(unit, ingredient_unit).rate
  end

  def current_unit_conversion_rate
    MaterialUnitConversion.new(material).main_to_ingredient_rate
  end

  def units_with_rate
    "1 #{unit.name} = #{current_unit_conversion_rate} #{ingredient_unit.name}"
  end

  def categories_for_selection
    @categories ||= MaterialCategory.all
  end

  def zones_for_selection
    @zones ||= Zone.all
  end

  def ordering_sizes
    material.ordering_sizes.includes(:unit).ordered
  end

  concerning :ProductionPlanning do
    delegate :demand, :demand_with_unit,
     :demand_coverage, :demand_coverage_limited, :demand_coverage_color,
      to: :inventory_item_presenter

    def demand_breakdown
      inventory_item_presenter.demand_breakdown[:as_ingredient]
    end

    def to_purchase
      inventory_item_presenter.to_cover
    end

    def to_purchase_with_unit
      inventory_item_presenter.to_cover_with_unit
    end

    def anything_to_purchase?
      inventory_item_presenter.anything_to_cover?
    end

    def to_purchase_cost
      inventory_item_presenter.to_cover_cost
    end

    def filestack_api_key
      FilestackApi.api_key
    end

    def pending_invoice?
      material.invoice_items.pending.any?
    end

    private

    # lazy initialized
    def inventory_item_presenter
      @ii_presenter ||= InventoryItemPresenter.new(material.inventory_item)
    end
  end
end
