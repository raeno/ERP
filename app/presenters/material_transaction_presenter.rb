# frozen_string_literal: true
class MaterialTransactionPresenter
  include Generic::FormattingHelper
  attr_reader :material_transaction, :material

  delegate :batch, :material_container, :quantity,
           :inventory_amount, :created_at,
           to: :material_transaction

  delegate :product, :zone, to: :batch, allow_nil: true
  delegate :id, to: :batch, prefix: true, allow_nil: true

  delegate :lot_number, to: :material_container

  def initialize(material, material_transaction)
    @material = material
    @material_transaction = material_transaction
  end

  def transaction_date
    material_transaction.created_at.to_s(:short)
  end

  def product_name
    return '' unless batch
    batch.product.name
  end

  def product_quantity
    batch&.quantity
  end

  def material_quantity
    unit_name = material.unit.name
    "-#{with_unit quantity, unit_name}".html_safe
  end
end
