# frozen_string_literal: true
class MaterialsPresenter
  attr_reader :materials, :category_id, :zone_id

  def initialize(materials, category_id, zone_id)
    @materials = materials
    @category_id = category_id&.to_i
    @zone_id = zone_id&.to_i
  end

  def material_presenters
    materials.map { |material| MaterialPresenter.new(material) }
  end

  def active_category_css_class(category)
    (category.id == category_id) ? 'active' : ''
  end

  def active_zone_css_class(zone)
    (zone.id == zone_id) ? 'active' : ''
  end

  def active_no_filter_css_class
    (!zone_id && !category_id) ? 'active' : ''
  end

  def all_categories
    MaterialCategory.ordered
  end

  def all_zones
    Zone.ordered
  end
end
