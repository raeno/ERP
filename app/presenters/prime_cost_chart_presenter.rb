# frozen_string_literal: true
class PrimeCostChartPresenter
  attr_reader :product, :product_cogs_snapshots

  def initialize(product, product_cogs_snapshots)
    @product_cogs_snapshots = product_cogs_snapshots
    @product = product
  end

  def chart_data
    days = product_cogs_snapshots.map do |snapshot|
      created_date = snapshot.created_at.to_date
      created_date.to_s(:short_no_century)
    end

    prices = product_cogs_snapshots.map { |entry| entry.cogs.round(3) }
    name = product.name

    DataStruct.new name: name, days: days, prices: prices
  end

  def as_json(_options = {})
    chart_data.to_h
  end
end
