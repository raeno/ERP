# frozen_string_literal: true

# Product Details - Inventory and Forecast sections presenter.
class ProductInventoryPresenter
  include Generic::FormattingHelper
  attr_reader :product

  def initialize(product)
    @product = product
  end

  # Returns InventoryItemPresenters array ordered by zone.
  def inventory_item_presenters
    inventory_item_presenters_hash.sort.map(&:last).compact
  end

  def inventory_item_presenter(zone)
    inventory_item_presenters_hash[zone]
  end

  def make_inventory_item_presenter
    inventory_item_presenter(Zone.make_zone)
  end

  def pack_inventory_item_presenter
    inventory_item_presenter(Zone.pack_zone)
  end

  def ship_inventory_item_presenter
    inventory_item_presenter(Zone.ship_zone)
  end

  concerning :Inventory do
    def current_inventory(zone)
      inventory_item_presenter(zone).current_inventory
    end
  end

  concerning :ProductionPlanning do
    def anything_to_cover_in_zone?(zone)
      inventory_item_presenter(zone).anything_to_cover?
    end

    def can_ship
      ship_inventory_item_presenter.can_cover
    end

    def total_demand
      ship_inventory_item_presenter&.demand
    end
  end

  protected

  # lazy initialized
  def inventory_item_presenters_hash
    inventory_items = product.inventory_items
    inventory_items.map { |ii| [ii.zone, InventoryItemPresenter.new(ii)] }.to_h
  end
end
