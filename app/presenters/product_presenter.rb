# frozen_string_literal: true
class ProductPresenter
  attr_reader :product
  attr_reader :product_inventory_presenter
  attr_reader :product_stats_presenter
  attr_reader :shipment_report
  attr_reader :product_cogs_history

  delegate  :id, :size, :name, :title,
            to: :product, prefix: true

  delegate  :internal_title, :seller_sku, :asin, :sales_rank,
            :items_per_case, :reserved, :production_buffer_days,
            :has_make_zone?, :has_pack_zone?,
            :fnsku, :production_urgency, :image_url, :notes,
            :to_allocate_case_quantity, :to_allocate_item_quantity,
            :gift_boxes, :gift_box?, :product_components,
            :need_to_allocate?, :attachments, :batches,
            :list_price_amount, :selling_price, :fee, :prime_cost, :cogs, :profit, :roi,
            :make_labour_cost, :pack_labour_cost, :labour_cost,
            to: :product

  delegate :current_inventory,
           :inventory_item_presenters, :inventory_item_presenter,
           :make_inventory_item_presenter, :pack_inventory_item_presenter,
           :ship_inventory_item_presenter,
           :anything_to_cover_in_zone?,
           :can_ship, :total_demand,
           to: :product_inventory_presenter

  # Quantities
  delegate :made_inventory_quantity, :packed_inventory_quantity,
    :packed_case_quantity, :made_plus_packed_quantity,
    :zone_inventory_quantity,
    :inbound_shipped, :fulfillable, :reserved, :fba_total,
    :total_amazon_inventory,
    :days_of_cover_hint,
    to: :product_stats_presenter

  # Costs
  delegate :made_inventory_total_cogs, :packed_inventory_total_cogs,
    :made_plus_packed_inventory_total_cogs,
    :inbound_shipped_cost_total, :fulfillable_cost_total, :fba_cost_total,
    to: :product_stats_presenter

  delegate :prime_cost_change_percent, :prime_cost_annual_change_percent,
    to: :product_cogs_history

  def initialize(product)
    @product = product
    @shipment_report = product.shipment_report
    @product_inventory_presenter = ProductInventoryPresenter.new(product)
    @product_stats_presenter = ProductStatsPresenter.new(product)
    @product_cogs_history = ProductCogsHistory.new(product)
  end

  def batch_presenters
    batches.fresh_first.map { |batch| BatchPresenter.new(batch) }
  end

  def zones
    product.zones.ordered
  end

  def fba_allocations_json
    allocations_json 'items'
  end

  def fba_cases_allocations_json
    allocations_json 'cases'
  end

  def items_per_case_ppc
    "#{items_per_case}&nbsp;/&nbsp;case".html_safe
  end

  def days_of_cover
    days = product.days_of_cover
    days.infinite? ? 'No demand' : "#{days} days"
  end

  def filestack_api_key
    FilestackApi.api_key
  end

  def gross_margin
    margin = product.gross_margin
    return unless margin
    "#{margin}%"
  end

  private

  def allocations_json(allocation_type)
    allocation_type == 'cases' ? shipment_report.cases_fba_allocation : shipment_report.items_fba_allocation
  end
end
