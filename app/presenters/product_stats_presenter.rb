# frozen_string_literal: true
class ProductStatsPresenter
  attr_reader :product, :product_stats

  delegate :product_name, :selling_price, :cogs, :profit,
    to: :product_presenter

  # Quantities
  delegate :made_inventory_quantity, :packed_inventory_quantity,
    :packed_case_quantity, :made_plus_packed_quantity,
    :zone_inventory_quantity,
    :inbound_shipped, :fulfillable, :reserved, :fba_total,
    :total_amazon_inventory, :total_inventory,
    to: :product_stats

  # Costs
  delegate :made_inventory_total_cogs, :packed_inventory_total_cogs,
    :made_plus_packed_inventory_total_cogs,
    :inbound_shipped_cost_total, :fulfillable_cost_total, :fba_cost_total,
    :total_inventory_price, :total_inventory_cogs,
    :total_inventory_profit, :reserved_quantity_profit,
    to: :product_stats

  def initialize(product)
    @product = product
    @product_stats = Products::ProductInventoryStats.new(product)
  end

  def product_presenter
    @product_presenter ||= ProductPresenter.new(product)
  end

  # rubocop:disable OutputSafety
  def days_of_cover_hint
    "= #{inbound_shipped + fulfillable} / #{reserved}".gsub(' ', '&nbsp;').html_safe
  end
  # rubocop:enable OutputSafety
end
