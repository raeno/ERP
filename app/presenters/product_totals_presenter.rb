# frozen_string_literal: true
class ProductTotalsPresenter
  attr_reader :products, :aggregated_stats
  attr_reader :stats_presenters

  delegate :average_selling_price,
    :total_inventory, :total_reserved,
    :total_inventory_price, :total_inventory_cogs,
    :total_inventory_profit, :total_reserved_quantity_profit,
    to: :aggregated_stats

  def initialize(products)
    @products = products
    @stats_presenters = products.map { |product| ProductStatsPresenter.new(product) }
    @aggregated_stats = Products::AggregatedStats.new(products)
  end
end
