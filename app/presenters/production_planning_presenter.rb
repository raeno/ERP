# frozen_string_literal: true

class ProductionPlanningPresenter
  attr_reader :inventory_items, :zone, :status

  delegate :id, :name, :slug, to: :zone, prefix: true, allow_nil: true

  def initialize(inventory_items, zone, status: nil)
    @inventory_items = inventory_items
    @zone = zone
    @status = status
  end

  def inventory_item_presenters
    inventory_items.map { |ii| InventoryItemPresenter.new(ii) }
  end

  def assignable_users
    User.active.in_zone(zone.id).ordered
  end

  def zone_unit_name
    zone.pack? ? 'cases' : 'units'
  end

  def new_task_form
    task = Task.new(date: Time.zone.today)
    TaskForm.new(task)
  end

  def filter_css_class(status)
    self.status == status ? 'active' : nil
  end
end
