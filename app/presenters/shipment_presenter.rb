# frozen_string_literal: true
class ShipmentPresenter
  attr_reader :shipment

  delegate :id, :name, :shipment_id, :status,
           :created_at, :cases_required, :shipment_entries,
           :fba_warehouse_name,
           to: :shipment

  def initialize(shipment)
    @shipment = shipment
  end

  def shipment_steps
  end

  def current_step
    case shipment.status
    when 'NEW', 'CHANGED'
      'new'
    when 'INITIALIZED', 'AMAZON_SYNCED', 'BATCHES_SYNCED'
      'processing'
    when 'WORKING'
      'amazon_processing'
    else
      'finished'
    end
  end

  def warehouse_name
    shipment.fba_warehouse&.name
  end

  def as_json(_ = {})
    { id: id, name: name }
  end
end
