# frozen_string_literal: true
class ShipmentReportPresenter < ApplicationPresenter
  attr_reader :shipment_report

  delegate :product, :product_id,
    :packed_quantity, :packed_cases_quantity, :total_amazon_inventory,
    :amazon_coverage_ratio, :can_ship,
    :days_of_cover, :to_allocate_item_quantity, :to_allocate_case_quantity,
    :can_ship_in_cases, :items_fba_allocation, :cases_fba_allocation,
    to: :shipment_report

  def initialize(shipment_report)
    @shipment_report = shipment_report
  end

  def items_per_case
    items_per_case = shipment_report.items_per_case
    "#{items_per_case}&nbsp;ppc".html_safe
  end

  def allocate_items?
    allocate? shipment_report.items_shipment_priority
  end

  def allocate_items_class
    allocate_items? ? '' : 'hidden'
  end

  def allocate_cases?
    allocate? shipment_report.cases_shipment_priority
  end

  def allocate_cases_class
    allocate_cases? ? '' : 'hidden'
  end

  private

  def allocate?(priority)
    priority >= ShippingPriority::ALLOCATION_LOWER_LIMIT
  end
end
