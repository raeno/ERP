# frozen_string_literal: true
class ShipmentReportsPresenter < ApplicationPresenter
  delegate :id, :name, to: :zone, prefix: true
  delegate :current_page, :total_pages, :next_page, to: :reports

  def initialize(reports, helpers)
    @reports = reports
    @helpers = helpers
  end

  def presenters
    @presenters ||= reports.map { |report| ShipmentReportPresenter.new(report) }
  end

  def zone
    @zone ||= Zone.ship_zone
  end

  def start_index
    (current_page - 1) * Products::ShipmentReport.per_page
  end

  def warehouses
    @warehouses ||= FbaWarehouse.all
  end

  def as_json(_ = {})
    { meta: { current_page: current_page,
              next_page: next_page,
              total_pages: total_pages,
              admin: @helpers.current_user.admin? },
      data: reports.map(&:serializable_hash) }
  end

  private

  attr_reader :reports
end
