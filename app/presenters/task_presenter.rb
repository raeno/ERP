# frozen_string_literal: true
class TaskPresenter
  include ActionView::Helpers::TextHelper
  include Generic::FormattingHelper

  attr_reader :task, :task_form

  delegate :product, :product_id, :product_image_url, :product_name,
    :zone, :make?, :pack?, :zone_name,
    :batch, :inventory_item, :inventory_item_id,
    :user, :pending?, :in_progress?, :in_progress_by?, :finished?,
    :quantity, :quantity_cases, :quantity_in_zone_units,
    :batch_quantity, :batch_quantity_cases, :coverage,
    :zone_unit_name,
    :duration, :started_at, :finished_at, :duration_estimate, :seconds_left, :date,
    :labour_cost, :unit_labour_cost, :batch_default_attributes,
    to: :task

  delegate :id, to: :task, prefix: true

  delegate :zone_slug, to: :inventory_item

  delegate :urgency_css_class, to: :inventory_item_presenter

  delegate :nickname, to: :user, prefix: true

  def initialize(task, task_form = nil)
    @task = task
    @task_form = task_form
  end

  def user_name
    user.display_name
  end

  def users_for_selection
    User.ordered
  end

  concerning :Presenters do
    def inventory_item_presenter
      InventoryItemPresenter.new(inventory_item)
    end

    def product_presenter
      ProductPresenter.new(product)
    end
  end

  concerning :DateTime do
    def start_time
      started_at&.to_s(:time_only)
    end

    def finish_time
      finished_at&.to_s(:time_only)
    end

    def start_finish_time
      return start_time.to_s unless finish_time
      "#{start_time} - #{finish_time}"
    end
  end

  concerning :ItemsAndCases do
    def batch_number
      make? ? batch_quantity : batch_quantity_cases
    end

    def aimed_quantity_with_unit
      qty_with_unit quantity_in_zone_units
    end

    def batch_quantity_with_unit
      qty_with_unit batch_number
    end

    private

    def qty_with_unit(qty)
      return unless qty
      "#{integer_with_delimiter qty} #{zone_unit_name}"
    end
  end
end
