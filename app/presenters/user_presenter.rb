# frozen_string_literal: true

class UserPresenter
  attr_reader :user

  delegate :display_name, :nickname,
    to: :user

  delegate :id, to: :user, prefix: true

  def initialize(user)
    @user = user
  end
end
