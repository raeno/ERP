# frozen_string_literal: true
class VendorPresenter
  include ActionView::Helpers::TextHelper

  delegate :name, to: :vendor

  attr_reader :vendor

  def initialize(vendor)
    @vendor = vendor
  end

  def lead_time_with_days
    days = vendor.lead_time_days
    return '?' unless days
    "#{days} #{'day'.pluralize(days)}"
  end

  def name_with_lead_time
    "#{name} - #{lead_time_with_days}"
  end
end
