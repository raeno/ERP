# frozen_string_literal: true
module MaterialContainers
  class OrderedByStatusQuery
    attr_reader :relation

    def initialize(relation = MaterialContainer.all)
      @relation = relation
    end

    def all
      ordering_sql = <<-SQL
      CASE status
        WHEN 'pending' THEN 1
        WHEN 'active' THEN 2
        WHEN 'closed' THEN 3
        WHEN 'expired' THEN 4
        ELSE 5
      END ASC, created_at ASC
      SQL

      relation.order ordering_sql
    end
  end
end
