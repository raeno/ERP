# frozen_string_literal: true
module MaterialContainers
  class SameMaterialQuery
    attr_reader :relation, :material

    def initialize(material, relation = MaterialContainer.all)
      @relation = relation
      @material = material
    end

    def all
      relation.where(material: material)
    end

    def active
      all.active.first
    end

    def latest
      ordered_by_invoice_date.first
    end

    def next_to_open
      with_invoice_items
        .order('invoices.date ASC NULLS LAST') # have to use ASC instead of reversing #ordered_by_invoice_date due to nulls in result
        .pending
        .first
    end

    def with_invoice_items
      all.left_outer_joins(invoice_item: :invoice)
    end

    def ordered_by_invoice_date
      with_invoice_items.order('invoices.date DESC NULLS LAST')
    end

    def already_received_ordered
      all.includes(:invoice_item)
         .joins(:invoice_item)
         .merge(InvoiceItem.received)
         .order('invoice_items.received_date ASC')
    end

    def before(date)
      items_before_date = InvoiceItem.before(date)
      with_invoice_items.merge(items_before_date)
    end

    def first_before(date)
      before(date).first
    end
  end
end
