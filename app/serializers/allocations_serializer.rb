# frozen_string_literal: true
class AllocationsSerializer
  attr_reader :allocations

  def initialize(allocations)
    @allocations = allocations
  end

  def as_json(_options = {})
    allocations.map { |allocation| FbaAllocationSerializer.new(allocation).serializable_hash }
  end
end
