# frozen_string_literal: true
class ApplicationSerializer < ActiveModel::Serializer
  def self.serialize(object, serializer: self)
    ActiveModelSerializers::SerializableResource.new(object, serializer: serializer).serializable_hash
  end
end
