# == Schema Information
#
# Table name: fba_allocations
#
#  id                   :integer          not null, primary key
#  fba_warehouse_id     :integer          not null
#  product_id           :integer          not null
#  quantity             :integer          not null
#  quantity_to_allocate :integer
#  created_at           :datetime
#  updated_at           :datetime
#  shipment_type        :text
#

class FbaAllocationSerializer
  attr_reader :allocation

  def initialize(allocation)
    @allocation = allocation
  end

  def serializable_hash
    warehouse = allocation.fba_warehouse
    { warehouse_id: warehouse.id,
      warehouse_name: warehouse.name,
      quantity: allocation.quantity,
      inventory_enough: allocation.could_be_fulfilled? }
  end
end
