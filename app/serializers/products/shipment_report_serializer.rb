# frozen_string_literal: true
# == Schema Information
# Table name: products_shipment_reports
#  id                        :integer          not null, primary key
#  product_id                :integer
#  packed_quantity           :integer
#  packed_cases_quantity     :integer
#  packed_quantity_in_days   :float
#  days_of_cover             :float
#  total_amazon_inventory    :integer
#  amazon_coverage_ratio     :decimal(, )
#  can_ship                  :integer
#  can_ship_in_cases         :integer
#  can_ship_in_days          :float
#  to_allocate_item_quantity :integer
#  to_allocate_case_quantity :integer
#  items_fba_allocation      :json
#  cases_fba_allocation      :json
#  items_shipment_priority   :integer          default(0)
#  cases_shipment_priority   :integer          default(0)
#

module Products
  class ShipmentReportSerializer < ApplicationSerializer
    class ProductSerializer < ApplicationSerializer
      include Rails.application.routes.url_helpers

      attributes :name, :fnsku, :seller_sku, :items_per_case
      attribute(:image_url) { object.small_image_url || '' }
      attribute(:url) { product_path(object) }
    end

    has_one :product

    attribute :product_id
    attributes :packed_quantity, :packed_cases_quantity
    attributes :total_amazon_inventory, :amazon_coverage_ratio,
               :can_ship, :can_ship_in_cases

    attribute(:days_of_cover) { escape_infinity(object.days_of_cover) }

    attribute(:to_allocate_items_quantity) { object.to_allocate_item_quantity }
    attribute(:to_allocate_cases_quantity) { object.to_allocate_case_quantity }

    attribute(:allocate_items) { object.allocate_items? }
    attribute(:allocate_cases) { object.allocate_cases? }
    attribute(:items_fba_allocation) { JSON.parse(object.items_fba_allocation) }
    attribute(:cases_fba_allocation) { JSON.parse(object.cases_fba_allocation) }

    private

    def escape_infinity(value)
      if value.is_a?(Float) && value.infinite?
        "Infinity"
      else
        value
      end
    end
  end
end
