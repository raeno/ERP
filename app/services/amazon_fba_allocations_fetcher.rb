# frozen_string_literal: true
class AmazonFbaAllocationsFetcher
  include Logging
  attr_reader :update_allocation_action

  def self.build(ship_from_address)
    update_allocation_action = ShipmentItems::RetrieveAllocations.build(ship_from_address)
    new(update_allocation_action)
  end

  def initialize(update_allocation_action)
    @update_allocation_action = update_allocation_action
  end

  # fetches allocations for each product and packed inventory quantities and
  # stores them to cache
  def run
    products = Product.active.select(&:need_to_allocate?)
    results = products.flat_map do |product|
      refresh_product_allocations product
    end
    CombinedResult.new(*results)
  end

  private

  def refresh_product_allocations(product)
    shipment_items = build_shipment_items product
    shipment_items.map do |item|
      update_allocation_action.call item
    end
  end

  def build_shipment_items(product)
    items_shipment = ShipmentItem.build product, product.to_allocate_item_quantity, 'items'
    cases_shipment = ShipmentItem.build product, product.to_allocate_case_quantity, 'cases'
    [items_shipment, cases_shipment]
  end
end
