# frozen_string_literal: true
class AmazonInboundShipmentParser
  attr_reader :address_parser, :instructions_parser

  def self.build
    address_parser = Amazon::Parsers::AddressParser.new
    instructions_parser = Amazon::Parsers::PrepInstructionsParser.new
    new(address_parser, instructions_parser)
  end

  def initialize(address_parser, instructions_parser)
    @address_parser = address_parser
    @instructions_parser = instructions_parser
  end

  def parse_preparation_instructions(response)
    instructions_parser.parse response
  end

  def parse_shipment_plans(parsed_shipment_plans_response)
    result = []

    return ShipmentPlansResult.new(result, false, ['Invalid response in creating shipment plan']) unless parsed_shipment_plans_response

    plans = parsed_shipment_plans_response['InboundShipmentPlans']['member']
    plans = [plans] unless plans.is_a?(Array)

    result = plans.map do |plan|
      shipment_plan = OpenStruct.new
      shipment_plan.destination_fulfillment_center_id = plan['DestinationFulfillmentCenterId']
      shipment_plan.label_prep_type = plan['LabelPrepType']
      shipment_plan.ship_to_address = parse_address(plan)
      shipment_plan.items = parse_items_on_shipment_plan(plan['Items']['member'])
      shipment_plan.shipment_id = plan['ShipmentId']

      shipment_plan
    end

    return ShipmentPlansResult.new(result, true, [])

  rescue => e
    return ShipmentPlansResult.new(result, false, [e.message])
  end

  private

    def parse_prep_instructions(parsed_prep_instructions_response)
    result = []
    return result unless parsed_prep_instructions_response

    prep_instruction_list = parsed_prep_instructions_response['SKUPrepInstructionsList']['SKUPrepInstructions']['PrepInstructionList']
    return result if prep_instruction_list.nil?

    result = prep_instruction_list['PrepInstruction']
    result = [result] unless result.is_a?(Array)

    result
  end
end
