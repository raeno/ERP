# frozen_string_literal: true
class AmazonInboundShipmentProcessor
  include WithThrottling
  def self.build(shipment_address)
    amazon_config = Setting.instance.amazon_config
    options = { ship_from_address: shipment_address }
    inbound_shipment_api = Amazon::InboundShipmentApi.build amazon_config, options
    prep_instructions_parser = Amazon::Parsers::PrepInstructionsParser.build
    shipment_plans_parser = Amazon::Parsers::ShipmentPlansParser.build
    new(inbound_shipment_api, prep_instructions_parser, shipment_plans_parser)
  end

  def initialize(inbound_shipment_api, prep_instructions_parser, shipment_plans_parser)
    @inbound_shipment_api = inbound_shipment_api
    @prep_instructions_parser = prep_instructions_parser
    @shipment_plans_parser = shipment_plans_parser
  end

  def create_shipment_plan(shipment_items)
    inbound_shipment_items = build_inbound_shipment_plan_items(shipment_items)
    # create shipment plans by calling amazon api and returns the response parsed simply using xml parser
    parsed_shipment_plans_response = inbound_shipment_api.create_shipment_plan(inbound_shipment_items)

    #  parse the simply parsed response to the shipment plans result object structurized
    shipment_plans_result = shipment_plans_parser.parse(parsed_shipment_plans_response)

    shipment_plans_result
  end

  def create_shipment(shipment_info)
    # fetch preparation instructions for all shipment info items
    items = prepare_items_with_instructions shipment_info
    shipment_header = shipment_info.header.to_h

    # create inbound shipment
    inbound_shipment_api.create_shipment(shipment_info.shipment_id, shipment_header, items)
  end

  def update_shipment(shipment_info)
    items = prepare_items_with_instructions shipment_info
    shipment_header = shipment_info.header.to_h

    inbound_shipment_api.update_shipment(shipment_info.shipment_id, shipment_header, items)
  end

  private

  def prepare_items_with_instructions(shipment_info)
    skus = shipment_info.items.map { |i| i[:seller_sku] }
    instructions_by_sku = fetch_instructions_for_skus skus

    # add relevant instructions to each item
    shipment_info.items.map do |item|
      seller_sku = item[:seller_sku]
      instructions = instructions_by_sku[seller_sku]
      item.to_h.merge prep_details_list: instructions
    end
  end

  def build_inbound_shipment_plan_items(shipment_items)
    skus = shipment_items.map(&:seller_sku)
    instructions_by_sku = fetch_instructions_for_skus skus

    shipment_items.map do |item|
      preparation_instructions = instructions_by_sku[item.seller_sku]
      params = item.to_h.merge(prep_details_list: preparation_instructions)
      Amazon::Structs::InboundShipmentPlanItem.new(params).to_h
    end
  end

  def fetch_instructions_for_skus(skus)
    response = inbound_shipment_api.get_prep_instructions_for_sku(*skus)
    prep_instructions_parser.parse response
  end

  attr_reader :inbound_shipment_api, :prep_instructions_parser, :shipment_plans_parser
end
