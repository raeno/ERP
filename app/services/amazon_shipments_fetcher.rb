# frozen_string_literal: true
class AmazonShipmentsFetcher
  def self.build
    shipments_api = Amazon::InboundShipmentApi.build Setting.instance.amazon_config
    shipments_parser = Amazon::Parsers::ShipmentParser.build
    persist_shipment = AmazonShipments::PersistShipment.build
    new(shipments_api, shipments_parser, persist_shipment)
  end

  def initialize(shipments_api, shipments_parser, persist_shipment)
    @shipments_api = shipments_api
    @parser = shipments_parser
    @persist_shipment = persist_shipment
  end

  def call(start_date:)
    raw_shipments = shipments_api.fetch_shipments(date_from: start_date)
    shipments = parser.parse raw_shipments

    shipments.each do |shipment_data|
      fba_warehouse = shipment_data.delete(:fba_warehouse)
      if fba_warehouse
        warehouse = FbaWarehouse.find_by name: fba_warehouse
        shipment_data[:fba_warehouse_id] = warehouse.id
      end
      persist_shipment.call shipment_data
    end
  end

  private

  attr_reader :shipments_api, :parser, :persist_shipment
end
