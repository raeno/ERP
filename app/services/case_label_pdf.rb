# frozen_string_literal: true
class CaseLabelPdf
  include Prawn::Measurements
  include Prawn::View

  attr_reader :product

  DEFAULT_MARGIN = 5
  DEFAULT_WIDTH = 1.86
  DEFAULT_HEIGHT = 1.26
  LINE_INTERVAL = 13

  def initialize(product)
    @document = Prawn::Document.new page_size: page_size, page_layout: :portrait, margin: DEFAULT_MARGIN
    @product = product
    generate_label
  end

  private

  def generate_label
    write_text product.name
    write_text "SKU: #{product.seller_sku}", overflow: :shrink_to_fit
    write_text "SIZE: #{product.size}"
    write_text "CASE: #{product.items_per_case}"
    write_text current_date
  end

  def write_text(content, options = {})
    default_options = { width: content_width, overflow: :truncate, single_line: true, at: [0, cursor] }
    text_box content, default_options.merge(options)
    move_down LINE_INTERVAL
  end

  def current_date
    Time.zone.today.to_s(:short)
  end

  def width
    in2pt DEFAULT_WIDTH
  end

  def height
    in2pt DEFAULT_HEIGHT
  end

  def content_width
    width - 2 * DEFAULT_MARGIN
  end

  def page_size
    [width, height]
  end
end
