# frozen_string_literal: true
class ShipmentNotificationService
  BASE_CHANNEL_NAME = "shipment_channel"

  def call(user, shipment_name, data = {})
    channel = channel_name user
    shipment_data = data.merge shipment_name: shipment_name
    notification_server.broadcast channel, shipment_data
  end

  def notification_server
    ActionCable.server
  end

  private

  def channel_name(user)
    [BASE_CHANNEL_NAME, user.id].join '_'
  end
end
