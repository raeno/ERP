# frozen_string_literal: true
BatchFormSchema = Dry::Validation.Schema do
  required(:batch) do |batch|
    batch.hash? do
      optional(:product_id)
      optional(:quantity)
      optional(:completed_on)
      optional(:zone_id).filled(:int?)
      optional(:notes).filled(:str?)
      optional(:notes)
      optional(:user_ids).each(:int?)
    end
  end
end
