# frozen_string_literal: true
kisoils_config = Rails.application.config_for :kisoils
Rails.application.configure do
  config.kisoils = ActiveSupport::OrderedOptions.new
  config.kisoils.allow_record_locking = kisoils_config[:allow_record_locking]
end
