# frozen_string_literal: true
Sidekiq.configure_server do |config|
  config.redis = { url: ENV['REDIS_SERVER'] }
end

Sidekiq.configure_client do |config|
  config.redis = { url: ENV['REDIS_CLIENT'] }
end

Sidekiq.default_worker_options = {
  unique: :until_executing, # forbid from adding jobs with same name/params while first one is waiting
  unique_args: ->(args) { [args.first.except('job_id')] }
}
