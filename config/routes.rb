# frozen_string_literal: true
Rails.application.routes.draw do
  mount ActionCable.server => '/cable'

  require 'sidekiq/web'
  authenticate :user do
    mount Sidekiq::Web => '/sidekiq'
  end

  devise_for :users, :controllers => { :registrations => "registrations" }

  resources :product_reports, only: [] do
    collection do
      get 'ship', to: 'product_reports#ship'
      get 'ship_test', to: 'product_reports#ship_test'
    end
  end

  namespace :planning do
    get 'make', to: 'make'
    get 'pack', to: 'pack'
    get 'workload', to: 'workload'
  end

  namespace :products do
    resources :archives, only: :index
    resources :inventories, only: :index
  end

  resources :products, :only => [:show, :update] do
    resources :fba_allocations, only: [:index]

    member do
      put 'override_quantity'

      get  'edit_ingredients', to: 'ingredients#edit'
      post 'ingredients', to: 'ingredients#update'
    end

    scope module: :products do
      resources :attachments, only: [:index, :create, :destroy]
      resource :inventory, only: :show
      resource :barcode, only: :show
      resource :case_label, only: :show
      resource :note, only: [:show, :edit, :update]
      resources :prime_costs, only: :index
      get 'properties', to: 'properties#show'
      resources :batches, only: :index
      resources :tasks, only: :index
    end
  end

  resources :batches do
    collection do
      get 'chart'
    end
  end

  resources :inventory_items, only: [:update, :destroy]
  resources :inventories, except: [:index]

  resources :material_categories, except: :show
  resources :materials do
    scope module: :materials do
      resources :forecasts, only: :index
      resources :invoices, only: :index
      resources :containers
      resources :transactions, only: :index
      resources :charts, only: :index do
        collection do 
          get 'cost_over_time'
        end
      end
      resources :attachments, only: [:index, :create, :destroy]
      resources :ordering_sizes, only: :index
    end
    get 'edit_sizes'
  end

  resources :vendors

  resources :invoices do
    collection do
      get 'chart'
      get 'vendors_chart'
    end
    put "generate", to: "invoices#generate", on: :collection
    patch "invoice_paid", to: "invoices#invoice_paid", on: :member, as: :paid
    resources :invoice_items, as: 'items', except: [:index] do
      patch 'item_received', on: :member, as: :received
    end
  end

  resources :units

  resources :amazon_shipments, only: [:index, :create, :update, :show]

  resources :tasks, except: :index do
    member do
      put 'start'
      put 'cancel'
    end

    collection do
      get 'make', to: 'tasks#make'
      get 'pack', to: 'tasks#pack'
      get 'current', to: 'tasks#current'
      get 'finished', to: 'tasks#finished'
    end
  end

  get 'reports/inventory_health', to: 'dashboard_reports#inventory_health'
  get 'reports/profit_and_loss',  to: 'dashboard_reports#profit_and_loss'

  namespace :admin do
    resources :users

    resources :zones, only: [:index, :edit, :update]

    resources :dashboard, :only => :index
    get 'fetch_products', to: 'dashboard#fetch_products'

    get 'refresh_fba_allocations', to: 'dashboard#refresh_fba_allocations'

    resource :settings, only: [:show, :edit, :update]
  end

  root to: redirect('tasks/current')
end
