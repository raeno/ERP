# frozen_string_literal: true
class CreateStockItems < ActiveRecord::Migration
  def change
    create_table :stock_items do |t|
      t.references :component
      t.float :quantity

      t.timestamps null: false
      t.index :component_id
    end
  end
end
