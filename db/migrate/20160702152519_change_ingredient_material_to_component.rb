# frozen_string_literal: true
class ChangeIngredientMaterialToComponent < ActiveRecord::Migration
  def change
    remove_column :ingredients, :material_id, :integer
    add_reference :ingredients, :component
    add_index :ingredients, :component_id
  end
end
