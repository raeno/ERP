class RemoveUnitFromMaterial < ActiveRecord::Migration
  def change
    remove_column :materials, :unit, :text

    create_table :units do |t|
      t.text :name, index: true
      t.text :full_name
    end

    add_reference :materials, :unit, index: true
  end
end
