class CreateComponents < ActiveRecord::Migration
  def change
    create_table :components do |t|
      t.references :zone
      t.references :componentable, polymorphic: true

      t.timestamps null: false
      t.index :zone_id
      t.index :componentable_id
    end
  end
end
