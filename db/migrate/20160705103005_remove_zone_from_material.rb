class RemoveZoneFromMaterial < ActiveRecord::Migration
  def change
    remove_reference :materials, :zone
  end
end
