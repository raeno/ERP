# frozen_string_literal: true
class RemoveInventoryQuantityFromMaterial < ActiveRecord::Migration
  def change
    remove_column :materials, :inventory_quantity, :integer
  end
end
