# frozen_string_literal: true
class ChangeStockItemQuantityToInteger < ActiveRecord::Migration
  def change
    change_column :stock_items, :quantity, :integer, default: 0
  end
end
