# frozen_string_literal: true
# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/MethodLength
class ChangeStockItemToInventory < ActiveRecord::Migration
  def change
    drop_table :stock_items do |t|
      t.references :component
      t.float :quantity
      t.timestamps null: false
    end

    create_table :inventories do |t|
      t.references :inventory_item
      t.integer :quantity
      t.timestamps null: false
    end

    drop_table :components do |t|
      t.references :zone
      t.references :componentable, polymorphic: true
      t.timestamps null: false
    end

    create_table :inventory_items do |t|
      t.references :zone
      t.references :inventoriable, polymorphic: true
      t.boolean :is_component
      t.timestamps null: false
    end

    remove_column :ingredients, :component_id, :integer
    add_column :ingredients, :inventory_item_id, :integer
  end
end
