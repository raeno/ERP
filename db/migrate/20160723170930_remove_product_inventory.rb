# frozen_string_literal: true
class RemoveProductInventory < ActiveRecord::Migration
  def change
    drop_table :product_inventories do |t|
      t.integer :quantity, default: 0
      t.references :product
      t.references :zone
    end
  end
end
