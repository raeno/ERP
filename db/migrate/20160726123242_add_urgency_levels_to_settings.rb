# frozen_string_literal: true
class AddUrgencyLevelsToSettings < ActiveRecord::Migration
  def change
    change_table :settings do |t|
      t.integer    :red_urgency_level_days, null: false, default: 10
      t.integer :yellow_urgency_level_days, null: false, default: 20
    end
  end
end
