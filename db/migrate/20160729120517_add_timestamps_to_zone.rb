# frozen_string_literal: true
class AddTimestampsToZone < ActiveRecord::Migration
  def change
    add_column :zones, :created_at, :datetime
    add_column :zones, :updated_at, :datetime
  end
end
