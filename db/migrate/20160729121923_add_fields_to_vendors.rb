# frozen_string_literal: true
class AddFieldsToVendors < ActiveRecord::Migration
  def change
    change_table :vendors do |t|
      t.integer :lead_time_days, null: false, default: 0
    end
  end
end
