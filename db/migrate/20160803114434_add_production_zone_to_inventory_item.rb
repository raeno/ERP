class AddProductionZoneToInventoryItem < ActiveRecord::Migration
  def change
    add_column :inventory_items, :production_zone_id, :integer
    add_index :inventory_items, :production_zone_id
  end
end
