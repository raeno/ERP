# frozen_string_literal: true
class AddFieldsToMaterials < ActiveRecord::Migration
  def change
    change_table :materials do |t|
      t.belongs_to :vendor
      t.integer :package_amount
      t.decimal :package_price

      t.index :vendor_id
    end

    add_foreign_key :materials, :vendors
  end
end
