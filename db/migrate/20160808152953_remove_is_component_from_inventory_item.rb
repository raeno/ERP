class RemoveIsComponentFromInventoryItem < ActiveRecord::Migration
  def change
    remove_column :inventory_items, :is_component, :boolean
  end
end
