class ChangesToMaterials < ActiveRecord::Migration
  def change
    change_table :materials do |t|
      t.string :files_url
      t.string :image_url
      t.string :vendor_url
    end
  end
end
