class IngredientsChange < ActiveRecord::Migration
  def change
    change_column :ingredients, :quantity, :decimal, default: 1, null: false
  end
end
