class MaterialsChange < ActiveRecord::Migration
  def change
    rename_column :materials, :files_url, :pdf_url
  end
end
