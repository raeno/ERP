class AddAuxillaryInfoToBatch < ActiveRecord::Migration
  def change
    add_column :batches, :auxillary_info, :hstore
    add_index :batches, :auxillary_info, using: :gist
  end
end
