class CreateMaterialCategories < ActiveRecord::Migration
  def up
    create_table :material_categories do |t|
      t.string :name, null: false
      t.timestamps
    end

    category = MaterialCategory.create!(name: "Other")

    change_table :materials do |t|
      t.integer :category_id, null: false, default: category.id
      t.index :category_id
    end
  end

  def down
    change_table :materials do |t|
      t.remove :category_id
    end

    drop_table :material_categories
  end
end
