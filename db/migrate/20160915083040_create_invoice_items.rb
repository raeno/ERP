class CreateInvoiceItems < ActiveRecord::Migration
  def change
    drop_table :invoices

    create_table :invoices do |t|
      t.belongs_to :vendor
      t.timestamps
    end

    create_table :invoice_items do |t|
      t.belongs_to :invoice, null: false
      t.belongs_to :material, null: false

      t.string  :pkg_external_name
      t.string  :pkg_form
      t.integer :pkg_unit_id, null: false
      t.float   :pkg_amount, null: false, default: 1
      t.decimal :pkg_price, null: false

      t.integer :quantity, null: false, default: 1
      t.float   :total_material_amount, null: false
      t.decimal :total_cost, null: false

      t.index :invoice_id
      t.foreign_key :invoices
      t.index :material_id
      t.foreign_key :materials
      t.index :pkg_unit_id
    end
  end
end
