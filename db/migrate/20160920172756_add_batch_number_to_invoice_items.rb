class AddBatchNumberToInvoiceItems < ActiveRecord::Migration
  def change
    change_table :invoice_items do |t|
      t.string :batch_number
    end
  end
end
