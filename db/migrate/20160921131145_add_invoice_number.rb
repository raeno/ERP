class AddInvoiceNumber < ActiveRecord::Migration
  def change
    change_table :invoices do |t|
      t.string :number, null: false, default: ''
    end
  end
end
