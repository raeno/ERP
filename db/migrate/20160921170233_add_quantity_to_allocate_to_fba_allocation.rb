class AddQuantityToAllocateToFbaAllocation < ActiveRecord::Migration
  def change
    add_column :fba_allocations, :quantity_to_allocate, :integer
    add_index :fba_allocations, :quantity_to_allocate
  end
end
