class AddTimestampsToFbaAllocation < ActiveRecord::Migration
  def change
    add_column :fba_allocations, :created_at, :datetime
    add_column :fba_allocations, :updated_at, :datetime
  end
end
