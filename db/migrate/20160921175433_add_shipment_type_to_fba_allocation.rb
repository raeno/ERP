class AddShipmentTypeToFbaAllocation < ActiveRecord::Migration
  def change
    add_column :fba_allocations, :shipment_type, :text
    remove_index :fba_allocations, [:product_id, :fba_warehouse_id]

    add_index :fba_allocations, [:product_id, :fba_warehouse_id,
                                 :quantity_to_allocate, :shipment_type],
              name: 'uniqueness_index', unique: true
    add_index :fba_allocations, [:product_id, :quantity_to_allocate, :shipment_type],
              name: 'product_quantity_shipment_type_index'
  end
end
