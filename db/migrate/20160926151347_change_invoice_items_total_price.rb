class ChangeInvoiceItemsTotalPrice < ActiveRecord::Migration
  def change
    change_table :invoice_items do |t|
      t.rename :total_cost, :total_price
    end
  end
end
