class CreateUnitConversions < ActiveRecord::Migration
  def change
    create_table :unit_conversions do |t|
      t.integer :from_unit_id, null: false
      t.integer :to_unit_id, null: false
      t.float :coefficient, null: false

      t.index :from_unit_id
      t.index [:to_unit_id, :from_unit_id], unique: true
    end
  end
end
