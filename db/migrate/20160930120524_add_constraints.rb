class AddConstraints < ActiveRecord::Migration
  def change
    change_table :inventories do |t|
      t.change :inventory_item_id, :integer, null: false
      t.change :quantity, :integer, null: false
      t.index :inventory_item_id, unique: true
    end
    add_foreign_key :inventories, :inventory_items

    change_table :amazon_inventories do |t|
      t.change :product_id, :integer, null: false
      t.remove_index :product_id
      t.index :product_id, unique: true
    end
    add_foreign_key :amazon_inventories, :products

    change_table :batches do |t|
      t.change :product_id, :integer, null: false
      t.change :quantity, :integer, null: false
      t.change :zone_id, :integer, null: false
      t.index :product_id
      t.index :zone_id
      t.index :completed_on
    end

    change_table :batches_users do |t|
      t.index [:batch_id, :user_id], unique: true
    end

    change_table :ingredients do |t|
      t.index :product_id
      t.index [:inventory_item_id, :product_id], unique: true
    end

    change_table :inventory_items do |t|
      t.index :zone_id
      t.index [:inventoriable_type, :inventoriable_id, :zone_id], unique: true,
        name: "inventory_items_unique_inventoriable_and_zone"
    end

    change_table :invoices do |t|
      t.index :vendor_id
    end

    change_table :product_zone_availabilities do |t|
      t.index [:product_id, :zone_id]
    end

    change_table :user_zones do |t|
      t.index [:user_id, :zone_id]
    end
  end
end
