class AddTimestampsToInvoiceItems < ActiveRecord::Migration
  def change
    change_table :invoice_items do |t|
      t.timestamps
    end
  end
end
