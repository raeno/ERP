class AddAttachmentUrlToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :attachment_url, :text
  end
end
