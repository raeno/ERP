class RemoveFieldsFromMaterials < ActiveRecord::Migration
  def change
    change_table :materials do |t|
      t.remove :package_amount
      t.remove :package_price
    end
  end
end
