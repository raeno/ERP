class AddFeeToProducts < ActiveRecord::Migration
  def change
    change_table :products do |t|
      t.decimal :fee, default: 0.0, null: false
    end
  end
end
