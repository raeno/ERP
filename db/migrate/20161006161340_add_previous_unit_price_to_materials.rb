class AddPreviousUnitPriceToMaterials < ActiveRecord::Migration
  def change
    change_table :materials do |t|
      t.decimal :previous_unit_price, default: 0.0, null: false
    end
  end
end
