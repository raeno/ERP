class ForceInventoryItemInIngredients < ActiveRecord::Migration
  def change
    change_table :ingredients do |t|
      t.change :inventory_item_id, :integer, null: false
    end
  end
end
