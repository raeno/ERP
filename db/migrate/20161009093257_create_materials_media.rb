class CreateMaterialsMedia < ActiveRecord::Migration
  def change
    create_table :material_media do |t|
      t.text :url
      t.references :material
      t.timestamps null: false
    end
    add_index :material_media, :material_id
  end
end
