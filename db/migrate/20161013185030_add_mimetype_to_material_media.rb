class AddMimetypeToMaterialMedia < ActiveRecord::Migration
  def change
    add_column :material_media, :mimetype, :text, nil: false
  end
end
