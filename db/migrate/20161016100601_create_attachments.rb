class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.text :url, null: false
      t.text :mimetype

      t.timestamps null: false
    end
  end
end
