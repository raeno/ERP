class CreateMaterialAttachments < ActiveRecord::Migration
  def change
    create_table :material_attachments do |t|
      t.references :material, null: false
      t.references :attachment, null: false

      t.timestamps null: false
    end
    add_index :material_attachments, :material_id
    add_index :material_attachments, :attachment_id
  end
end
