class CreateProductAttachments < ActiveRecord::Migration
  def change
    create_table :product_attachments do |t|
      t.references :product
      t.references :attachment

      t.timestamps null: false
    end
    add_index :product_attachments, :product_id
    add_index :product_attachments, :attachment_id
  end
end
