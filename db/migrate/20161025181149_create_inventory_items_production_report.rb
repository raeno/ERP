class CreateInventoryItemsProductionReport < ActiveRecord::Migration
  def change
    create_table :inventory_items_production_reports do |t|
      t.references :inventory_item
      t.decimal :in_stock
      t.decimal :demand
      t.decimal :demand_coverage
      t.decimal :supply
      t.decimal :to_cover
      t.decimal :can_cover
      t.decimal :can_cover_in_cases
    end

    add_index :inventory_items_production_reports, :inventory_item_id, unique: true
    add_index :inventory_items_production_reports, :can_cover
    add_index :inventory_items_production_reports, :can_cover_in_cases
  end
end
