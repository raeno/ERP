class CreateProductsShipmentReport < ActiveRecord::Migration
  def change
    create_table :products_shipment_reports do |t|
      t.references :product
      t.integer :packed_quantity
      t.integer :packed_cases_quantity
      t.float :packed_quantity_in_days
      t.float :days_of_cover
      t.integer :total_amazon_inventory
      t.decimal :amazon_coverage_ratio
      t.integer :shipment_priority
      t.integer :can_ship
      t.integer :can_ship_in_cases
      t.float :can_ship_in_days
      t.integer :to_allocate_item_quantity
      t.integer :to_allocate_case_quantity
      t.json :items_fba_allocation
      t.json :cases_fba_allocation
    end
    add_index :products_shipment_reports, :product_id, unique: true
    add_index :products_shipment_reports, :shipment_priority
  end
end
