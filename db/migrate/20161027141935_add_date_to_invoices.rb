class AddDateToInvoices < ActiveRecord::Migration
  def change
    change_table :invoices do |t|
      t.date :date, null: false, default: Time.zone.today
      t.index :date
    end
  end
end
