class AddShipmentPriorityForCasesToShipmentReport < ActiveRecord::Migration
  def change
    remove_column :products_shipment_reports, :shipment_priority, :integer
    add_column :products_shipment_reports, :items_shipment_priority, :integer, default: 0
    add_column :products_shipment_reports, :cases_shipment_priority, :integer, default: 0

    add_index :products_shipment_reports, :items_shipment_priority
    add_index :products_shipment_reports, :cases_shipment_priority
  end
end
