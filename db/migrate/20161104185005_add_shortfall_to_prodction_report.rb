class AddShortfallToProdctionReport < ActiveRecord::Migration
  def change
    change_table :inventory_items_production_reports do |t|
      t.boolean :shortfall_is_previous_zone
      t.integer :shortfall_ingredient_id
    end
  end
end
