class RequiredProductionReportFields < ActiveRecord::Migration
  def change
    change_table :inventory_items_production_reports do |t|
      t.change :inventory_item_id, :integer, null: false
      t.change :in_stock, :decimal, null: false
      t.change :demand, :decimal, null: false
      t.change :demand_coverage, :decimal, null: false
      t.change :supply, :decimal, null: false
      t.change :to_cover, :decimal, null: false
      t.change :can_cover, :decimal, null: false
      t.change :can_cover_in_cases, :decimal, null: false
    end
  end
end
