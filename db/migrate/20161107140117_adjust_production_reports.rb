class AdjustProductionReports < ActiveRecord::Migration
  def change
    change_table :inventory_items_production_reports do |t|
      t.change :can_cover_in_cases, :decimal, null: true
    end
  end
end
