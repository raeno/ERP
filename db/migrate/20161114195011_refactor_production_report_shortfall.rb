class RefactorProductionReportShortfall < ActiveRecord::Migration
  def change
    table_name = :inventory_items_production_reports
    rename_column table_name, :shortfall_is_previous_zone, :previous_zone_deficit
    remove_column table_name, :shortfall_ingredient_id, :integer
    add_column table_name, :deficit_inventoriable_id, :integer
    add_column table_name, :deficit_inventoriable_type, :string
    add_column table_name, :deficit_ingredient_name, :string
  end
end
