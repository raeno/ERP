class AddTotalPriceToInvoices < ActiveRecord::Migration
  def change
    change_column_default :invoices, :date, nil

    change_table :invoices do |t|
      t.decimal :total_price
    end

    Invoice.find_each do |invoice|
      invoice.update_column(:total_price, invoice.total_item_price)
    end

    change_table :invoices do |t|
      t.change :total_price, :decimal, null: false
    end
  end
end
