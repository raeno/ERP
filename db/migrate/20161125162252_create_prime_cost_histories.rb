class CreatePrimeCostHistories < ActiveRecord::Migration
  def change
    create_table :prime_cost_histories do |t|
      t.references :product
      t.references :batch
      t.decimal :prime_cost
      t.timestamps null: false
    end
    add_index :prime_cost_histories, :product_id
    add_index :prime_cost_histories, :batch_id
  end
end
