class RemoveMaterialPreviousUnitPrice < ActiveRecord::Migration
  def change
    remove_column :materials, :previous_unit_price
  end
end
