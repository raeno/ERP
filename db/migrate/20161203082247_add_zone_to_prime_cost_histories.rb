class AddZoneToPrimeCostHistories < ActiveRecord::Migration
  def change
    add_column :prime_cost_histories, :zone_id, :integer
    add_index :prime_cost_histories, :zone_id
  end
end
