class AddFieldsToInvoices < ActiveRecord::Migration
  def change
    change_table :invoices do |t|
      t.date :received_date
    end

    create_table :invoice_fees do |t|
      t.belongs_to :invoice, null: false
      t.string :name, null: false
      t.decimal :price, null: false
      t.timestamps

      t.index :invoice_id
      t.foreign_key :invoices
    end
  end
end
