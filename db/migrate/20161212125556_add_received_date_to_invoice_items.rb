class AddReceivedDateToInvoiceItems < ActiveRecord::Migration
  def change
    change_table :invoice_items do |t|
      t.date :received_date
    end
  end
end
