class AddNotesToProduct < ActiveRecord::Migration
  def change
    change_table :products do |t|
      t.text :notes
    end
  end
end
