class AddExpirationDateToInvoiceItems < ActiveRecord::Migration
  def change
    change_table :invoice_items do |t|
      t.date :expiration_date
    end
  end
end
