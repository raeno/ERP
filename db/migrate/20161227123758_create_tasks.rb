class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.belongs_to :user, null: false
      t.belongs_to :inventory_item, null: false
      t.datetime :started_at
      t.datetime :finished_at
      t.integer :quantity
      t.string :quantity_unit
      t.timestamps

      t.index :user_id
      t.foreign_key :users
      t.index :inventory_item_id
      t.foreign_key :inventory_items
    end
  end
end
