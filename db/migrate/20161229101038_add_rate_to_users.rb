class AddRateToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.decimal :labour_rate, null: false, default: 0
    end
  end
end
