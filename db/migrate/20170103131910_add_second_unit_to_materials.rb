class AddSecondUnitToMaterials < ActiveRecord::Migration
  def change
    change_table :materials do |t|
      t.change :unit_id, :integer, null: false
      t.rename :unit_id, :small_unit_id

      t.integer :unit_id
      t.index :unit_id
    end

    Material.find_each do |m|
      m.update_column(:unit_id, m.small_unit_id)
    end

    change_table :materials do |t|
      t.change :unit_id, :integer, null: false
    end

    change_column_default(:invoice_items, :pkg_amount, nil)

    change_table :inventories do |t|
      t.change :quantity, :decimal, null: false
    end

    change_table :invoice_items do |t|
      t.change :total_material_amount, :decimal, null: false
    end
  end
end
