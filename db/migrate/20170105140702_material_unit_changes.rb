class MaterialUnitChanges < ActiveRecord::Migration
  def change
    change_table :materials do |t|
      t.rename :small_unit_id, :ingredient_unit_id
    end

    change_table :unit_conversions do |t|
      t.change :coefficient, :decimal,  null: false
    end
  end
end
