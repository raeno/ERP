class CreateMaterialContainers < ActiveRecord::Migration
  def change
    create_table :material_containers do |t|
      t.belongs_to :material, null: false
      t.text :external_name
      t.text :container_form
      t.belongs_to :unit, null: false
      t.float :amount, null: false, default: 1
      t.decimal :price, null: false
      t.text :lot_number
      t.date :expiration_date

      t.timestamps null: false

      t.index :material_id
      t.foreign_key :materials
      t.index :unit_id
    end
  end
end
