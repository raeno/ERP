class LooseSomeFieldsRestrictionsForInvoiceItem < ActiveRecord::Migration
  def change
    change_column :invoice_items, :material_id, :integer, null: true
    change_column :invoice_items, :pkg_unit_id, :integer, null: true
    change_column :invoice_items, :pkg_amount, :float, null: true
    change_column :invoice_items, :pkg_price, :decimal, null: true
  end
end
