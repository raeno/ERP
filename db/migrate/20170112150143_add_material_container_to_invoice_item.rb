class AddMaterialContainerToInvoiceItem < ActiveRecord::Migration
  def change
    add_column :invoice_items, :material_container_id, :integer
    add_index :invoice_items, :material_container_id, unique: true
  end
end
