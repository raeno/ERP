class RenamePrimeCostHistories < ActiveRecord::Migration
  def change
    rename_table :prime_cost_histories, :product_cogs_snapshots
    rename_column :product_cogs_snapshots, :prime_cost, :cogs
  end
end
