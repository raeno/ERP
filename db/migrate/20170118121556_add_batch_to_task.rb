class AddBatchToTask < ActiveRecord::Migration
  def change
    change_table :batches do |t|
      t.belongs_to :task
      t.index :task_id, unique: true
    end
  end
end
