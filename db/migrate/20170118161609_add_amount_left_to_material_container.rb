class AddAmountLeftToMaterialContainer < ActiveRecord::Migration
  def change
    add_column :material_containers, :amount_left, :float,
               null: false, default: 0.0
    add_index :material_containers, :amount_left
  end
end
