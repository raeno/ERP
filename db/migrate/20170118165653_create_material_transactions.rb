class CreateMaterialTransactions < ActiveRecord::Migration
  def change
    create_table :material_transactions do |t|
      t.float :quantity
      t.float :inventory_amount
      t.belongs_to :batch
      t.belongs_to :material_container
      t.index :batch_id
      t.index :material_container_id

      t.timestamps null: false
    end
  end
end
