class ChangeMaterialContainerAmountsType < ActiveRecord::Migration
  def up
    change_column :material_containers, :amount, :decimal
    change_column :material_containers, :amount_left, :decimal
  end

  def down
    change_column :material_containers, :amount, :float
    change_column :material_containers, :amount_left, :float
  end
end
