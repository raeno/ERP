class AddUsedNowToMaterialContainer < ActiveRecord::Migration
  def change
    add_column :material_containers, :used_now, :boolean, default: false
    add_index :material_containers, :used_now, where: 'used_now'
  end
end
