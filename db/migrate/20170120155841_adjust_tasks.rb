class AdjustTasks < ActiveRecord::Migration
  def change
    change_table :tasks do |t|
      t.integer :quantity_cases
      t.remove :quantity_unit
      t.decimal :total_labour_cost
    end
  end
end
