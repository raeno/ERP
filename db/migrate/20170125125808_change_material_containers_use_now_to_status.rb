class ChangeMaterialContainersUseNowToStatus < ActiveRecord::Migration
  def change
    remove_column :material_containers, :used_now, :boolean
    add_column :material_containers, :status, :integer, default: 0, index: true
  end
end
