class TasksMultiuserness < ActiveRecord::Migration
  def change
    create_table :contributions do |t|
      t.belongs_to :task, null: false
      t.belongs_to :user, null: false
      t.datetime :started_at, null: false
      t.datetime :finished_at
      t.float :coefficient, null: false, default: 1
      t.decimal :labour_cost

      t.index :task_id
      t.index :user_id
      t.index :started_at
      t.index :finished_at
    end

    change_table :tasks do |t|
      t.rename :total_labour_cost, :total_labour_cost_obsolete
    end

    Task.find_each do |task|
      Contribution.create!(
        task_id: task.id, user_id: task.user_id,
        started_at: task.started_at, finished_at: task.finished_at,
        coefficient: 1,
        labour_cost: task.total_labour_cost_obsolete
      )
    end

    change_table :tasks do |t|
      t.remove :user_id
      t.remove :total_labour_cost_obsolete
    end
  end
end
