class AddNotesToInvoices < ActiveRecord::Migration
  def change
    change_table :invoices do |t|
      t.text :notes
    end
  end
end
