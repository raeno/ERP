class CreateOrderingSizes < ActiveRecord::Migration
  def change
    create_table :ordering_sizes do |t|
      t.belongs_to :material, null: false
      t.belongs_to :unit, null: false
      t.float :amount, null: false
      t.float :amount_in_main_material_units, null: false
      t.string :name

      t.index :material_id
      t.index :amount_in_main_material_units
      t.foreign_key :units
      t.foreign_key :materials
    end

    change_table :material_containers do |t|
      t.belongs_to :ordering_size
      t.rename :container_form, :container_form_obsolete
    end
  end
end
