class AddPaidDateToInvoices < ActiveRecord::Migration
  def change
    change_table :invoices do |t|
      t.date :paid_date
    end
  end
end
