class MaterialsHabtmVendors < ActiveRecord::Migration
  def up
    create_table :material_vendors do |t|
      t.integer :material_id, null: false
      t.integer :vendor_id, null: false
      t.string :url

      t.index [:material_id, :vendor_id], unique: true
      t.index :vendor_id
    end

    change_table :materials do |t|
      t.rename :vendor_id, :vendor_id_obsolete
      t.rename :vendor_url, :vendor_url_obsolete
    end
  end

  def down
    change_table :materials do |t|
      t.rename :vendor_id_obsolete, :vendor_id
      t.rename :vendor_url_obsolete, :vendor_url
    end

    drop_table :material_vendors
  end
end
