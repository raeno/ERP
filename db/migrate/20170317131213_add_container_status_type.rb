class AddContainerStatusType < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      CREATE TYPE container_status AS ENUM ('active', 'pending', 'closed', 'expired');
    SQL
    rename_column :material_containers, :status, :old_status
    add_column :material_containers, :status, :container_status, default: 'pending', index: true
  end

  def down
    remove_column :material_containers, :status
    rename_column :material_containers, :old_status, :status
    execute <<-SQL
      DROP TYPE container_status;
    SQL
  end
end
