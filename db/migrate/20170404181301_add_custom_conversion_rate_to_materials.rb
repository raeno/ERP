class AddCustomConversionRateToMaterials < ActiveRecord::Migration[5.0]
  def up
    change_table :materials do |t|
      t.decimal :unit_conversion_rate
    end

    rename_table :unit_conversions, :conversion_rates
    change_table :conversion_rates do |t|
      t.rename :coefficient, :rate
    end
  end

  def down
    change_table :materials do |t|
      t.remove :unit_conversion_rate
    end

    rename_table :conversion_rates, :unit_conversions
    change_table :conversion_rates do |t|
      t.rename :rate, :coefficient
    end
  end
end
