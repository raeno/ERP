class AddQuantityInSmallUnitsToIngredients < ActiveRecord::Migration[5.0]
  def up
    change_table :ingredients do |t|
      t.decimal :quantity_in_small_units, default: 1, null: false
      t.rename :quantity, :quantity_obsolete
    end
  end

  def down
    change_table :ingredients do |t|
      t.remove :quantity_in_small_units
      t.rename :quantity_obsolete, :quantity
    end
  end
end
