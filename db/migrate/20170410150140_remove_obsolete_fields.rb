class RemoveObsoleteFields < ActiveRecord::Migration[5.0]
  def up
    remove_column :materials, :vendor_id_obsolete
    remove_column :materials, :vendor_url_obsolete
    remove_column :material_containers, :container_form_obsolete
    remove_column :contributions, :coefficient
  end

  def down
    add_column :materials, :vendor_id_obsolete, :integer
    add_column :materials, :vendor_url_obsolete, :string
    add_column :material_containers, :container_form_obsolete, :string
    add_column :contributions, :coefficient, :decimal, null: false, default: 1
  end
end
