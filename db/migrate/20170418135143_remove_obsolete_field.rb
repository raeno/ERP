class RemoveObsoleteField < ActiveRecord::Migration[5.0]
  def up
    remove_column :materials, :pdf_url
  end

  def down
    add_column :materials, :pdf_url, :string
  end
end
