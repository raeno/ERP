class CreateAmazonShipment < ActiveRecord::Migration[5.0]
  def change
    create_table :amazon_shipments do |t|
      t.text :shipment_id, index: true
      t.text :name
      t.string :status, index: true
      t.boolean :cases_required
      t.references :fba_warehouse, index: true

      t.timestamps null: false
    end
  end
end
