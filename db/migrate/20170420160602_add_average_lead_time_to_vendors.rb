class AddAverageLeadTimeToVendors < ActiveRecord::Migration[5.0]
  def up
    change_table :vendors do |t|
      t.rename :lead_time_days, :lead_time_days_obsolete
      t.integer :lead_time_days
    end
  end

  def down
    change_table :vendors do |t|
      t.remove :lead_time_days
      t.rename :lead_time_days_obsolete, :lead_time_days
    end
  end
end
