class AddSecondsPerUnitToReports < ActiveRecord::Migration[5.0]
  def change
    change_table :inventory_items_production_reports do |t|
      t.float :seconds_per_unit
    end
  end
end
