class RenameColumnInProducts < ActiveRecord::Migration[5.0]
  def change
    rename_column :products, :selling_price_amount, :selling_price
  end
end
