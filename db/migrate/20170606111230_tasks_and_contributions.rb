class TasksAndContributions < ActiveRecord::Migration[5.0]
  def change
    change_table :tasks do |t|
      t.decimal :labour_cost
      t.integer :user_id, index: true
      t.integer :duration_estimate
      t.date :date
    end

    reversible do |dir|
      dir.up do
        MigrationTask.find_each do |task|
          contribution = MigrationContribution.where(task_id: task.id).first
          task.update_column(:user_id,     contribution.user_id)
          task.update_column(:labour_cost, contribution.labour_cost)
        end
        change_column :tasks, :user_id, :integer, null: false, index: true, foreign_key: true
      end

      dir.down do
        # do nothing
      end
    end
  end
end

class MigrationTask < ApplicationRecord
  self.table_name = :tasks
end

class MigrationContribution < ApplicationRecord
  self.table_name = :contributions
end
