class CreateShipmentEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :shipment_entries do |t|
      t.references :amazon_shipment, index: true
      t.references :product, index: true
      t.integer :quantity

      t.timestamps
    end
  end
end
