class TasksQuantityCases < ActiveRecord::Migration[5.0]
  def change
    change_table :tasks do |t|
      t.rename :quantity_cases, :quantity_cases_obsolete
      t.integer :product_items_per_case
    end

    reversible do |dir|
      dir.up do
        MigrationTask.find_each do |task|
          ii = MigrationInventoryItem.find(task.inventory_item_id)
          product = MigrationProduct.find(ii.inventoriable_id)
          task.update_column(:product_items_per_case, product.items_per_case)
        end
      end

      dir.down do
        # do nothing
      end
    end
  end
end

class MigrationTask < ApplicationRecord
  self.table_name = :tasks
end

class MigrationInventoryItem < ApplicationRecord
  self.table_name = :inventory_items
end

class MigrationProduct < ApplicationRecord
  self.table_name = :products
end
