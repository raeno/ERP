class AddUserToAmazonShipment < ActiveRecord::Migration[5.0]
  def change
    add_column :amazon_shipments, :user_id, :integer, index: true
  end
end
