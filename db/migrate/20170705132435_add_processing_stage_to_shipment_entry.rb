class AddProcessingStageToShipmentEntry < ActiveRecord::Migration[5.0]
  def change
    add_column :shipment_entries, :processing_stage, :string, index: true, default: 'NEW'
  end
end
