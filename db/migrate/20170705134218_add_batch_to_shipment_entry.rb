class AddBatchToShipmentEntry < ActiveRecord::Migration[5.0]
  def change
    add_column :shipment_entries, :batch_id, :integer, index: true
  end
end
