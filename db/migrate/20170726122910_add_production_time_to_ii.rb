class AddProductionTimeToIi < ActiveRecord::Migration[5.0]
  def up
    change_table :inventory_items do |t|
      t.float :production_seconds_per_unit, null: false, default: 0
      t.integer :production_setup_minutes, null: false, default: 0
    end

    change_table :inventory_items_production_reports do |t|
      t.remove :seconds_per_unit
    end
  end

  def down
    change_table :inventory_items_production_reports do |t|
      t.float :seconds_per_unit
    end

    change_table :inventory_items do |t|
      t.remove :production_seconds_per_unit
      t.remove :production_setup_minutes
    end
  end
end
