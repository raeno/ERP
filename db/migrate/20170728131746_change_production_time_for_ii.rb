class ChangeProductionTimeForIi < ActiveRecord::Migration[5.0]

  def inventory_item_class
    klass = Class.new(ActiveRecord::Base)
    klass.table_name = :inventory_items
    klass
  end

  def up
    change_table :inventory_items do |t|
      t.integer :production_units_per_minute
    end

    inventory_item_class.where.not(production_seconds_per_unit: 0).find_each do |ii|
      ii.update_column(:production_units_per_minute, (60 / ii.production_seconds_per_unit).round)
    end

    change_table :inventory_items do |t|
      t.remove :production_seconds_per_unit
    end
  end

  def down
    change_table :inventory_items do |t|
      t.float :production_seconds_per_unit, null: false, default: 0
    end

    inventory_item_class.where.not(production_units_per_minute: [0, nil]).find_each do |ii|
      ii.update_column(:production_seconds_per_unit, (60 / ii.production_units_per_minute).round)
    end

    change_table :inventory_items do |t|
      t.remove :production_units_per_minute
    end
  end
end
