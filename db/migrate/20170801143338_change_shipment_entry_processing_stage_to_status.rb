class ChangeShipmentEntryProcessingStageToStatus < ActiveRecord::Migration[5.0]
  def change
    rename_column :shipment_entries, :processing_stage, :status
  end
end
