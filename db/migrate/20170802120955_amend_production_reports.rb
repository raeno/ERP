class AmendProductionReports < ActiveRecord::Migration[5.0]
  def change
    change_table :inventory_items_production_reports do |t|
      t.decimal :can_cover_optimal
      t.decimal :assigned, null: false, default: 0
    end
  end
end
