class AddAmazonShipmentToBatch < ActiveRecord::Migration[5.0]
  def change
    add_column :batches, :amazon_shipment_id, :integer, index: true
  end
end
