class ProductionReportCanCoverOptimal < ActiveRecord::Migration[5.0]
  def up
    change_table :inventory_items_production_reports do |t|
      t.change :can_cover_optimal, :decimal, null: false
    end
  end

  def down
    change_table :inventory_items_production_reports do |t|
      t.change :can_cover_optimal, :decimal, null: true
    end
  end
end
