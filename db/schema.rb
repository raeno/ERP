# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170317131213) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"


  create_enum "container_status", "active", "pending", "closed", "expired"
  create_table "amazon_inventories", force: :cascade do |t|
    t.integer "fulfillable",     :default=>0
    t.integer "reserved",        :default=>0
    t.integer "inbound_working", :default=>0
    t.integer "inbound_shipped", :default=>0
    t.integer "product_id",      :null=>false

    t.index ["product_id"], :name=>"index_amazon_inventories_on_product_id", :unique=>true, :using=>:btree
  end

  create_table "amazon_inventory_histories", force: :cascade do |t|
    t.integer  "fulfillable"
    t.integer  "reserved"
    t.integer  "inbound_working"
    t.integer  "inbound_shipped"
    t.datetime "reported_at"
    t.integer  "amazon_inventory_id"
    t.datetime "created_at",          :null=>false
    t.datetime "updated_at",          :null=>false

    t.index ["amazon_inventory_id"], :name=>"index_amazon_inventory_histories_on_amazon_inventory_id", :using=>:btree
    t.index ["reported_at"], :name=>"index_amazon_inventory_histories_on_reported_at", :using=>:btree
  end

  create_table "attachments", force: :cascade do |t|
    t.text     "url",        :null=>false
    t.text     "mimetype"
    t.datetime "created_at", :null=>false
    t.datetime "updated_at", :null=>false
  end

  create_table "batches", force: :cascade do |t|
    t.integer  "product_id",     :null=>false
    t.integer  "quantity",       :null=>false
    t.text     "notes"
    t.datetime "created_at",     :null=>false
    t.datetime "updated_at",     :null=>false
    t.date     "completed_on"
    t.integer  "zone_id",        :null=>false
    t.hstore   "auxillary_info"
    t.integer  "task_id"

    t.index ["auxillary_info"], :name=>"index_batches_on_auxillary_info", :using=>:gist
    t.index ["completed_on"], :name=>"index_batches_on_completed_on", :using=>:btree
    t.index ["product_id"], :name=>"index_batches_on_product_id", :using=>:btree
    t.index ["task_id"], :name=>"index_batches_on_task_id", :unique=>true, :using=>:btree
    t.index ["zone_id"], :name=>"index_batches_on_zone_id", :using=>:btree
  end

  create_table "batches_users", force: :cascade do |t|
    t.integer "batch_id"
    t.integer "user_id"

    t.index ["batch_id", "user_id"], :name=>"index_batches_users_on_batch_id_and_user_id", :unique=>true, :using=>:btree
  end

  create_table "contributions", force: :cascade do |t|
    t.integer  "task_id",     :null=>false
    t.integer  "user_id",     :null=>false
    t.datetime "started_at",  :null=>false
    t.datetime "finished_at"
    t.float    "coefficient", :default=>1.0, :null=>false
    t.decimal  "labour_cost"

    t.index ["finished_at"], :name=>"index_contributions_on_finished_at", :using=>:btree
    t.index ["started_at"], :name=>"index_contributions_on_started_at", :using=>:btree
    t.index ["task_id"], :name=>"index_contributions_on_task_id", :using=>:btree
    t.index ["user_id"], :name=>"index_contributions_on_user_id", :using=>:btree
  end

  create_table "fba_allocations", force: :cascade do |t|
    t.integer  "fba_warehouse_id",     :null=>false
    t.integer  "product_id",           :null=>false
    t.integer  "quantity",             :null=>false
    t.integer  "quantity_to_allocate"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "shipment_type"

    t.index ["fba_warehouse_id"], :name=>"index_fba_allocations_on_fba_warehouse_id", :using=>:btree
    t.index ["product_id", "fba_warehouse_id", "quantity_to_allocate", "shipment_type"], :name=>"uniqueness_index", :unique=>true, :using=>:btree
    t.index ["product_id", "quantity_to_allocate", "shipment_type"], :name=>"product_quantity_shipment_type_index", :using=>:btree
    t.index ["quantity_to_allocate"], :name=>"index_fba_allocations_on_quantity_to_allocate", :using=>:btree
  end

  create_table "fba_warehouses", force: :cascade do |t|
    t.string   "name",       :null=>false
    t.datetime "created_at"
    t.datetime "updated_at"

    t.index ["name"], :name=>"index_fba_warehouses_on_name", :using=>:btree
  end

  create_table "ingredients", force: :cascade do |t|
    t.integer  "product_id",        :null=>false
    t.decimal  "quantity",          :default=>"1.0", :null=>false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "inventory_item_id", :null=>false

    t.index ["inventory_item_id", "product_id"], :name=>"index_ingredients_on_inventory_item_id_and_product_id", :unique=>true, :using=>:btree
    t.index ["product_id"], :name=>"index_ingredients_on_product_id", :using=>:btree
  end

  create_table "inventories", force: :cascade do |t|
    t.integer  "inventory_item_id", :null=>false
    t.decimal  "quantity",          :null=>false
    t.datetime "created_at",        :null=>false
    t.datetime "updated_at",        :null=>false

    t.index ["inventory_item_id"], :name=>"index_inventories_on_inventory_item_id", :unique=>true, :using=>:btree
  end

  create_table "inventory_items", force: :cascade do |t|
    t.integer  "zone_id"
    t.integer  "inventoriable_id"
    t.string   "inventoriable_type"
    t.datetime "created_at",         :null=>false
    t.datetime "updated_at",         :null=>false
    t.integer  "production_zone_id"

    t.index ["inventoriable_type", "inventoriable_id", "zone_id"], :name=>"inventory_items_unique_inventoriable_and_zone", :unique=>true, :using=>:btree
    t.index ["production_zone_id"], :name=>"index_inventory_items_on_production_zone_id", :using=>:btree
    t.index ["zone_id"], :name=>"index_inventory_items_on_zone_id", :using=>:btree
  end

  create_table "inventory_items_production_reports", force: :cascade do |t|
    t.integer "inventory_item_id",          :null=>false
    t.decimal "in_stock",                   :null=>false
    t.decimal "demand",                     :null=>false
    t.decimal "demand_coverage",            :null=>false
    t.decimal "supply",                     :null=>false
    t.decimal "to_cover",                   :null=>false
    t.decimal "can_cover",                  :null=>false
    t.decimal "can_cover_in_cases"
    t.boolean "previous_zone_deficit"
    t.integer "deficit_inventoriable_id"
    t.string  "deficit_inventoriable_type"
    t.string  "deficit_ingredient_name"

    t.index ["can_cover"], :name=>"index_inventory_items_production_reports_on_can_cover", :using=>:btree
    t.index ["can_cover_in_cases"], :name=>"index_inventory_items_production_reports_on_can_cover_in_cases", :using=>:btree
    t.index ["inventory_item_id"], :name=>"index_inventory_items_production_reports_on_inventory_item_id", :unique=>true, :using=>:btree
  end

  create_table "invoice_fees", force: :cascade do |t|
    t.integer  "invoice_id", :null=>false
    t.string   "name",       :null=>false
    t.decimal  "price",      :null=>false
    t.datetime "created_at"
    t.datetime "updated_at"

    t.index ["invoice_id"], :name=>"index_invoice_fees_on_invoice_id", :using=>:btree
  end

  create_table "invoice_items", force: :cascade do |t|
    t.integer  "invoice_id",            :null=>false
    t.integer  "material_id"
    t.string   "pkg_external_name"
    t.string   "pkg_form"
    t.integer  "pkg_unit_id"
    t.float    "pkg_amount"
    t.decimal  "pkg_price"
    t.integer  "quantity",              :default=>1, :null=>false
    t.decimal  "total_material_amount", :null=>false
    t.decimal  "total_price",           :null=>false
    t.string   "batch_number"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "received_date"
    t.date     "expiration_date"
    t.integer  "material_container_id"

    t.index ["invoice_id"], :name=>"index_invoice_items_on_invoice_id", :using=>:btree
    t.index ["material_container_id"], :name=>"index_invoice_items_on_material_container_id", :unique=>true, :using=>:btree
    t.index ["material_id"], :name=>"index_invoice_items_on_material_id", :using=>:btree
    t.index ["pkg_unit_id"], :name=>"index_invoice_items_on_pkg_unit_id", :using=>:btree
  end

  create_table "invoices", force: :cascade do |t|
    t.integer  "vendor_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "number",         :default=>"", :null=>false
    t.text     "attachment_url"
    t.date     "date",           :null=>false
    t.decimal  "total_price",    :null=>false
    t.date     "received_date"
    t.text     "notes"
    t.date     "paid_date"

    t.index ["date"], :name=>"index_invoices_on_date", :using=>:btree
    t.index ["vendor_id"], :name=>"index_invoices_on_vendor_id", :using=>:btree
  end

  create_table "material_attachments", force: :cascade do |t|
    t.integer  "material_id",   :null=>false
    t.integer  "attachment_id", :null=>false
    t.datetime "created_at",    :null=>false
    t.datetime "updated_at",    :null=>false

    t.index ["attachment_id"], :name=>"index_material_attachments_on_attachment_id", :using=>:btree
    t.index ["material_id"], :name=>"index_material_attachments_on_material_id", :using=>:btree
  end

  create_table "material_categories", force: :cascade do |t|
    t.string   "name",       :null=>false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

# Could not dump table "material_containers" because of following StandardError
#   Unknown type 'container_status' for column 'status'


  create_table "material_media", force: :cascade do |t|
    t.text     "url"
    t.integer  "material_id"
    t.datetime "created_at",  :null=>false
    t.datetime "updated_at",  :null=>false
    t.text     "mimetype"

    t.index ["material_id"], :name=>"index_material_media_on_material_id", :using=>:btree
  end

  create_table "material_transactions", force: :cascade do |t|
    t.float    "quantity"
    t.float    "inventory_amount"
    t.integer  "batch_id"
    t.integer  "material_container_id"
    t.datetime "created_at",            :null=>false
    t.datetime "updated_at",            :null=>false

    t.index ["batch_id"], :name=>"index_material_transactions_on_batch_id", :using=>:btree
    t.index ["material_container_id"], :name=>"index_material_transactions_on_material_container_id", :using=>:btree
  end

  create_table "material_vendors", force: :cascade do |t|
    t.integer "material_id", :null=>false
    t.integer "vendor_id",   :null=>false
    t.string  "url"

    t.index ["material_id", "vendor_id"], :name=>"index_material_vendors_on_material_id_and_vendor_id", :unique=>true, :using=>:btree
    t.index ["vendor_id"], :name=>"index_material_vendors_on_vendor_id", :using=>:btree
  end

  create_table "materials", force: :cascade do |t|
    t.string   "name",                :null=>false
    t.decimal  "unit_price",          :default=>"0.0", :null=>false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "ingredient_unit_id",  :null=>false
    t.integer  "vendor_id_obsolete"
    t.string   "pdf_url"
    t.string   "image_url"
    t.string   "vendor_url_obsolete"
    t.integer  "category_id",         :default=>1, :null=>false
    t.integer  "unit_id",             :null=>false

    t.index ["category_id"], :name=>"index_materials_on_category_id", :using=>:btree
    t.index ["ingredient_unit_id"], :name=>"index_materials_on_ingredient_unit_id", :using=>:btree
    t.index ["unit_id"], :name=>"index_materials_on_unit_id", :using=>:btree
    t.index ["vendor_id_obsolete"], :name=>"index_materials_on_vendor_id_obsolete", :using=>:btree
  end

  create_table "ordering_sizes", force: :cascade do |t|
    t.integer "material_id",                   :null=>false
    t.integer "unit_id",                       :null=>false
    t.float   "amount",                        :null=>false
    t.float   "amount_in_main_material_units", :null=>false
    t.string  "name"

    t.index ["amount_in_main_material_units"], :name=>"index_ordering_sizes_on_amount_in_main_material_units", :using=>:btree
    t.index ["material_id"], :name=>"index_ordering_sizes_on_material_id", :using=>:btree
  end

  create_table "product_attachments", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "attachment_id"
    t.datetime "created_at",    :null=>false
    t.datetime "updated_at",    :null=>false

    t.index ["attachment_id"], :name=>"index_product_attachments_on_attachment_id", :using=>:btree
    t.index ["product_id"], :name=>"index_product_attachments_on_product_id", :using=>:btree
  end

  create_table "product_cogs_snapshots", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "batch_id"
    t.decimal  "cogs"
    t.datetime "created_at", :null=>false
    t.datetime "updated_at", :null=>false
    t.integer  "zone_id"

    t.index ["batch_id"], :name=>"index_product_cogs_snapshots_on_batch_id", :using=>:btree
    t.index ["product_id"], :name=>"index_product_cogs_snapshots_on_product_id", :using=>:btree
    t.index ["zone_id"], :name=>"index_product_cogs_snapshots_on_zone_id", :using=>:btree
  end

  create_table "product_zone_availabilities", force: :cascade do |t|
    t.integer "product_id"
    t.integer "zone_id"

    t.index ["product_id", "zone_id"], :name=>"index_product_zone_availabilities_on_product_id_and_zone_id", :using=>:btree
    t.index ["product_id"], :name=>"index_product_zone_availabilities_on_product_id", :using=>:btree
    t.index ["zone_id"], :name=>"index_product_zone_availabilities_on_zone_id", :using=>:btree
  end

  create_table "products", force: :cascade do |t|
    t.string   "title"
    t.string   "seller_sku"
    t.text     "asin"
    t.string   "fnsku"
    t.string   "size"
    t.string   "list_price_amount"
    t.string   "list_price_currency"
    t.integer  "total_supply_quantity"
    t.integer  "in_stock_supply_quantity"
    t.string   "small_image_url"
    t.boolean  "is_active",                :default=>true
    t.datetime "created_at",               :null=>false
    t.datetime "updated_at",               :null=>false
    t.float    "inbound_qty",              :default=>0.0
    t.float    "sold_last_24_hours",       :default=>0.0
    t.float    "weeks_of_cover",           :default=>0.0
    t.float    "sellable_qty",             :default=>0.0
    t.string   "internal_title"
    t.integer  "sales_rank"
    t.float    "selling_price_amount"
    t.string   "selling_price_currency"
    t.integer  "items_per_case",           :default=>0, :null=>false
    t.integer  "production_buffer_days",   :default=>35, :null=>false
    t.integer  "batch_min_quantity",       :default=>0, :null=>false
    t.integer  "batch_max_quantity"
    t.decimal  "fee",                      :default=>"0.0", :null=>false
    t.text     "notes"
  end

  create_table "products_shipment_reports", force: :cascade do |t|
    t.integer "product_id"
    t.integer "packed_quantity"
    t.integer "packed_cases_quantity"
    t.float   "packed_quantity_in_days"
    t.float   "days_of_cover"
    t.integer "total_amazon_inventory"
    t.decimal "amazon_coverage_ratio"
    t.integer "can_ship"
    t.integer "can_ship_in_cases"
    t.float   "can_ship_in_days"
    t.integer "to_allocate_item_quantity"
    t.integer "to_allocate_case_quantity"
    t.json    "items_fba_allocation"
    t.json    "cases_fba_allocation"
    t.integer "items_shipment_priority",   :default=>0
    t.integer "cases_shipment_priority",   :default=>0

    t.index ["cases_shipment_priority"], :name=>"index_products_shipment_reports_on_cases_shipment_priority", :using=>:btree
    t.index ["items_shipment_priority"], :name=>"index_products_shipment_reports_on_items_shipment_priority", :using=>:btree
    t.index ["product_id"], :name=>"index_products_shipment_reports_on_product_id", :unique=>true, :using=>:btree
  end

  create_table "settings", force: :cascade do |t|
    t.string   "aws_access_key_id"
    t.string   "aws_secret_key"
    t.string   "mws_marketplace_id"
    t.string   "mws_merchant_id"
    t.string   "address_name"
    t.string   "address_line1"
    t.string   "address_line2"
    t.string   "address_city"
    t.string   "address_state"
    t.string   "address_zip_code"
    t.string   "address_country"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "red_urgency_level_days",    :default=>10, :null=>false
    t.integer  "yellow_urgency_level_days", :default=>20, :null=>false
  end

  create_table "tasks", force: :cascade do |t|
    t.integer  "inventory_item_id", :null=>false
    t.datetime "started_at"
    t.datetime "finished_at"
    t.integer  "quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "quantity_cases"

    t.index ["inventory_item_id"], :name=>"index_tasks_on_inventory_item_id", :using=>:btree
  end

  create_table "unit_conversions", force: :cascade do |t|
    t.integer "from_unit_id", :null=>false
    t.integer "to_unit_id",   :null=>false
    t.decimal "coefficient",  :null=>false

    t.index ["from_unit_id"], :name=>"index_unit_conversions_on_from_unit_id", :using=>:btree
    t.index ["to_unit_id", "from_unit_id"], :name=>"index_unit_conversions_on_to_unit_id_and_from_unit_id", :unique=>true, :using=>:btree
  end

  create_table "units", force: :cascade do |t|
    t.text "name"
    t.text "full_name"

    t.index ["name"], :name=>"index_units_on_name", :using=>:btree
  end

  create_table "user_zones", force: :cascade do |t|
    t.integer "user_id"
    t.integer "zone_id"

    t.index ["user_id", "zone_id"], :name=>"index_user_zones_on_user_id_and_zone_id", :using=>:btree
    t.index ["user_id"], :name=>"index_user_zones_on_user_id", :using=>:btree
    t.index ["zone_id"], :name=>"index_user_zones_on_zone_id", :using=>:btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  :default=>"", :null=>false
    t.string   "encrypted_password",     :default=>"", :null=>false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default=>0, :null=>false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",             :null=>false
    t.datetime "updated_at",             :null=>false
    t.boolean  "is_active",              :default=>true
    t.string   "first_name"
    t.string   "last_name"
    t.boolean  "admin",                  :default=>false
    t.decimal  "labour_rate",            :default=>"0.0", :null=>false

    t.index ["email"], :name=>"index_users_on_email", :unique=>true, :using=>:btree
    t.index ["reset_password_token"], :name=>"index_users_on_reset_password_token", :unique=>true, :using=>:btree
  end

  create_table "vendors", force: :cascade do |t|
    t.string   "name",           :null=>false
    t.string   "contact_name"
    t.string   "phone"
    t.string   "contact_email"
    t.string   "contact_fax"
    t.string   "order_email"
    t.string   "order_fax"
    t.string   "tags"
    t.string   "address"
    t.text     "notes"
    t.datetime "created_at",     :null=>false
    t.datetime "updated_at",     :null=>false
    t.string   "website"
    t.integer  "lead_time_days", :default=>0, :null=>false
  end

  create_table "zones", force: :cascade do |t|
    t.string   "name"
    t.integer  "production_order"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_foreign_key "amazon_inventories", "products"
  add_foreign_key "ingredients", "products"
  add_foreign_key "inventories", "inventory_items"
  add_foreign_key "invoice_fees", "invoices"
  add_foreign_key "invoice_items", "invoices"
  add_foreign_key "invoice_items", "materials"
  add_foreign_key "material_containers", "materials"
  add_foreign_key "materials", "vendors", column: "vendor_id_obsolete"
  add_foreign_key "ordering_sizes", "materials"
  add_foreign_key "ordering_sizes", "units"
  add_foreign_key "tasks", "inventory_items"
end
