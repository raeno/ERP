# frozen_string_literal: true
unless Setting.any?
  Setting.create!(
    aws_access_key_id:  "AKIAIO5LDEK7A4ST4NBQ",
    aws_secret_key:     "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",
    mws_marketplace_id: "ATVPDKIKX0DER",
    mws_merchant_id:    "A3801N6NZH2X3P"
  )
end

unless Zone.any?
  %w(Make Pack Ship).each_with_index { |name, index| Zone.create(name: name, production_order: index) }
end

Unit.find_or_create_by name: 'Each'
inch = Unit.find_or_create_by! name: 'Inch'
foot = Unit.find_or_create_by! name: 'Foot'
ml   = Unit.find_or_create_by! name: 'ML'
lbs  = Unit.find_or_create_by! name: 'Lbs'
oz   = Unit.find_or_create_by! name: 'Oz'

ConversionRate.delete_all
action = ConversionRates::Create.new
action.call(from_unit_id: foot.id, to_unit_id: inch.id, rate: 12)
action.call(from_unit_id: lbs.id,  to_unit_id: ml.id,   rate: 473.18)
action.call(from_unit_id: lbs.id,  to_unit_id: oz.id,   rate: 16)
action.call(from_unit_id: oz.id,   to_unit_id: ml.id,   rate: 28.35)

# These are dummy data for demoing.
unless Material.any?
  ml_unit = Unit.find_by name: 'ml'
  each_unit = Unit.find_by name: 'each'
  make_materials =
    [
      { name: "Lemon oil",        unit: ml_unit, unit_price: "0.09" },
      { name: "Bergamot oil",     unit: ml_unit, unit_price: "0.25" },
      { name: "Sweet Orange oil", unit: ml_unit, unit_price: "0.32" },
      { name: "Lavender oil",     unit: ml_unit, unit_price: "0.12" },
      { name: "Bottle 10ml",      unit: each_unit,  unit_price: "0.07" },
      { name: "Bottle 30ml",      unit: each_unit,  unit_price: "0.09" },
      { name: "Cap",              unit: each_unit,  unit_price: "0.02" }
    ]

  make_materials.map do |hash|
    Material.create! hash
  end

  pack_materials =
    [
      { name: "Bobble bag",       unit: each_unit, unit_price: "0.005" },
      { name: "Business card",    unit: each_unit, unit_price: "0.009" },
      { name: "Gift box",         unit: each_unit, unit_price: "2.99" }
    ]

  pack_materials.map do |hash|
    Material.create! hash
  end
end

Rake::Task["data:inventory_items:import_all"].invoke
