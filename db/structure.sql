--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.8
-- Dumped by pg_dump version 9.5.8

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


SET search_path = public, pg_catalog;

--
-- Name: container_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE container_status AS ENUM (
    'active',
    'pending',
    'closed',
    'expired'
);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: amazon_inventories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE amazon_inventories (
    id integer NOT NULL,
    fulfillable integer DEFAULT 0,
    reserved integer DEFAULT 0,
    inbound_working integer DEFAULT 0,
    inbound_shipped integer DEFAULT 0,
    product_id integer NOT NULL
);


--
-- Name: amazon_inventories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE amazon_inventories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: amazon_inventories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE amazon_inventories_id_seq OWNED BY amazon_inventories.id;


--
-- Name: amazon_inventory_histories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE amazon_inventory_histories (
    id integer NOT NULL,
    fulfillable integer,
    reserved integer,
    inbound_working integer,
    inbound_shipped integer,
    reported_at timestamp without time zone,
    amazon_inventory_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: amazon_inventory_histories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE amazon_inventory_histories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: amazon_inventory_histories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE amazon_inventory_histories_id_seq OWNED BY amazon_inventory_histories.id;


--
-- Name: amazon_shipments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE amazon_shipments (
    id integer NOT NULL,
    shipment_id text,
    name text,
    status character varying,
    cases_required boolean,
    fba_warehouse_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    user_id integer
);


--
-- Name: amazon_shipments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE amazon_shipments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: amazon_shipments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE amazon_shipments_id_seq OWNED BY amazon_shipments.id;


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: attachments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE attachments (
    id integer NOT NULL,
    url text NOT NULL,
    mimetype text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE attachments_id_seq OWNED BY attachments.id;


--
-- Name: batches; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE batches (
    id integer NOT NULL,
    product_id integer NOT NULL,
    quantity integer NOT NULL,
    notes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    completed_on date,
    zone_id integer NOT NULL,
    auxillary_info hstore,
    task_id integer,
    amazon_shipment_id integer
);


--
-- Name: batches_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE batches_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: batches_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE batches_id_seq OWNED BY batches.id;


--
-- Name: batches_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE batches_users (
    id integer NOT NULL,
    batch_id integer,
    user_id integer
);


--
-- Name: batches_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE batches_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: batches_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE batches_users_id_seq OWNED BY batches_users.id;


--
-- Name: contributions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE contributions (
    id integer NOT NULL,
    task_id integer NOT NULL,
    user_id integer NOT NULL,
    started_at timestamp without time zone NOT NULL,
    finished_at timestamp without time zone,
    labour_cost numeric
);


--
-- Name: contributions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE contributions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contributions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE contributions_id_seq OWNED BY contributions.id;


--
-- Name: conversion_rates; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE conversion_rates (
    id integer NOT NULL,
    from_unit_id integer NOT NULL,
    to_unit_id integer NOT NULL,
    rate numeric NOT NULL
);


--
-- Name: conversion_rates_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE conversion_rates_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: conversion_rates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE conversion_rates_id_seq OWNED BY conversion_rates.id;


--
-- Name: fba_allocations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE fba_allocations (
    id integer NOT NULL,
    fba_warehouse_id integer NOT NULL,
    product_id integer NOT NULL,
    quantity integer NOT NULL,
    quantity_to_allocate integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    shipment_type text
);


--
-- Name: fba_allocations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE fba_allocations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: fba_allocations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE fba_allocations_id_seq OWNED BY fba_allocations.id;


--
-- Name: fba_warehouses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE fba_warehouses (
    id integer NOT NULL,
    name character varying NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: fba_warehouses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE fba_warehouses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: fba_warehouses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE fba_warehouses_id_seq OWNED BY fba_warehouses.id;


--
-- Name: ingredients; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ingredients (
    id integer NOT NULL,
    product_id integer NOT NULL,
    quantity_obsolete numeric DEFAULT 1 NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    inventory_item_id integer NOT NULL,
    quantity_in_small_units numeric DEFAULT 1 NOT NULL
);


--
-- Name: ingredients_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE ingredients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ingredients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE ingredients_id_seq OWNED BY ingredients.id;


--
-- Name: inventories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE inventories (
    id integer NOT NULL,
    inventory_item_id integer NOT NULL,
    quantity numeric NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: inventories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE inventories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: inventories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE inventories_id_seq OWNED BY inventories.id;


--
-- Name: inventory_items; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE inventory_items (
    id integer NOT NULL,
    zone_id integer,
    inventoriable_id integer,
    inventoriable_type character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    production_zone_id integer,
    production_setup_minutes integer DEFAULT 0 NOT NULL,
    production_units_per_minute integer
);


--
-- Name: inventory_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE inventory_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: inventory_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE inventory_items_id_seq OWNED BY inventory_items.id;


--
-- Name: inventory_items_production_reports; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE inventory_items_production_reports (
    id integer NOT NULL,
    inventory_item_id integer NOT NULL,
    in_stock numeric NOT NULL,
    demand numeric NOT NULL,
    demand_coverage numeric NOT NULL,
    supply numeric NOT NULL,
    to_cover numeric NOT NULL,
    can_cover numeric NOT NULL,
    can_cover_in_cases numeric,
    previous_zone_deficit boolean,
    deficit_inventoriable_id integer,
    deficit_inventoriable_type character varying,
    deficit_ingredient_name character varying,
    can_cover_optimal numeric NOT NULL,
    assigned numeric DEFAULT 0 NOT NULL
);


--
-- Name: inventory_items_production_reports_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE inventory_items_production_reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: inventory_items_production_reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE inventory_items_production_reports_id_seq OWNED BY inventory_items_production_reports.id;


--
-- Name: invoice_fees; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice_fees (
    id integer NOT NULL,
    invoice_id integer NOT NULL,
    name character varying NOT NULL,
    price numeric NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: invoice_fees_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE invoice_fees_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: invoice_fees_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE invoice_fees_id_seq OWNED BY invoice_fees.id;


--
-- Name: invoice_items; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoice_items (
    id integer NOT NULL,
    invoice_id integer NOT NULL,
    material_id integer,
    pkg_external_name character varying,
    pkg_form character varying,
    pkg_unit_id integer,
    pkg_amount double precision,
    pkg_price numeric,
    quantity integer DEFAULT 1 NOT NULL,
    total_material_amount numeric NOT NULL,
    total_price numeric NOT NULL,
    batch_number character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    received_date date,
    expiration_date date,
    material_container_id integer
);


--
-- Name: invoice_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE invoice_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: invoice_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE invoice_items_id_seq OWNED BY invoice_items.id;


--
-- Name: invoices; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invoices (
    id integer NOT NULL,
    vendor_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    number character varying DEFAULT ''::character varying NOT NULL,
    attachment_url text,
    date date NOT NULL,
    total_price numeric NOT NULL,
    received_date date,
    notes text,
    paid_date date
);


--
-- Name: invoices_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE invoices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: invoices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE invoices_id_seq OWNED BY invoices.id;


--
-- Name: material_attachments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE material_attachments (
    id integer NOT NULL,
    material_id integer NOT NULL,
    attachment_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: material_attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE material_attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: material_attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE material_attachments_id_seq OWNED BY material_attachments.id;


--
-- Name: material_categories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE material_categories (
    id integer NOT NULL,
    name character varying NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: material_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE material_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: material_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE material_categories_id_seq OWNED BY material_categories.id;


--
-- Name: material_containers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE material_containers (
    id integer NOT NULL,
    material_id integer NOT NULL,
    external_name text,
    unit_id integer NOT NULL,
    amount numeric DEFAULT 1.0 NOT NULL,
    price numeric NOT NULL,
    lot_number text,
    expiration_date date,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    amount_left numeric DEFAULT 0.0 NOT NULL,
    old_status integer DEFAULT 0,
    ordering_size_id integer,
    status container_status DEFAULT 'pending'::container_status
);


--
-- Name: material_containers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE material_containers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: material_containers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE material_containers_id_seq OWNED BY material_containers.id;


--
-- Name: material_media; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE material_media (
    id integer NOT NULL,
    url text,
    material_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    mimetype text
);


--
-- Name: material_media_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE material_media_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: material_media_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE material_media_id_seq OWNED BY material_media.id;


--
-- Name: material_transactions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE material_transactions (
    id integer NOT NULL,
    quantity double precision,
    inventory_amount double precision,
    batch_id integer,
    material_container_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: material_transactions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE material_transactions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: material_transactions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE material_transactions_id_seq OWNED BY material_transactions.id;


--
-- Name: material_vendors; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE material_vendors (
    id integer NOT NULL,
    material_id integer NOT NULL,
    vendor_id integer NOT NULL,
    url character varying
);


--
-- Name: material_vendors_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE material_vendors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: material_vendors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE material_vendors_id_seq OWNED BY material_vendors.id;


--
-- Name: materials; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE materials (
    id integer NOT NULL,
    name character varying NOT NULL,
    unit_price numeric DEFAULT 0 NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    ingredient_unit_id integer NOT NULL,
    image_url character varying,
    category_id integer DEFAULT 1 NOT NULL,
    unit_id integer NOT NULL,
    unit_conversion_rate numeric
);


--
-- Name: materials_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE materials_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: materials_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE materials_id_seq OWNED BY materials.id;


--
-- Name: ordering_sizes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ordering_sizes (
    id integer NOT NULL,
    material_id integer NOT NULL,
    unit_id integer NOT NULL,
    amount double precision NOT NULL,
    amount_in_main_material_units double precision NOT NULL,
    name character varying
);


--
-- Name: ordering_sizes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE ordering_sizes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ordering_sizes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE ordering_sizes_id_seq OWNED BY ordering_sizes.id;


--
-- Name: product_attachments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE product_attachments (
    id integer NOT NULL,
    product_id integer,
    attachment_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: product_attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_attachments_id_seq OWNED BY product_attachments.id;


--
-- Name: product_cogs_snapshots; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE product_cogs_snapshots (
    id integer NOT NULL,
    product_id integer,
    batch_id integer,
    cogs numeric,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    zone_id integer
);


--
-- Name: product_cogs_snapshots_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_cogs_snapshots_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_cogs_snapshots_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_cogs_snapshots_id_seq OWNED BY product_cogs_snapshots.id;


--
-- Name: product_zone_availabilities; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE product_zone_availabilities (
    id integer NOT NULL,
    product_id integer,
    zone_id integer
);


--
-- Name: product_zone_availabilities_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_zone_availabilities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_zone_availabilities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_zone_availabilities_id_seq OWNED BY product_zone_availabilities.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE products (
    id integer NOT NULL,
    title character varying,
    seller_sku character varying,
    asin text,
    fnsku character varying,
    size character varying,
    list_price_amount character varying,
    list_price_currency character varying,
    total_supply_quantity integer,
    in_stock_supply_quantity integer,
    small_image_url character varying,
    is_active boolean DEFAULT true,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    inbound_qty double precision DEFAULT 0.0,
    sold_last_24_hours double precision DEFAULT 0.0,
    weeks_of_cover double precision DEFAULT 0.0,
    sellable_qty double precision DEFAULT 0.0,
    internal_title character varying,
    sales_rank integer,
    selling_price double precision,
    selling_price_currency character varying,
    items_per_case integer DEFAULT 0 NOT NULL,
    production_buffer_days integer DEFAULT 35 NOT NULL,
    batch_min_quantity integer DEFAULT 0 NOT NULL,
    batch_max_quantity integer,
    fee numeric DEFAULT 0.0 NOT NULL,
    notes text
);


--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE products_id_seq OWNED BY products.id;


--
-- Name: products_shipment_reports; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE products_shipment_reports (
    id integer NOT NULL,
    product_id integer,
    packed_quantity integer,
    packed_cases_quantity integer,
    packed_quantity_in_days double precision,
    days_of_cover double precision,
    total_amazon_inventory integer,
    amazon_coverage_ratio numeric,
    can_ship integer,
    can_ship_in_cases integer,
    can_ship_in_days double precision,
    to_allocate_item_quantity integer,
    to_allocate_case_quantity integer,
    items_fba_allocation json,
    cases_fba_allocation json,
    items_shipment_priority integer DEFAULT 0,
    cases_shipment_priority integer DEFAULT 0
);


--
-- Name: products_shipment_reports_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE products_shipment_reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: products_shipment_reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE products_shipment_reports_id_seq OWNED BY products_shipment_reports.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: settings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE settings (
    id integer NOT NULL,
    aws_access_key_id character varying,
    aws_secret_key character varying,
    mws_marketplace_id character varying,
    mws_merchant_id character varying,
    address_name character varying,
    address_line1 character varying,
    address_line2 character varying,
    address_city character varying,
    address_state character varying,
    address_zip_code character varying,
    address_country character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    red_urgency_level_days integer DEFAULT 10 NOT NULL,
    yellow_urgency_level_days integer DEFAULT 20 NOT NULL
);


--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE settings_id_seq OWNED BY settings.id;


--
-- Name: shipment_entries; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE shipment_entries (
    id integer NOT NULL,
    amazon_shipment_id integer,
    product_id integer,
    quantity integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    status character varying DEFAULT 'NEW'::character varying,
    batch_id integer
);


--
-- Name: shipment_entries_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE shipment_entries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: shipment_entries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE shipment_entries_id_seq OWNED BY shipment_entries.id;


--
-- Name: tasks; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE tasks (
    id integer NOT NULL,
    inventory_item_id integer NOT NULL,
    started_at timestamp without time zone,
    finished_at timestamp without time zone,
    quantity integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    quantity_cases_obsolete integer,
    labour_cost numeric,
    user_id integer NOT NULL,
    duration_estimate integer,
    date date,
    product_items_per_case integer
);


--
-- Name: tasks_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tasks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tasks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tasks_id_seq OWNED BY tasks.id;


--
-- Name: units; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE units (
    id integer NOT NULL,
    name text,
    full_name text
);


--
-- Name: units_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE units_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: units_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE units_id_seq OWNED BY units.id;


--
-- Name: user_zones; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_zones (
    id integer NOT NULL,
    user_id integer,
    zone_id integer
);


--
-- Name: user_zones_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_zones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_zones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_zones_id_seq OWNED BY user_zones.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying,
    last_sign_in_ip character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    is_active boolean DEFAULT true,
    first_name character varying,
    last_name character varying,
    admin boolean DEFAULT false,
    labour_rate numeric DEFAULT 0 NOT NULL
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: vendors; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE vendors (
    id integer NOT NULL,
    name character varying NOT NULL,
    contact_name character varying,
    phone character varying,
    contact_email character varying,
    contact_fax character varying,
    order_email character varying,
    order_fax character varying,
    tags character varying,
    address character varying,
    notes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    website character varying,
    lead_time_days_obsolete integer DEFAULT 0 NOT NULL,
    lead_time_days integer
);


--
-- Name: vendors_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE vendors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: vendors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE vendors_id_seq OWNED BY vendors.id;


--
-- Name: zones; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE zones (
    id integer NOT NULL,
    name character varying,
    production_order integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: zones_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zones_id_seq OWNED BY zones.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY amazon_inventories ALTER COLUMN id SET DEFAULT nextval('amazon_inventories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY amazon_inventory_histories ALTER COLUMN id SET DEFAULT nextval('amazon_inventory_histories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY amazon_shipments ALTER COLUMN id SET DEFAULT nextval('amazon_shipments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY attachments ALTER COLUMN id SET DEFAULT nextval('attachments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY batches ALTER COLUMN id SET DEFAULT nextval('batches_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY batches_users ALTER COLUMN id SET DEFAULT nextval('batches_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY contributions ALTER COLUMN id SET DEFAULT nextval('contributions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY conversion_rates ALTER COLUMN id SET DEFAULT nextval('conversion_rates_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY fba_allocations ALTER COLUMN id SET DEFAULT nextval('fba_allocations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY fba_warehouses ALTER COLUMN id SET DEFAULT nextval('fba_warehouses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY ingredients ALTER COLUMN id SET DEFAULT nextval('ingredients_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY inventories ALTER COLUMN id SET DEFAULT nextval('inventories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY inventory_items ALTER COLUMN id SET DEFAULT nextval('inventory_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY inventory_items_production_reports ALTER COLUMN id SET DEFAULT nextval('inventory_items_production_reports_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_fees ALTER COLUMN id SET DEFAULT nextval('invoice_fees_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_items ALTER COLUMN id SET DEFAULT nextval('invoice_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoices ALTER COLUMN id SET DEFAULT nextval('invoices_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY material_attachments ALTER COLUMN id SET DEFAULT nextval('material_attachments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY material_categories ALTER COLUMN id SET DEFAULT nextval('material_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY material_containers ALTER COLUMN id SET DEFAULT nextval('material_containers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY material_media ALTER COLUMN id SET DEFAULT nextval('material_media_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY material_transactions ALTER COLUMN id SET DEFAULT nextval('material_transactions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY material_vendors ALTER COLUMN id SET DEFAULT nextval('material_vendors_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY materials ALTER COLUMN id SET DEFAULT nextval('materials_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY ordering_sizes ALTER COLUMN id SET DEFAULT nextval('ordering_sizes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_attachments ALTER COLUMN id SET DEFAULT nextval('product_attachments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_cogs_snapshots ALTER COLUMN id SET DEFAULT nextval('product_cogs_snapshots_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_zone_availabilities ALTER COLUMN id SET DEFAULT nextval('product_zone_availabilities_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY products ALTER COLUMN id SET DEFAULT nextval('products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY products_shipment_reports ALTER COLUMN id SET DEFAULT nextval('products_shipment_reports_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY settings ALTER COLUMN id SET DEFAULT nextval('settings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY shipment_entries ALTER COLUMN id SET DEFAULT nextval('shipment_entries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tasks ALTER COLUMN id SET DEFAULT nextval('tasks_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY units ALTER COLUMN id SET DEFAULT nextval('units_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_zones ALTER COLUMN id SET DEFAULT nextval('user_zones_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY vendors ALTER COLUMN id SET DEFAULT nextval('vendors_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zones ALTER COLUMN id SET DEFAULT nextval('zones_id_seq'::regclass);


--
-- Name: amazon_inventories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY amazon_inventories
    ADD CONSTRAINT amazon_inventories_pkey PRIMARY KEY (id);


--
-- Name: amazon_inventory_histories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY amazon_inventory_histories
    ADD CONSTRAINT amazon_inventory_histories_pkey PRIMARY KEY (id);


--
-- Name: amazon_shipments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY amazon_shipments
    ADD CONSTRAINT amazon_shipments_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY attachments
    ADD CONSTRAINT attachments_pkey PRIMARY KEY (id);


--
-- Name: batches_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY batches
    ADD CONSTRAINT batches_pkey PRIMARY KEY (id);


--
-- Name: batches_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY batches_users
    ADD CONSTRAINT batches_users_pkey PRIMARY KEY (id);


--
-- Name: contributions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY contributions
    ADD CONSTRAINT contributions_pkey PRIMARY KEY (id);


--
-- Name: conversion_rates_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY conversion_rates
    ADD CONSTRAINT conversion_rates_pkey PRIMARY KEY (id);


--
-- Name: fba_allocations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY fba_allocations
    ADD CONSTRAINT fba_allocations_pkey PRIMARY KEY (id);


--
-- Name: fba_warehouses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY fba_warehouses
    ADD CONSTRAINT fba_warehouses_pkey PRIMARY KEY (id);


--
-- Name: ingredients_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ingredients
    ADD CONSTRAINT ingredients_pkey PRIMARY KEY (id);


--
-- Name: inventories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY inventories
    ADD CONSTRAINT inventories_pkey PRIMARY KEY (id);


--
-- Name: inventory_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY inventory_items
    ADD CONSTRAINT inventory_items_pkey PRIMARY KEY (id);


--
-- Name: inventory_items_production_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY inventory_items_production_reports
    ADD CONSTRAINT inventory_items_production_reports_pkey PRIMARY KEY (id);


--
-- Name: invoice_fees_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_fees
    ADD CONSTRAINT invoice_fees_pkey PRIMARY KEY (id);


--
-- Name: invoice_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_items
    ADD CONSTRAINT invoice_items_pkey PRIMARY KEY (id);


--
-- Name: invoices_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoices
    ADD CONSTRAINT invoices_pkey PRIMARY KEY (id);


--
-- Name: material_attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY material_attachments
    ADD CONSTRAINT material_attachments_pkey PRIMARY KEY (id);


--
-- Name: material_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY material_categories
    ADD CONSTRAINT material_categories_pkey PRIMARY KEY (id);


--
-- Name: material_containers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY material_containers
    ADD CONSTRAINT material_containers_pkey PRIMARY KEY (id);


--
-- Name: material_media_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY material_media
    ADD CONSTRAINT material_media_pkey PRIMARY KEY (id);


--
-- Name: material_transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY material_transactions
    ADD CONSTRAINT material_transactions_pkey PRIMARY KEY (id);


--
-- Name: material_vendors_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY material_vendors
    ADD CONSTRAINT material_vendors_pkey PRIMARY KEY (id);


--
-- Name: materials_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY materials
    ADD CONSTRAINT materials_pkey PRIMARY KEY (id);


--
-- Name: ordering_sizes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ordering_sizes
    ADD CONSTRAINT ordering_sizes_pkey PRIMARY KEY (id);


--
-- Name: product_attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_attachments
    ADD CONSTRAINT product_attachments_pkey PRIMARY KEY (id);


--
-- Name: product_cogs_snapshots_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_cogs_snapshots
    ADD CONSTRAINT product_cogs_snapshots_pkey PRIMARY KEY (id);


--
-- Name: product_zone_availabilities_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_zone_availabilities
    ADD CONSTRAINT product_zone_availabilities_pkey PRIMARY KEY (id);


--
-- Name: products_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: products_shipment_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY products_shipment_reports
    ADD CONSTRAINT products_shipment_reports_pkey PRIMARY KEY (id);


--
-- Name: settings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- Name: shipment_entries_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY shipment_entries
    ADD CONSTRAINT shipment_entries_pkey PRIMARY KEY (id);


--
-- Name: tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT tasks_pkey PRIMARY KEY (id);


--
-- Name: units_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY units
    ADD CONSTRAINT units_pkey PRIMARY KEY (id);


--
-- Name: user_zones_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_zones
    ADD CONSTRAINT user_zones_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: vendors_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY vendors
    ADD CONSTRAINT vendors_pkey PRIMARY KEY (id);


--
-- Name: zones_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zones
    ADD CONSTRAINT zones_pkey PRIMARY KEY (id);


--
-- Name: index_amazon_inventories_on_product_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_amazon_inventories_on_product_id ON amazon_inventories USING btree (product_id);


--
-- Name: index_amazon_inventory_histories_on_amazon_inventory_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_amazon_inventory_histories_on_amazon_inventory_id ON amazon_inventory_histories USING btree (amazon_inventory_id);


--
-- Name: index_amazon_inventory_histories_on_reported_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_amazon_inventory_histories_on_reported_at ON amazon_inventory_histories USING btree (reported_at);


--
-- Name: index_amazon_shipments_on_fba_warehouse_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_amazon_shipments_on_fba_warehouse_id ON amazon_shipments USING btree (fba_warehouse_id);


--
-- Name: index_amazon_shipments_on_shipment_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_amazon_shipments_on_shipment_id ON amazon_shipments USING btree (shipment_id);


--
-- Name: index_amazon_shipments_on_status; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_amazon_shipments_on_status ON amazon_shipments USING btree (status);


--
-- Name: index_batches_on_auxillary_info; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_batches_on_auxillary_info ON batches USING gist (auxillary_info);


--
-- Name: index_batches_on_completed_on; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_batches_on_completed_on ON batches USING btree (completed_on);


--
-- Name: index_batches_on_product_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_batches_on_product_id ON batches USING btree (product_id);


--
-- Name: index_batches_on_task_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_batches_on_task_id ON batches USING btree (task_id);


--
-- Name: index_batches_on_zone_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_batches_on_zone_id ON batches USING btree (zone_id);


--
-- Name: index_batches_users_on_batch_id_and_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_batches_users_on_batch_id_and_user_id ON batches_users USING btree (batch_id, user_id);


--
-- Name: index_contributions_on_finished_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_contributions_on_finished_at ON contributions USING btree (finished_at);


--
-- Name: index_contributions_on_started_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_contributions_on_started_at ON contributions USING btree (started_at);


--
-- Name: index_contributions_on_task_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_contributions_on_task_id ON contributions USING btree (task_id);


--
-- Name: index_contributions_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_contributions_on_user_id ON contributions USING btree (user_id);


--
-- Name: index_conversion_rates_on_from_unit_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_conversion_rates_on_from_unit_id ON conversion_rates USING btree (from_unit_id);


--
-- Name: index_conversion_rates_on_to_unit_id_and_from_unit_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_conversion_rates_on_to_unit_id_and_from_unit_id ON conversion_rates USING btree (to_unit_id, from_unit_id);


--
-- Name: index_fba_allocations_on_fba_warehouse_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_fba_allocations_on_fba_warehouse_id ON fba_allocations USING btree (fba_warehouse_id);


--
-- Name: index_fba_allocations_on_quantity_to_allocate; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_fba_allocations_on_quantity_to_allocate ON fba_allocations USING btree (quantity_to_allocate);


--
-- Name: index_fba_warehouses_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_fba_warehouses_on_name ON fba_warehouses USING btree (name);


--
-- Name: index_ingredients_on_inventory_item_id_and_product_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_ingredients_on_inventory_item_id_and_product_id ON ingredients USING btree (inventory_item_id, product_id);


--
-- Name: index_ingredients_on_product_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_ingredients_on_product_id ON ingredients USING btree (product_id);


--
-- Name: index_inventories_on_inventory_item_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_inventories_on_inventory_item_id ON inventories USING btree (inventory_item_id);


--
-- Name: index_inventory_items_on_production_zone_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_inventory_items_on_production_zone_id ON inventory_items USING btree (production_zone_id);


--
-- Name: index_inventory_items_on_zone_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_inventory_items_on_zone_id ON inventory_items USING btree (zone_id);


--
-- Name: index_inventory_items_production_reports_on_can_cover; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_inventory_items_production_reports_on_can_cover ON inventory_items_production_reports USING btree (can_cover);


--
-- Name: index_inventory_items_production_reports_on_can_cover_in_cases; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_inventory_items_production_reports_on_can_cover_in_cases ON inventory_items_production_reports USING btree (can_cover_in_cases);


--
-- Name: index_inventory_items_production_reports_on_inventory_item_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_inventory_items_production_reports_on_inventory_item_id ON inventory_items_production_reports USING btree (inventory_item_id);


--
-- Name: index_invoice_fees_on_invoice_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_invoice_fees_on_invoice_id ON invoice_fees USING btree (invoice_id);


--
-- Name: index_invoice_items_on_invoice_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_invoice_items_on_invoice_id ON invoice_items USING btree (invoice_id);


--
-- Name: index_invoice_items_on_material_container_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_invoice_items_on_material_container_id ON invoice_items USING btree (material_container_id);


--
-- Name: index_invoice_items_on_material_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_invoice_items_on_material_id ON invoice_items USING btree (material_id);


--
-- Name: index_invoice_items_on_pkg_unit_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_invoice_items_on_pkg_unit_id ON invoice_items USING btree (pkg_unit_id);


--
-- Name: index_invoices_on_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_invoices_on_date ON invoices USING btree (date);


--
-- Name: index_invoices_on_vendor_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_invoices_on_vendor_id ON invoices USING btree (vendor_id);


--
-- Name: index_material_attachments_on_attachment_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_material_attachments_on_attachment_id ON material_attachments USING btree (attachment_id);


--
-- Name: index_material_attachments_on_material_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_material_attachments_on_material_id ON material_attachments USING btree (material_id);


--
-- Name: index_material_containers_on_amount_left; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_material_containers_on_amount_left ON material_containers USING btree (amount_left);


--
-- Name: index_material_containers_on_material_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_material_containers_on_material_id ON material_containers USING btree (material_id);


--
-- Name: index_material_containers_on_unit_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_material_containers_on_unit_id ON material_containers USING btree (unit_id);


--
-- Name: index_material_media_on_material_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_material_media_on_material_id ON material_media USING btree (material_id);


--
-- Name: index_material_transactions_on_batch_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_material_transactions_on_batch_id ON material_transactions USING btree (batch_id);


--
-- Name: index_material_transactions_on_material_container_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_material_transactions_on_material_container_id ON material_transactions USING btree (material_container_id);


--
-- Name: index_material_vendors_on_material_id_and_vendor_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_material_vendors_on_material_id_and_vendor_id ON material_vendors USING btree (material_id, vendor_id);


--
-- Name: index_material_vendors_on_vendor_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_material_vendors_on_vendor_id ON material_vendors USING btree (vendor_id);


--
-- Name: index_materials_on_category_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_materials_on_category_id ON materials USING btree (category_id);


--
-- Name: index_materials_on_ingredient_unit_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_materials_on_ingredient_unit_id ON materials USING btree (ingredient_unit_id);


--
-- Name: index_materials_on_unit_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_materials_on_unit_id ON materials USING btree (unit_id);


--
-- Name: index_ordering_sizes_on_amount_in_main_material_units; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_ordering_sizes_on_amount_in_main_material_units ON ordering_sizes USING btree (amount_in_main_material_units);


--
-- Name: index_ordering_sizes_on_material_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_ordering_sizes_on_material_id ON ordering_sizes USING btree (material_id);


--
-- Name: index_product_attachments_on_attachment_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_product_attachments_on_attachment_id ON product_attachments USING btree (attachment_id);


--
-- Name: index_product_attachments_on_product_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_product_attachments_on_product_id ON product_attachments USING btree (product_id);


--
-- Name: index_product_cogs_snapshots_on_batch_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_product_cogs_snapshots_on_batch_id ON product_cogs_snapshots USING btree (batch_id);


--
-- Name: index_product_cogs_snapshots_on_product_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_product_cogs_snapshots_on_product_id ON product_cogs_snapshots USING btree (product_id);


--
-- Name: index_product_cogs_snapshots_on_zone_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_product_cogs_snapshots_on_zone_id ON product_cogs_snapshots USING btree (zone_id);


--
-- Name: index_product_zone_availabilities_on_product_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_product_zone_availabilities_on_product_id ON product_zone_availabilities USING btree (product_id);


--
-- Name: index_product_zone_availabilities_on_product_id_and_zone_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_product_zone_availabilities_on_product_id_and_zone_id ON product_zone_availabilities USING btree (product_id, zone_id);


--
-- Name: index_product_zone_availabilities_on_zone_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_product_zone_availabilities_on_zone_id ON product_zone_availabilities USING btree (zone_id);


--
-- Name: index_products_shipment_reports_on_cases_shipment_priority; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_products_shipment_reports_on_cases_shipment_priority ON products_shipment_reports USING btree (cases_shipment_priority);


--
-- Name: index_products_shipment_reports_on_items_shipment_priority; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_products_shipment_reports_on_items_shipment_priority ON products_shipment_reports USING btree (items_shipment_priority);


--
-- Name: index_products_shipment_reports_on_product_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_products_shipment_reports_on_product_id ON products_shipment_reports USING btree (product_id);


--
-- Name: index_shipment_entries_on_amazon_shipment_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_shipment_entries_on_amazon_shipment_id ON shipment_entries USING btree (amazon_shipment_id);


--
-- Name: index_shipment_entries_on_product_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_shipment_entries_on_product_id ON shipment_entries USING btree (product_id);


--
-- Name: index_tasks_on_inventory_item_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_tasks_on_inventory_item_id ON tasks USING btree (inventory_item_id);


--
-- Name: index_units_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_units_on_name ON units USING btree (name);


--
-- Name: index_user_zones_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_zones_on_user_id ON user_zones USING btree (user_id);


--
-- Name: index_user_zones_on_user_id_and_zone_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_zones_on_user_id_and_zone_id ON user_zones USING btree (user_id, zone_id);


--
-- Name: index_user_zones_on_zone_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_zones_on_zone_id ON user_zones USING btree (zone_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);


--
-- Name: inventory_items_unique_inventoriable_and_zone; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX inventory_items_unique_inventoriable_and_zone ON inventory_items USING btree (inventoriable_type, inventoriable_id, zone_id);


--
-- Name: product_quantity_shipment_type_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX product_quantity_shipment_type_index ON fba_allocations USING btree (product_id, quantity_to_allocate, shipment_type);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: uniqueness_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX uniqueness_index ON fba_allocations USING btree (product_id, fba_warehouse_id, quantity_to_allocate, shipment_type);


--
-- Name: fk_rails_25bf3d2c5e; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_items
    ADD CONSTRAINT fk_rails_25bf3d2c5e FOREIGN KEY (invoice_id) REFERENCES invoices(id);


--
-- Name: fk_rails_5472d742df; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY amazon_inventories
    ADD CONSTRAINT fk_rails_5472d742df FOREIGN KEY (product_id) REFERENCES products(id);


--
-- Name: fk_rails_56ae90982e; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY inventories
    ADD CONSTRAINT fk_rails_56ae90982e FOREIGN KEY (inventory_item_id) REFERENCES inventory_items(id);


--
-- Name: fk_rails_809dba9182; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ordering_sizes
    ADD CONSTRAINT fk_rails_809dba9182 FOREIGN KEY (unit_id) REFERENCES units(id);


--
-- Name: fk_rails_a29a951df3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_fees
    ADD CONSTRAINT fk_rails_a29a951df3 FOREIGN KEY (invoice_id) REFERENCES invoices(id);


--
-- Name: fk_rails_bc3d614920; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY material_containers
    ADD CONSTRAINT fk_rails_bc3d614920 FOREIGN KEY (material_id) REFERENCES materials(id);


--
-- Name: fk_rails_bfc0778a90; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invoice_items
    ADD CONSTRAINT fk_rails_bfc0778a90 FOREIGN KEY (material_id) REFERENCES materials(id);


--
-- Name: fk_rails_d7dc35dc62; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT fk_rails_d7dc35dc62 FOREIGN KEY (inventory_item_id) REFERENCES inventory_items(id);


--
-- Name: fk_rails_db974bf3ef; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ingredients
    ADD CONSTRAINT fk_rails_db974bf3ef FOREIGN KEY (product_id) REFERENCES products(id);


--
-- Name: fk_rails_fa857654f8; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ordering_sizes
    ADD CONSTRAINT fk_rails_fa857654f8 FOREIGN KEY (material_id) REFERENCES materials(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO "schema_migrations" (version) VALUES
('20160407123906'),
('20160415125600'),
('20160415125837'),
('20160415132935'),
('20160415133700'),
('20160423131450'),
('20160426133108'),
('20160426161352'),
('20160426163804'),
('20160426163957'),
('20160426164305'),
('20160429214919'),
('20160429220436'),
('20160429221219'),
('20160429222127'),
('20160430030138'),
('20160430030332'),
('20160430032854'),
('20160502024817'),
('20160502192337'),
('20160503043028'),
('20160503124241'),
('20160504135815'),
('20160512133448'),
('20160518203441'),
('20160519132631'),
('20160521021109'),
('20160524190739'),
('20160525162221'),
('20160525205448'),
('20160526194602'),
('20160526195235'),
('20160528144159'),
('20160530154857'),
('20160603122405'),
('20160603163415'),
('20160604114203'),
('20160605120930'),
('20160605144808'),
('20160608232310'),
('20160609204813'),
('20160610122853'),
('20160617114144'),
('20160630140022'),
('20160702143918'),
('20160702152519'),
('20160704152037'),
('20160705102316'),
('20160705103005'),
('20160713155849'),
('20160715110620'),
('20160718125007'),
('20160721200038'),
('20160723170930'),
('20160726123242'),
('20160729120517'),
('20160729121923'),
('20160803114434'),
('20160803114436'),
('20160808152953'),
('20160809191207'),
('20160818153340'),
('20160822081320'),
('20160901144204'),
('20160901172546'),
('20160909100634'),
('20160915083040'),
('20160920172756'),
('20160921131145'),
('20160921170233'),
('20160921170523'),
('20160921175433'),
('20160926151347'),
('20160927141832'),
('20160930120524'),
('20161005152355'),
('20161005162059'),
('20161005162433'),
('20161006161325'),
('20161006161340'),
('20161007133421'),
('20161009093257'),
('20161013185030'),
('20161016100601'),
('20161016100759'),
('20161016100811'),
('20161025181149'),
('20161026145152'),
('20161027141935'),
('20161031111453'),
('20161104185005'),
('20161107131457'),
('20161107140117'),
('20161114195011'),
('20161117131935'),
('20161125162252'),
('20161201210510'),
('20161203082247'),
('20161209132324'),
('20161212125556'),
('20161219090632'),
('20161219123329'),
('20161227123758'),
('20161229101038'),
('20170103131910'),
('20170105140702'),
('20170110183310'),
('20170112101330'),
('20170112150143'),
('20170116095356'),
('20170118121556'),
('20170118161609'),
('20170118165653'),
('20170119152647'),
('20170119160256'),
('20170120155841'),
('20170125125808'),
('20170126123959'),
('20170207180942'),
('20170209161434'),
('20170227152005'),
('20170301172104'),
('20170317131213'),
('20170404181301'),
('20170406184022'),
('20170410150140'),
('20170418135143'),
('20170418182711'),
('20170420160602'),
('20170427173217'),
('20170505214920'),
('20170606111230'),
('20170624091553'),
('20170624115955'),
('20170627080243'),
('20170705132435'),
('20170705134218'),
('20170726122910'),
('20170728131746'),
('20170729150849'),
('20170801143338'),
('20170802120955'),
('20170815135037'),
('20170910142352');


