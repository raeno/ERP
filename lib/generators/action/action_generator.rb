class ActionGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('../templates', __FILE__)

  def copy_action_and_test_files
    template 'action.rb', File.join('app/actions', "#{name}.rb")
    template 'action_test.rb', File.join('test/actions', "#{name}_test.rb")
  end
  
  private

  def has_module?
    module_name.present?
  end

  def module_name
    class_name.deconstantize
  end

  def action_name
    class_name.demodulize
  end
end
