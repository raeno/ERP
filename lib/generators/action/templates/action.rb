<% if has_module? -%>
module <%= module_name %>
  class <%= action_name %> < BaseAction
    def call
    end
  end
end
<% else %>
class <%= action_name %> < BaseAction
  def call
  end
end
<% end %>
