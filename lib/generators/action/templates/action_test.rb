require 'test_helper'

<% if has_module? -%>
module <%= module_name %>
  class <%= action_name %>Test < ActiveSupport::TestCase
  end
end
<% else %>
class <%= action_name %>Test < ActiveSupport::TestCase
end
<% end %>


