# frozen_string_literal: true
namespace :data do
  namespace :amazon_histories do
    desc 'Clean old amazon histories'
    task clean_old: :environment do
      AmazonInventoryHistory.old_histories.destroy_all
    end
  end
end
