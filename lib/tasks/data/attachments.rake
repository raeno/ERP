# frozen_string_literal: true
namespace :data do
  namespace :attachments do
    desc 'Migrate all material media to attachments'
    task migrate_material_media: :environment do
      MaterialMedia.all.each do |media|
        attachment = Attachment.create url: media.url, mimetype: media.mimetype
        MaterialAttachment.create material: media.material, attachment: attachment
      end
    end
  end
end
