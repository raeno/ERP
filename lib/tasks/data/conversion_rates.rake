# frozen_string_literal: true
namespace :data do
  namespace :conversion_rates do
    desc 'Remove unnecessary conversion rate records'
    task cleanup: :environment do
      puts 'Concersion rules cleanup...'
      ConversionRate.all.each { |rate| rate.destroy if rate.id.even? }
      puts 'Done'
    end
  end
end
