# frozen_string_literal: true
namespace :data do
  namespace :fba_allocations do
    desc 'Clean stale fba allocations'
    task clean_stale: :environment do
      FbaAllocation.stale.destroy_all
    end
  end
end
