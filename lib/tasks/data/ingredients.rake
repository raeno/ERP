# frozen_string_literal: true
namespace :data do
  namespace :ingredients do
    desc 'Migrate from large to small units of measure'
    task migrate_to_small_units: :environment do
      print 'Migrating ingredients'
      Ingredient.find_each do |ingredient|
        qty =
          if ingredient.product?
            ingredient.quantity_obsolete
          else
            MaterialUnitConversion.new(ingredient.inventoriable)
              .main_to_ingredient(ingredient.quantity_obsolete).round(10)
          end

        ingredient.update_column(:quantity_in_small_units, qty)
        print '.'
      end
      puts 'Done'
    end
  end
end
