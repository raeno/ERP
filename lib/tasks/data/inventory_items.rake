# frozen_string_literal: true
namespace :data do
  namespace :inventory_items do
    desc 'Import products and materials'
    task import_all: :environment do
      Rake::Task["data:inventory_items:import_materials"].invoke
      Rake::Task["data:inventory_items:import_products"].invoke
    end

    desc 'Import all materials as inventory items'
    task import_materials: :environment do
      zone = Zone.default_zone
      create_action = InventoryItems::CreateInventoryItem.build
      existing_material_items = InventoryItem.materials.map(&:inventoriable_id)
      Material.where.not(id: existing_material_items).map do |material|
        create_action.call zone: zone, inventoriable: material
      end
    end

    desc 'Import all products as inventory items'
    task import_products: :environment do
      create_action = InventoryItems::CreateInventoryItem.build
      existing_product_items = InventoryItem.products.map(&:inventoriable_id)
      Product.where.not(id: existing_product_items).includes(:zones).each do |product|
        product.zones.each do |zone|
          create_action.call zone: zone, inventoriable: product
        end
      end
    end

    desc 'Populate production_seconds_per_unit based on existing tasks'
    task set_seconds_per_unit: :environment do
      print 'Updating inventory items'

      InventoryItem.all.includes(:tasks).find_each do |inventory_item|
        next if inventory_item.material?

        tasks = inventory_item.tasks.to_a
        total_quantity = tasks.sum(&:batch_quantity)
        next if total_quantity.zero?
        total_time = tasks.sum(&:duration)

        inventory_item.production_seconds_per_unit = (total_time.to_f / total_quantity).round
        inventory_item.save!
        print '.'
      end

      puts ' done'
    end
  end
end
