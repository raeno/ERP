# frozen_string_literal: true
namespace :data do
  namespace :invoice_items do
    desc 'Extract information to material container'
    task extract_material_containers_info: :environment do
      create_material_container = MaterialContainers::CreateMaterialContainer.build
      update_invoice_item = InvoiceItems::UpdateInvoiceItem.build
      invoice_items = InvoiceItem.all
      puts "Extracting material containers data from #{invoice_items.count} invoice items"
      ActiveRecord::Base.transaction do
        invoice_items.each do |item|
          extract_info item, create_material_container, update_invoice_item
        end
      end
      puts "Finished extracting materials container info"
    end

    def extract_info(item, create_material_container, update_invoice_item)
      params = { external_name: item.pkg_external_name,
                 container_form_obsolete: item.pkg_form, unit_id: item.pkg_unit_id,
                 material_id: item.material_id,
                 amount: item.pkg_amount, price: item.pkg_price,
                 lot_number: item.batch_number,
                 expiration_date: item.expiration_date }
      result = create_material_container.call params
      material_container = result.entity
      update_result = update_invoice_item.call item, material_container_id: material_container.id
      puts "InvoiceItem: #{item.id}, #{result.success?}, #{update_result.error_message}"
    end
  end
end
