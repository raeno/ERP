# frozen_string_literal: true
namespace :data do
  namespace :material_containers do
    desc 'Set oldest non-empty container as active'
    task assign_in_use_containers: :environment do
      choose_container_action = MaterialContainers::ChooseActiveContainer.build
      Material.all.includes(:material_containers).each do |material|
        containers = material.material_containers
        choose_container_action.call material unless containers.in_use.present?
      end
    end

    desc 'Migrate numeric status to enum'
    task migrate_container_statuses: :environment do
      MaterialContainer.all.each do |container|
        statuses = { 0 => 'active', 1 => 'closed', 2 => 'pending', 3 => 'expired'}
        container.status = statuses[container.old_status]
        container.save
      end
    end

    desc 'Create dummy container to cover inventory excess'
    task create_dummy_containers: :environment do
      create_container = MaterialContainers::CreateMaterialContainer.build
      Material.all.includes(:material_containers).each do |material|
        puts "Checking #{material.name}"
        amount_stored = material.inventory.quantity
        amount_in_containers = material.material_containers.map(&:amount_in_material_units).reduce(&:+) || 0
        not_logged = amount_stored - amount_in_containers

        next if not_logged <= 0

        puts "Not all inventory amount is distributed among containers. Creating dummy"

        ordering_size = OrderingSize.create!(amount: not_logged, unit: material.unit, name: 'unknown', material: material)
        price = (not_logged * material.unit_price).round(2)
        params = { price: price, amount_left: not_logged,
                   ordering_size: ordering_size,
                   material: material, lot_number: 'ADJUSTMENT' }
        status = MaterialContainer.where(material: material).in_use.empty? ? :active : :used
        params[:status] = MaterialContainer.statuses[status]
        result = create_container.call params
        puts result.error_message unless result.success?
      end
    end
  end
end
