# frozen_string_literal: true
namespace :data do
  namespace :materials do
    desc 'Refresh all reports for all materials'
    task refresh_reports: :environment do
      action = Materials::RefreshMaterialReports.build
      Material.find_each do |material|
        action.call material
      end
    end
  end
end
