# frozen_string_literal: true
namespace :data do
  namespace :vendors do
    desc 'Migrate from large to small units of measure'
    task set_lead_times: :environment do
      puts 'Setting lead times'
      update_lead_time = Vendors::RefreshLeadTime.build

      Vendor.find_each do |vendor|
        result = update_lead_time.call(vendor)
        print '.'.green if result.success?
        print "ERROR:#{vendor.id}".red if result.failure?
      end

      puts 'Done'
    end
  end
end
