# frozen_string_literal: true
namespace :scheduled do
  desc 'Fetches product from Amazon'
  task :get_products => :environment do
    FetchProductsJob.perform_later
  end

  desc 'Updated amazon inventory status for each product'
  task :get_inventory => :environment do
    UpdateAmazonInventoryJob.perform_later
  end

  desc 'Requests report for inventory at Amazon'
  task :request_amazon_report => :environment do
    RequestAmazonReportJob.perform_later
  end

  desc 'Refresh allocations for products that we can ship'
  task :fetch_allocations => :environment do
    FetchFbaAllocationsJob.perform_later
  end

  desc 'Fetch shipments from Amazon'
  task :fetch_shipments => :environment do
    PullAmazonShipmentsJob.perform_later
  end
end
