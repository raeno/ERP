require 'test_helper'

module AmazonProcessing
  class InitializeShipmentTest < ActiveSupport::TestCase

    let(:shipment_processor) { stub(create_shipment_plan: shipment_result) }
    let(:update_shipment) { stub(call: success_result) }
    let(:update_entries) { stub(call: success_result) }
    let(:shipment_plans) { [stub(shipment_id: 'ABC'), stub(shipment_id: 'ABC')] }
    let(:shipment_result) { stub(success?: true, entities: shipment_plans)}
    let(:shipment) { create :amazon_shipment, shipment_id: nil }

    before do
      @shipment_entries = create_list :shipment_entry, 2, amazon_shipment: shipment
    end

    subject { described_class.new shipment_processor, update_shipment, update_entries }

    describe '.build' do
      it 'creates instance of class with all dependencies' do
        instance = described_class.build
        assert_instance_of described_class, instance
      end
    end

    it 'creates shipment plan to fetch shipment id' do
      shipment_processor = stub
      shipment_processor.expects(:create_shipment_plan).returns(shipment_result).once
      action = described_class.new shipment_processor, update_shipment, update_entries
      action.call shipment
    end

    describe 'when fails to create shipment plan' do
      it 'responds with failure' do
        shipment_processor = stub(create_shipment_plan: failure_result)
        action = described_class.new shipment_processor, update_shipment, update_entries
        result = action.call shipment
        refute result.success?
      end

      it 'still has shipment in returning result' do
        shipment_processor = stub(create_shipment_plan: failure_result)
        action = described_class.new shipment_processor, update_shipment, update_entries
        result = action.call shipment
        assert_equal shipment, result.entity
      end
    end

    it 'updates shipment with received shipment_id and new status' do
      update_shipment = stub
      update_shipment.expects(:call).once.with(shipment, shipment_id: 'ABC', status: 'INITIALIZED').returns(success_result)
      action = described_class.new shipment_processor, update_shipment, update_entries
      action.call shipment
    end

    it 'responds with success' do
      result = subject.call shipment
      assert result.success?
    end
  end
end
