require 'test_helper'

module AmazonProcessing
  class PatchShipmentTest < ActiveSupport::TestCase
    let(:warehouse_name) { 'BNA2' }
    let(:shipment_address) { build :address }
    let(:amazon_shipment) { create :amazon_shipment }

    let(:shipment_processor) { stub(update_shipment: success_result) }
    let(:update_shipment) { stub(call: success_result) }

    before do
      create_list :shipment_entry, 2, amazon_shipment: amazon_shipment
    end

    it 'call shipment processor to update Amazon' do
      shipment_processor = stub
      shipment_processor.expects(:update_shipment).once.returns(success_result)
      instance = described_class.new shipment_processor, shipment_address, update_shipment
      instance.call amazon_shipment
    end

    describe 'when successfully updated shipment on Amazon' do
      it 'updates shipment with new status' do
        update_shipment = stub
        update_shipment.expects(:call).with(amazon_shipment, status: 'AMAZON_SYNCED').returns(success_result)

        instance = described_class.new shipment_processor, shipment_address, update_shipment
        instance.call amazon_shipment
      end
    end

    it 'returns successful result' do
      instance = described_class.new shipment_processor, shipment_address, update_shipment
      result = instance.call amazon_shipment
      assert result.success?
    end
  end
end
