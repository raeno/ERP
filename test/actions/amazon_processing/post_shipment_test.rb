require 'test_helper'

module AmazonProcessing
  class PostShipmentTest < ActiveSupport::TestCase
    let(:shipment_processor) { stub(create_shipment: success_result) }
    let(:update_shipment) { stub(call: success_result) }
    let(:address) { build :address }

    let(:shipment) { create :amazon_shipment }
    subject { described_class.new shipment_processor, address, update_shipment }

    describe '.build' do
      it 'creates instance of class with all dependencies' do
        instance = described_class.build
        assert_instance_of described_class, instance
      end
    end

    describe 'when shipment is nil' do
      it 'responds with error'  do
        result = subject.call nil
        refute result.success?
      end
    end

    describe 'when shipment already synced with amazon' do
      let(:shipment) { create :amazon_shipment, :synced }

      it 'responds with success' do
        result = subject.call shipment
        assert result.success?
      end

      it 'omits request to Amazon' do
        shipment_processor = stub
        shipment_processor.expects(:create_shipment).never
        instance = described_class.new shipment_processor, address, update_shipment
        instance.call shipment
      end
    end

    it 'creates spipment on Amazon' do
      shipment_processor = stub
      shipment_processor.expects(:create_shipment).once.returns(success_result)
      action = described_class.new shipment_processor, address, update_shipment
      action.call shipment
    end

    it 'updates amazon shipment status' do
      update_shipment = stub
      update_shipment.expects(:call).with(shipment, status: 'AMAZON_SYNCED').once.returns(success_result)
      action = described_class.new shipment_processor, address, update_shipment
      action.call shipment
    end

    describe 'when failed to ship to Amazon' do
      before  do
        @shipment_processor = stub(create_shipment: failure_result)
        @action = described_class.new @shipment_processor, address, update_shipment
      end

      it 'responds with error' do
        result = @action.call shipment
        refute result.success?
      end

      it 'contains shipment in result' do
        result = @action.call shipment
        assert_equal result.entity, shipment
      end
    end

    it 'responds with success' do
      result = subject.call shipment
      assert result.success?
    end
  end
end
