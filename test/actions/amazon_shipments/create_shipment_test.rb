require 'test_helper'

module AmazonShipments
  class CreateShipmentTest < ActiveSupport::TestCase

    subject { described_class.new(create_entries) }

    let(:user) { build :user }

    let(:success_result) { stub(success?: true, entity: Object.new,
                               entities: [stub, stub], errors: [])}
    let(:create_entries) { stub(call: success_result) }

    let(:shipment_name) { 'Test shipment name' }
    let(:fba_warehouse) { create :fba_warehouse }
    let(:shipment_id) { 'some_shipment_id' }
    let(:product_1) { create :product }
    let(:params) { { name: shipment_name, fba_warehouse_id: fba_warehouse.id,
                                        shipment_entries: [ { product_id: product_1.id, quantity: 10 }]
                                      }}

    it 'creates shipment from params' do
      assert_difference 'AmazonShipment.count', +1 do
        subject.call user, params
      end
    end

    it 'creates shipment entries as well' do
      create_entries = stub
      create_entries.expects(:call).once.returns(success_result)
      instance = described_class.new create_entries
      instance.call user, params
    end

    it 'is successful' do
      result = subject.call user, params
      assert result.success?
    end

    describe 'when cannot create shipment' do
      before do
        failure_result = stub(success?: false, entity: Object.new,
                              entities: [stub, stub], errors: ['Some error'])
        subject.stubs(:call_on).returns(failure_result)
      end
      it 'returns failure' do
        result = subject.call user, params
        refute result.success?
      end
    end
  end
end
