require 'test_helper'

module AmazonShipments
  class DestroyAllShipmentsTest < ActiveSupport::TestCase
    subject { described_class.new }

    it 'destroys all persisted shipments' do
      create_list :amazon_shipment, 2
      assert_equal 2, AmazonShipment.count

      subject.call
      assert_equal 0, AmazonShipment.count
    end
  end
end
