require 'test_helper'

module AmazonShipments
  class PersistShipmentTest < ActiveSupport::TestCase
    let(:create_shipment) { stub(call: success_result) }
    let(:update_shipment) { stub(call: success_result) }

    subject { described_class.new create_shipment, update_shipment }

    let(:params) { { shipment_id: 'ABC', name: 'TEST_NAME' } }

    describe '.build' do
      it 'creates instance of class with all dependencies' do
        instance = described_class.build
        assert_kind_of described_class, instance
      end
    end

    describe 'when shipment_id is absent' do
      let(:params) { { name: 'TEST_NAME' } }
      it 'responds with error' do

        result = subject.call params
        refute result.success?
      end
    end

    describe 'when shipment does not exist' do
      it 'creates it' do
        create_shipment = stub
        create_shipment.expects(:call).once.returns(success_result)

        action = described_class.new create_shipment, update_shipment
        action.call params
      end
    end

    describe 'when shipment exists' do
      before do
        @shipment = create :amazon_shipment, shipment_id: 'ABC', status: 'OLD_STATUS'
      end

      it 'updates it with passed params' do
        update_shipment = stub
        update_shipment.expects(:call).once.returns(success_result)
        action = described_class.new create_shipment, update_shipment
        action.call params
      end
    end
  end
end
