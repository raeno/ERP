require 'test_helper'

module AmazonShipments
  class ScheduleShipmentTest < ActiveSupport::TestCase
    include ActiveJob::TestHelper

    let(:shipment) { create :amazon_shipment }
    let(:entity) { stub(shipment: shipment, entries: [] )}
    let(:result) { stub(success?: true, entity: entity, error: [] )}
    let(:create_shipment) { stub(call: result) }
    let(:user) { build :user }
    let(:params) { { name: 'Test name', shipment_id: 'some_id', shipment_entries: []} }

    subject { described_class.new create_shipment }

    describe '.build' do
      it 'creates instance of class with dependencies' do
        instance = described_class.build
        assert_kind_of described_class, instance
      end
    end

    it "it creates amazon shipment first" do
      create_shipment = stub
      create_shipment.expects(:call).with(user, params).once.returns(result)
      instance = described_class.new create_shipment
      instance.call user, params
    end

    describe "when failed to create shipment" do
      it 'returns failure result' do
        create_shipment = stub(call: failure_result)
        instance = described_class.new create_shipment
        result = instance.call user, params
        refute result.success?
      end
    end

    it 'enqueues job to send created shipment to Amazon' do
      assert_enqueued_with job: ShipProductsJob do
        subject.call user, params
      end
    end

    it 'returns successful result' do
      result = subject.call user, params
      assert result.success?
    end
  end
end
