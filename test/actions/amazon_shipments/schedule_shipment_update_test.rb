require 'test_helper'

module AmazonShipments
  class ScheduleShipmentUpdateTest < ActiveSupport::TestCase
    let(:persist_entries) { stub(call: success_result) }
    let(:enqueued_job) { Object.new }
    let(:update_shipment_job) { stub(perform_later: enqueued_job) }

    let(:user) { create :user }
    let(:shipment) { create :amazon_shipment, user: user }

    let(:product_1) { create :product }
    let(:product_2) { create :product }
    let(:entries) { [Object.new, Object.new] }
    let(:params)  { { shipment_entries: entries, shipment: shipment } }

    subject { described_class.new persist_entries, update_shipment_job }

    describe '.build' do
      it 'builds instance of class with all dependencies' do
        instance = described_class.build
        assert_instance_of described_class, instance
      end
    end

    it 'updates passed shipment entries first' do
      persist_entries = stub
      persist_entries.expects(:call).with(shipment, entries).once.returns(success_result)

      action = described_class.new persist_entries, update_shipment_job
      action.call params, user
    end

    it 'schedules job to sync shipment with amazon and batches' do
      update_shipment_job = stub
      update_shipment_job.expects(:perform_later).once.with(shipment).returns(enqueued_job)

      action = described_class.new persist_entries, update_shipment_job
      action.call params, user
    end

    it 'responds with success' do
      result = subject.call params, user
      assert result.success?
    end

    it 'has enqueued job in result' do
      result = subject.call params, user
      assert_equal enqueued_job, result.job
    end
  end
end
