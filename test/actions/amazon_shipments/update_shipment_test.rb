require 'test_helper'

module AmazonShipments
  class UpdateShipmentTest < ActiveSupport::TestCase
    let(:update_entry) { stub(call: success_result) }
    let(:shipment) { create :amazon_shipment, status: 'OLD_STATUS' }
    let(:params) { { status: 'NEW_STATUS' } }

    subject { described_class.new update_entry}

    describe 'when passed shipment is nil' do
      it 'responds with error' do
        result = subject.call nil, params
        refute result.success?
      end
    end

    it 'is successful' do
      result = subject.call shipment, params
      assert result.success?
    end

    it 'updates shipment' do
      subject.call shipment, params
      new_status = shipment.reload.status
      assert_equal new_status, 'NEW_STATUS'
    end

    describe 'when shipment status is changed to "AMAZON_SYNCED' do
      let(:params) { { status: 'AMAZON_SYNCED' } }
      before do
        @entries = create_list :shipment_entry, 2, amazon_shipment: shipment
      end

      it 'updates shipment entries processing stage' do
        update_entry = stub
        update_entry.expects(:call).twice.returns(success_result)
        action = described_class.new update_entry
        action.call shipment, params
      end
    end
  end
end



