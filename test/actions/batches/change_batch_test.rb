# frozen_string_literal: true
require 'test_helper'

module Batches
  class ChangeBatchTest < ActiveSupport::TestCase
    let(:user) { create :user }
    let(:batch) { create :batch, quantity: 100 }
    let(:params) { { quantity: 50, user_ids: [user.id] } }
    let(:success_result) { stub(success?: true) }
    let(:failure_result) { stub(failure?: true, success?: false) }
    let(:update_inventory_action) { stub(call: success_result, rollback: success_result) }
    subject { described_class.new(update_inventory_action) }

    describe '.build' do
      it 'creates an instance of action with all dependencies' do
        instance = described_class.build
        assert_kind_of described_class, instance
      end
    end

    describe 'when batch is nil' do
      it 'returns failure result' do
        result = subject.call(nil, params)
        assert result.failure?
      end
    end

    describe 'when arguments are valid' do
      it 'updates batch' do
        assert_difference 'batch.reload.quantity', -50 do
          subject.call(batch, params)
        end
      end

      it 'returns successful result' do
        result = subject.call(batch, params)
        assert result.success?
      end

      it 'calls an action to update inventory with new amount' do
        subject.call(batch, params)
      end
    end

    describe 'when unable to save' do
      before { batch.stubs(:save).returns(false) }
      it 'result is a failure' do
        result = subject.call(batch, params)
        assert result.failure?
      end

      it 'rollbacks inventory change' do
        update_inventory_action.expects(:rollback)
        subject.call batch, params
      end
    end

    describe 'when encounters error during inventories update' do
      let(:update_inventory_action) { stub(call: failure_result, rollback: success_result) }
      subject { described_class.new(update_inventory_action) }

      it 'does not update batch' do
        assert_no_difference 'batch.reload.quantity' do
          subject.call batch, params
        end
      end

      it 'returns failure result' do
        result = subject.call batch, params
        assert result.failure?
      end
    end
  end
end
