# frozen_string_literal: true
require 'test_helper'

module Batches
  class CreateBatchTest < ActiveSupport::TestCase
    let(:user) { create :user }
    let(:product) { create :product }
    let(:zone) { create :zone }
    let(:task) { create :task }
    let(:params) do
      { quantity: 20, product_id: product.id, zone_id: zone.id, task_id: task.id, user_ids: [user.id] }
    end
    let(:success_result) { stub(success?: true) }
    let(:update_inventory_action) { stub(call: success_result) }
    let(:track_prime_cost) { stub(call: success_result) }
    let(:finish_task) { stub(call: success_result) }

    subject { described_class.new(update_inventory_action, track_prime_cost, finish_task) }

    describe '.build' do
      it 'creates an instance of action with all dependencies' do
        instance = described_class.build
        assert_kind_of described_class, instance
        assert_kind_of Products::MoveProductsInBatch, instance.update_inventory_action
        assert_kind_of Products::TrackPrimeCost, instance.track_prime_cost
        assert_kind_of Tasks::FinishTask, instance.finish_task
      end
    end

    describe 'when params are valid' do
      it 'creates a batch' do
        assert_difference 'Batch.count', +1 do
          subject.call(params)
        end
      end

      it 'returns successful result' do
        result = subject.call(params)
        assert result.success?
      end

      it 'can update batch auxillary info as well' do
        params = { quantity: 20, product_id: product.id, zone_id: zone.id, task_id: task.id,
          user_ids: [user.id], auxillary_info: { shipment_id: '123' } }

        result = subject.call params
        assert result.success?

        created_batch = result.entity
        assert '123', created_batch.auxillary_info[:shipment_id]
      end

      it 'calls an action to update inventory with new amount' do
        update_inventory_action.expects(:call).once.returns(success_result)
        subject.call params
      end

      it 'calls FinishTask action' do
        finish_task.expects(:call).with(task).once.returns(success_result)
        subject.call params
      end
    end

    describe 'when unable to create' do
      before { Batch.any_instance.stubs(:save).returns(false) }
      it 'returns failed result' do
        update_inventory_action.stubs(:rollback)
        result = subject.call params
        assert result.failure?
      end

      it 'does not track prime cost' do
        update_inventory_action.stubs(:rollback).returns(success_result)
        track_prime_cost = stub
        track_prime_cost.expects(:call).never
        action = described_class.new update_inventory_action, track_prime_cost, finish_task
        action.call params
      end
    end

    describe 'when encounters an error while updating inventories' do
      let(:failure_result) { stub(success?: false) }
      let(:update_inventory_action) { stub(call: failure_result, rollback: success_result) }
      let(:subject) { described_class.new(update_inventory_action, track_prime_cost, finish_task) }

      it 'returns failure result' do
        result = subject.call params
        assert result.failure?
      end

      it 'base error message to beginning' do
        result = subject.call params
        failure = stub(success?: false, errors: ['Not enough Anise Oil'])
        update_action = stub(call: failure, rollback: success_result)
        action = described_class.new(update_action, track_prime_cost, finish_task)
        result = action.call params
        # assert_match /Failed to create batch/, result.error_message
        refute result.success?
      end

      it 'does not create batch' do
        assert_no_difference 'Batch.count' do
          subject.call params
        end
      end

      it 'does not track prime cost either' do
        track_prime_cost = stub
        track_prime_cost.expects(:call).never
        action = described_class.new(update_inventory_action, track_prime_cost, finish_task)
        action.call params
      end
    end
  end
end
