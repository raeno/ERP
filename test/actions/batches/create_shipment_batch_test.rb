# frozen_string_literal: true
require 'test_helper'

module Batches
  class CreateShipmentBatchTest < ActiveSupport::TestCase
    let(:batch) { build :batch }
    let(:success_result) { stub(success?: true, entity: batch) }
    let(:create_batch) { stub(call: success_result) }
    let(:user) { build :user }
    let(:product) { create :product, seller_sku: 'abcd' }
    let(:shipment_entry) { build :shipment_entry, quantity: 10, product: product }
    let(:shipment_id) { 'ABCDEFFG' }

    class ShipmentBatchParamsTest < ActiveSupport::TestCase
      let(:user) { build :user }
      let(:product) { create :product, seller_sku: 'abcd' }
      let(:shipment_entry) { build :shipment_entry, quantity: 10, product: product }
      let(:shipment_id) { 'ABCDEFFG' }

      subject { CreateShipmentBatch::ShipmentBatchParams.new user, shipment_entry, shipment_id }

      describe '#product_id' do
        it 'fetches product by seller sku' do
          assert_equal product.id, subject.product_id
        end
      end

      describe '#zone_id' do
        before do
          @zone = create :zone, :ship
        end

        it 'fetches id of the ship zone' do
          assert_equal @zone.id, subject.zone_id
        end
      end

      describe '#completed_on' do
        it 'uses current time in specific format' do
          time = Time.zone.local(2016, 10, 1, 10, 5, 2)
          Timecop.freeze(time) do
            assert_equal '10/01/2016', subject.completed_on
          end
        end
      end

      describe '#to_hash' do
        it 'contains important keys' do
          params = subject.to_h
          expected_keys = [:product_id, :quantity, :completed_on,
                           :amazon_shipment_id, :user_ids, :zone_id].sort
          assert_equal expected_keys, params.keys.sort
        end
      end
    end

    it 'creates batch based on shipment information' do
      create_batch = stub
      create_batch.expects(:call).once.returns(success_result)
      action = described_class.new(create_batch)
      action.call user, shipment_entry, shipment_id
    end

    describe 'when batch created successfully' do
      it 'is successful' do
        action = described_class.new(create_batch)
        result = action.call user, shipment_entry, shipment_id
        assert result.success?
      end
    end

    describe 'when batch creation failed' do
      let(:failure_result) { stub(success?: false, errors: []) }
      let(:create_batch) { stub(call: failure_result) }

      it 'is failure' do
        action = described_class.new(create_batch)
        result = action.call user, shipment_entry, shipment_id
        refute result.success?
      end
    end
  end
end
