require 'test_helper'


module Batches
  class SyncShipmentBatchesTest < ActiveSupport::TestCase
    let(:amazon_shipment) { create :amazon_shipment }
    let(:batch) { create :batch }
    let(:batch_result) { stub(success?: true, entity: batch) }
    let(:shipment_result) { stub(success?: true, entity: amazon_shipment)}

    let(:create_batch)  { stub(call: success_result) }
    let(:update_batch) { stub(call: success_result) }
    let(:update_entry) { stub(call: success_result) }
    let(:update_shipment) { stub(call: shipment_result) }
    subject { described_class.new create_batch, update_batch, update_entry, update_shipment }

    before do
      @entries_with_batch = create_list :shipment_entry, 3, :with_batch, :amazon_synced,
                             amazon_shipment: amazon_shipment
      @new_entries = create_list :shipment_entry, 2, :amazon_synced, amazon_shipment: amazon_shipment
    end

    it 'creates batch for each shipment entry without batch' do
      create_batch = stub
      create_batch.expects(:call).times(2).returns(success_result)
      instance = described_class.new create_batch, update_batch, update_entry, update_shipment
      instance.call amazon_shipment
    end

    it 'updates batch for each shipment entry with batch existing' do
      update_batch = stub
      update_batch.expects(:call).times(3).returns(success_result)
      instance = described_class.new create_batch, update_batch, update_entry, update_shipment
      instance.call amazon_shipment
    end

    it 'is successful when all batches successfully created or updated' do
      result = subject.call amazon_shipment
      assert result.success?
    end

    it 'contains amazon shipment in the result' do
      result = subject.call amazon_shipment
      assert_equal amazon_shipment, result.entity
    end

    it 'returns failure result when some batches were not created' do
      create_batch = stub
      call_sequence = sequence('batches')
      create_batch.expects(:call).in_sequence(call_sequence).returns(success_result)
      create_batch.expects(:call).in_sequence(call_sequence).returns(failure_result)

      instance = described_class.new create_batch, update_batch, update_entry, update_shipment
      result = instance.call amazon_shipment
      assert result.failure?
    end
  end
end
