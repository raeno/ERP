# frozen_string_literal: true
require 'test_helper'

module Fba
  class DeleteAllocationsTest < ActiveSupport::TestCase
    let(:product) { create :product }
    let(:quantity) { 100 }
    let(:amazon_shipment) { build :amazon_shipment, cases_required: true }
    let(:shipment_entry) { create :shipment_entry, product: product, quantity: quantity, amazon_shipment: amazon_shipment }
    subject { described_class.new }

    before do
      create_list :fba_allocation, 2, product: product, quantity_to_allocate: quantity, shipment_type: 'cases'
      create :fba_allocation, product: product, quantity_to_allocate: quantity + 10, shipment_type: 'cases'
      create :fba_allocation, product: product, quantity_to_allocate: quantity, shipment_type: 'items'
    end

    it 'deletes all allocations that match params in shipment info' do
      assert_difference 'FbaAllocation.count', -2 do
        subject.call shipment_entry
      end
    end

    it 'is successful when deletes without problem' do
      result = subject.call shipment_entry
      assert result.success?
    end

    describe 'when failed to delete some entries' do
      it 'is not successful' do
        allocations_mock = stub(destroy_all: false)
        FbaAllocation.stubs(:where).returns(allocations_mock)
        result = subject.call shipment_entry
        refute result.success?
      end
    end
  end
end
