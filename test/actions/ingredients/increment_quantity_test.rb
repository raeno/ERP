# frozen_string_literal: true
require 'test_helper'

module Ingredients
  class IncrementQuantityTest < ActiveSupport::TestCase
    describe '.build' do
      it 'builds instance of action' do
        action = described_class.build
        assert_kind_of described_class, action
      end
    end

    let(:inventory) { build :inventory }
    let(:ingredient) { create :ingredient }
    let(:success_result) { stub(success?: true) }
    let(:increment_inventory_action) { stub(call: success_result) }

    before do
      ingredient.stubs(:inventory).returns(inventory)
      # We want #quantity (not #quantity_in_small_units) to be used in the action
      ingredient.stubs(:quantity).returns(25)
    end

    describe '#call' do
      it 'increments ingredient inventory item quantity' do
        increment_inventory = stub
        increment_inventory.expects(:call).once.with(inventory, 100 * 25).returns(success_result)
        action = described_class.new increment_inventory
        action.call ingredient, 100
      end
    end
  end
end
