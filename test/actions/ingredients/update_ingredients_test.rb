# frozen_string_literal: true
require 'test_helper'

module Ingredients
  class UpdateIngredientsTest < ActiveSupport::TestCase
    let(:product) { create :product }
    let(:make_zone) { build :zone, name: 'Make', production_order: 1 }
    let(:pack_zone) { build :zone, name: 'Pack', production_order: 2 }

    let(:success_result) { stub(success?: true, errors: []) }
    let(:increment_quantity) { stub(call: success_result) }

    subject { described_class.new increment_quantity }

    let(:ingredients) { build_list :ingredient, 3, product: product }
    let(:other_zone_ingredients) { build_list :ingredient, 2, product: product }

    describe '#call' do
      before do
        product.stubs(:ingredients_for_zone).returns(ingredients)
      end

      it 'is successful' do
        result = subject.call product, make_zone, amount: -2
        assert result.success?
      end

      it 'updates quantity of each ingredient for specified zone' do
        increment_action = stub
        ingredients.each do |ingredient|
          increment_action.expects(:call).with(ingredient, -3).once.returns(success_result)
        end
        action = described_class.new increment_action
        action.call product, make_zone, amount: -3
      end

      it 'does not change other zones components amount' do
        product.expects(:ingredients_for_zone).with(make_zone).returns(ingredients)

        increment_action = stub
        other_zone_ingredients.each do |ingredient|
          increment_quantity.expects(:call).with(ingredient, any_parameters).never
        end

        increment_action.stubs(:call).returns(success_result)

        action = described_class.new increment_action
        action.call product, make_zone, amount: -10
      end

      describe 'when amount is 0' do
        it 'does not change anything' do
          increment_quantity.expects(:call).never
          subject.call product, make_zone, amount: 0
        end

        it 'is successful though' do
          result = subject.call product, make_zone, amount: 0
          assert result.success?
        end
      end

      describe 'when unable to update some inventories' do
        let(:failure_result) { stub(success?: false, errors: ['Some error']) }
        before do
          increment_quantity.stubs(:call).returns(success_result, failure_result, success_result)
        end
        it 'is unsuccessful' do
          result = subject.call product, pack_zone, amount: -15
          assert result.failure?
        end

        it 'contains all failed ingredients error messages' do
          result = subject.call product, pack_zone, amount: -15
          assert_match(/Some error/, result.error_message)
        end
      end
    end
  end
end
