# frozen_string_literal: true

require 'test_helper'
module Inventories
  class IncrementQuantityTest < ActiveSupport::TestCase
    describe 'call' do
      let(:inventory) { create :inventory, quantity: 500 }
      subject { described_class.new }

      it 'increments inventory quantity' do
        result = subject.call(inventory, 111)
        assert result.success?
        assert_equal 500 + 111, inventory.quantity
      end

      it 'deducts inventory when negative amount is passed' do
        result = subject.call(inventory, -333)
        assert result.success?
        assert_equal 500 - 333, inventory.quantity
      end

      it 'fails to deduct more than we have' do
        result = subject.call(inventory, -1000)
        assert result.failure?
        assert_equal 500, inventory.reload.quantity
      end

      it 'publishes message when inventory quantity changed' do
        subject.expects(:publish).once.with(:inventory_quantity_changed, inventory, 100)
        subject.call inventory, 100
      end

      it 'does not publish message when failed to change quantity' do
        subject.expects(:publish).never
        subject.call inventory, -1000
      end
    end
  end
end
