# frozen_string_literal: true

require 'test_helper'
module Inventories
  class MoveProductsTest < ActiveSupport::TestCase
    let(:product) { create :product }
    let(:current_zone) { Zone.create production_order: 2 }
    let(:previous_zone) { Zone.create production_order: 1 }
    let(:success_result) { stub(success?: true, errors: []) }
    let(:failure_result) { stub(success?: false, errors: 'Failed to change') }

    before do
      [current_zone, previous_zone].each { |zone| ProductZoneAvailability.create product: product, zone: zone }
      current_inv_item, prev_inv_item = [current_zone, previous_zone].map do |zone|
        create :inventory_item, inventoriable: product, zone: zone
      end

      @from_inventory = Inventory.create inventory_item: current_inv_item, quantity: 100
      @to_inventory = Inventory.create inventory_item: prev_inv_item, quantity: 150
    end

    describe '#call' do
      it 'moves specified amount of product from source to destination inventory' do
        increment_quantity_action = stub
        increment_quantity_action.expects(:call).with(@from_inventory, -10).once.returns(success_result)
        increment_quantity_action.expects(:call).with(@to_inventory, 10).once.returns(success_result)
        action = described_class.new increment_quantity_action
        action.call @from_inventory, @to_inventory, 10
      end

      it 'returns successful result' do
        action = stub(call: success_result)
        result = action.call @from_inventory, @to_inventory, 10
        assert result.success?
      end

      describe 'when failed to update one of inventories ' do
        before do
          increment_quantity_action = stub
          increment_quantity_action.stubs(:call).returns(success_result, failure_result)
          @action = described_class.new increment_quantity_action
        end

        it 'returns with failure' do
          result = @action.call @from_inventory, @to_inventory, 300
          refute result.success?
        end
      end

      describe 'when from inventory is absent' do
        it 'just updates destination inventory' do
          increment_inventory = stub
          increment_inventory.expects(:call).once.returns(success_result)
          action = described_class.new increment_inventory
          action.call nil, @to_inventory, 10
        end
      end
    end

    describe '#rollback' do
      it 'rollbacks inventory changes' do
        increment_inventory = stub
        increment_inventory.expects(:call).with(@from_inventory, -15).once
                           .returns(success_result)
        increment_inventory.expects(:call).with(@to_inventory, 15).once
                           .returns(success_result)
        increment_inventory.expects(:call).with(@from_inventory, 15).once
                           .returns(success_result)
        increment_inventory.expects(:call).with(@to_inventory, -15).once
                           .returns(success_result)

        action = described_class.new increment_inventory
        action.call @from_inventory, @to_inventory, 15
        action.rollback
      end
    end
  end
end
