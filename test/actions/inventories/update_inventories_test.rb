# frozen_string_literal: true

require 'test_helper'

module Inventories
  class UpdateInventoriesTest < ActiveSupport::TestCase
    let(:success_result) { stub(success?: true) }
    let(:inventory_action) { stub(call: success_result) }
    let(:ingredients_action) { stub(call: success_result) }
    let(:product) { Product.new }
    let(:source_zone) { build :zone }
    let(:destination_zone) { build :zone }
    let(:source_inventory) { stub(production_order: 1, zone: source_zone) }
    let(:destination_inventory) { stub(production_order: 2, zone: destination_zone) }
    let(:amount) { 100 }

    subject { described_class.new(inventory_action, ingredients_action) }

    describe '.build' do
      it 'creates an instance of action with all dependencies' do
        instance = described_class.build
        assert_kind_of described_class, instance
      end
    end

    describe '#call' do
      describe 'when source inventory has lower production order then destination' do
        # we need to spend ingredient to move product to destination zone
        it 'substracts ingredients related to destination zone' do
          ingredients_action.expects(:call).with(product, destination_zone, amount: -amount)
          subject.call product, source_inventory, destination_inventory, amount
        end
      end

      # usually it means that we update batch reducing previous amount
      describe 'when source inventory has higher production order then destination' do
        before do
          source_inventory.stubs(:production_order).returns(2)
          destination_inventory.stubs(:production_order).returns(1)
        end
        # we return part of used ingredients back
        it 'adds ingredients for source zone' do
          ingredients_action.expects(:call).with(product, source_zone, amount: amount)
          subject.call product, source_inventory, destination_inventory, amount
        end
      end

      it 'produces combined result of both actions' do
        ingredients_action.stubs(:call).returns(stub(success?: false, errors: ['ingredients error']))
        inventory_action.stubs(:call).returns(stub(success?: false, errors: ['inventory error']))

        result = subject.call product, source_inventory, destination_inventory, amount
        refute result.success?
        assert_match(/ingredients error/, result.error_message)
        assert_match(/inventory error/, result.error_message)
      end
    end

    describe '#rollback' do
      it 'rollbacks inventory action' do
        inventory_action.expects(:rollback).once
        ingredients_action.stubs(:rollback)
        subject.rollback
      end

      it 'rollbacks ingredients action' do
        inventory_action.stubs(:rollback)
        ingredients_action.expects(:rollback).once
        subject.rollback
      end
    end
  end
end
