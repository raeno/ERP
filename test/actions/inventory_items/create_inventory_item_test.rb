# frozen_string_literal: true
require 'test_helper'

class CreateInventoryItemTest < ActiveSupport::TestCase
  let(:described_class) { InventoryItems::CreateInventoryItem }
  let(:zone) { create :zone }
  let(:material) { create :material }
  let(:success_result) { stub(success?: true) }
  let(:successful_action) { stub(call: success_result) }
  subject { described_class.new(inventory_action: successful_action) }

  let(:valid_params) do
    { zone_id: zone.to_param, inventoriable_id: material.to_param,
      inventoriable_type: 'Material' }
  end

  let(:valid_params_with_array) do
    { zone_id: zone.to_param,
      inventoriable_id: [material.id, "Material"].to_s }
  end

  describe 'when params ok' do
    it 'creates new inventory_item' do
      assert_difference 'InventoryItem.count', +1 do
        subject.call valid_params
      end
    end

    it 'provides successful result' do
      result = subject.call valid_params
      assert result.success?
    end

    it 'calls an action to create stock item' do
      inventory_action = mock
      inventory_action.expects(:call).once.returns(success_result)
      action = described_class.new inventory_action: inventory_action
      action.call valid_params
    end
  end

  describe 'when id and type are presented as array' do
    it 'properly parses that' do
      result = subject.call valid_params_with_array
      inventory_item = result.entity
      assert_equal material.id, inventory_item.inventoriable_id
      assert_equal "Material", inventory_item.inventoriable_type
    end
  end

  describe 'when could not create inventory_item' do
    let(:params) { { zone_id: zone.to_param, inventoriable_type: 'WRONG_TYPE' } }
    let(:error_message) { 'Inventoriable type is not included in the list' }

    it 'returns failure result' do
      result = subject.call params
      assert result.failure?
    end

    it 'has inventory_item creation errors in error message' do
      result = subject.call params
      assert_match(/#{error_message}/, result.error_message)
    end

    it 'skips stock item creation' do
      inventory_action = mock
      inventory_action.expects(:call).never
      action = described_class.new inventory_action: inventory_action
      action.call params
    end
  end
end
