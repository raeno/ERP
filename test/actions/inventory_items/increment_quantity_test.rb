# frozen_string_literal: true
require 'test_helper'

module InventoryItems
  class IncrementQuantityTest < ActiveSupport::TestCase
    describe '.build' do
      it 'builds an instance with proper dependencies' do
        action = described_class.build
        assert_kind_of described_class, action
      end
    end

    let(:success_result) { stub(success?: true) }
    let(:increment_inventory) { stub(call: success_result) }
    let(:inventory) { create :inventory }
    let(:inventory_item) { create :inventory_item, inventory: inventory }
    subject { described_class.new increment_inventory }

    describe '#call' do
      describe 'when trying to change quantity for product in ship zone' do
        before do
          inventory_item.stubs(:product_in_ship_zone?).returns(true)
        end

        it 'products failure result' do
          result = subject.call inventory_item, 10
          assert result.failure?
        end

        it 'does not change quantity' do
          increment_inventory = stub
          increment_inventory.expects(:call).never
          subject.call inventory_item, 10
        end
      end

      describe 'when inventory item quantity can be changed' do
        before do
          inventory_item.stubs(:product_in_ship_zone?).returns(false)
        end

        it 'calls increment action on inventory to change quantity' do
          increment_inventory = stub
          increment_inventory.expects(:call).once.returns(success_result)
          action = described_class.new increment_inventory
          action.call inventory_item, 10
        end

        it 'returns result of called action' do
          expected_result = Object.new
          increment_inventory = stub
          increment_inventory.expects(:call).once.returns(expected_result)

          action = described_class.new increment_inventory
          result = action.call inventory_item, 10
          assert_equal expected_result, result
        end
      end
    end
  end
end
