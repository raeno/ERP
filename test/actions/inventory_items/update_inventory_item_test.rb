# frozen_string_literal: true
require 'test_helper'

class UpdateInventoryItemTest < ActiveSupport::TestCase
  subject { InventoryItems::UpdateInventoryItem.new }
  let(:zone) { create :zone }
  let(:material) { create :material }

  let(:valid_params) do
    { zone_id: zone.to_param,
      inventoriable_id: material.to_param,
      inventoriable_type: 'Material' }
  end

  let(:valid_params_with_array) do
    { zone_id: zone.to_param,
      inventoriable_id: [material.to_param, 'Material'].to_s }
  end

  describe 'when passed inventory_item is nil' do
    it 'returns failure result' do
      result = subject.call nil, valid_params
      assert result.failure?
    end
  end

  describe 'when passed valid params' do
    let(:another_zone) { create :zone }
    before { @inventory_item = create :inventory_item, zone: another_zone, inventoriable: material, production_zone: another_zone }

    it 'updates inventory_item' do
      subject.call @inventory_item, valid_params
      assert_equal zone, @inventory_item.reload.zone
    end

    describe 'when working with material type' do
      it 'sync production_zone with changed inventory zone' do
        assert_equal another_zone, @inventory_item.production_zone
        subject.call @inventory_item, zone_id: zone.to_param
        assert_equal zone, @inventory_item.reload.production_zone
      end
    end
    it 'returns successful result' do
      result = subject.call @inventory_item, valid_params
      assert result.success?
    end
  end

  describe 'when inventoriable_id passed with type' do
    let(:product) { create :product }
    before { @inventory_item = create :inventory_item, zone: zone, inventoriable: product }
    it 'updates inventory_item as well' do
      subject.call @inventory_item, valid_params
      assert_equal material.id, @inventory_item.reload.inventoriable_id
      assert_equal 'Material', @inventory_item.reload.inventoriable_type
    end
  end

  describe 'when passed invalid params' do
    let(:product) { create :product }
    let(:another_zone) { create :zone }
    before { @inventory_item = create :inventory_item, zone: zone, inventoriable: product }
    let(:invalid_params) do
      # ActiveRecordi expects for inventoriable type to be existing class name
      { zone_id: another_zone.to_param, inventoriable_type: 'Unit' }
    end

    it "will not update inventory_item" do
      assert_no_difference '@inventory_item.reload.zone_id' do
        subject.call @inventory_item, invalid_params
      end
    end

    it 'returns failure result' do
      result = subject.call @inventory_item, invalid_params
      assert result.failure?
    end

    it 'has inventory_item errors in result error message' do
      result = subject.call @inventory_item, invalid_params
      assert_equal 'Inventoriable type is not included in the list', result.error_message
    end
  end
end
