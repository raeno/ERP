# frozen_string_literal: true
require 'test_helper'

module InvoiceItems
  class CreateInvoiceItemTest < ActiveSupport::TestCase
    let(:subject)  { described_class.new(create_material_container) }

    let(:material_container) { create(:material_container) }
    let(:success_result) { stub(success?: true, entity: material_container) }
    let(:create_material_container) { stub(call: success_result) }
    let(:invoice)  { create :invoice }
    let(:params) do
      { invoice_id: invoice.id, material_container: { price: 100, amount: 3 }, quantity: 2 }
    end

    describe '#call' do
      describe 'when success' do
        it 'saves the invoice item' do
          assert_difference "InvoiceItem.count", +1 do
            result = subject.call params
            assert result.success?
            assert_instance_of InvoiceItem, result.entity
          end
        end
      end

      describe 'when validation errors' do
        before { InvoiceItem.any_instance.stubs(:save).returns(false) }

        it "does not save the new item" do
          assert_no_difference "InvoiceItem.count" do
            result = subject.call params
            refute result.success?
            assert_instance_of InvoiceItem, result.entity
          end
        end
      end

      describe 'when invoice is locked' do
        before do
          invoice.become_received
          invoice.save!
          assert invoice.locked?
        end

        it 'does not save the invoice item, returns an error' do
          message = "Cannot save. The record is read-only."

          assert_no_difference "InvoiceItem.count" do
            result = subject.call params
            refute result.success?
            assert_instance_of InvoiceItem, result.entity
            assert result.errors.include?(message)
          end
        end
      end
    end

    describe 'material_container#lot_number setting' do
      let(:create_material_container) { stub }
      let(:subject) { described_class.new(create_material_container) }

      it 'sets container lot number if not set (nil)' do
        expected_container_params = params[:material_container].merge(lot_number: "000001/1")
        create_material_container.expects(:call).with(expected_container_params).returns(success_result)

        subject.call params
      end

      it 'sets container lot number if not set (blank)' do
        params[:material_container][:lot_number] = ''

        expected_container_params = params[:material_container].merge(lot_number: "000001/1")
        create_material_container.expects(:call).with(expected_container_params).returns(success_result)

        subject.call params
      end

      it 'does not overwrite the lot number if specified' do
        params[:material_container][:lot_number] = 'ABC123'

        expected_container_params = params[:material_container].merge(lot_number: "ABC123")
        create_material_container.expects(:call).with(expected_container_params).returns(success_result)

        subject.call params
      end
    end
  end
end
