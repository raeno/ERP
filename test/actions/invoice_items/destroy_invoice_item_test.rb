# frozen_string_literal: true
require 'test_helper'
module InvoiceItems
  class DestroyInvoiceItemTest < ActiveSupport::TestCase
    let(:subject)  { described_class.new }
    let(:invoice)  { create :invoice }
    let(:invoice_item)  { create :invoice_item, invoice: invoice }
    before { invoice_item }

    describe '#call' do
      describe 'when success' do
        it 'deletes the invoice item' do
          assert_difference "InvoiceItem.count", -1 do
            result = subject.call invoice_item
            assert result.success?
          end
        end
      end

      describe 'when cannot delete' do
        before { InvoiceItem.any_instance.stubs(:destroy).returns(false) }

        it "returns failure result" do
          assert_no_difference "InvoiceItem.count" do
            result = subject.call invoice_item
            refute result.success?
            assert_equal invoice_item, result.entity
          end
        end
      end

      describe 'when invoice is locked' do
        before do
          invoice_item.become_received
          invoice_item.save!
        end

        it 'does not delete the item, returns an error' do
          message = "Cannot destroy. The record is read-only."

          assert_no_difference "InvoiceItem.count" do
            result = subject.call invoice_item
            refute result.success?
            assert_equal invoice_item, result.entity
            assert result.errors.include?(message)
          end
        end
      end
    end
  end
end
