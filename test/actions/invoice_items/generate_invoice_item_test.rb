# frozen_string_literal: true
require 'test_helper'
module InvoiceItems
  class GenerateInvoiceItemTest < ActiveSupport::TestCase
    let(:material) { create :material }
    let(:material_container) { build(:material_container) }
    let(:success_result) { stub(success?: true, entity: material_container) }
    let(:invoice) { create :invoice }

    describe '#call' do
      describe 'when success' do
        let(:generate_material_container) { stub(call: success_result) }
        let(:subject)  { described_class.new(generate_material_container) }

        it 'generates an invoice item' do
          assert_difference -> { InvoiceItem.count }, 1 do
            result = subject.call invoice, material
            assert result.success?
          end
        end

        it 'assigns proper values to fields' do
          new_item = subject.call(invoice, material).entity
          assert_equal invoice, new_item.invoice
          assert_equal 1, new_item.quantity
          refute_nil new_item.material_container
        end
      end

      describe 'when validation errors' do
        let(:generate_material_container) { stub(call: success_result) }
        let(:subject)  { described_class.new(generate_material_container) }
        before { InvoiceItem.any_instance.stubs(:save).returns(false) }

        it "does not save the invoice item" do
          assert_no_difference -> { InvoiceItem.count } do
            result = subject.call invoice, material
            refute result.success?
          end
        end
      end

      describe 'when cannot generate a material container' do
        let(:error_message) { "Container error for #{material.name}" }
        let(:failure_result) { stub(success?: false, failure?: true, errors: [error_message]) }
        let(:generate_material_container) { stub(call: failure_result) }
        let(:subject)  { described_class.new(generate_material_container) }

        it 'returns failure, pops up the container error message' do
          result = subject.call invoice, material
          refute result.success?
          assert result.errors.include? error_message
        end
      end
    end

    describe 'material_container#lot_number setting' do
      let(:generate_material_container) { stub }
      let(:subject) { described_class.new(generate_material_container) }

      it 'sets container lot number if not set' do
        generate_material_container.expects(:call).with(material, "000001/1").returns(success_result)

        subject.call invoice, material
      end
    end
  end
end
