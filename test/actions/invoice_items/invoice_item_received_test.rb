# frozen_string_literal: true
require 'test_helper'
module InvoiceItems
  class InvoiceItemReceivedTest < ActiveSupport::TestCase
    let(:successful_result) { stub(success?: true,  failure?: false) }
    let(:failed_result) { stub(success?: false, failure?: true) }

    let(:update_material_price_action) { stub }
    let(:increment_inventory_action) { stub }
    let(:refresh_invoice_status_action) { stub }
    let(:action) do
      described_class.new update_material_price_action, increment_inventory_action, refresh_invoice_status_action
    end

    let(:material) { create :material }
    let(:inventory_item) { find_or_create_material_inventory_item(material) }
    let(:invoice) { create :invoice }
    let(:material_container) { create :material_container, unit: material.unit, price: 222, amount: 111, material: material }
    let(:invoice_item) { create :invoice_item, invoice: invoice, material_container: material_container, quantity: 2 }
    before { invoice.stubs(:total_price_matching?).returns(true) }

    describe "when everything goes well" do
      it 'sets received_date and calls three related actions with correct params' do
        # Expect successful material price update action
        update_material_price_action.expects(:call).with(material, 222, 444).returns(successful_result).once

        # Expect successful inventory update action
        increment_inventory_action.expects(:call).with(inventory_item, 222).returns(successful_result).once

        # Expect successful invoice status  action
        refresh_invoice_status_action.expects(:call).with(invoice).returns(successful_result).once

        result = action.call invoice_item
        assert result.success?
        assert invoice_item.reload.received?
      end
    end

    describe "when invoice totals do not match" do
      it 'returns failure, does not make any changes' do
        invoice_item.stubs(:invoice_total_price_matching?).returns(false)

        increment_inventory_action.expects(:call).never
        update_material_price_action.expects(:call).never

        assert_untouched material, inventory_item.inventory do
          result = action.call invoice_item
          assert result.failure?
        end
      end
    end

    describe "when the item is already received" do
      it 'returns failure, does not make any changes' do
        invoice_item.become_received
        invoice_item.save!
        assert invoice_item.received?

        increment_inventory_action.expects(:call).never
        update_material_price_action.expects(:call).never

        assert_untouched material, inventory_item.inventory do
          result = action.call invoice_item
          assert result.failure?
        end
      end
    end

    describe "when cannot update inventory" do
      it 'returns failure, does not make any changes' do
        increment_inventory_action.expects(:call).with(inventory_item, 222).returns(failed_result).once
        update_material_price_action.expects(:call).returns(successful_result).once
        assert_untouched material, inventory_item.inventory do
          result = action.call invoice_item
          assert result.failure?
        end
      end
    end

    describe "when cannot update material price" do
      it 'returns failure, does not make any changes' do
        update_material_price_action.expects(:call).with(material, 222, 444).returns(failed_result).once
        assert_untouched material, inventory_item.inventory do
          result = action.call invoice_item
          assert result.failure?
        end
      end
    end

    describe "when cannot refresh invoice status" do
      it 'returns failure, does not make any changes' do
        increment_inventory_action.expects(:call).returns(successful_result).once
        update_material_price_action.expects(:call).returns(successful_result).once

        refresh_invoice_status_action.expects(:call).with(invoice).returns(failed_result).once

        assert_untouched material, inventory_item.inventory do
          result = action.call invoice_item
          assert result.failure?
        end
      end
    end
  end
end
