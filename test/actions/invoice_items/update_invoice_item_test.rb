# frozen_string_literal: true
require 'test_helper'
module InvoiceItems
  class UpdateInvoiceItemTest < ActiveSupport::TestCase
    let(:success_result) { stub(success?: true, entity: material_container, errors: []) }
    let(:update_material_container) { stub(call: success_result) }

    let(:subject)  { described_class.new(update_material_container) }
    let(:invoice)  { create :invoice }
    let(:material_container) { create :material_container }
    let(:invoice_item)  { create :invoice_item, invoice: invoice, material_container: material_container }
    let(:params) { { quantity: 4 } }

    describe '#call' do
      describe 'when success' do
        it 'updates the invoice item' do
          result = subject.call invoice_item, params
          assert result.success?
          assert_equal invoice_item, result.entity
        end
      end

      describe 'when validation errors' do
        before { InvoiceItem.any_instance.stubs(:save).returns(false) }

        it "does not save the item" do
          assert_untouched invoice_item do
            result = subject.call invoice_item, params
            refute result.success?
            assert_equal invoice_item, result.entity
          end
        end
      end

      describe 'when has material container params inside' do
        let(:params) { { quantity: 3, material_container: { amount: 4 } } }
        it 'updates related material container' do
          update_material_container = stub
          update_material_container.expects(:call).with(material_container, amount: 4).once.returns(success_result)

          action = described_class.new update_material_container
          action.call invoice_item, params
        end
      end
    end
  end
end
