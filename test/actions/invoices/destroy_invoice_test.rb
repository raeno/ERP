# frozen_string_literal: true
require 'test_helper'
module Invoices
  class DestroyInvoiceTest < ActiveSupport::TestCase
    let(:subject)  { described_class.new }
    let(:invoice)  { create :invoice }
    before { invoice }

    describe '#call' do
      describe 'when success' do
        it 'deletes the invoice' do
          assert_difference "Invoice.count", -1 do
            result = subject.call invoice
            assert result.success?
          end
        end
      end

      describe 'when cannot delete' do
        before { Invoice.any_instance.stubs(:destroy).returns(false) }

        it "returns failure result" do
          assert_no_difference "Invoice.count" do
            result = subject.call invoice
            refute result.success?
            assert_equal invoice, result.entity
          end
        end
      end

      describe 'when invoice is locked' do
        before do
          invoice.become_received
          invoice.save!
          assert invoice.locked?
        end

        it 'does not delete the invoice, returns an error' do
          message = "Cannot destroy. The record is read-only."

          assert_no_difference "Invoice.count" do
            result = subject.call invoice
            refute result.success?
            assert_equal invoice, result.entity
            assert result.errors.include?(message)
          end
        end
      end
    end
  end
end
