# frozen_string_literal: true
require 'test_helper'
module Invoices
  class GenerateInvoiceTest < ActiveSupport::TestCase
    let(:subject)  { described_class.build }
    let(:vendor) { create :vendor }

    let(:materials) { [] }
    before do
      3.times do
        material = create :material, unit_price: 0.99
        material.stubs(:production_can_cover).returns(100)
        Material.stubs(:find).with(material.id).returns(material)
        materials << material
        create :ordering_size, material: material, unit: material.unit, amount: 100
      end
    end

    let(:params) { { vendor_id: vendor.id, material_ids: materials.map(&:id) } }

    describe '#call' do
      describe 'when success' do
        it 'generates an invoice with an invoice item per each material' do
          assert_difference -> { Invoice.count }, 1 do
            assert_difference -> { InvoiceItem.count }, 3 do
              result = subject.call params
              assert result.success?
              new_invoice = result.entity

              assert_equal 3, new_invoice.items.count
              assert new_invoice.number.present?
              assert new_invoice.fees.any?
              assert_equal 0.99 * 100 * 3, new_invoice.total_price.to_f
            end
          end
        end
      end

      describe 'when materials are empty' do
        it 'does not generate anything, returns an error' do
          params = { vendor_id: vendor.id, material_ids: [] }

          assert_no_difference -> { Invoice.count } do
            assert_no_difference -> { InvoiceItem.count } do
              result = subject.call params
              refute result.success?
            end
          end
        end
      end

      describe 'when ordering size is missing for a material' do
        before do
          material = materials.first
          material.update_column(:name, "Lemon Oil")
          material.ordering_sizes.delete_all
        end

        it 'does not generate anything, returns an error, mentions the material' do
          assert_no_difference -> { Invoice.count } do
            assert_no_difference -> { InvoiceItem.count } do
              result = subject.call params
              refute result.success?
              assert_match(/Cannot find an ordering size for Lemon Oil/, result.error_message)
            end
          end
        end
      end

      describe 'when materials param is missing' do
        it 'does not generate anything, returns an error' do
          params = { vendor_id: vendor.id }

          assert_no_difference -> { Invoice.count } do
            assert_no_difference -> { InvoiceItem.count } do
              result = subject.call params
              refute result.success?
            end
          end
        end
      end
    end
  end
end
