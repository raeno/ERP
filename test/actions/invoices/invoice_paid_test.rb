# frozen_string_literal: true
require 'test_helper'
module Invoices
  class InvoicePaidTest < ActiveSupport::TestCase
    let(:subject)  { described_class.new }
    let(:invoice)  { create :invoice }
    before { invoice.stubs(:total_price_matching?).returns(true) }

    describe '#call' do
      describe 'when success' do
        it 'sets the invoice paid_date' do
          refute invoice.paid_date
          result = subject.call invoice
          assert result.success?
          assert_equal invoice, result.entity
          assert_equal Time.zone.today, invoice.paid_date
        end
      end

      describe 'when validation errors' do
        before { Invoice.any_instance.stubs(:save).returns(false) }

        it "returns failure" do
          result = subject.call invoice
          refute result.success?
          assert_equal invoice, result.entity
        end
      end

      describe 'when invoice is already paid' do
        before { invoice.update_column(:paid_date, 5.days.ago) }

        it "leaves the invoice untouched, returns failure" do
          result = subject.call invoice
          refute result.success?
          assert_equal 5.days.ago.to_date, invoice.reload.paid_date
        end
      end

      describe "when invoice totals do not match" do
        it 'returns failure, does not make any changes' do
          invoice.stubs(:total_price_matching?).returns(false)

          assert_untouched invoice do
            result = subject.call invoice
            assert result.failure?
          end
        end
      end

      describe 'when invoice is locked' do
        before do
          invoice.become_received
          invoice.save!
          assert invoice.locked?
        end

        it 'still works successfully' do
          refute invoice.paid_date
          result = subject.call invoice
          assert result.success?
          assert_equal invoice, result.entity
          assert_equal Time.zone.today, invoice.paid_date
        end
      end
    end
  end
end
