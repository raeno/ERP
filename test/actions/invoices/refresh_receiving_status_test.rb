# frozen_string_literal: true
require 'test_helper'
module Invoices
  class RefreshReceivingStatusTest < ActiveSupport::TestCase
    let(:invoice)  { create :invoice }

    let(:successful_result) { stub(success?: true,  failure?: false) }
    let(:vendor_action) { stub(call: successful_result) }

    let(:action) { described_class.new vendor_action }

    before { refute invoice.received? }

    describe '#call' do
      describe 'when all invoice items have been received' do
        before { invoice.stubs(:all_items_received?).returns(true) }

        it 'returns successful result' do
          result = action.call invoice
          assert result.success?
        end

        it 'marks the invoice as received, too' do
          action.call invoice
          assert invoice.received?
        end

        it 'calls the vendor lead time refresh action' do
          vendor_action.expects(:call).with(invoice.vendor).returns(successful_result).once
          action.call invoice
        end
      end

      describe 'when NOT all invoice items are received' do
        before { invoice.stubs(:all_items_received?).returns(false) }

        it 'returns successful result' do
          result = action.call invoice
          assert result.success?
        end

        it 'does not change invoice status' do
          action.call invoice
          refute invoice.received?
        end
      end
    end
  end
end
