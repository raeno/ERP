# frozen_string_literal: true
require 'test_helper'
module Invoices
  class UpdateInvoiceTest < ActiveSupport::TestCase
    let(:subject)  { described_class.new }
    let(:invoice)  { create :invoice }
    let(:params) { { number: "33333333" } }

    describe '#call' do
      describe 'when success' do
        it 'updates the invoice' do
          result = subject.call invoice, params
          assert result.success?
          assert_equal invoice, result.entity
          assert_equal "33333333", invoice.number
        end
      end

      describe 'when validation errors' do
        before { Invoice.any_instance.stubs(:save).returns(false) }

        it "does not save the invoice" do
          assert_untouched invoice do
            result = subject.call invoice, params
            refute result.success?
            assert_equal invoice, result.entity
          end
        end
      end
    end
  end
end
