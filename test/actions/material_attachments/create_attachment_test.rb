require 'test_helper'

module MaterialAttachments
  class CreateAttachmentTest < ActiveSupport::TestCase

    let(:material) { create :material }
    let(:attachment_params) { { url: 'some_url' } }
    let(:params) { attachment_params.merge(material_id: material.id) }

    subject { described_class.new }

    describe '#call' do
      it 'creates attachment' do
        assert_difference 'Attachment.count', +1 do
          subject.call params
        end
      end

      it 'creates material attachment' do
        assert_difference 'MaterialAttachment.count', +1 do
          subject.call params
        end
      end

      it 'provides successul result' do
        result = subject.call params
        assert result.success?
      end

      it 'has created material attachment as entity' do
        result = subject.call params
        refute_nil result.entity
        assert_kind_of MaterialAttachment, result.entity
      end

      describe 'when cannot create attachment' do
        before do
          Attachment.stubs(:create).returns(false)
        end

        it 'does not create material attachment as well' do
          assert_no_difference 'MaterialAttachment.count' do
            subject.call params
          end
        end

        it 'returns with failure result' do
          result = subject.call params
          assert result.failure?
        end
      end

      describe 'when cannot create material attachment' do
        before do
          MaterialAttachment.stubs(:create).returns(false)
        end

        it 'also returns failure result' do
          result = subject.call params
          assert result.failure?
        end
      end
    end
  end
end
