# frozen_string_literal: true
require 'test_helper'

module MaterialContainers
  class CreateMaterialContainerTest < ActiveSupport::TestCase
    let(:unit) { create :unit }
    let(:material) { create :material, unit: unit }
    let(:ordering_size) { create :ordering_size, material: material, amount: 10, unit: unit }

    let(:params) do
      { material_id: material.id, ordering_size_id: ordering_size.id,
        external_name: 'some_name', price: 30, lot_number: 'Lot Number' }
    end
    subject { described_class.new }

    it 'creates container from params' do
      assert_difference 'MaterialContainer.count', +1 do
        subject.call params
      end
    end

    it 'takes amount and unit from the ordering_size' do
      result = subject.call params
      container = result.entity
      assert_equal 10, container.amount
      assert_equal unit, container.unit
    end

    it 'sets amount left as intiial amount by default' do
      result = subject.call params
      container = result.entity
      assert_equal 10, container.amount
      assert_equal 10, container.amount_left
    end

    it 'saves the lot number' do
      result = subject.call params
      container = result.entity
      assert_equal 'Lot Number', container.lot_number
    end

    describe 'when ordering size is missing' do
      before { params[:ordering_size_id] = nil }

      it 'returns error' do
        result = subject.call params
        assert result.failure?
        assert result.errors.include?('Please select ordering size')
      end
    end

    describe 'when already has active container' do
      before do
        create :material_container, material: material, unit: unit, status: 'active'
      end

      it 'sets container status as pending' do
        result = subject.call params
        container = result.entity
        assert_equal 'pending', container.status
      end
    end

    describe 'when no active containers for this material present' do
      it 'sets container as active' do
        result = subject.call params
        container = result.entity
        assert_equal 'active', container.status
      end
    end
  end
end
