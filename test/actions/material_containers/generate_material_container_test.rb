# frozen_string_literal: true
require 'test_helper'

module MaterialContainers
  class GenerateMaterialContainerTest < ActiveSupport::TestCase
    let(:subject) { described_class.new }
    let(:unit) { create :unit }
    let(:material) { create :material, unit: unit, unit_price: 5.99 }
    let(:ordering_size) { create :ordering_size, material: material, unit: unit, amount: 1111.33 }
    let(:lot_number) { 'ABC123' }

    before do
      material.stubs(:production_can_cover).returns(1100)
    end

    describe 'when success' do
      before do
        MaterialPurchaseSizeSelector.any_instance.expects(:best_size).with(1100).returns(ordering_size)
      end

      it 'generates a material container' do
        assert_difference 'MaterialContainer.count', +1 do
          result = subject.call material, lot_number
          assert result.success?
        end
      end

      it 'assigns proper values to fields' do
        ordering_size.stubs(:estimated_price).returns(777.77)
        new_container = subject.call(material, lot_number).entity

        assert_equal material, new_container.material

        assert_equal ordering_size, new_container.ordering_size
        assert_equal unit, new_container.unit
        assert_equal 1111.33, new_container.amount

        assert_equal 777.77, new_container.price
        assert_equal lot_number, new_container.lot_number
      end

      it 'sets amount left to initial amount' do
        container = subject.call(material, lot_number).entity
        assert_equal 1111.33, container.amount
        assert_equal 1111.33, container.amount_left
      end
    end

    describe 'when there is no ordering sizes' do
      before do
        MaterialPurchaseSizeSelector.any_instance.expects(:best_size).with(1100).returns(nil)
      end

      it 'returns an error' do
        assert_no_difference  -> { MaterialContainer.count } do
          result = subject.call material, lot_number
          assert result.failure?
          assert result.errors.include? "Cannot find an ordering size for #{material.name}"
        end
      end
    end
  end
end
