# frozen_string_literal: true
require 'test_helper'

module MaterialContainers
  class TrackMaterialUsageTest < ActiveSupport::TestCase
    let(:choose_active_container) { stub(call: success_result) }

    subject { described_class.new(choose_active_container) }

    describe 'when passes container is nil' do
      it 'responds with nil entity error' do
        result = subject.call nil, 10
        assert result.failure?
        assert_equal 'Passed entity should not be nil', result.error_message
      end
    end

    describe 'when does not have enough material in container' do
      let(:container) { build :material_container, amount_left: 20 }

      it 'returns error' do
        result = subject.call container, 100
        assert result.failure?
        assert_equal 'Using more material than container has', result.error_message
      end
    end

    describe 'when container has more amount than needed' do
      let(:container) { create :material_container, amount_left: 100 }

      it 'deducts given amount from container' do
        subject.call container, 30
        assert_equal 70, container.reload.amount_left
      end

      it 'returns successful result' do
        result = subject.call container, 30
        assert result.success?
      end
    end

    describe 'when container is empty after deduct' do
      let(:unit) { create :unit }
      let(:material) { create :material, unit: unit }
      let(:container) { create :material_container, amount_left: 30, status: 'active', material: material, unit: unit }

      it 'marks container as closed' do
        assert_equal 'active', container.status
        subject.call container, 30
        assert_equal 'closed', container.reload.status
      end

      it 'chooses next active container' do
        choose_active_container = stub
        choose_active_container.expects(:call).with(material).once.returns(success_result)
        subject = described_class.new choose_active_container
        subject.call container, 30
      end
    end
  end
end
