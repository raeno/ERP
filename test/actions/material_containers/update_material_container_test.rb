# frozen_string_literal: true
require 'test_helper'

module MaterialContainers
  class UpdateMaterialContainerTest < ActiveSupport::TestCase
    let(:unit) { create :unit }
    let(:material) { create :material, unit: unit }
    let(:ordering_size) { create :ordering_size, material: material, amount: 11, unit: unit }
    let(:material_container) { create :material_container }

    let(:params) do
      { material_id: material.id, ordering_size_id: ordering_size.id,
        external_name: 'some_name', price: 30 }
    end
    subject { described_class.new }

    it 'updates container from params' do
      result = subject.call material_container, params
      assert result.success?
      assert_equal 11, material_container.reload.amount
      assert_equal unit, material_container.unit
      assert_equal 30, material_container.price
    end

    describe 'when ordering size is missing' do
      before { params[:ordering_size_id] = nil }

      it 'returns error' do
        result = subject.call material_container, params
        assert result.failure?
        assert result.errors.include?('Please select ordering size')
      end
    end
  end
end
