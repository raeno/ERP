# frozen_string_literal: true
require 'test_helper'

class CreateMaterialTest < ActiveSupport::TestCase
  before { create :zone, :make }
  let(:success_result) { stub(success?: true) }
  let(:create_inv_item) { stub(call: success_result) }
  let(:action) { Materials::CreateMaterial.new(create_inv_item) }
  let(:unit) { create :unit }

  let(:params) { { name: 'Some oil', unit_id: unit.id, ingredient_unit_id: unit.id } }

  describe 'when params are valid' do
    it 'creates a material' do
      assert_difference 'Material.count', +1 do
        action.call params
      end
    end

    it 'returns successful result' do
      result = action.call params
      assert result.success?
    end

    it 'calls an action to create an inventory item' do
      create_inv_item.expects(:call).returns(success_result)
      action.call params
    end
  end

  describe 'when unable to create' do
    before { Material.any_instance.stubs(:save).returns(false) }

    it 'returns failure' do
      result = action.call params
      assert result.failure?
    end

    it 'does not create any material inventories' do
      assert_no_difference 'Inventory.count' do
        assert_no_difference 'InventoryItem.count' do
          action.call params
        end
      end
    end
  end
end
