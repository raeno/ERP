# frozen_string_literal: true
require 'test_helper'

module Materials
  class UpdateMaterialTest < ActiveSupport::TestCase
    let(:subject) { described_class.build }
    let(:material) { create :material }

    describe '#call' do
      describe 'when success' do
        describe 'zone behaviour' do
          let(:zone) { create :zone }
          let(:inventory_item) do
            find_or_create_material_inventory_item(material, production_zone: zone)
          end
          before do
            assert_equal zone, inventory_item.zone
            assert_equal zone, inventory_item.production_zone
          end

          describe 'when zone is passed in params' do
            let(:new_zone) { create :zone }
            let(:params) { { inventory_item_production_zone_id: new_zone.id } }

            it 'updates zone and production_zone for the ii' do
              result = subject.call material, params
              assert result.success?

              assert_equal new_zone, inventory_item.reload.zone
              assert_equal new_zone, inventory_item.production_zone
            end
          end

          # Eliminates a bug: when editing material ordering sizes via a separate form,
          # material production zone got unexpectedly reset.
          describe 'when zone is not passed in params' do
            let(:params) { { name: "Orange" } }

            it 'keeps previous zone and production_zone for the ii' do
              result = subject.call material, params
              assert result.success?
              material = result.entity

              # something is updated:
              assert_equal "Orange", material.name
              # but zones remain the same:
              assert_equal zone, inventory_item.reload.zone
              assert_equal zone, inventory_item.production_zone
            end
          end
        end
      end
    end
  end
end
