# frozen_string_literal: true
require 'test_helper'
module Materials
  class UpdateUnitPriceTest < ActiveSupport::TestCase
    describe 'call' do
      let(:action) { described_class.new }
      let(:material) { create :material, unit_price: 2.22 }
      before { find_or_create_material_inventory_item material, quantity: 100 }

      describe 'when adding more material' do
        it 'adjusts unit price up if adding a more expensive portion of material' do
          result = action.call(material, 100, 4.44 * 100)
          assert result.success?
          assert_equal 3.33, material.reload.unit_price.round(2)
        end

        it 'adjusts unit price down when adding a cheaper portion of material' do
          result = action.call(material, 100, 1 * 100)
          assert result.success?
          assert_equal 1.61, material.reload.unit_price.round(2)
        end

        it 'keeps unit price when adding a portion of material at the same price' do
          result = action.call(material, 500, 2.22 * 500)
          assert result.success?
          assert_equal 2.22, material.reload.unit_price.round(2)
        end

        it 'works correctly with any portion sizes and prices' do
          result = action.call(material, 10, 108)
          assert result.success?
          assert_equal 3.0, material.reload.unit_price.round(2)
        end
      end

      describe 'when cancelling material purchase' do
        it 'works correctly with negative numbers' do
          result = action.call(material, -50, -147)
          assert result.success?
          assert_equal 1.5, material.reload.unit_price.round(2)
        end

        it 'leaves previous unit price if cancellation results in zero inventory' do
          result = action.call(material, -100, -222)
          assert result.success?
          assert_equal 2.22, material.reload.unit_price.round(2)
        end
      end

      it 'works correctly if only changing invoice price, leaving quantity the same' do
        result = action.call(material, 0, -22)
        assert result.success?
        assert_equal 2.0, material.reload.unit_price.round(2)
      end

      it 'works correctly if only changing invoice amount, leaving price the same' do
        result = action.call(material, -80, 0)
        assert result.success?
        assert_equal 11.1, material.reload.unit_price.round(2)
      end
    end
  end
end
