# frozen_string_literal: true
require 'test_helper'

module ProductAttachments
  class CreateAttachmentTest < ActiveSupport::TestCase
    let(:product) { create :product }
    let(:attachment_params) { { url: 'some_url' } }
    let(:params) { attachment_params.merge(product_id: product.id) }
    subject { described_class.new }

    describe '#call' do
      it 'creates attachment' do
        assert_difference 'Attachment.count', +1 do
          subject.call params
        end
      end

      it 'creates product attachment' do
        assert_difference 'ProductAttachment.count', +1 do
          subject.call params
        end
      end

      it 'provides successul result' do
        result = subject.call params
        assert result.success?
      end

      it 'has created product attachment as entity' do
        result = subject.call params
        refute_nil result.entity
        assert_kind_of ProductAttachment, result.entity
      end

      describe 'when cannot create attachment' do
        before do
          Attachment.stubs(:create).returns(false)
        end

        it 'does not create product attachment as well' do
          assert_no_difference 'ProductAttachment.count' do
            subject.call params
          end
        end

        it 'returns with failure result' do
          result = subject.call params
          assert result.failure?
        end
      end

      describe 'when cannot create product attachment' do
        before do
          ProductAttachment.stubs(:create).returns(false)
        end

        it 'also returns failure result' do
          result = subject.call params
          assert result.failure?
        end
      end
    end
  end
end
