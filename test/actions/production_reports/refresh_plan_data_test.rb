# frozen_string_literal: true

require 'test_helper'

module ProductionReports
  class RefreshPlanDataTest < ActiveSupport::TestCase
    subject { described_class.new }

    describe "#call" do
      let(:product) { create :product, title: 'some_product' }
      let(:inventory_item) { create :inventory_item, inventoriable: product }
      before do
        plan = stub(demand: 1, demand_coverage: 2, supply: 3, to_cover: 4, can_cover: 5,
                    current_inventory: 6, can_cover_in_cases: 7, can_cover_optimal: 8,
                    deficit_in_previous_zone?: false,
                    deficit_inventoriable: product)
        ProductionPlan::Plan.stubs(:build).returns(plan)
      end

      describe "when there is no report" do
        it "creates one" do
          assert_difference 'InventoryItems::ProductionReport.count', +1 do
            subject.call inventory_item
          end
        end
      end

      describe "when report already exists" do
        let(:report) { create :production_report, inventory_item: inventory_item }
        before { report }

        it "updates the existing report, does not create new" do
          assert_not_equal 7, report.can_cover_in_cases

          assert_no_difference 'InventoryItems::ProductionReport.count' do
            subject.call inventory_item
          end

          report.reload
          assert_equal 7, report.can_cover_in_cases
        end

        it 'copies production plan fields into report' do
          subject.call inventory_item
          assert_equal 1, report.demand
          assert_equal 2, report.demand_coverage
          assert_equal 3, report.supply
          assert_equal 4, report.to_cover
          assert_equal 5, report.can_cover
          assert_equal 6, report.in_stock
          assert_equal 7, report.can_cover_in_cases
          assert_equal 8, report.can_cover_optimal
        end

        it 'copies deficit information as well' do
          subject.call inventory_item
          refute report.previous_zone_deficit?
          assert_equal product, report.deficit_inventoriable
          assert_equal 'some_product', report.deficit_ingredient_name
        end
      end
    end
  end
end
