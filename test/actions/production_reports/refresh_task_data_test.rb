# frozen_string_literal: true

require 'test_helper'

module ProductionReports
  class RefreshTaskDataTest < ActiveSupport::TestCase
    subject { described_class.new }

    describe "#call" do
      let(:inventory_item) { create :inventory_item, :with_production_report }

      describe "when there are related tasks" do
        it "updates the :assigned field of the report based on the current tasks" do
          create :task, inventory_item: inventory_item, quantity: 111
          create :task, inventory_item: inventory_item, quantity: 222

          subject.call inventory_item

          assert_equal 111 + 222, inventory_item.production_report.assigned
        end
      end

      describe "when there are no tasks" do
        it "updates the :assigned field with zero" do
          subject.call inventory_item
          assert_equal 0, inventory_item.production_report.assigned
        end
      end
    end
  end
end
