# frozen_string_literal: true

require 'test_helper'

module Products
  class AddProductsToShipmentTest < ActiveSupport::TestCase

    describe '.build' do
      it 'creates instance of class with all dependencies' do
        instance = described_class.build
        assert_instance_of described_class, instance
      end
    end

    let(:user) { create :user }
    let(:shipment_id) { 'TEST_SHIPMENT_ID' }
    let(:shipment_name) { 'TEST_SHIPMENT_NAME'}
    let(:shipment) { create :amazon_shipment, user: user, name: shipment_name, shipment_id: shipment_id  }
    let(:shipment_items) { 3.times.map { create :shipment_entry, amazon_shipment: shipment } }

    let(:success_result) { stub(success?: true, entity: shipment )}
    let(:update_shipment) { stub(call: success_result) }
    let( :patch_shipment) { stub(call: success_result) }
    let(:sync_batches) { stub(call: success_result) }
    let(:notification_service) { stub(call: success_result) }

    it 'patches shipment on Amazon first' do
      patch_shipment = stub
      patch_shipment.expects(:call).once.returns(success_result)

      instance = described_class.new update_shipment, patch_shipment, sync_batches, notification_service
      instance.call shipment
    end

    it 'syncs shipment entries with batches' do
      sync_batches = stub
      sync_batches.expects(:call).once.returns(success_result)

      instance = described_class.new update_shipment, patch_shipment, sync_batches, notification_service
      instance.call shipment
    end

    it 'returns successful result' do
      instance = described_class.new update_shipment, patch_shipment, sync_batches, notification_service
      result = instance.call shipment
      assert result.success?
    end

    it 'has processed shipment as result entity' do
      instance = described_class.new update_shipment, patch_shipment, sync_batches, notification_service
      result = instance.call shipment
      assert_equal shipment, result.entity
    end
  end
end
