# frozen_string_literal: true
require 'test_helper'

module Products
  class AssessDependenciesTest < ActiveSupport::TestCase
    describe '.build' do
      it 'builds an instance with all necessary dependencies' do
        instance = described_class.build
        assert_kind_of described_class, instance
        refute_nil instance.create_inventory_item
        refute_nil instance.refresh_production_report
        refute_nil instance.destroy_production_report
        refute_nil instance.destroy_inventory_item
      end
    end

    describe '#call' do
      let(:product) { create :product }
      let(:create_inventory_item) { stub(call: success_result) }
      let(:refresh_production_report) { stub(call: success_result) }
      let(:destroy_production_report) { stub(call: success_result) }
      let(:destroy_inventory_item) { stub(call: success_result) }

      before do
        @make_zone, @pack_zone, @ship_zone = %w(make pack ship).map { |name| create :zone, name: name }

        [@make_zone, @ship_zone].each { |z| create :product_zone_availability, product: product, zone: z }
        @make_ii, @pack_ii = [@make_zone, @pack_zone].map { |z| create :inventory_item, inventoriable: product, zone: z }
        @make_report, @pack_report = [@make_ii, @pack_ii].map { |ii| create :production_report, inventory_item: ii }
      end

      it 'creates all missing inventory items for product zones' do
        create_inventory_item = stub
        create_inventory_item.expects(:call).with(inventoriable: product, zone: @ship_zone).once.returns(success_result)
        action = described_class.new create_inventory_item, refresh_production_report,
                                     destroy_production_report, destroy_inventory_item
        action.call product
      end

      it 'refreshes productions report for each created inventory item' do
        new_inv_item = build :inventory_item, inventoriable: product, zone: @ship_zone
        result = stub(success?: true, entity: new_inv_item)
        create_inventory_item = stub(call: result)

        refresh_production_report = stub
        refresh_production_report.expects(:call).with(product).once.returns(success_result)

        action = described_class.new create_inventory_item, refresh_production_report,
                                     destroy_production_report, destroy_inventory_item
        action.call product
      end

      it 'deletes inventory items for unchecked zones' do
        destroy_inventory_item = stub
        destroy_inventory_item.expects(:call).with(@pack_ii).once.returns(success_result)
        action = described_class.new create_inventory_item, refresh_production_report,
                                     destroy_production_report, destroy_inventory_item
        action.call product
      end

      it 'destroys production reports for deleted items' do
        destroy_production_report = stub
        destroy_production_report.expects(:call).with(@pack_report).once.returns(success_result)
        action = described_class.new create_inventory_item, refresh_production_report,
                                     destroy_production_report, destroy_inventory_item
        action.call product
      end
    end
  end
end
