# frozen_string_literal: true
require 'test_helper'

class CreateProductTest < ActiveSupport::TestCase
  let(:product_exposer) { stub(expose_to_all_zones: true) }
  let(:create_inv_item) { stub(call: true) }
  let(:refresh_reports) { stub(call: success_result) }

  let(:params) { { title: 'Some oil', size: 'big', weeks_of_cover: 10 } }

  let(:action) { Products::CreateProduct.new(product_exposer, create_inv_item, refresh_reports) }

  describe 'when params are valid' do
    it 'creates a product' do
      assert_difference 'Product.count', +1 do
        action.call params
      end
    end

    it 'returns successful result' do
      result = action.call params
      assert result.success?
    end

    it 'exposes product to all zones' do
      product_exposer.expects(:expose_to_all_zones).returns(true)
      action.call params
    end

    it 'creates amazon inventory for product' do
      assert_difference 'AmazonInventory.count', +1 do
        action.call params
      end
    end

    it 'creates reports' do
      refresh_reports.expects(:call).returns(success_result)
      action.call params
    end

    it 'calls an action to create inventory item for each zone for that product' do
      create_list :zone, 3
      create_inv_item.expects(:call).times(3)
      action.call params
    end
  end

  describe 'when unable to create' do
    before { Product.any_instance.stubs(:save).returns(false) }

    it 'returns failure' do
      result = action.call params
      assert result.failure?
    end

    it 'does not expose product' do
      product_exposer.expects(:expose_to_all_zones).never
      action.call params
    end

    it 'does not create amazon inventory' do
      assert_no_difference 'AmazonInventory.count' do
        action.call params
      end
    end

    it 'does not create reports' do
      refresh_reports.expects(:call).never
      action.call params
    end

    it 'does not create any product inventories' do
      assert_no_difference 'Inventory.count' do
        action.call params
      end
    end
  end
end
