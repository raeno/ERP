# frozen_string_literal: true
require 'test_helper'

module Products
  class MoveProductsInBatchTest < ActiveSupport::TestCase
    let(:product) { create :product }
    let(:current_zone) { create :zone, production_order: 2 }
    let(:previous_zone) { create :zone, production_order: 1 }
    let(:zones) { [previous_zone, current_zone] }
    let(:amount) { 10 }
    let(:batch) { Batch.new product: product, zone: current_zone, quantity: amount }
    let(:success_result) { stub(success?: true) }
    let(:update_inventories_action) { stub(call: success_result) }

    before do
      zones.each { |z| create :product_zone_availability, product: product, zone: z }
      inventory_items = zones.map do |zone|
        create :inventory_item, inventoriable: product, zone: zone
      end
      inventories = inventory_items.map { |ii| create :inventory, inventory_item: ii }
      @prev_inventory, @current_inventory = inventories
    end

    subject { described_class.new ZoneWorkflow, update_inventories_action }

    describe '.build' do
      it 'builds an instance of action' do
        instance = described_class.build
        assert_kind_of described_class, instance
      end
    end

    describe '#call' do
      describe 'when batch is new' do
        it 'calls an action to update related inventories with batch amount' do
          update_inventories_action.expects(:call).with do |product_param, from, to, amount_param|
            product_param.id == product.id &&
              from.id == @prev_inventory.id &&
              to.id == @current_inventory.id &&
              amount_param == amount
          end.returns(success_result)
          subject.call batch
        end

        it 'returns the result of update inventories action' do
          some_result = stub(success?: false, errors: ['Some error'])
          update_inventories_action.stubs(:call).returns(some_result)
          result = subject.call batch
          assert_equal some_result, result
        end

        describe 'when amount is 0' do
          it 'does not call update action' do
            update_inventories_action.expects(:call).never
            subject.call batch
          end

          it 'returns successful result immediately' do
            result = subject.call batch
            assert result.success?
          end
        end
      end

      describe 'when batch is updating' do
        describe 'when amount is increased on update' do
          let(:old_batch) { Batch.new product: product, zone: current_zone, quantity: 5 }

          it 'moves to destination inventory the difference' do
            difference = amount - old_batch.quantity
            update_inventories_action.expects(:call).with do |product_param, from, to, amount_param|
              product_param.id == product.id &&
                from.id == @prev_inventory.id &&
                to.id == @current_inventory.id &&
                amount_param == difference
            end.returns(success_result)
            subject.call batch, old_batch
          end
        end

        describe 'when amount is decreased on update' do
          let(:old_batch) { Batch.new product: product, zone: current_zone, quantity: 15 }

          it 'returns the difference to source inventory' do
            difference = (amount - old_batch.quantity).abs
            update_inventories_action.expects(:call).with do |product_param, from, to, amount_param|
              product_param.id == product.id &&
                from.id == @current_inventory.id &&
                to.id == @prev_inventory.id &&
                amount_param == difference
            end.returns(success_result)
            subject.call batch, old_batch
          end
        end
      end
    end
  end
end
