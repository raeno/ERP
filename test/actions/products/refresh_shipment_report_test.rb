# frozen_string_literal: true
require 'test_helper'

module Products
  class RefreshShipmentReportTest < ActiveSupport::TestCase
    let(:product) { create :product }
    let(:production_report) { build :production_report }
    let(:report) { ShipmentReport.new }
    let(:builder) { ShipmentReportBuilder }

    before do
      zone = create :zone, name: 'Ship'
      inventory_item = create :inventory_item, inventoriable: product, zone: zone
      inventory_item.stubs(:production_report).returns(production_report)
      product.stubs(:ship_inventory_item).returns(inventory_item)
    end

    describe '#call'  do
      describe 'when shipment report not exist' do
        it 'creates new one for product' do
          assert_difference 'ShipmentReport.count', +1 do
            described_class.new(builder).call(product)
          end
        end
      end

      describe 'when shipment report exists' do
        before do
          create :shipment_report, product: product
        end

        it 'does not create new one' do
          assert_no_difference 'ShipmentReport.count' do
            described_class.new(builder).call(product)
          end
        end
      end
    end
  end
end
