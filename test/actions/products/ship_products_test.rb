require 'test_helper'

module Products
  class ShipProductsTest < ActiveSupport::TestCase
    let(:success_result) { stub(success?: true, entity: shipment )}
    let(:shipment) { create :amazon_shipment }

    let(:initialize_shipment) { stub(call: success_result) }
    let(:submit_shipment) { stub(call: success_result) }
    let(:sync_batches) { stub(call: success_result) }
    let(:notification_service) { stub(call: success_result) }

    subject { described_class.new initialize_shipment, submit_shipment, sync_batches, notification_service }

    it 'responds with success' do
      result = subject.call shipment
      assert result.success?
    end

    it 'initializes shipment first' do
      initialize_shipment = stub
      initialize_shipment.expects(:call).once.with(shipment).returns(success_result)
      action = described_class.new initialize_shipment, submit_shipment, sync_batches, notification_service
      action.call shipment
    end

    it 'submits it to Amazon then' do
      submit_shipment = stub
      submit_shipment.expects(:call).once.with(shipment).returns(success_result)

      action = described_class.new initialize_shipment, submit_shipment, sync_batches, notification_service
      action.call shipment
    end

    it 'syncs batches quantities with performed shipment' do
      sync_batches = stub
      sync_batches.expects(:call).once.with(shipment).returns(success_result)

      action = described_class.new initialize_shipment, submit_shipment, sync_batches, notification_service
      action.call shipment
    end

    it 'broascasts status during execution of workflow' do
      notification_service = stub
      notification_service.expects(:call).times(4).returns(success_result)
      action = described_class.new initialize_shipment, submit_shipment, sync_batches, notification_service
      action.call shipment
    end
  end
end
