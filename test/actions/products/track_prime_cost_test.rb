# frozen_string_literal: true
require 'test_helper'

module Products
  class TrackPrimeCostTest < ActiveSupport::TestCase
    let(:product) { create :product }
    let(:pack_zone) { create :zone, :pack }
    let(:ship_zone) { create :zone, :ship }
    let(:batch) { create :batch, zone: pack_zone }

    before do
      product.stubs(:cumulative_cogs).returns(100)
      batch.stubs(:product).returns(product)
    end

    subject { described_class.new }

    it 'creates prime cost history entry' do
      assert_difference 'ProductCogsSnapshot.count', +1 do
        subject.call batch
      end
    end

    it 'calculates ingredients cost according to batch zone' do
      product.expects(:cumulative_cogs).with(pack_zone).returns(300)
      product.expects(:cumulative_cogs).with(ship_zone).never.returns(500)
      batch.stubs(:product).returns(product)

      result = subject.call batch
      history = result.entity
      assert_equal 300, history.cogs
    end

    it 'stores product prime cost on the moment the batch created' do
      result = subject.call batch
      history = result.entity
      assert_equal batch.id, history.batch_id
      assert_equal product.id, history.product_id
      assert_equal 100, history.cogs
    end
  end
end
