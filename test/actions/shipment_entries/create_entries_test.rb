require 'test_helper'

module ShipmentEntries
  class CreateEntriesTest < ActiveSupport::TestCase
    let(:product_1) { create :product }
    let(:product_2) { create :product }
    let(:shipment) { create :amazon_shipment }

    let(:params) { [ { product_id: product_1.id, quantity: 10 },
                     { product_id: product_2.id, quantity: 20}
                   ]}
    subject { described_class.new }

    it 'creates entry for each passed element in params' do
      assert_difference 'ShipmentEntry.count', +2 do
        subject.call shipment, params
      end
    end

    it 'responds with success' do
      result = subject.call shipment, params
      assert result.success?
    end

    describe 'when fails to create one or more entries' do
      before do
        subject.stubs(:call_on).returns(failure_result)
      end

      it 'responds with failure' do
        result = subject.call shipment, params
        assert result.failure?
      end

      it 'does not create any records' do
        assert_no_difference 'ShipmentEntry.count', +1 do
          subject.call shipment, params
        end
      end
    end
  end
end
