require 'test_helper'

module ShipmentEntries
  class PersistEntriesTest < ActiveSupport::TestCase

    let(:shipment) { create :amazon_shipment }

    let(:entry_1_product) { create :product }
    let(:entry_1_params) { { product_id: entry_1_product.id, quantity: 20 } }

    let(:entry_2_product) { create :product }
    let(:entry_2_params) { { product_id: entry_2_product.id, quantity: 30 } }
    let(:params) { [entry_1_params, entry_2_params] }

    let(:create_entries) { stub(call: success_result) }
    let(:update_entries) { stub(call: success_result) }

    before do
      @entry2 = create :shipment_entry, product: entry_2_product, amazon_shipment: shipment, quantity: 10
    end

    subject { described_class.new(create_entries, update_entries) }

    describe '.build' do
      it 'builds an instance with all dependencies' do
        instance = described_class.build
        assert_instance_of described_class, instance
      end
    end

    it 'creates entries for each product that is not present yet in shipment' do
      create_entries = stub
      expected_params = { amazon_shipment: shipment, product_id: entry_1_product.id, quantity: 20 }
      create_entries.expects(:call).once.with( expected_params).returns(success_result)
      action = described_class.new create_entries, update_entries
      action.call shipment, params
    end

    it 'updates entries for product that we already added to shipment' do
      update_entries = stub
      expected_params = { status: 'CHANGED', quantity: 10 + 30 }
      update_entries.expects(:call).once.with(@entry2, expected_params).returns(success_result)
      action = described_class.new create_entries, update_entries
      action.call shipment, params
    end
  end
end


