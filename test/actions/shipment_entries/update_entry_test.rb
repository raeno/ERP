require 'test_helper'

module ShipmentEntries
  class UpdateEntryTest < ActiveSupport::TestCase

    describe '.build' do
      it 'builds instance of class with all dependencies' do
        instance = described_class.build
        assert_kind_of described_class, instance
      end
    end

    let(:amazon_shipment) { create :amazon_shipment }
    let(:entry) { create :shipment_entry, status: 'NEW', amazon_shipment: amazon_shipment }
    let(:update_shipment) { stub(call: success_result) }
    let(:batch) { create :batch }
    subject { described_class.new update_shipment }

    it 'updates entry with passed params' do
      subject.call entry, status: 'NEW_STATUS', batch: batch

      updated_entry = entry.reload
      assert_equal 'NEW_STATUS', updated_entry.status
      assert_equal batch, updated_entry.batch
    end

    describe 'when passed entry is nil' do
      it 'returns with error' do
        result = subject.call nil, status: 'NEW_STATUS'
        refute result.success?
        assert_match /Passed entity should not be nil/, result.error_message
      end
    end

    describe 'when entry status is set to "CHANGED' do
      it 'sets parent shipment status to "CHANGED" as well' do
        update_shipment = stub
        update_shipment.expects(:call).with(amazon_shipment, status: 'CHANGED')
          .once.returns(success_result)

        action = described_class.new update_shipment
        action.call entry, status: 'CHANGED'
      end
    end
  end
end



