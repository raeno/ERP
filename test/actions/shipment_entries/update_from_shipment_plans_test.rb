require 'test_helper'

module ShipmentEntries
  class UpdateFromShipmentPlansTest < ActiveSupport::TestCase

    describe '.build' do
      it 'builds instance of class with all dependencies' do
        instance = described_class.build
        assert_instance_of described_class, instance
      end
    end

    let(:plan_1_items) { [ OpenStruct.new(seller_sku: 'ABC', quantity: '20'), OpenStruct.new(seller_sku: 'XYZ', quantity: "40") ] }
    let(:shipment_plan_1) { OpenStruct.new destination_fulfillment_center_id: 'MDW2', items: plan_1_items }

    let(:plan_2_items) { [ OpenStruct.new(seller_sku: 'ABC', quantity: '40'), OpenStruct.new(seller_sku: 'XYZ', quantity: '60')] }
    let(:shipment_plan_2) { OpenStruct.new destination_fulfillment_center_id: 'PDB5', items: plan_2_items }

    let(:shipment_plans) { [shipment_plan_1, shipment_plan_2] }

    let(:product) { create :product, seller_sku: 'ABC' }
    let(:warehouse) { create :fba_warehouse, name: 'MDW2'}
    let(:shipment) { create :amazon_shipment, fba_warehouse: warehouse }
    let(:shipment_entry) { build :shipment_entry, product: product, amazon_shipment: shipment, quantity: 3 }

    subject { described_class.build }

    it 'updates entries from received from Amazon shipment plans' do
      update_entry = stub
      update_entry.expects(:call).with(shipment_entry, quantity: 20).once.returns(success_result)
      action = described_class.new update_entry
      action.call [shipment_entry], shipment_plans
    end

    it 'returns success result' do
      result = subject.call [shipment_entry], shipment_plans
      assert result.success?
    end
  end
end
