require 'test_helper'

module ShipmentItems
  class FetchAllocationsTest < ActiveSupport::TestCase
    describe '.build' do
      it 'builds an instance of described class with default dependencies' do
        instance = described_class.build
        assert_kind_of described_class, instance
      end
    end

    describe '#call' do
      let(:product) { build :product }
      let(:shipment_item) { build :shipment_item, product: product }

      let(:fetched_allocations) { build_list :fba_allocation, 2  }
      let(:retrieve_allocations_result) { stub(success?: true, entity: fetched_allocations) }
      let(:retrieve_allocations_action) { stub(call: retrieve_allocations_result) }
      subject { described_class.new retrieve_allocations_action }

      describe 'when shipment item is nil' do
        it 'provides unsuccessful result' do
          result = subject.call nil
          refute result.success?
        end
      end

      describe 'when allocations for these product, quantity and shipment type are present' do
        let(:stored_allocations) { create_list :fba_allocation, 2, product: product }
        before do
          FbaAllocation.expects(:shipment_item_allocations).with(shipment_item)
                       .returns(stored_allocations)
        end

        it 'fetches allocation from database' do
          result = subject.call shipment_item
          assert_equal stored_allocations, result.entity
        end

        it 'is successful' do
          result = subject.call shipment_item
          assert result.success?
        end
      end

      describe 'when allocations for these product & quantity are not in database' do
        before do
          FbaAllocation.stubs(:shipment_item_allocations).returns([])
        end

        it 'fetches them from amazon' do
          retrieve_allocations_action.expects(:call).with(shipment_item).once
                                  .returns(retrieve_allocations_result)
          result = subject.call shipment_item
          assert_equal fetched_allocations, result.entity
        end

        it 'is successful' do
          result = subject.call shipment_item
          assert result.success?
        end
      end
    end
  end
end
