require 'test_helper'

module ShipmentItems
  class RetrieveAllocationsTest < ActiveSupport::TestCase

    let(:ship_from_address) { stub }

    describe '.build' do
      it 'builds an instance with proper dependencies' do
        instance = described_class.build ship_from_address
        assert_kind_of described_class, instance
      end
    end

    def build_shipment_entity(warehouse, quantities = [])
      items = quantities.map { |q| OpenStruct.new quantity: q }
      stub(destination_fulfillment_center_id: warehouse.id, items: items)
    end

    describe '#call' do
      let(:product) { create :product }
      let(:shipment_item) { build :shipment_item, product: product }
      let(:warehouse_1) { create :fba_warehouse }
      let(:warehouse_2) { create :fba_warehouse }

      let(:shipment_entities) do
        [build_shipment_entity(warehouse_1, [1, 2, 3]),
         build_shipment_entity(warehouse_2, [10, 20, 30])]
      end

      let(:failure_result) { stub(success?: false) }
      let(:success_result) { stub(success?: true, errors: []) }
      let(:shipment_result) { stub(success?: true, entities: shipment_entities) }
      let(:shipment_processor) { stub(create_shipment_plan: shipment_result) }
      let(:allocation_results) do
        allocations = (1..3).map { |i| build :fba_allocation, quantity: i, quantity_to_allocate: i }
        allocations.map { |a| stub(success?: true, entity: a) }
      end
      let(:create_allocation_action) do
        action = mock
        action.stubs(:call).returns(*allocation_results)
        action
      end
      let(:delete_allocations_action) { stub(call: success_result) }
      subject { described_class.new(shipment_processor, create_allocation_action, delete_allocations_action) }

      describe 'when product is nil' do
        it 'returns failure result' do
          result = subject.call nil
          assert result.failure?
        end
      end

      describe 'when product is valid' do
        it 'creates shipment plan via shipment processor' do
          shipment_processor.expects(:create_shipment_plan).once.returns(shipment_result)
          subject.call shipment_item
        end

        describe 'when shipment plan successfully created' do
          it 'creates allocation for each item-warehouse pair' do
            result = subject.call shipment_item
            assert_equal 6, result.entity.count
          end

          it 'is successful if all allocations are successfully created' do
            result = subject.call shipment_item
            assert result.success?
          end
        end

        describe 'when shipment plan fails' do
          it 'produces failure result' do
            shipment_processor.stubs(create_shipment_plan: failure_result)
            result = subject.call shipment_item
            refute result.success?
          end
        end
      end
    end
  end
end


