# frozen_string_literal: true
require 'test_helper'

module SimpleActions
  class BaseTest < ActiveSupport::TestCase
    let(:klass) { MaterialCategory }
    let(:klass_name) { klass.name }
    describe 'constructor' do
      it 'creates simple action for any model' do
        action = described_class.new klass
        assert_equal klass, action.model
      end

      it 'accepts class name as string as well' do
        action = described_class.new klass_name
        assert_equal klass, action.model
      end
    end
  end
end
