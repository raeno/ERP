# frozen_string_literal: true
require 'test_helper'

module SimpleActions
  class CreateTest < ActiveSupport::TestCase
    let(:klass) { MaterialCategory }
    let(:klass_name) { klass.name }
    let(:params) { { name: 'name2' } }

    describe '#call' do
      let(:subject) { described_class.new klass }
      describe 'when successful' do
        it 'persists an instance of that model in db' do
          assert_difference "#{klass_name}.count", +1 do
            subject.call params
          end
        end

        it 'provides successful result' do
          result = subject.call params
          assert result.success?
        end

        it 'contains created instance as part of result' do
          result = subject.call params
          refute_nil result.entity
          assert_instance_of klass, result.entity
        end
      end

      describe 'when unsuccessful' do
        before { klass.any_instance.stubs(:save).returns(false) }

        it "does not persist new instance" do
          assert_no_difference "#{klass_name}.count" do
            subject.call params
          end
        end

        it 'provides unsuccessful result' do
          result = subject.call params
          refute result.success?
        end

        it 'stores problems during creation in result errors' do
          messages = ["error 1", "error 2"]
          errors = stub(full_messages: messages)
          klass.any_instance.stubs(:errors).returns(errors)

          result = subject.call params
          assert_equal messages, result.errors
        end
      end
    end
  end
end
