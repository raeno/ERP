# frozen_string_literal: true
require 'test_helper'

module SimpleActions
  class DestroyTest < ActiveSupport::TestCase
    let(:klass) { MaterialCategory }
    let(:klass_name) { klass.name }
    subject { described_class.new klass }

    before do
      @instance = klass.create(name: 'old name')
    end

    describe '#call' do
      describe 'when successful' do
        it 'deletes an instance of that model from db' do
          assert_difference "#{klass_name}.count", -1 do
            subject.call @instance
          end
        end

        it 'provides successful result' do
          result = subject.call @instance
          assert result.success?
        end
      end
    end

    describe 'when unsuccessful' do
      before { @instance.stubs(:destroy).returns(false) }

      it "does not change amount in database" do
        assert_no_difference "#{klass_name}.count" do
          subject.call @instance
        end
      end

      it 'provides unsuccessful result' do
        result = subject.call @instance
        assert result.failure?
      end
    end
  end
end
