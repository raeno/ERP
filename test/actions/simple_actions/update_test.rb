# frozen_string_literal: true
require 'test_helper'

module SimpleActions
  class UpdateTest < ActiveSupport::TestCase
    let(:klass) { MaterialCategory }
    let(:klass_name) { klass.name }
    let(:instance) { klass.create(name: 'old name') }
    let(:new_params) { { name: 'new name' } }

    describe '#call' do
      let(:subject) { described_class.new klass }

      describe 'when successful' do
        it 'updates an instance of that model in db' do
          subject.call instance, new_params
          assert_equal 'new name', instance.reload.name
        end

        it 'provides successful result' do
          result = subject.call instance, new_params
          assert result.success?
        end

        it 'contains updated instance as part of result' do
          result = subject.call instance, new_params
          refute_nil result.entity
          assert_instance_of klass, result.entity
          assert_equal 'new name', result.entity.name
        end
      end

      describe 'when unsuccessful' do
        before { instance.stubs(:save).returns(false) }

        it "does not persist changed instance" do
          subject.call instance, new_params
          assert_equal 'old name', instance.reload.name
        end

        it 'provides unsuccessful result' do
          result = subject.call instance, new_params
          refute result.success?
        end
      end
    end
  end
end
