# frozen_string_literal: true

require 'test_helper'

module Tasks
  class CancelTaskTest < ActiveSupport::TestCase
    let(:subject) { described_class.build }
    let(:task) { create :task, started_at: 1.minute.ago }

    describe '#call' do
      describe 'when success' do
        it 'resets the start time' do
          assert task.started_at
          result = subject.call task
          assert result.success?
          refute task.started_at
          refute task.started?
        end
      end

      describe 'when failure' do
        it 'does not reset the start time' do
          task.stubs(:valid?).returns(false)
          result = subject.call task
          assert result.failure?
          assert task.reload.started_at
        end
      end
    end
  end
end
