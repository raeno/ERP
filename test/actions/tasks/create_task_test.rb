# frozen_string_literal: true

require 'test_helper'

module Tasks
  class CreateTaskTest < ActiveSupport::TestCase
    subject { described_class.build }

    let(:product) { create :product, items_per_case: 111 }
    let(:inventory_item) { create :inventory_item, :with_production_report, inventoriable: product }
    let(:task) { build :task, inventory_item: inventory_item }

    describe '#call' do
      let(:task_form) { TaskForm.new(task) }

      describe 'when success' do
        let(:params) { { quantity_in_zone_units: 555, user_id: 1 } }

        it 'creates a task, sets fields, returns success' do
          stub_duration_estimate(inventory_item, 555, 12_345)

          assert_difference -> { Task.count }, 1 do
            result = subject.call task_form, params
            assert result.success?

            task = result.entity
            assert_equal 111, task.product_items_per_case
            assert_equal 12_345, task.duration_estimate
          end
        end

        it 'calls #call_on_task' do
          subject.expects(:call_on_task).once.returns(nil)
          subject.call task_form, params
        end
      end

      describe 'when form is invalid' do
        # invalid params: missing user_id
        let(:params) { { quantity_in_zone_units: 555, user_id: nil } }

        it 'returns error' do
          assert_no_difference -> { Task.count } do
            result = subject.call task_form, params
            assert result.failure?
          end
        end
      end
    end

    describe '#call_on_task' do
      before { task.quantity_in_zone_units = 555 }

      describe 'when success' do
        it 'creates a task, sets fields, returns success' do
          stub_duration_estimate(inventory_item, 555, 12_345)

          assert_difference -> { Task.count }, 1 do
            result = subject.call_on_task task
            assert result.success?

            task = result.entity
            assert_equal 111, task.product_items_per_case
            assert_equal 12_345, task.duration_estimate
          end
        end

        it 'calls refresh_report action for the related inventory_item' do
          success_result = stub(success?: true)
          refresh_report_action = stub
          subject = described_class.new(refresh_report: refresh_report_action)

          refresh_report_action.expects(:call).with(inventory_item).once.returns(success_result)
          subject.call_on_task task
        end
      end

      describe 'when the task is invalid' do
        before { task.user_id = nil }

        it 'returns error' do
          assert_no_difference -> { Task.count } do
            result = subject.call_on_task task
            assert result.failure?
            assert result.errors.include?("User can't be blank")
          end
        end
      end
    end
  end
end
