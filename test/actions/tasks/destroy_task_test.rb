# frozen_string_literal: true
require 'test_helper'

module Tasks
  class DestroyTaskTest < ActiveSupport::TestCase
    let(:subject) { described_class.build }
    let(:task) { create :task }
    before { task }

    describe '#call' do
      it 'destroys the task' do
        assert_difference -> { Task.count }, -1 do
          result = subject.call task
          assert result.success?
        end
      end

      it 'calls refresh_report action for the related inventory_item' do
        success_result = stub(success?: true)
        refresh_report_action = stub
        subject = described_class.new(refresh_report: refresh_report_action)

        refresh_report_action.expects(:call).with(task.inventory_item).once.returns(success_result)
        subject.call task
      end
    end
  end
end
