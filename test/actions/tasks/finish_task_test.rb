# frozen_string_literal: true
require 'test_helper'

module Tasks
  class FinishTaskTest < ActiveSupport::TestCase
    let(:subject) { described_class.build }
    let(:task) { create :task }

    describe '#call' do
      describe 'when success' do
        it 'updates the task with the finish time' do
          result = subject.call task
          assert result.success?
          assert (task.finished_at - Time.zone.now).abs < 5
        end

        it 'sets labour cost' do
          task.expects(:set_labour_cost).once
          result = subject.call task
          assert result.success?
        end

        it 'calls refresh_report action for the related inventory_item' do
          success_result = stub(success?: true)
          create_task_action = stub(call_on_task: success_result)
          refresh_report_action = stub
          subject = described_class.new(create_task: create_task_action, refresh_report: refresh_report_action)

          refresh_report_action.expects(:call).with(task.inventory_item).once.returns(success_result)
          subject.call task
        end

        describe 'when produced product amount is less than aimed quantity' do
          let(:task) { create :task, quantity: 999 }
          let(:batch) { create :batch, task: task, quantity: 777, product_id: task.product.id }
          before { batch }

          it 'issues another task for the remainder quantity' do
            assert_difference -> { Task.count }, 1 do
              subject.call task
            end

            remainder_task = Task.order(:id).last
            assert_equal 222, remainder_task.quantity
            assert_equal task.date, remainder_task.date
            assert_equal task.inventory_item_id, remainder_task.inventory_item_id
          end

          describe 'when cannot ussue the remainder task' do
            it 'does not make any changes' do
              failure_result = stub(success?: false)
              Tasks::CreateTask.any_instance.expects('call_on_task').returns(failure_result)
              result = subject.call task
              refute result.success?
              refute task.reload.finished?
            end
          end
        end
      end

      describe 'when failure' do
        it 'does not set the finish time' do
          task.stubs(:valid?).returns(false)
          result = subject.call task
          assert result.failure?
          refute task.reload.finished_at
        end
      end

      describe 'when nil is passed' do
        it 'returns failure' do
          result = subject.call nil
          assert result.failure?
        end
      end
    end
  end
end
