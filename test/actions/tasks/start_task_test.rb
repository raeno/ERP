# frozen_string_literal: true
require 'test_helper'

module Tasks
  class StartTaskTest < ActiveSupport::TestCase
    let(:subject) { described_class.build }
    let(:user) { create :user }
    let(:task) { create :task }

    describe '#call' do
      describe 'when success' do
        it 'starts the task, sets fields' do
          result = subject.call task, user
          assert result.success?
          assert_equal user, task.reload.user
          assert (task.started_at - Time.zone.now).abs < 5
        end
      end

      describe 'when failure' do
        it 'returns error' do
          task.stubs(:valid?).returns(false)
          result = subject.call task, user
          assert result.failure?
          refute_equal user, task.reload.user
          refute task.started_at
        end
      end
    end
  end
end
