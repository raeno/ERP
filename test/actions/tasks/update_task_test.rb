# frozen_string_literal: true

require 'test_helper'

module Tasks
  class UpdateTaskTest < ActiveSupport::TestCase
    subject { described_class.build }

    let(:inventory_item) { create :inventory_item, :with_production_report }
    let(:task) { build :task, inventory_item: inventory_item }
    let(:task_form) { TaskForm.new(task) }

    describe 'when success' do
      let(:params) { { quantity_in_zone_units: 555, user_id: 1 } }

      it 'updates the task, sets fields, returns success' do
        stub_duration_estimate(inventory_item, 555, 12_345)

        result = subject.call task_form, params
        assert result.success?

        task = result.entity
        assert_equal 12_345, task.duration_estimate
      end

      it 'calls refresh_report action for the related inventory_item' do
        success_result = stub(success?: true)
        refresh_report_action = stub
        subject = described_class.new(refresh_report: refresh_report_action)

        refresh_report_action.expects(:call).with(inventory_item).once.returns(success_result)
        subject.call task_form, params
      end
    end

    describe 'when form is invalid' do
      # invalid params: missing user_id
      let(:params) { { quantity_in_zone_units: 555, user_id: nil } }

      it 'returns error' do
        result = subject.call task_form, params
        assert result.failure?
      end
    end
  end
end
