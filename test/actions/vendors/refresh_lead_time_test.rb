# frozen_string_literal: true
require 'test_helper'
module Vendors
  class RefreshLeadTimeTest < ActiveSupport::TestCase
    let(:subject)  { described_class.new }
    let(:vendor)  { create :vendor }

    describe '#call' do
      it 'sets lead_time to the average based on the latest 5 invoices received' do
        refute vendor.lead_time_days

        # Initializing invoices:
        # latest invoice - not received, should not throw error
        # then 5 invoices - received, with lead times 1 day, 2 days, 3, 4, 5 days.
        # then an old invoice with a long lead time that should not be included in the average.

        invoices = []
        7.times do |i|
          invoices << create(:invoice, vendor: vendor, date: (i * 2).days.ago, received_date: i.days.ago)
        end

        latest_invoice = invoices.first
        latest_invoice.update_column(:received_date, nil)
        refute latest_invoice.received?

        old_invoice = invoices.last
        old_invoice.update_column(:date, 100.days.ago)
        assert old_invoice.lead_time_days > 50

        # Checking the average set correctly to 3 days - (1+2+3+4+5)/5
        subject.call vendor
        assert_equal 3, vendor.lead_time_days
      end

      describe 'when the vendor has no invoices' do
        before { assert vendor.invoices.empty? }

        it 'does nothing, returns success' do
          result = subject.call vendor
          assert result.success?
        end
      end
    end
  end
end
