# frozen_string_literal: true
require 'test_helper'

class Admin::SettingsControllerTest < ActionController::TestCase
  before { quick_login_admin }
  before { Setting.create! }
  let(:setting) { Setting.first }

  describe 'GET #show' do
    it 'is successful' do
      get :show
      assert_response 200
    end
  end

  describe 'GET #edit_settings for address' do
    it 'is successful' do
      get :edit, params: { address: true }
      assert_response 200
    end
  end

  describe 'GET #edit_settings for amazon credentials' do
    it 'is successful' do
      get :edit, params: { amazon_credentials: true }
      assert_response 200
    end
  end

  describe 'GET #edit_settings for urgency levels' do
    it 'is successful' do
      get :edit, params: { urgency_levels: true }
      assert_response 200
    end
  end

  describe 'PATCH #edit_settings' do
    describe 'when params are ok' do
      let(:params) { { setting: { address_zip_code: "123456", mws_merchant_id: "MERCH1" } } }

      it 'redirects to dashboard' do
        patch :update, params: params
        assert_redirected_to admin_settings_path
      end

      it 'updates settings' do
        patch :update, params: params
        assert_equal "123456", setting.reload.address_zip_code
        assert_equal "MERCH1", setting.reload.mws_merchant_id
      end
    end

    describe 'when params are invalid' do
      let(:params) { { setting: { red_urgency_level_days: -10 } } }

      it 'renders the related edit template' do
        patch :update, params: params.merge(urgency_levels: true)
        assert_template "edit_urgency_levels"
      end
    end
  end
end
