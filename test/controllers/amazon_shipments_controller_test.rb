# frozen_string_literal: true
require 'test_helper'

class AmazonShipmentsControllerTest < ActionController::TestCase
  before { quick_login_user }

  let(:params) { { amazon_shipment: { name: 'New Shipment' } } }
  def stub_controller_action(action_name, action)
    @controller.stubs(:actions).returns(action_name => action)
  end

  describe 'GET #index' do
    it 'responds with success' do
      get :index
      assert_response 200
    end
  end

  describe 'POST #create' do
    describe 'when success' do
      it 'calls :create_shipment action' do
        result = stub(success?: true, job_id: 1, shipment_name: 'ShipmentName')
        action = mock
        action.expects(:call).returns(result)
        stub_controller_action :create_shipment, action
        post :create, params: params.merge(format: :json)
        assert_response 200
      end
    end

    describe 'when failure' do
      it 'responds with error in json' do
        expect_failure_once AmazonShipments::ScheduleShipment, error_message: 'failed to ship'
        post :create, params: params.merge(format: :json)
        body = JSON.parse response.body
        assert_equal 'failed to ship', body["error"]
      end
    end
  end
end
