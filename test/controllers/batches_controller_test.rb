# frozen_string_literal: true
require 'test_helper'

class BatchesControllerTest < ActionController::TestCase
  before { quick_login_user }

  def stub_controller_action(action_name, action)
    @controller.stubs(:actions).returns(action_name => action)
  end

  describe 'GET #index' do
    it 'renders index' do
      get :index
      assert_template :index
    end

    it 'is successful when no batches' do
      get :index
      assert_response 200
    end

    it 'is successful when there are batches' do
      create :batch
      create :batch, completed_on: Time.zone.today, notes: "abc"
      get :index
      assert_response 200
    end
  end

  describe 'GET #new'  do
    it 'is successful' do
      get :new
      assert_response 200
    end

    it 'renders "new"' do
      get :new
      assert_template :new
    end
  end

  describe 'GET #show' do
    let(:batch) { create :batch }

    it 'is successful' do
      get :show, params: { id: batch.id }
      assert_response 200
    end

    it 'renders show' do
      get :show, params: { id: batch.id }
      assert_template :show
    end
  end

  describe 'GET #chart' do
    it 'is successful' do
      get :chart, params: { date_range: 30, format: :json }
      assert_response 200
    end

    it 'supports only json requests' do
      assert_raise ActionController::UnknownFormat do
        get :chart, params: { date_range: 30 }
      end
    end

    it 'provides chart data in json format' do
      chart_data = { days: [1, 2, 3].to_s, chart_data: "some_data" }
      chart_presenter = stub(chart_data: chart_data)
      BatchChartPresenter.stubs(:new).returns(chart_presenter)
      get :chart, params: { date_range: 30, format: :json }
      chart_json = JSON.parse response.body
      expected = { "days" => "[1, 2, 3]", "chart_data" => "some_data" }
      assert_equal expected, chart_json
    end
  end

  describe 'GET #edit' do
    let(:batch) { create :batch }

    it 'renders "new" with proper batch' do
      get :edit, params: { id: batch.id }
      assert_template :edit
    end

    it 'is successful' do
      get :edit, params: { id: batch.id }
      assert_response 200
    end
  end

  describe 'POST #create' do
    let(:new_batch) { build :batch }
    let(:params) { { batch: { 'quantity' => '10', 'user_ids' => [@user.id.to_s] } } }

    describe 'when success' do
      it 'calls create action and redirects to batches index' do
        expect_success_once(Batches::CreateBatch, with: [params[:batch]], result_entity: new_batch)
        post :create, params: params
        assert_redirected_to batches_path
      end
    end

    describe 'when failure' do
      it 'renders form again' do
        expect_failure_once(Batches::CreateBatch, with: [params[:batch]], result_entity: new_batch)
        post :create, params: params
        assert_template :new
      end
    end
  end

  describe 'PUT #update' do
    let(:batch) { create :batch }
    let(:params) { { id: batch.id, batch: { 'quantity' => '10' } } }

    describe "when success" do
      it 'calls update action and redirects to batches index' do
        expect_success_once(Batches::ChangeBatch, with: [batch, params[:batch]], result_entity: batch)
        put :update, params: params
        assert_redirected_to batch
      end
    end

    describe 'when failure' do
      it 'renders "edit" page again' do
        expect_failure_once(Batches::ChangeBatch, with: [batch, params[:batch]], result_entity: batch)
        put :update, params: params
        assert_template :edit
      end
    end
  end

  describe 'DELETE #destroy' do
    let(:batch) { create :batch }
    let(:success_result) { stub(success?: true) }

    describe 'when success' do
      it 'calls destroy action and redirects to batches index' do
        expect_success_once(Batches::DestroyBatch, with: [batch], result_entity: batch)
        delete :destroy, params: { id: batch.id }
        assert_redirected_to batches_path
      end
    end

    describe 'when failure' do
      it 'redirects to batches index' do
        expect_failure_once(Batches::DestroyBatch, with: [batch], result_entity: batch)
        delete :destroy, params: { id: batch.id }
        assert_redirected_to batches_path
      end
    end
  end
end
