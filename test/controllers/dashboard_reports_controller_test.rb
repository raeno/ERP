# frozen_string_literal: true
require 'test_helper'

class DashboardReportsControllerTest < ActionController::TestCase
  let(:product) { create :product }
  before do
    create :zone, :ship
    product
    quick_login_user
  end

  describe 'GET #inventory_health' do
    it 'is successful and renders inventory health page' do
      get :inventory_health
      assert_response :success
      assert_template :inventory_health
    end
  end

  describe 'GET #profit_and_loss' do
    it 'is successful and renders profit_and_loss page' do
      get :profit_and_loss
      assert_response :success
      assert_template :profit_and_loss
    end
  end
end
