# frozen_string_literal: true
require 'test_helper'

class FbaAllocationsControllerTest < ActionController::TestCase
  before { quick_login_user }
  let(:product) { create :product }

  describe 'GET #index' do
    let(:quantity) { 100 }
    let(:shipment_type) { 'cases' }

    it 'retrieves allocations for given product, quantity and shipment type' do
      allocations_fetcher = mock
      result = stub(success?: true, entity: [])
      allocations_fetcher.expects(:call)
                         .once
                         .returns(result)
      @controller.stubs(:actions).returns(fetch_product_allocations: allocations_fetcher)
      get :index, params: { product_id: product.id, shipment_type: 'cases', quantity: quantity }
    end

    describe 'when successfully retrieved allocations' do
      let(:allocation) { build(:fba_allocation) }
      let(:allocations) { [allocation] }
      let(:result) { stub(success?: true, entity: allocations) }
      let(:fetcher) { stub(call: result) }

      before do
        @controller.stubs(:actions).returns(fetch_product_allocations: fetcher)
      end

      it 'renders them as json' do
        get :index, params: { product_id: product.id, shipment_type: :cases }
        result = JSON.parse response.body
        expected_result = { 'warehouse_id' => allocation.fba_warehouse.id,
                            'warehouse_name' => allocation.fba_warehouse.name,
                            'quantity' => 500,
                            'inventory_enough' => false
                          }
        assert_equal [expected_result], result
      end

      it 'is successful' do
        get :index, params: { product_id: product.id, shipment_type: :cases }
        assert_response 200
      end
    end

    describe 'when failed to retrieve allocations' do
      let(:result) { stub(success?: false, error_message: "Failed to get allocations") }
      let(:fetcher) { stub(call: result) }
      before do
        @controller.stubs(:actions).returns(fetch_product_allocations: fetcher)
      end

      it 'responds with 500' do
        get :index, params: { product_id: product.id, shipment_type: 'cases', quantity: quantity }
        assert_response 500
      end

      it 'provides error message in response body' do
        get :index, params: { product_id: product.id, shipment_type: 'cases', quantity: quantity }
        result = JSON.parse response.body
        assert_equal 'Failed to get allocations', result['error']
        assert_equal 'error', result['status']
      end
    end
  end
end
