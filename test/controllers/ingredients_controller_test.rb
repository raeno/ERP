# frozen_string_literal: true
require 'test_helper'

class IngredientsControllerTest < ActionController::TestCase
  before { quick_login_user }
  let(:product) { create :product }
  let(:inventory_item) { create :inventory_item }

  describe 'GET #edit' do
    it 'renders "edit"' do
      get :edit, params: { id: product.id }
      assert_template :edit
    end

    it 'is successful' do
      get :edit, params: { id: product.id }
      assert_response 200
    end
  end

  describe 'POST #update in json' do
    let(:params) do
      {
        :format => :json,
        :id => product.id,
        "ingredients" => {
          "0" => { "inventory_item_id" => inventory_item.to_param, "quantity_in_small_units" => "5" }
        }
      }
    end

    it 'responds :ok if successful' do
      post :update, params: params
      assert_response 200
    end

    it 'responds :unprocessable_entity if bad params' do
      params["ingredients"]["0"].delete("inventory_item_id")
      post :update, params: params
      assert_response 422
    end
  end
end
