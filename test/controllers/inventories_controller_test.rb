# frozen_string_literal: true
require 'test_helper'

class InventoriesControllerTest < ActionController::TestCase
  let(:error_message) { 'Failed to create' }
  let(:success_result) { stub(success?: true) }
  let(:failure_result) { stub(success?: false, error_message: error_message) }
  let(:params) { { inventory: { quantity: 100 }, format: :json } }

  before { quick_login_user }
  describe 'POST #create' do
    it 'calls an action to create inventory' do
      action = mock
      action.expects(:call).returns(success_result)
      @controller.stubs(:actions).returns(create: action)

      post :create, params: params
    end

    describe 'when format is not json' do
      let(:params) { { inventory: { quantity: 10 }, format: :html } }
      it 'responds with unknown format error' do
        action = stub(call: success_result)
        @controller.stubs(:actions).returns(create: action)
        assert_raise 'ActionController::UnknownFormat' do
          post :create, params: params
        end
      end
    end

    describe 'when format is json' do
      describe 'when result is successful' do
        before do
          action = stub(call: success_result)
          @controller.stubs(:actions).returns(create: action)
        end

        it 'responds with :no_content' do
          post :create, params: params
          assert_response :no_content
        end
      end

      describe 'when result is failure' do
        before do
          action = stub(call: failure_result)
          @controller.stubs(:actions).returns(create: action)
        end

        it 'responds with :unprocessable_entity' do
          post :create, params: params
          assert_response :unprocessable_entity
        end

        it 'contains result error message in json body' do
          post :create, params: params
          body = JSON.parse response.body
          assert_equal error_message, body["error"]
        end
      end
    end
  end

  describe 'PATCH #update' do
    let(:inventory) { create :inventory, quantity: 100 }
    let(:params) do
      { id: inventory.to_param,
        inventory: { quantity: 200 },
        format: :json }
    end

    describe 'when format is json' do
      describe 'when result successful' do
        before do
          action = stub(call: success_result)
          @controller.stubs(:actions).returns(update: action)
        end

        it 'responds with :no_content' do
          patch :update, params: params
          assert_response :no_content
        end
      end

      describe 'when result is failure' do
        before do
          action = stub(call: failure_result)
          @controller.stubs(:actions).returns(update: action)
        end

        it 'responds with unprocessable entity' do
          patch :update, params: params
          assert_response :unprocessable_entity
        end

        it 'contains error message in response body' do
          patch :update, params: params
          body = JSON.parse response.body
          assert_equal error_message, body["error"]
        end
      end
    end
  end
end
