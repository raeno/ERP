# frozen_string_literal: true
require 'test_helper'

class InventoryItemsControllerTest < ActionController::TestCase
  before { quick_login_user }

  let(:failure_result) { stub(entity: InventoryItem.new, success?: false, error_message: 'Update error') }
  let(:success_result) { stub(entity: InventoryItem.new, success?: true) }

  describe 'PATCH #update' do
    let(:inventory_item) { create :inventory_item }

    let(:params) do
      inventory_item_params = { zone_id: 1, inventoriable_id: 1, inventoriable_type: 'Material' }
      { id: inventory_item.to_param, inventory_item: inventory_item_params }
    end

    describe 'when inventory_item successfully updated' do
      before do
        action = stub(call: success_result)
        @controller.stubs(:actions).returns(update_inventory_item: action)
      end

      describe 'when json/ajax request' do
        it 'responds with :no_content ' do
          patch :update, params: params.merge(format: :json)
          assert_response :no_content
        end
      end
    end

    describe 'when failed to update inventory_item' do
      before do
        action = stub(call: failure_result)
        @controller.stubs(:actions).returns(update_inventory_item: action)
      end

      describe 'when json/ajax request' do
        it 'responds with :unprocessable_entity' do
          patch :update, params: params.merge(format: :json)
          assert_response :unprocessable_entity
        end

        it 'contains error in response' do
          patch :update, params: params.merge(format: :json)
          body = JSON.parse response.body
          assert_equal "Update error", body["error"]
        end
      end
    end
  end
end
