# frozen_string_literal: true
require 'test_helper'

class InvoiceItemsControllerTest < ActionController::TestCase
  let(:invoice) { create(:invoice) }

  before do
    quick_login_user
  end

  describe 'GET #new' do
    it 'is successful' do
      get :new, params: { invoice_id: invoice }
      assert_response :success
    end
  end

  describe 'GET #edit' do
    let(:invoice_item) { create :invoice_item, invoice: invoice }

    it 'is successful' do
      get :edit, params: { invoice_id: invoice, id: invoice_item }
      assert_response :success
    end
  end

  describe 'POST #create' do
    let(:invoice_item) { create :invoice_item, invoice: invoice }
    let(:params) { { invoice_id: invoice, invoice_item: { 'material_container' => { 'lot_number' => 'ABC123' } } } }

    describe 'when success' do
      it 'calls :create_item action, redirects to invoice page' do
        expect_success_once(InvoiceItems::CreateInvoiceItem, with: params[:invoice_item], result_entity: invoice_item)
        post :create, params: params
        assert_redirected_to invoice_path(invoice)
      end
    end

    describe 'when fail' do
      it 'calls :create_item action, renders :new again' do
        expect_failure_once(InvoiceItems::CreateInvoiceItem,
          with: [params[:invoice_item]], result_entity: invoice_item)
        post :create, params: params
        assert_template :new
      end
    end
  end

  describe 'PATCH #update' do
    let(:invoice_item) { create :invoice_item, invoice: invoice }
    let(:params) do
      { invoice_id: invoice,
        id: invoice_item,
        invoice_item: { 'material_container' => { 'lot_number' => 'ABC123' } } }
    end

    describe 'when success' do
      it 'calls :update_item action, redirects to invoice page' do
        expect_success_once(InvoiceItems::UpdateInvoiceItem,
          with: [invoice_item, params[:invoice_item]], result_entity: invoice_item)
        patch :update, params: params
        assert_redirected_to invoice_path(invoice)
      end
    end

    describe 'when fail' do
      it 'calls :update_item action, renders :new again' do
        expect_failure_once(InvoiceItems::UpdateInvoiceItem,
          with: [invoice_item, params[:invoice_item]],
          result_entity: invoice_item)
        patch :update, params: params
        assert_template :edit
      end
    end

    describe 'when the invoice item is locked' do
      before do
        invoice_item.become_received
        invoice_item.save!
        assert invoice_item.locked?
      end

      let(:params) do
        { invoice_id: invoice, id: invoice_item, invoice_item: { 'quantity' => '3' } }
      end

      it 'filters the params - disallows changing quantities and prices' do
        expect_success_once(
          InvoiceItems::UpdateInvoiceItem,
          with: [invoice_item, {}], result_entity: invoice_item
        )
        patch :update, params: params
        assert_redirected_to invoice_path(invoice)
      end
    end
  end

  describe 'PATCH #item_received, :json' do
    let(:invoice_item) { create :invoice_item, invoice: invoice }

    describe 'when success' do
      it 'calls :received action, responds with success' do
        expect_success_once(InvoiceItems::InvoiceItemReceived, with: [invoice_item], result_entity: invoice)
        patch :item_received, params: { invoice_id: invoice, id: invoice_item, format: :json }
        assert_response :success
      end
    end

    describe 'when fail' do
      it 'responds with failure' do
        expect_failure_once(InvoiceItems::InvoiceItemReceived,
          with: [invoice_item], result_entity: invoice)
        patch :item_received, params: { invoice_id: invoice, id: invoice_item, format: :json }
        assert_response :unprocessable_entity
      end
    end
  end

  describe 'DELETE #destroy' do
    let(:invoice_item) { create :invoice_item, invoice: invoice }

    describe 'when success' do
      it 'calls :destroy_item action, redirects to invoice page' do
        expect_success_once(InvoiceItems::DestroyInvoiceItem, with: [invoice_item], result_entity: invoice_item)
        delete :destroy, params: { id: invoice_item, invoice_id: invoice }
        assert_redirected_to invoice_path(invoice)
        assert_equal 'Line item was deleted', flash[:notice]
      end
    end

    describe 'when fail' do
      it 'calls :destroy_item action, redirects to invoice, displays error' do
        expect_failure_once(InvoiceItems::DestroyInvoiceItem,
          with: [invoice_item], result_entity: invoice_item)
        delete :destroy, params: { id: invoice_item, invoice_id: invoice }
        assert_redirected_to invoice_path(invoice)
        assert_match(/Failed to destroy line item/, flash[:error])
      end
    end
  end
end
