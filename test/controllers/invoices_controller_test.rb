# frozen_string_literal: true
require 'test_helper'

class InvoicesControllerTest < ActionController::TestCase
  before do
    quick_login_user
  end

  describe 'GET #index' do
    it 'is successful' do
      create :invoice
      get :index
      assert_response :success
    end
  end

  describe 'GET #chart' do
    it 'is successful' do
      get :chart, format: :json
      assert_response :success
    end
  end

  describe 'GET #vendors_chart' do
    it 'is successful' do
      get :vendors_chart, format: :json
      assert_response :success
    end
  end

  describe 'GET #show' do
    it 'is successful' do
      invoice = create :invoice
      create :invoice_item, invoice: invoice
      get :show, params: { id: invoice.id }
      assert_response :success
    end
  end

  describe 'GET #new' do
    it 'is successful' do
      get :new
      assert_response :success
    end
  end

  describe 'GET #edit' do
    let(:invoice) { create :invoice }
    it 'is successful' do
      get :edit, params: { id: invoice }
      assert_response :success
    end
  end

  describe 'POST #create' do
    let(:invoice) { create :invoice }
    let(:params) { { invoice: { 'number' => '999' } } }

    describe 'when success' do
      it 'calls :create_invoice action, redirects to invoice page' do
        expect_success_once(SimpleActions::Create, with: [params[:invoice]], result_entity: invoice)
        post :create, params: params
        assert_redirected_to invoice_path(invoice)
      end
    end

    describe 'when fail' do
      it 'calls :create_invoice action, renders :new again' do
        expect_failure_once(SimpleActions::Create, with: [params[:invoice]], result_entity: invoice)
        post :create, params: params
        assert_template :new
      end
    end
  end

  describe 'POST #generate' do
    let(:invoice) { create :invoice }
    let(:params) { { invoice: { 'number' => '999' }, format: :json } }

    describe 'when success' do
      it 'calls :generate_invoice action, responds with :ok' do
        expect_success_once(Invoices::GenerateInvoice, with: [params[:invoice]], result_entity: invoice)
        post :generate, params: params
        assert_response :ok

        response_json = JSON.parse response.body
        assert_equal invoice_path(invoice), response_json["path"]
      end
    end

    describe 'when fail' do
      it 'calls :generate_invoice action, responds with error' do
        expect_failure_once(Invoices::GenerateInvoice, with: [params[:invoice]], result_entity: invoice)
        post :generate, params: params
        assert_response :unprocessable_entity
      end
    end
  end

  describe 'PATCH #update' do
    let(:invoice) { create :invoice }
    let(:params) do
      { id: invoice.id, invoice: { 'number' => '999', 'total_price' => '10.01' } }
    end

    describe 'when success' do
      it 'calls :update_invoice action, redirects to invoice page' do
        expect_success_once(Invoices::UpdateInvoice, with: [invoice, params[:invoice]], result_entity: invoice)
        patch :update, params: params
        assert_redirected_to invoice_path(invoice)
      end
    end

    describe 'when fail' do
      it 'calls :update_invoice action, renders form again' do
        expect_failure_once(Invoices::UpdateInvoice,
          with: [invoice, params[:invoice]], result_entity: invoice)
        patch :update, params: params
        assert_template :edit
      end
    end

    describe 'when the invoice is locked (paid or received)' do
      before do
        invoice.become_received
        invoice.save!
        assert invoice.locked?
      end

      let(:params) do
        { id: invoice.id, invoice: { 'notes' => 'abc123', 'total_price' => '10.01' } }
      end

      it 'filters the params - disallows changing quantities and prices' do
        expect_success_once(Invoices::UpdateInvoice,
          with: [invoice, { 'notes' => 'abc123' }],
          result_entity: invoice)
        patch :update, params: params
        assert_redirected_to invoice_path(invoice)
      end
    end
  end

  describe 'PATCH #invoice_paid' do
    let(:invoice) { create :invoice }

    describe 'when success' do
      it 'calls :invoice_paid action, redirects to the invoice page' do
        expect_success_once(Invoices::InvoicePaid, with: invoice, result_entity: invoice)
        patch :invoice_paid, params: { id: invoice.id }
        assert_redirected_to invoice_path(invoice)
      end
    end

    describe 'when fail' do
      it 'redirects to the invoice page, displays an error message' do
        expect_failure_once(Invoices::InvoicePaid, with: invoice, result_entity: invoice)
        patch :invoice_paid, params: { id: invoice.id }
        assert_redirected_to invoice_path(invoice)
        assert_match(/Cannot mark invoice as paid/, flash[:error])
      end
    end
  end

  describe 'DELETE #destroy' do
    let(:invoice) { create :invoice }

    describe 'when success' do
      it 'calls :destroy_invoice action, redirects to index' do
        expect_success_once(Invoices::DestroyInvoice, with: [invoice], result_entity: invoice)
        delete :destroy, params: { id: invoice }
        assert_redirected_to invoices_path
        assert_equal 'Invoice was deleted', flash[:notice]
      end
    end

    describe 'when fail' do
      it 'calls :destroy_invoice action, redirects to index, displays error' do
        expect_failure_once(Invoices::DestroyInvoice, with: [invoice], result_entity: invoice)
        delete :destroy, params: { id: invoice }
        assert_redirected_to invoices_path
        assert_match(/Failed to destroy invoice/, flash[:error])
      end
    end
  end
end
