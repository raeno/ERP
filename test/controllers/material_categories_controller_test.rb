# frozen_string_literal: true
require 'test_helper'

class MaterialCategoriesControllerTest < ActionController::TestCase
  before { quick_login_user }
  let(:category) { create :material_category }

  describe 'GET #index' do
    describe 'witout params' do
      it 'is successful' do
        get :index
        assert_response 200
      end

      it 'renders index template' do
        get :index
        assert_template 'index'
      end
    end
  end

  describe 'GET #new' do
    it 'renders new' do
      get :new
      assert_template :new
    end

    it 'is successful' do
      get :new
      assert_response 200
    end
  end

  describe 'GET #edit' do
    it 'renders edit' do
      get :edit, params: { id: category.id }
      assert_template :edit
    end

    it 'is successful' do
      get :edit, params: { id: category.id }
      assert_response 200
    end
  end

  describe 'POST #create' do
    describe 'when category successfully created' do
      let(:params) do
        { material_category: { name: "Category name" } }
      end

      it 'redirects to index' do
        post :create, params: params
        assert_redirected_to material_categories_path
        category = MaterialCategory.order(:id).last
        assert_equal "Category name", category.name
      end
    end

    describe 'when failed to create category' do
      let(:params) { { material_category: { name: "" } } }

      it 'renders :new again' do
        post :create, params: params
        assert_template :new
      end
    end
  end

  describe 'PUT #update' do
    describe 'when successful' do
      let(:params) do
        { id: category.id,
          material_category: { name: "Oil" } }
      end

      it 'redirects to index' do
        put :update, params: params
        assert_redirected_to material_categories_path
      end

      it 'updates the category from params' do
        put :update, params: params
        category.reload
        assert_equal "Oil", category.name
      end
    end

    describe 'when failed' do
      let(:params) do
        { id: category.id,
          material_category: { name: "" } }
      end

      it 'renders edit again' do
        put :update, params: params
        assert_template :edit
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'redirects to inventory items index' do
      delete :destroy, params: { id: category.id }
      assert_redirected_to material_categories_path
    end

    it 'destroys the category' do
      category = create :material_category
      assert_difference -> { MaterialCategory.count }, -1 do
        delete :destroy, params: { id: category.id }
      end
    end
  end
end
