# frozen_string_literal: true
require 'test_helper'

module Materials
  class AttachmentsControllerTest < ActionController::TestCase
    before { quick_login_user }

    let(:material) { create :material }
    let(:attachment) { create :attachment, url: 'some/file/url' }
    let(:material_attachment) { create :material_attachment, material: material, attachment: attachment }

    let(:success_result) { stub(success?: true, entity: attachment) }
    let(:failure_result) { stub(success?: false, error_message: 'Failed to perform action') }

    let(:zone) { create :zone }
    before { find_or_create_material_inventory_item(material, production_zone: zone) }

    describe 'GET #index' do
      it 'renders material attachments' do
        get :index, params: { material_id: material.id }
        assert_response 200
      end
    end

    describe 'POST #create' do
      it 'calls an action to create new attachment for material' do
        action = stub
        action.expects(:call).once.returns(success_result)
        @controller.stubs(:actions).returns(create_attachment: action)
        post :create, params: { material_id: material.id, attachment: { url: 'new_url' }, format: :json }
      end

      describe 'when successful' do
        before do
          action = stub(call: success_result)
          @controller.stubs(:actions).returns(create_attachment: action)
        end

        it 'responds with preview_url in json body' do
          post :create, params: { material_id: material.id, attachment: { url: 'some_url' }, format: :json }
          body = JSON.parse response.body
          refute_nil body['preview_url']
        end

        it 'has 200 status code' do
          post :create, params: { material_id: material.id, attachment: { url: 'some_url' }, format: :json }
          assert_response 200
        end
      end

      describe 'when unsuccessful' do
        before do
          action = stub(call: failure_result)
          @controller.stubs(:actions).returns(create_attachment: action)
        end

        it 'responds with error message in json body' do
          post :create, params: { material_id: material.id, attachment: { url: '' }, format: :json }
          body = JSON.parse response.body
          refute_nil body['error']
        end

        it 'responds with 500 status code' do
          post :create, params: { material_id: material.id, attachment: { url: '' }, format: :json }
          assert_response 500
        end
      end
    end

    describe 'DELETE #destroy' do
      before do
        create :material_attachment, material: material, attachment: attachment
      end

      it 'calls an action to delete material attachment' do
        action = stub
        action.expects(:call).once.returns(success_result)
        @controller.stubs(:actions).returns(destroy_attachment: action)
        delete :destroy, params: { material_id: material.id, id: attachment.id, format: :json }
      end

      describe 'when successful' do
        before do
          action = stub(call: success_result)
          @controller.stubs(:actions).returns(destroy_attachment: action)
        end

        it 'responds with status in json body' do
          delete :destroy, params: { material_id: material.id, id: attachment.id, format: :json }
          body = JSON.parse response.body
          assert_equal 'success', body['status']
        end

        it 'has 200 status code' do
          delete :destroy, params: { material_id: material.id, id: attachment.id, format: :json }
          assert_response 200
        end
      end

      describe 'when unsuccessful' do
        before do
          action = stub(call: failure_result)
          @controller.stubs(:actions).returns(destroy_attachment: action)
        end

        it 'responds with error message in json body' do
          delete :destroy, params: { material_id: material.id, id: attachment.id, format: :json }
          body = JSON.parse response.body
          refute_nil body['error']
        end

        it 'responds with 500 status code' do
          delete :destroy, params: { material_id: material.id, id: attachment.id, format: :json }
          assert_response 500
        end
      end
    end
  end
end
