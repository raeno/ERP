# frozen_string_literal: true
require 'test_helper'

module Materials
  class ChartsControllerTest < ActionController::TestCase
    before { quick_login_user }

    let(:material) { create :material }
    let(:zone) { create :zone }
    before { find_or_create_material_inventory_item(material, production_zone: zone) }

    describe '#index' do
      it 'renders material graphs' do
        get :index, params: { material_id: material.id }
        assert_response 200
      end
    end

    describe '#cost_over_time' do
      before do
        @days = ["12/14/16", "04/25/17"]
        @series = [ { "name" => material.name, "data" => [1.25, 3.55] }]
        chart_data = DataStruct.new days: @days, series: @series
        MaterialCostChartPresenter.any_instance.stubs(chart_data: chart_data )
      end

      it 'responds with success' do
        get :cost_over_time, format: :json, material_id: material.id
        assert_response 200
      end

      it 'renders data for chart in json' do
        get :cost_over_time, format: :json, material_id: material.id

        body = JSON.parse response.body
        expected_body = { "days" => @days, "series" => @series}

        assert_equal(expected_body, body)
      end
    end
  end
end
