# frozen_string_literal: true
require 'test_helper'

module Materials
  class ForecastsControllerTest < ActionController::TestCase
    before { quick_login_user }

    let(:material) { create :material }
    let(:zone) { create :zone }
    before do
      find_or_create_material_inventory_item(
        material, production_zone: zone, with_report: true
      )
    end

    describe '#index' do
      it 'renders material forecasts' do
        get :index, params: { material_id: material.id }
        assert_response 200
      end
    end
  end
end
