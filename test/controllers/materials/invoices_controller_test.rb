# frozen_string_literal: true
require 'test_helper'

module Materials
  class InvoicesControllerTest < ActionController::TestCase
    before { quick_login_user }

    let(:material) { create :material }
    let(:zone) { create :zone }
    before { find_or_create_material_inventory_item(material, production_zone: zone) }

    describe '#index' do
      it 'renders material invoices' do
        get :index, params: { material_id: material.id }
        assert_response 200
      end
    end
  end
end
