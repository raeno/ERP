# frozen_string_literal: true
require 'test_helper'

module Materials
  class OrderingSizesControllerTest < ActionController::TestCase
    before { quick_login_user }
    let(:material) { create :material }
    let(:can)    { create :ordering_size, material: material, amount: 20 }
    let(:barrel) { create :ordering_size, material: material, amount: 100 }

    before do
      barrel
      can
    end

    describe "#ordering_sizes" do
      it 'renders materials ordering sizes' do
        get :index, params: { material_id: material.id, format: :json }
        result = JSON.parse response.body

        can_json = result.first
        assert_equal can.id, can_json["id"]
        assert can_json["name"]

        barrel_json = result.last
        assert_equal barrel.id, barrel_json["id"]
        assert barrel_json["name"]
      end
    end
  end
end
