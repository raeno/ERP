# frozen_string_literal: true
require 'test_helper'

class MaterialsControllerTest < ActionController::TestCase
  before { quick_login_user }

  let(:material) { create :material }
  let(:failure_result) { stub(entity: Material.new, success?: false, error_message: 'Update error') }
  let(:success_result) { stub(entity: Material.new, success?: true) }

  describe 'GET #index' do
    describe 'without params' do
      it 'is successful' do
        get :index
        assert_response 200
      end

      it 'renders index_inventory template' do
        get :index
        assert_template 'index_inventory'
      end
    end

    describe 'with :purchase param' do
      # Create a material so there is something to test against
      before do
        find_or_create_material_inventory_item material, with_report: true
      end

      it 'is successful' do
        get :index, params: { purchase: true }
        assert_response 200
      end

      it 'renders index_purchase template' do
        get :index, params: { purchase: true }
        assert_template 'index_purchase'
      end
    end
  end

  describe 'GET #show' do
    let(:zone) { create :zone }
    before { find_or_create_material_inventory_item(material, production_zone: zone) }

    it 'renders show' do
      get :show, params: { id: material.id }
      assert_response 200
    end
  end

  describe 'GET #new' do
    it 'renders new' do
      get :new
      assert_template :new
    end

    it 'is successful' do
      get :new
      assert_response 200
    end
  end

  describe 'GET #edit' do
    it 'renders edit' do
      get :edit, params: { id: material.id }
      assert_template :edit
    end

    it 'is successful' do
      get :edit, params: { id: material.id }
      assert_response 200
    end
  end

  describe 'POST #create' do
    let(:unit) { create :unit }
    let(:params) do
      { material: { name: "Material name", unit_id: unit.id, ingredient_unit_id: unit.id } }
    end
    before { create :zone, :make }

    describe 'when material successfully created' do
      it 'redirects to show' do
        post :create, params: params
        material = Material.order(:id).last
        assert material
        assert_redirected_to material_path(material)
      end
    end

    describe 'when failed to create material' do
      before do
        action = stub(call: failure_result)
        @controller.stubs(:actions).returns(create_material: action)
      end

      it 'renders :new again' do
        post :create, params: params
        assert_template :new
      end
    end
  end

  describe 'PUT #update' do
    let(:unit) { create :unit, name: 'ml' }
    let(:zone) { create :zone }
    before { find_or_create_material_inventory_item(material, production_zone: zone) }

    describe 'when successful' do
      let(:params) do
        { id: material.id,
          material: { name: "Oil", unit: unit.to_param, inventory_item_production_zone_id: zone.id } }
      end

      it 'redirects to show' do
        put :update, params: params
        assert_redirected_to material_path(material)
      end

      it 'updates the material from params' do
        put :update, params: params
        material.reload
        assert_equal "Oil", material.name
        assert_equal "ML",  material.unit_name
      end
    end

    describe 'when failed' do
      let(:params) { { id: material.id, material: { name: nil } } }

      it 'renders edit again' do
        put :update, params: params
        assert_template :edit
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'redirects to inventory items index' do
      delete :destroy, params: { id: material.id }
      assert_redirected_to materials_path
    end

    it 'destroys the material' do
      material = create :material

      assert_difference -> { Material.count }, -1 do
        delete :destroy, params: { id: material.id }
      end
    end
  end
end
