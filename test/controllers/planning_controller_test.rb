# frozen_string_literal: true

require 'test_helper'

class PlanningControllerTest < ActionController::TestCase
  before { quick_login_user }

  describe 'GET #make' do
    let(:zone) { Zone.make_zone || create(:zone, :make) }
    let(:product) { create :product, :with_shipment_report, internal_title: "Blue Raspberry" }
    let(:inventory_item) { create :inventory_item, zone: zone, product: product, inventoriable: product }
    let(:production_report) { create :production_report, inventory_item: inventory_item }
    before { zone }

    it 'responds with success' do
      production_report

      get :make
      assert_response 200
      assert_match(/Blue Raspberry/, response.body)
    end

    it 'works with :unassigned status param' do
      get :make, params: { status: :unassigned }
      assert_response 200
    end

    it 'works with :low_supply status param' do
      get :make, params: { status: :low_supply }
      assert_response 200
    end

    it 'works with :managed status param' do
      get :make, params: { status: :managed }
      assert_response 200
    end
  end

  describe 'GET #pack' do
    let(:zone) { Zone.pack_zone || create(:zone, :pack) }
    let(:product) { create :product, :with_shipment_report, internal_title: "Blue Raspberry" }
    let(:inventory_item) { create :inventory_item, zone: zone, product: product, inventoriable: product }
    let(:production_report) { create :production_report, inventory_item: inventory_item }
    before { zone }

    it 'responds with success' do
      production_report

      get :pack
      assert_response 200
      assert_match(/Blue Raspberry/, response.body)
    end

    it 'works with :unassigned status param' do
      get :pack, params: { status: :unassigned }
      assert_response 200
    end

    it 'works with :low_supply status param' do
      get :pack, params: { status: :low_supply }
      assert_response 200
    end

    it 'works with :managed status param' do
      get :pack, params: { status: :managed }
      assert_response 200
    end
  end

  describe 'GET #workload' do
    before do
      user = create :user
      create :task, user: user, duration_estimate: 6000
    end

    it 'responds with success' do
      get :workload
      assert_response 200
      assert_template :workload
      assert_match(/01:40/, response.body)
    end
  end
end
