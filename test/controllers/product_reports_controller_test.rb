# frozen_string_literal: true
require 'test_helper'

class ProductReportsControllerTest < ActionController::TestCase
  before { quick_login_user }

  describe 'GET #ship' do
    before do
      create :zone, :ship
    end

    it 'responds with success' do
      get :ship
      assert_response 200
    end
  end
end
