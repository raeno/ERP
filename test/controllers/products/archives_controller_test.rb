# frozen_string_literal: true
require 'test_helper'

module Products
  class ArchivesControllerTest < ActionController::TestCase
    before { quick_login_user }
    let(:product) { create :product, :inactive }
    before { product }

    describe '#index' do
      it 'renders archived products' do
        assert Product.inactive.any?
        get :index
        assert_response :success
      end
    end
  end
end
