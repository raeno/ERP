# frozen_string_literal: true
require 'test_helper'

module Products
  class AttachmentsControllerTest < ActionController::TestCase
    before { quick_login_user }
    let(:product) { create :product }

    let(:attachment) { create :attachment, url: 'some/file/url' }
    let(:success_result) { stub(success?: true, entity: attachment) }
    let(:failure_result) { stub(success?: false, error_message: 'Failed to perform action') }

    describe 'GET #index' do
      it 'renders product media' do
        get :index, params: { product_id: product.id }
        assert_response 200
      end
    end

    describe 'POST #create' do
      it 'calls an action to create new attachment for product' do
        action = stub
        action.expects(:call).once.returns(success_result)
        @controller.stubs(:actions).returns(create_attachment: action)
        post :create, params: { product_id: product.id, attachment: { url: 'new_url' }, format: :json }
      end

      describe 'when successful' do
        before do
          action = stub(call: success_result)
          @controller.stubs(:actions).returns(create_attachment: action)
        end

        it 'responds with preview_url in json body' do
          post :create, params: { product_id: product.id, attachment: { url: 'some_url' }, format: :json }
          body = JSON.parse response.body
          refute_nil body['preview_url']
        end

        it 'has 200 status code' do
          post :create, params: { product_id: product.id, attachment: { url: 'some_url' }, format: :json }
          assert_response 200
        end
      end

      describe 'when unsuccessful' do
        before do
          action = stub(call: failure_result)
          @controller.stubs(:actions).returns(create_attachment: action)
        end

        it 'responds with error message in json body' do
          post :create, params: { product_id: product.id, attachment: { url: '' }, format: :json }
          body = JSON.parse response.body
          refute_nil body['error']
        end

        it 'responds with 500 status code' do
          post :create, params: { product_id: product.id, attachment: { url: '' }, format: :json }
          assert_response 500
        end
      end
    end

    describe 'DELETE #destroy' do
      before do
        create :product_attachment, product: product, attachment: attachment
      end

      it 'calls an action to delete product attachment' do
        action = stub
        action.expects(:call).once.returns(success_result)
        @controller.stubs(:actions).returns(destroy_attachment: action)
        delete :destroy, params: { product_id: product.id, id: attachment.id, format: :json }
      end

      describe 'when successful' do
        before do
          action = stub(call: success_result)
          @controller.stubs(:action).returns(destroy_attachment: action)
        end

        it 'responds with status in json body' do
          delete :destroy, params: { product_id: product.id, id: attachment.id, format: :json }
          body = JSON.parse response.body
          assert_equal 'success', body['status']
        end

        it 'has 200 status code' do
          delete :destroy, params: { product_id: product.id, id: attachment.id, format: :json }
          assert_response 200
        end
      end

      describe 'when unsuccessful' do
        before do
          action = stub(call: failure_result)
          @controller.stubs(:actions).returns(destroy_attachment: action)
        end

        it 'responds with error message in json body' do
          delete :destroy, params: { product_id: product.id, id: attachment.id, format: :json }
          body = JSON.parse response.body
          refute_nil body['error']
        end

        it 'responds with 500 status code' do
          delete :destroy, params: { product_id: product.id, id: attachment.id, format: :json }
          assert_response 500
        end
      end
    end
  end
end
