# frozen_string_literal: true
require 'test_helper'

module Products
  class BatchesControllerTest < ActionController::TestCase
    before { quick_login_user }
    let(:product) { create :product }

    describe 'GET #index' do
      before do
        create :batch, product_id: product.id
        assert product.reload.batches.any?
      end

      it 'renders product batches' do
        get :index, params: { product_id: product.id }
        assert_response :success
      end
    end
  end
end
