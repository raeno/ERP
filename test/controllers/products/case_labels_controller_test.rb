# frozen_string_literal: true
require 'test_helper'

module Products
  class CaseLabelsControllerTest < ActionController::TestCase
    before do
      quick_login_user
    end

    let(:product) { create :product }

    describe '#show' do
      it 'responds with success' do
        get :show, params: { product_id: product.id, format: 'pdf' }
        assert_response :success
      end
    end
  end
end
