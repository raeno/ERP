# frozen_string_literal: true
require 'test_helper'

module Products
  class InventoriesControllerTest < ActionController::TestCase
    let(:product) { create :product }
    before do
      create :zone, :ship
      product
      quick_login_user
    end

    describe 'GET #index' do
      it 'is successful and renders inventory page' do
        zone = create :zone
        get :index, params: { zone_id: zone.id }
        assert_template :index
        assert_response :success
      end
    end

    describe 'GET #show' do
      it 'renders product inventory page' do
        get :show, params: { product_id: product.id }
        assert_response :success
      end

      it 'throws no error against ingredients' do
        create :ingredient, product: product
        assert product.ingredients.any?

        get :show, params: { product_id: product.id }
        assert_response :success
      end
    end
  end
end
