# frozen_string_literal: true
require 'test_helper'

module Products
  class NotesControllerTest < ActionController::TestCase
    before { quick_login_user }
    let(:product) { create :product }

    describe '#show' do
      it 'renders product notes' do
        get :show, params: { product_id: product.id }
        assert_response 200
      end
    end

    describe 'GET #edit' do
      it 'is successful' do
        get :edit, params: { product_id: product }
        assert_response :success
        assert_template :edit
      end
    end

    describe 'PATCH #update' do
      let(:params) { { product_id: product, product: { notes: "Text123" } } }

      describe 'when success' do
        it 'saves the notes, redirects to the product page' do
          patch :update, params: params
          assert_redirected_to product_note_path(product)
          assert_equal "Text123", product.reload.notes
        end
      end

      describe 'when fail' do
        it 'renders form again' do
          Product.any_instance.stubs(:save).returns(false)
          patch :update, params: params
          assert_template :edit
        end
      end
    end
  end
end
