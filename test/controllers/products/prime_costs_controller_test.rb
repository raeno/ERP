# frozen_string_literal: true
require 'test_helper'

module Products
  class PrimeCostsControllerTest < ActionController::TestCase
    before { quick_login_user }
    describe 'GET #index' do
      describe 'when product not found' do
        it 'responds with 404' do
          get :index, params: { product_id: 100 }
          assert_response 404
        end

        it 'has same error in json body' do
          get :index, params: { product_id: 100 }
          body = JSON.parse(response.body)
          assert_equal 'Product not found', body['error']
        end
      end

      describe 'when product exists' do
        before do
          @product = create :product
          create_list :product_cogs_snapshot, 3, product: @product
        end

        it 'is successful' do
          get :index, params: { product_id: @product.id }
          assert_response 200
        end

        it 'renders json with information for prime cost histories chart' do
          get :index, params: { product_id: @product.id }
          body = JSON.parse(response.body)
          assert_equal @product.name, body['name']
          assert_equal 3, body['days'].size
          assert_equal 3, body['prices'].size
        end
      end
    end
  end
end
