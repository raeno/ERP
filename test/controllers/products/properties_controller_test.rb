# frozen_string_literal: true
require 'test_helper'

module Products
  class PropertiesControllerTest < ActionController::TestCase
    before { quick_login_user }
    let(:product) { create :product }

    describe 'GET #show' do
      it 'renders product properties page' do
        get :show, params: { product_id: product.id }
        assert_response :success
      end
    end
  end
end
