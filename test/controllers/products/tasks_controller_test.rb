# frozen_string_literal: true
require 'test_helper'

module Products
  class TasksControllerTest < ActionController::TestCase
    before { quick_login_user }
    let(:zone) { create :zone }
    let(:product) { create :product }
    let(:ii) { find_or_create_product_inventory_item(product, passed_zone: zone) }
    let(:task) { create :task, inventory_item: ii, quantity: 1168 }
    before { task }

    describe 'GET #index' do
      it 'renders product tasks' do
        get :index, params: { product_id: product.id }
        assert_response :success
        assert_match(/1,168 units/, response.body)
      end
    end
  end
end
