# frozen_string_literal: true
require 'test_helper'

class ProductsControllerTest < ActionController::TestCase
  before { quick_login_user }

  let(:product) { create :product }

  describe 'GET #show' do
    it 'redirects to product inventory' do
      get :show, params: { id: product.id }
      assert_redirected_to product_inventory_path(product.id)
    end
  end

  describe 'PATCH #update' do
    before { create :zone, name: 'Make' }
    let(:update_product) { stub(call: success_result) }
    before do
      @controller.stubs(:actions).returns(update_product: update_product)
    end

    let(:params) { { id: product.id, product: { is_active: false } } }

    it "successfully executes" do
      patch :update, params: params
      assert_redirected_to product_properties_path(product)
    end

    it 'calls a product change action with passed params' do
      action = mock
      expected_params = ActionController::Parameters.new('is_active' => 'false')
      expected_params.permit!
      action.expects(:call).with(product, expected_params).returns(success_result)
      @controller.stubs(:actions).returns(update_product: action)
      patch :update, params: params
    end

    it 'requires product param to be present' do
      assert_raise ActionController::ParameterMissing do
        patch :update, params: { id: product.id, something_else: {} }
      end
    end
  end

  describe 'PUT #override_quantity' do
    let(:fba_warehouse) { create :fba_warehouse }
    let(:fba_warehouse_2) { create :fba_warehouse }
    let(:allocations) { [
      build(:fba_allocation, fba_warehouse: fba_warehouse, quantity: 100),
      build(:fba_allocation, fba_warehouse: fba_warehouse_2, quantity: 200)] }
    let(:allocations_result) { stub(success?: true, entity: allocations)}
    let(:fetch_allocations) { stub(call: allocations_result) }
    let(:params) { { id: product.id, product: { to_ship_items_qty_override: 20 }}}

    before do
      @controller.stubs(:actions).returns(fetch_allocations: fetch_allocations)
    end

    it 'is successful' do
      put :override_quantity, params: params, format: :json
      assert_response 200
    end

    it 'fetches allocations for given product, shipment type and amount' do
      fetch_allocations = stub
      fetch_allocations.expects(:call).with do |shipment_item|
        shipment_item.product == product &&
        shipment_item.quantity == 20 &&
        !shipment_item.shipment_in_cases?
      end.once.returns(allocations_result)
      @controller.stubs(:actions).returns(fetch_allocations: fetch_allocations)

      put :override_quantity, params: params, format: :json
    end

    it 'serializes fetched allocations to JSON' do
      put :override_quantity, params: params, format: :json

      result = JSON.parse response.body
      expected = [
        { "warehouse_id" => fba_warehouse.id, "warehouse_name" => fba_warehouse.name, "quantity" => 100, 'inventory_enough' => false },
        { "warehouse_id" => fba_warehouse_2.id, "warehouse_name" => fba_warehouse_2.name, "quantity" => 200, 'inventory_enough' => false } ]
      assert_equal expected, result
    end
  end
end
