# frozen_string_literal: true
require 'test_helper'

class TasksControllerTest < ActionController::TestCase
  let(:task) { create :task, user: current_user }

  before do
    quick_login_user
    task
  end

  describe 'GET #make' do
    before do
      Zone.delete_all
      zone = create :zone, :make
      product = create :product, :with_shipment_report, internal_title: "Blue Raspberry"
      ii = create :inventory_item, zone: zone, product: product, inventoriable: product
      create :task, inventory_item: ii, date: Time.zone.today, user: current_user
      create :production_report, inventory_item: ii
    end

    it 'is successful for a normal request' do
      get :make
      assert_response :success
      assert_template :index_cards
      assert_match(/Blue Raspberry/, response.body)
    end

    it 'is successful for an aAJAX reuest' do
      get :make, xhr: true, params: { date_range: { start_date: 5.days.ago.to_date } }
      assert_response :success
      assert_template 'tasks/task_cards/_days'
      assert_match(/Blue Raspberry/, response.body)
    end
  end

  describe 'GET #pack' do
    before do
      Zone.delete_all
      zone = create :zone, :pack
      product = create :product, :with_shipment_report, internal_title: "Blue Raspberry"
      ii = create :inventory_item, zone: zone, product: product, inventoriable: product
      create :task, inventory_item: ii, date: Time.zone.today, user: current_user
      create :production_report, inventory_item: ii
    end

    it 'is successful for a normal request' do
      get :pack
      assert_response :success
      assert_template :index_cards
      assert_match(/Blue Raspberry/, response.body)
    end

    it 'is successful for an AJAX reuest' do
      get :pack, xhr: true, params: { date_range: { start_date: 5.days.ago.to_date } }
      assert_response :success
      assert_template 'tasks/task_cards/_days'
      assert_match(/Blue Raspberry/, response.body)
    end
  end

  describe 'GET #current' do
    before do
      product = create :product, :with_shipment_report, internal_title: "Blue Raspberry"
      ii = create :inventory_item, product: product, inventoriable: product
      create :task, inventory_item: ii, date: Time.zone.today, user: current_user
      create :production_report, inventory_item: ii
    end

    it 'is successful for a normal request' do
      get :current
      assert_response :success
      assert_template :index_cards
      assert_match(/Blue Raspberry/, response.body)
    end

    it 'is successful for an AJAX reuest' do
      get :current, xhr: true, params: { date_range: { start_date: 5.days.ago.to_date } }
      assert_response :success
      assert_template 'tasks/task_cards/_days'
      assert_match(/Blue Raspberry/, response.body)
    end
  end

  describe 'GET #finished' do
    it 'non-admins can only see their own tasks' do
      orange_task = create :task, finished_at: 1.day.ago
      orange_task.product.update_column(:title, "Orange oil")

      lavender_task = create :task, finished_at: 1.day.ago, user: current_user
      lavender_task.product.update_column(:title, "Lavender oil")

      get :finished
      assert_response :success
      assert_match(/Lavender oil/, response.body)
      assert_no_match(/Orange oil/, response.body)
    end
  end

  describe 'GET #show' do
    it 'for a pending task, renders show' do
      assert task.pending?
      get :show, params: { id: task.id }
      assert_response :success
    end

    it 'for an in-progress task, renders show' do
      task.update_column(:started_at, Time.zone.now)
      assert task.in_progress?
      get :show, params: { id: task.id }
      assert_response :success
    end

    it 'for a finished task, renders show' do
      task.update_column(:started_at, 3.hours.ago)
      task.update_column(:finished_at, 1.hour.ago)
      assert task.finished?
      get :show, params: { id: task.id }
      assert_response :success
    end
  end

  describe 'GET #edit' do
    it 'renders edit' do
      get :edit, params: { id: task.id }
      assert_response :success
      assert_template :edit
    end
  end

  describe 'POST #create' do
    let(:inventory_item) { create :inventory_item, :for_product, :with_production_report }
    let(:user) { create :user }
    let(:params) do
      { task: { inventory_item_id: inventory_item.id, user_id: user.id, quantity_in_zone_units: 345 } }
    end

    describe 'when successful' do
      it 'creates a task, responds with success' do
        inventory_item.product.shipment_report = build :shipment_report, days_of_cover: 20
        assert_difference -> { Task.count }, 1 do
          post :create, params: params
          assert_response :success
          assert_template 'planning/_record'
        end
      end
    end

    describe 'when failed' do
      before do
        failure_result = stub(success?: false, error_message: 'Error')
        action = stub(call: failure_result)
        @controller.stubs(:actions).returns(create: action)
      end

      it 'responds with :unprocessable_entity' do
        post :create, params: params
        assert_response :unprocessable_entity
        assert_match(/Cannot create task/, response.body)
      end
    end
  end

  describe 'PUT #update' do
    let(:params) do
      { id: task.id,
        task: { quantity_in_zone_units: 345 } }
    end

    describe 'when successful' do
      it 'redirects back' do
        put :update, params: params
        assert_redirected_to task_path(task)
        assert_match(/Task was updated/, flash[:notice])
      end
    end

    describe 'when failed' do
      before do
        failure_result = stub(success?: false, error_message: 'Error')
        action = stub(call: failure_result)
        @controller.stubs(:actions).returns(update: action)
      end

      it 'renders edit again' do
        put :update, params: params
        assert_template 'edit'
      end
    end
  end

  describe 'POST #start from the task show page' do
    let(:params) { { id: task.id } }

    describe 'when success' do
      it 'calls :start action, responds with :ok' do
        expect_success_once(Tasks::StartTask,
          with: [task, current_user], result_entity: task)

        post :start, params: params
        assert_redirected_to task_path(task)
      end
    end

    describe 'when fail' do
      it 'redirects back' do
        expect_failure_once(Tasks::StartTask, with: [task, current_user])

        post :start, params: params
        assert_redirected_to task_path(task)
      end
    end
  end

  describe 'POST #start from the task list page' do
    let(:params) { { id: task.id } }
    before { request.env["HTTP_REFERER"] = make_tasks_url }

    describe 'when success' do
      it 'calls :start action, responds with :ok' do
        expect_success_once(Tasks::StartTask,
          with: [task, current_user], result_entity: task)

        post :start, params: params
        assert_template 'tasks/task_cards/_card_front_in_progress'
      end
    end

    describe 'when fail' do
      it 'redirects back' do
        expect_failure_once(Tasks::StartTask, with: [task, current_user])

        post :start, params: params
        assert_template 'shared/_error'
      end
    end
  end

  describe 'PUT #cancel' do
    let(:params) { { id: task.id } }
    before { task.update_column(:started_at, Time.zone.now) }

    describe 'when success' do
      it 'calls :cancel action, redirects back' do
        request.env["HTTP_REFERER"] = pack_tasks_url
        expect_success_once(Tasks::CancelTask, with: task)
        put :cancel, params: params
        assert_redirected_to pack_tasks_path
        assert_match(/Work on the task was cancelled/, flash[:notice])
      end
    end

    describe 'when fail' do
      it 'redirects to the task' do
        expect_failure_once(Tasks::CancelTask, with: task)
        put :cancel, params: params
        assert_redirected_to task
        assert_match(/Could not cancel the task/, flash[:error])
      end
    end
  end

  describe 'DELETE #destroy' do
    let(:params) { { id: task.id } }
    before { task }

    describe 'when success' do
      it 'calls destroy action, redirects back' do
        expect_success_once(Tasks::DestroyTask, with: task)

        delete :destroy, params: { id: task }
        assert_redirected_to planning_make_path
      end
    end

    describe 'when fail' do
      it 'redirects to the task, displays error' do
        error_message = "Somehow the task cannot be cancelled"
        expect_failure_once(Tasks::DestroyTask,
          with: task, error_message: error_message)

        delete :destroy, params: { id: task }
        assert_redirected_to task_path(task)
        assert_match(/#{error_message}/, flash[:error])
      end
    end
  end
end
