# frozen_string_literal: true
require 'test_helper'

class UnitsControllerTest < ActionController::TestCase
  before do
    quick_login_user
  end

  let(:success_result) { stub(success?: true) }
  let(:failure_result) { stub(success?: false, entity: Unit.new, error_message: 'Error') }

  describe 'GET #index' do
    it 'is successful' do
      get :index
      assert_response :success
    end
  end

  describe 'GET #new' do
    it 'is successful' do
      get :new
      assert_response :success
    end
  end

  describe 'GET #edit' do
    let(:unit) { create :unit }
    it 'is successful' do
      get :edit, params: { id: unit.id }
      assert_response :success
    end
  end

  describe 'POST #create' do
    let(:action) { stub(call: success_result) }
    let(:params) { { unit: { name: 'ml' } } }

    before do
      @controller.stubs(:actions).returns(create_unit: action)
    end

    it 'calls an action to create a unit' do
      action.expects(:call).once.returns(success_result)
      @controller.stubs(:actions).returns(create_unit: action)
      post :create, params: params
    end

    describe 'when creation successful' do
      it 'redirects to units index' do
        post :create, params: params
        assert_redirected_to units_path
      end
    end

    describe 'when creation failed' do
      let(:action) { stub(call: failure_result) }

      it 'renders "new" again' do
        post :create, params: params
        assert_template :new
      end
    end
  end

  describe 'PATCH #update' do
    let(:action) { stub(call: success_result) }
    let(:unit) { create :unit }
    let(:params) { { id: unit.id, unit: { name: 'ml' } } }

    before do
      @controller.stubs(:actions).returns(update_unit: action)
    end

    it 'calls an action to update a unit' do
      action.expects(:call).once.returns(success_result)
      @controller.stubs(:actions).returns(update_unit: action)
      patch :update, params: params
    end

    describe 'when update successful' do
      it 'redirects to units index' do
        patch :update, params: params
        assert_redirected_to units_path
      end
    end

    describe 'when update failed' do
      let(:action) { stub(call: failure_result) }

      it 'renders "edit" again' do
        patch :update, params: params
        assert_template :edit
      end
    end
  end

  describe 'DELETE #destroy' do
    let(:action) { stub(call: success_result) }
    let(:unit) { create :unit }

    before do
      @controller.stubs(:actions).returns(destroy_unit: action)
    end

    it 'calls an action to destroy unit' do
      action.expects(:call).once.returns(success_result)
      @controller.stubs(:actions).returns(destroy_unit: action)
      delete :destroy, params: { id: unit.to_param }
    end

    describe 'when successfully destroyed' do
      it 'redirects to units index' do
        delete :destroy, params: { id: unit.to_param }
        assert_redirected_to units_path
      end

      it 'notifies user about successful destroy' do
        delete :destroy, params: { id: unit.to_param }
        assert_equal 'Unit was deleted', flash[:notice]
      end
    end

    describe 'when destroy failed' do
      let(:action) { stub(call: failure_result) }

      it 'redirects to units index as well' do
        delete :destroy, params: { id: unit.to_param }
        assert_redirected_to units_path
      end

      it 'has error message in flash error' do
        delete :destroy, params: { id: unit.to_param }
        assert_match(/Failed to destroy unit/, flash[:error])
      end
    end
  end
end
