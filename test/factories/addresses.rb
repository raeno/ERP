FactoryGirl.define do
  factory :address, class: Amazon::Structs::Address do
    name 'Some name'
    address_line_1 'Sesame street'
    city 'New York'
    state_or_province_code 'NY'
    postal_code '12345'
    country_code 'US'
  end
end
