# frozen_string_literal: true
# == Schema Information
#
# Table name: amazon_shipments
#
#  id               :integer          not null, primary key
#  shipment_id      :text
#  name             :text
#  status           :string
#  cases_required   :boolean
#  fba_warehouse_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  user_id          :integer
#

FactoryGirl.define do
  factory :amazon_shipment, aliases: [:shipment] do
    name 'some shipment name'
    sequence(:shipment_id) { |n| "TEST_SHIPMENT_#{n}" }
    status 'WORKING'
    cases_required true
    fba_warehouse
    user

    trait :initialized do
      status 'INITIALIZED'
    end

    trait :synced do
      status 'AMAZON_SYNCED'
    end
  end
end
