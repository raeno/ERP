# frozen_string_literal: true
# == Schema Information
#
# Table name: attachments
#
#  id         :integer          not null, primary key
#  url        :text             not null
#  mimetype   :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :attachment do
    url 'http://some_url.com'
  end
end
