# == Schema Information
#
# Table name: batches
#
#  id                 :integer          not null, primary key
#  product_id         :integer          not null
#  quantity           :integer          not null
#  notes              :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  completed_on       :date
#  zone_id            :integer          not null
#  auxillary_info     :hstore
#  task_id            :integer
#  amazon_shipment_id :integer
#

FactoryGirl.define do
  factory :batch do
    quantity 20
    user_ids { [create(:user).id] }
    product
    zone
    task

    trait :with_shipment do
      amazon_shipment
    end
  end
end
