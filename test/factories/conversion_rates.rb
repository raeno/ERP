# frozen_string_literal: true
# == Schema Information
#
# Table name: conversion_rates
#
#  id           :integer          not null, primary key
#  from_unit_id :integer          not null
#  to_unit_id   :integer          not null
#  rate         :decimal(, )      not null
#

FactoryGirl.define do
  factory :conversion_rate do
    association :from_unit, factory: :unit
    association :to_unit, factory: :unit
    rate 1000
  end
end
