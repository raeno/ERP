# frozen_string_literal: true
# == Schema Information
#
# Table name: fba_allocations
#
#  id                   :integer          not null, primary key
#  fba_warehouse_id     :integer          not null
#  product_id           :integer          not null
#  quantity             :integer          not null
#  quantity_to_allocate :integer
#  created_at           :datetime
#  updated_at           :datetime
#  shipment_type        :text
#

FactoryGirl.define do
  factory :fba_allocation do
    quantity 500
    quantity_to_allocate 500
    product
    fba_warehouse
  end
end
