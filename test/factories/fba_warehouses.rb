# == Schema Information
#
# Table name: fba_warehouses
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  created_at :datetime
#  updated_at :datetime
#

FactoryGirl.define do
  factory :fba_warehouse, aliases: [:warehouse] do
    sequence(:name) { |n| "test_#{n}"}
  end
end
