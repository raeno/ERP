FactoryGirl.define do
  factory :inbound_shipment_header, class: Amazon::Structs::InboundShipmentHeader do
    skip_create
    
    shipment_name 'ABC'

  end
end
