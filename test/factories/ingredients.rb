# == Schema Information
#
# Table name: ingredients
#
#  id                      :integer          not null, primary key
#  product_id              :integer          not null
#  quantity_obsolete       :decimal(, )      default(1.0), not null
#  created_at              :datetime
#  updated_at              :datetime
#  inventory_item_id       :integer          not null
#  quantity_in_small_units :decimal(, )      default(1.0), not null
#
# Foreign Keys
#
#  fk_rails_...  (product_id => products.id)
#

FactoryGirl.define do
  factory :ingredient do
    product
    inventory_item
    quantity_in_small_units 10
  end
end
