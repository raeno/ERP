# frozen_string_literal: true
# == Schema Information
#
# Table name: inventories
#
#  id                :integer          not null, primary key
#  inventory_item_id :integer          not null
#  quantity          :decimal(, )      not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Foreign Keys
#
#  fk_rails_...  (inventory_item_id => inventory_items.id)
#

FactoryGirl.define do
  factory :inventory do
    inventory_item
    quantity 10
  end
end
