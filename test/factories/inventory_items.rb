# frozen_string_literal: true
# == Schema Information
#
# Table name: inventory_items
#
#  id                          :integer          not null, primary key
#  zone_id                     :integer
#  inventoriable_id            :integer
#  inventoriable_type          :string
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  production_zone_id          :integer
#  production_setup_minutes    :integer          default(0), not null
#  production_units_per_minute :integer
#

FactoryGirl.define do
  factory :inventory_item do
    zone
    association :inventoriable, factory: :material

    trait :for_product do
      association :inventoriable, factory: :product
    end

    trait :for_product_with_shipment_report do
      association :inventoriable, factory: [:product, :with_shipment_report]
    end

    trait :with_production_report do
      production_report
    end
  end
end
