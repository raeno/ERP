# frozen_string_literal: true
# == Schema Information
#
# Table name: invoice_items
#
#  id                    :integer          not null, primary key
#  material_id           :integer          not null
#  pkg_external_name     :string
#  pkg_form              :string
#  pkg_unit_id           :integer          not null
#  pkg_amount            :float            default(1.0), not null
#  pkg_price             :decimal(, )      not null
#  quantity              :integer          default(1), not null
#  total_material_amount :float            not null
#  total_price            :decimal(, )      not null
#
# Foreign Keys
#
#  fk_rails_bfc0778a90  (material_id => materials.id)
#

FactoryGirl.define do
  factory :invoice_item do
    invoice
    material
    material_container
    pkg_amount 10
    pkg_price 100
    quantity 1
  end
end
