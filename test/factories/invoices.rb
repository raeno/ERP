# frozen_string_literal: true
# == Schema Information
#
# Table name: invoices
#
#  id             :integer          not null, primary key
#  vendor_id      :integer
#  created_at     :datetime
#  updated_at     :datetime
#  number         :string           default(""), not null
#  attachment_url :text
#  date           :date             not null
#  total_price    :decimal(, )      not null
#  received_date  :date
#  notes          :text
#  paid_date      :date
#

FactoryGirl.define do
  factory :invoice do
    vendor
    total_price 599.99

    trait :paid do
      paid_date Time.zone.now
    end

    trait :received do
      received_date Time.zone.now
    end

    trait :pending do
      paid_date nil
      received_date nil
    end
  end
end
