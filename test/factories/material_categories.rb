# frozen_string_literal: true
# == Schema Information
#
# Table name: material_categories
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  created_at :datetime
#  updated_at :datetime

FactoryGirl.define do
  factory :material_category do
    name 'All'
  end
end
