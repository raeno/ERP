# frozen_string_literal: true
# == Schema Information
#
# Table name: material_containers
#
#  id               :integer          not null, primary key
#  material_id      :integer          not null
#  external_name    :text
#  unit_id          :integer          not null
#  amount           :decimal(, )      default(1.0), not null
#  price            :decimal(, )      not null
#  lot_number       :text
#  expiration_date  :date
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  amount_left      :decimal(, )      default(0.0), not null
#  old_status       :integer          default(0)
#  ordering_size_id :integer
#  status           :enum             default("pending")
#
# Foreign Keys
#
#  fk_rails_...  (material_id => materials.id)
#

FactoryGirl.define do
  factory :material_container do
    unit
    amount 10
    price 20
    lot_number "ABCDEF"
    expiration_date 10.days.from_now
    after(:build) do |container|
      container.material = FactoryGirl.create(:material, unit: container.unit) unless container.material.present?
    end
  end
end
