# frozen_string_literal: true
# == Schema Information
#
# Table name: material_media
#
#  id          :integer          not null, primary key
#  url         :text
#  material_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  mimetype    :text
#

FactoryGirl.define do
  factory :material_media do
    url 'http://some_url.com/image'
    mimetype 'image/jpeg'
  end
end
