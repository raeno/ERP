# frozen_string_literal: true

# == Schema Information
#
# Table name: material_vendors
#
#  id          :integer          not null, primary key
#  material_id :integer          not null
#  vendor_id   :integer          not null
#  url         :string
#

FactoryGirl.define do
  factory :material_vendor do
    material
    vendor
  end
end
