# == Schema Information
#
# Table name: materials
#
#  id                   :integer          not null, primary key
#  name                 :string           not null
#  unit_price           :decimal(, )      default(0.0), not null
#  created_at           :datetime
#  updated_at           :datetime
#  ingredient_unit_id   :integer          not null
#  image_url            :string
#  category_id          :integer          default(1), not null
#  unit_id              :integer          not null
#  unit_conversion_rate :decimal(, )
#

FactoryGirl.define do
  factory :material do
    name 'Bottle'
    unit_price 1.99
    unit
    ingredient_unit { unit }
    association :category, factory: :material_category
  end
end
