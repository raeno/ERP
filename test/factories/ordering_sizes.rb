# frozen_string_literal: true
# == Schema Information
#
# Table name: ordering_sizes
#
#  id                            :integer          not null, primary key
#  material_id                   :integer          not null
#  unit_id                       :integer          not null
#  amount                        :float            not null
#  amount_in_main_material_units :float            not null
#  name                          :string
#
# Foreign Keys
#
#  fk_rails_...  (material_id => materials.id)
#  fk_rails_...  (unit_id => units.id)
#

FactoryGirl.define do
  factory :ordering_size do
    material
    unit { material.unit }
    amount 10
  end
end
