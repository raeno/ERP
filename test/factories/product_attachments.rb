# frozen_string_literal: true
# == Schema Information
#
# Table name: product_attachments
#
#  id            :integer          not null, primary key
#  product_id    :integer
#  attachment_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryGirl.define do
  factory :product_attachment do
  end
end
