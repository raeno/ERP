# frozen_string_literal: true
# == Schema Information
#
# Table name: product_cogs_snapshots
#
#  id         :integer          not null, primary key
#  product_id :integer
#  batch_id   :integer
#  cogs       :decimal(, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  zone_id    :integer
#

FactoryGirl.define do
  factory :product_cogs_snapshot do
    sequence(:cogs) { |n| (n + 1) * 1.5 }
    product
    batch
    zone { Zone.ship_zone }
  end
end
