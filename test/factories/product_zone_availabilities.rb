# frozen_string_literal: true
# == Schema Information
#
# Table name: product_zone_availabilities
#
#  id         :integer          not null, primary key
#  product_id :integer
#  zone_id    :integer
#

FactoryGirl.define do
  factory :product_zone_availability do
    product
    zone
  end
end
