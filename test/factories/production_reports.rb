# frozen_string_literal: true
FactoryGirl.define do
  factory :production_report, class: InventoryItems::ProductionReport do
    inventory_item
    in_stock 100
    demand 200
    demand_coverage 50
    supply 50
    to_cover 100
    can_cover 50
    can_cover_optimal 50
    can_cover_in_cases 1
  end
end
