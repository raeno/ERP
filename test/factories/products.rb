# == Schema Information
#
# Table name: products
#
#  id                       :integer          not null, primary key
#  title                    :string
#  seller_sku               :string
#  asin                     :text
#  fnsku                    :string
#  size                     :string
#  list_price_amount        :string
#  list_price_currency      :string
#  total_supply_quantity    :integer
#  in_stock_supply_quantity :integer
#  small_image_url          :string
#  is_active                :boolean          default(TRUE)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  inbound_qty              :float            default(0.0)
#  sold_last_24_hours       :float            default(0.0)
#  weeks_of_cover           :float            default(0.0)
#  sellable_qty             :float            default(0.0)
#  internal_title           :string
#  sales_rank               :integer
#  selling_price            :float
#  selling_price_currency   :string
#  items_per_case           :integer          default(0), not null
#  production_buffer_days   :integer          default(35), not null
#  batch_min_quantity       :integer          default(0), not null
#  batch_max_quantity       :integer
#  fee                      :decimal(, )      default(0.0), not null
#  notes                    :text
#

FactoryGirl.define do
  factory :product do
    production_buffer_days 35

    trait :active do
      is_active  true
    end

    trait :inactive do
      is_active  false
    end

    trait :with_shipment_report do
      shipment_report
    end
  end
end
