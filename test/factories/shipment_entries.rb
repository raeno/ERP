# == Schema Information
#
# Table name: shipment_entries
#
#  id                 :integer          not null, primary key
#  amazon_shipment_id :integer
#  product_id         :integer
#  quantity           :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  status             :string           default("NEW")
#  batch_id           :integer
#

FactoryGirl.define do
  factory :shipment_entry do
    product
    amazon_shipment
    quantity 10

    trait :with_batch do
      batch
    end

    trait :new do
      status 'NEW'
    end

    trait :changed do
      status 'CHANGED'
    end

    trait :amazon_synced do
      status 'AMAZON_SYNCED'
    end

    trait :synced do
      status 'SYNCED'
    end
  end
end
