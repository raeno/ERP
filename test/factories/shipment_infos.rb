 FactoryGirl.define do
   factory :shipment_info do
     skip_create

     initialize_with do
       new(shipment_id, header, items)
     end

     shipment_id 'SOME_SHIPMENT_ID'
     association :header, factory: :inbound_shipment_header
     items []
   end
 end
