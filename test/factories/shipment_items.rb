FactoryGirl.define do
  factory :shipment_item do
    skip_create

    initialize_with do
      new(product, quantity)
    end

    product
    quantity 10
  end
end
