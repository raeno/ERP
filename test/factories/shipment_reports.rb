# frozen_string_literal: true
FactoryGirl.define do
  factory :shipment_report, class: Products::ShipmentReport do
    amazon_coverage_ratio 30
    items_shipment_priority 1000
    cases_shipment_priority 2000
    to_allocate_item_quantity 3
    to_allocate_case_quantity 5
    items_fba_allocation '[]'
    cases_fba_allocation '[]'
  end
end
