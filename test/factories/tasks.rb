# frozen_string_literal: true
# == Schema Information
#
# Table name: tasks
#
#  id                      :integer          not null, primary key
#  inventory_item_id       :integer          not null
#  started_at              :datetime
#  finished_at             :datetime
#  quantity                :integer
#  created_at              :datetime
#  updated_at              :datetime
#  quantity_cases_obsolete :integer
#  labour_cost             :decimal(, )
#  user_id                 :integer          not null
#  duration_estimate       :integer
#  date                    :date
#  product_items_per_case  :integer
#
# Foreign Keys
#
#  fk_rails_...  (inventory_item_id => inventory_items.id)
#

FactoryGirl.define do
  factory :task do
    association :inventory_item, :for_product, :with_production_report
    quantity 1
    user
  end
end
