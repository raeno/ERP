# frozen_string_literal: true
# == Schema Information
#
# Table name: units
#
#  id        :integer          not null, primary key
#  name      :text
#  full_name :text
#

FactoryGirl.define do
  factory :unit do
    name 'ML'
    full_name 'Milliliters'

    trait :each do
      name 'Each'
      full_name 'Each'
    end

    trait :oz do
      name 'oz'
      full_name 'oz'
    end
  end
end
