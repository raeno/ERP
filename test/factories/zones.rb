# frozen_string_literal: true
# == Schema Information
#
# Table name: zones
#
#  id               :integer          not null, primary key
#  name             :string
#  production_order :integer
#  created_at       :datetime
#  updated_at       :datetime
#

FactoryGirl.define do
  factory :zone do
    name 'Make'
    production_order 1

    trait :make do
      name 'Make'
      production_order 1
    end

    trait :pack do
      name 'Pack'
      production_order 2
    end

    trait :ship do
      name 'Ship'
      production_order 3
    end
  end
end
