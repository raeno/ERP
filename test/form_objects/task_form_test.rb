# frozen_string_literal: true

require 'test_helper'

class TaskFormTest < ActiveSupport::TestCase
  let(:task) { build :task }
  subject { TaskForm.new task }

  describe '#date' do
    let(:date) { Time.zone.today }
    let(:date_str) { date.to_s(:short) }

    describe 'getter' do
      it 'converts date to string' do
        task.date = date
        assert_equal date_str, subject.date
      end
    end

    describe 'setter' do
      it 'converts stringto date' do
        subject.date = date_str
        subject.sync
        assert_equal date, task.date
      end
    end
  end
end
