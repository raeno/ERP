# frozen_string_literal: true
require 'test_helper'

class FormattingHelperTest < ActionView::TestCase
  include Generic::FormattingHelper

  describe 'with_unit' do
    it 'appends unit of measure to the number given' do
      assert_equal "15&nbsp;ml", with_unit(15, 'ml')
    end

    it 'rounds the number to integer by default' do
      assert_equal "16&nbsp;ml", with_unit(15.99, 'ml')
    end

    describe 'when convert_to_integer option is false' do
      it 'cuts unnecessary .0 for whole numbers' do
        assert_equal "11&nbsp;ml", with_unit(11.0, 'ml', convert_to_integer: false)
      end

      it 'leaves decimal numbers as is' do
        assert_equal "15.99&nbsp;ml", with_unit(15.99, 'ml', convert_to_integer: false)
      end
    end

    it 'accepts formatted numbers (strings)' do
      assert_equal "11,222.33&nbsp;ml", with_unit("11,222.33", 'ml')
    end

    it 'does not throw errors when nils are passed' do
      assert_nothing_raised { with_unit(nil, 'whatever') }
      assert_nothing_raised { with_unit(50, nil) }
    end
  end

  describe "integer_with_delimiter" do
    it "converts 111222333.999 to 111,222,334" do
      assert_equal "111,222,334", integer_with_delimiter(111_222_333.999)
    end

    it "does not convert what is already converted" do
      formatted_number = "111,222,334"
      assert_equal formatted_number, integer_with_delimiter(formatted_number)
    end
  end

  describe "duration" do
    it 'converts duration is seconds to hh:mm:ss format' do
      assert_equal '00:00:30', duration(30)
      assert_equal '00:01:40', duration(100)
      assert_equal '02:13:20', duration(8000)
    end
  end

  describe "percent" do
    it 'converts 0.54321 to "54.32%"' do
      assert_equal '54.32%', percent(0.54321)
    end
  end
end
