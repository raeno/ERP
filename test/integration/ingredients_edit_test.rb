# frozen_string_literal: true
require 'test_helper'

class IngredientsEditTest < ActionDispatch::IntegrationTest
  include Logging
  let(:product) { create :product }
  let(:kg_unit) { create :unit, name: 'Kg' }
  let(:ml_unit) { create :unit, name: 'ML' }
  let(:each_unit) { create :unit, name: 'Each' }
  let(:zone) { create :zone, :ship }

  let(:lavender_oil) do
    create :material, name: "Lavender Oil", unit: ml_unit, ingredient_unit: ml_unit
  end
  let(:lemon_oil) do
    create :material, name: "Lemon Oil", unit: kg_unit, ingredient_unit: kg_unit
  end
  let(:empty_bottle) do
    create :material, name: "Empty Bottle", unit: each_unit, ingredient_unit: each_unit
  end

  before do
    Zone.destroy_all
    create :product_zone_availability, product: product, zone: zone

    @lavender_oil_comp, @lemon_oil_comp, @empty_bottle_comp = [lavender_oil, lemon_oil, empty_bottle].map do |material|
      find_or_create_material_inventory_item(material, production_zone: zone, quantity: 20, with_report: true)
    end
    find_or_create_product_inventory_item(product, passed_zone: zone, with_report: true)
  end

  it 'allows to add ingredients' do
    visit edit_ingredients_product_path(product)

    click_add_ingredient
    last_ingredient_material_select.find(:option, empty_bottle.name).select_option

    assert_equal "Each", last_ingredient_unit_of_measure

    click_add_ingredient
    last_ingredient_material_select.find(:option, lemon_oil.name).select_option
    last_ingredient_quantity_input.set(2)
    assert_equal "Kg", last_ingredient_unit_of_measure
    last_ingredient_delete_button.click # add and delete right away

    click_add_ingredient
    last_ingredient_material_select.find(:option, lavender_oil.name).select_option
    assert_equal "ML", last_ingredient_unit_of_measure
    last_ingredient_quantity_input.set(10)

    assert_difference -> { Ingredient.count }, 2 do
      click_on 'Save'
      assert page.has_content? "Ingredients were successfully saved"
    end

    check_ingredients(product, @lavender_oil_comp.id => 10, @empty_bottle_comp.id => 1)
  end

  it 'allows to remove ingredients' do
    create :ingredient, product_id: product.id, inventory_item_id: @lavender_oil_comp.id, quantity_in_small_units: 1
    create :ingredient, product_id: product.id, inventory_item_id: @lemon_oil_comp.id, quantity_in_small_units: 1
    check_ingredients(product.reload, @lavender_oil_comp.id => 1, @lemon_oil_comp.id => 1)

    visit edit_ingredients_product_path(product)
    last_ingredient_delete_button.click
    last_ingredient_delete_button.click

    assert_difference -> { Ingredient.count }, -2 do
      click_on 'Save'
      assert page.has_content? "Ingredients were successfully saved"
    end

    check_ingredients(product, {})
  end

  protected

  def click_add_ingredient
    find(".js-add-ingredient").click
  end

  def last_ingredient_material_select
    last_ingredient_form_tr.find("select.js-inventory-item")
  end

  def last_ingredient_quantity_input
    last_ingredient_form_tr.find("input.js-quantity")
  end

  def last_ingredient_unit_of_measure
    last_ingredient_form_tr.find(".js-unit").text
  end

  def last_ingredient_delete_button
    last_ingredient_form_tr.find(".js-remove")
  end

  def last_ingredient_form_tr
    all(".js-edit-product-ingredients table tbody tr").last
  end

  # ingredients_hash: { component_id => quantity, ...}
  def check_ingredients(product, ingredients_hash)
    product_ingredients = product.ingredients
    ingredients_hash.each do |inventory_item_id, quantity|
      ingredient = product_ingredients.find_by(inventory_item_id: inventory_item_id)
      assert_equal quantity, ingredient.quantity
    end
    assert_equal product_ingredients.count, ingredients_hash.count
  end
end
