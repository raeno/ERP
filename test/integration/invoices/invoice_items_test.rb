# frozen_string_literal: true
require 'test_helper'

module Invoices
  class InvoiceItemsTest < ActionDispatch::IntegrationTest
    let(:vendor) { create :vendor }
    let(:unit) { create :unit }
    let(:invoice) { create :invoice, vendor: vendor }
    let(:material) { create :material, vendors: [vendor], unit: unit }
    let(:ordering_size) { create :ordering_size, material: material, amount: 33.22, unit: unit }

    before do
      ordering_size
    end

    describe 'new action' do
      it 'allows creating of an invoice item' do
        visit invoice_path(invoice)
        find('.add-invoice-item').click

        select_material material
        specify_price "55.99"
        click_on 'Save'
        assert page.has_content? 'successfully created'

        new_invoice_item = InvoiceItem.reorder(:id).last
        assert_equal ordering_size, new_invoice_item.material_container.ordering_size
        assert_equal 33.22, new_invoice_item.material_container.amount
        assert_equal unit, new_invoice_item.material_container.unit
        assert_equal material, new_invoice_item.material_container.material
      end

      describe 'when user did not specify ordering size' do
        it 'displays a validation error' do
          OrderingSize.delete_all
          visit invoice_path(invoice)
          find('.add-invoice-item').click

          select_material material
          specify_price "55.99"
          click_on 'Save'

          assert page.has_content? 'Please select ordering size'
        end
      end
    end

    describe 'edit action' do
      let(:invoice_item) { create_invoice_item_with_ordering_size ordering_size, invoice: invoice }
      let(:another_size) { create :ordering_size, material: material, amount: 11.44, unit: unit }

      before do
        another_size
        visit edit_invoice_item_path(invoice, invoice_item)
      end

      it 'allows changing ordering size' do
        select_size another_size
        click_on 'Save'
        assert page.has_content? 'successfully updated'

        invoice_item.reload
        assert_equal another_size, invoice_item.material_container.ordering_size
        assert_equal 11.44, invoice_item.material_container.amount
      end
    end

    private

    def select_material(material)
      select material.name, from: "invoice_item[material_container][material_id]"
    end

    def select_size(ordering_size)
      select ordering_size.to_s, from: "invoice_item[material_container][ordering_size_id]"
    end

    def specify_price(price)
      fill_in "invoice_item[material_container][price]", with: price
    end
  end
end
