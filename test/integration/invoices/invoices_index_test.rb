# frozen_string_literal: true
require 'test_helper'

module Invoices
  class InvoicesIndexTest < ActionDispatch::IntegrationTest
    let(:vendor_names) { %w(Vendor1 Vendor2 Vendor3) }

    def create_invoices(options_per_each = [])
      options_per_each.each_with_index do |options, index|
        vendor_name = options.delete(:vendor_name)
        create :vendor, name: vendor_name if vendor_name

        options[:created_at] ||= index.months.ago

        status = options.delete(:status) || :pending
        create :invoice, status, options
      end
    end

    def create_default_invoices
      vendors = vendor_names.map { |name| create :vendor, name: name }

      options = vendors.flat_map.each_with_index do |vendor, index|
        date = (index + 1).months.ago
        [{ vendor: vendor, status: :paid, created_at: date },
         { vendor: vendor, status: :received, created_at: date },
         { vendor: vendor, status: :pending, created_at: date }]
      end
      create_invoices options
    end

    it 'lists all invoices in list view by default' do
      create_default_invoices
      visit invoices_path
      assert_text 'Invoices'
      vendor_names.each do |name|
        assert_text name
      end
    end

    it 'renders invoices chart on top of invoices page' do
      create_default_invoices
      visit invoices_path
      assert_selector '#invoices-series-chart .highcharts-series-group'
      # have 3 ':paid' columns
      assert_selector '.highcharts-series.highcharts-series-2 rect', count: 3

      # have 3 ':received' columns
      assert_selector '.highcharts-series.highcharts-series-1 rect', count: 3

      # have 1 ':pending' column since all pending invoices
      # are accumulated in the last month
      assert_selector '.highcharts-series.highcharts-series-0 rect', count: 3
    end

    it 'renders vendors pie chart on top of invoices page' do
      create_default_invoices
      visit invoices_path
      assert_selector '#invoices-vendors-pie-chart .highcharts-series-group'
      assert_selector '.highcharts-series.highcharts-pie-series'
    end
  end
end
