# frozen_string_literal: true
require 'test_helper'

module Materials
  class MaterialPageTest < ActionDispatch::IntegrationTest
    let(:unit) { create :unit }
    let(:zone) { create :zone }
    let(:material) { create :material, unit: unit }
    let(:invoice) { create :invoice }

    before do
      inventory_item = create :inventory_item, inventoriable: material, zone: zone
      create :production_report, inventory_item: inventory_item
    end

    it 'displays Details tab by default' do
      visit material_path(material)
      assert_text 'Details'
      assert_text 'Ordering sizes'
    end

    it 'allows to switch to Forecast tab' do
      visit material_path(material)
      select_tab 'Forecast'
      assert_text 'Inventory and Forecast'
      assert_text 'Demand Details'
    end

    it 'allows to switch to Invoices tab' do
      material_container = create :material_container, material: material, unit: unit, lot_number: 'some_lot_#'
      create :invoice_item, invoice: invoice, material_container: material_container

      visit material_path(material)
      select_tab 'Invoices'
      assert_text invoice.number
      assert_text 'some_lot_#'
    end

    it 'allows to swtich to Containers tab' do
      container = create :material_container, material: material, unit: unit, lot_number: 'lot_#1', status: 'active'
      create :invoice_item, invoice: invoice, material_container: container

      visit material_path(material)
      select_tab 'Containers'
      within '.material-containers' do
        assert_text 'active'
        assert_text 'lot_#1'
        assert_text 'Edit'
      end
    end

    it 'allow to switch to Transactions tab' do
      container = create :material_container, material: material, unit: unit
      product = create :product, title: 'Test Product' 
      inventory_item = create :inventory_item, inventoriable: material
      create :ingredient, product: product, inventory_item: inventory_item
      batch = create :batch, product: product
      create :material_transaction, material_container: container, batch: batch

      visit material_path(material)
      select_tab 'Transactions'
      assert_text 'Make'
      assert_text 'Test Product'
    end

    it 'allows to switch to Graph tab' do
      visit material_path(material)
      select_tab 'Graph view'
      assert_text 'Material cost over time'
    end

    it 'allows to switch to Media tab' do
      visit material_path(material)
      select_tab 'Media'
      assert_text 'Upload'
    end

    private

    def select_tab(tab_name)
      within('#page-wrapper') { click_on(tab_name) }
    end
  end
end
