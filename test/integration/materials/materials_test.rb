# frozen_string_literal: true
require 'test_helper'

class MaterialsTest < ActionDispatch::IntegrationTest
  describe 'index page - inventory quantity column' do
    let(:old_qty) { 111.0 }
    before do
      material = create :material
      @inventory_item = create :inventory_item, inventoriable: material
      create :inventory, quantity: old_qty, inventory_item: @inventory_item
    end

    it 'is in-place editable' do
      new_qty = 222
      visit materials_path
      inventory_item_tr = inventory_item_tr(@inventory_item)

      within inventory_item_tr do
        edit_link = find('.js-edit-link')
        assert_equal old_qty.to_s, edit_link.text
        edit_link.click
        input = find('input')
        assert_equal old_qty, input.value.to_i

        input.set new_qty # fill_in equivalent
        assert page.has_text? new_qty
      end

      # check the value has been updated on the backend
      assert_equal new_qty, @inventory_item.reload.inventory_quantity
    end

    it 'processes validation' do
      visit materials_path
      inventory_item_tr = inventory_item_tr(@inventory_item)

      within inventory_item_tr do
        find('.js-edit-link').click
        input = find('input')
        input.set "-5000"
        assert page.has_content? "must be greater than or equal to 0"
      end

      assert_equal old_qty, @inventory_item.reload.inventory_quantity
    end
  end

  describe 'new action' do
    let(:make_zone) { create :zone, :make }
    let(:pack_zone) { create :zone, :pack }

    before do
      create :material_category
      create :unit
      Zone.delete_all
      make_zone
      pack_zone
    end

    it 'processes zone properly' do
      visit new_material_path

      # Check zone dropdown has exact list of options: Make (selected) and Pack. No blank.
      assert page.has_select?(zone_dropdown, options: %w(Make Pack))
      assert_zone_selected(make_zone)

      fill_in "material_name", with: "Olive oil"
      select "Pack", from: zone_dropdown

      assert_difference -> { Material.count }, 1 do
        assert_difference -> { InventoryItem.count }, 1 do
          assert_difference -> { Inventory.count }, 1 do
            click_on 'Save'
            assert page.has_content? "Material was successfully created"
          end
        end
      end

      new_material = Material.order(:id).last
      assert_equal pack_zone, new_material.production_zone
      assert_equal "Olive oil", new_material.name
    end

    it 'throws no errors on validation' do
      visit new_material_path
      click_on 'Save'
      assert page.has_content? "Name can't be blank"
    end
  end

  describe 'edit action' do
    let(:pack_zone) { create :zone, :pack }
    let(:ship_zone) { create :zone, :ship }
    let(:material) { create :material }

    before do
      Zone.delete_all
      pack_zone
      find_or_create_material_inventory_item(material, production_zone: ship_zone)
    end

    it 'processes zone properly' do
      visit edit_material_path(material)
      assert_zone_selected(ship_zone)
      select "Pack", from: zone_dropdown

      assert_no_difference -> { Material.count } do
        assert_no_difference -> { InventoryItem.count } do
          assert_no_difference -> { Inventory.count } do
            click_on 'Save'
            assert page.has_content? "Material was updated"
          end
        end
      end

      assert_equal pack_zone, material.reload.production_zone
    end

    describe 'edit conversion rate' do
      before do
        lbs_unit = create :unit
        ml_unit = create :unit
        material.update_columns(unit_id: lbs_unit.id, ingredient_unit_id: ml_unit.id)
        create :conversion_rate, from_unit: lbs_unit, to_unit: ml_unit, rate: 428.15
      end

      it 'can specify a custom conversion rate' do
        visit edit_material_path(material)
        rate_input = find('input#material_unit_conversion_rate')
        assert_equal '428.15', rate_input['placeholder']

        fill_in 'material_unit_conversion_rate', with: 555.777
        click_on 'Save'
        assert page.has_content?('Material was updated')
        assert_equal 555.777, material.reload.unit_conversion_rate
      end

      it 'can reset the rate to the default' do
        material.update_column(:unit_conversion_rate, 555.777)

        visit edit_material_path(material)
        rate_input = find('input#material_unit_conversion_rate')
        assert_equal '555.777', rate_input.value
        assert_equal '428.15', rate_input['placeholder']

        fill_in 'material_unit_conversion_rate', with: ''
        click_on 'Save'
        assert page.has_content?('Material was updated')
        assert_nil material.reload.unit_conversion_rate
      end
    end
  end

  private

  def inventory_item_tr(inventory_item)
    find("tr[data-inventory-item-id='#{inventory_item.to_param}']")
  end

  def assert_zone_selected(zone)
    assert_equal zone.id, find_field(zone_dropdown).value.to_i
  end

  def zone_dropdown
    "material_inventory_item_production_zone_id"
  end
end
