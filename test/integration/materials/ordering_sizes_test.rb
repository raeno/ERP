# frozen_string_literal: true

require 'test_helper'

module Materials
  class OrderingSizesTest < ActionDispatch::IntegrationTest
    include Logging
    let(:unit) { create :unit, :oz }
    let(:material) { create :material, unit: unit }

    before do
      find_or_create_material_inventory_item material
    end

    it 'allows adding ordering sizes' do
      visit material_edit_sizes_path(material)

      click_add_size
      last_size_name.set('Barrel')
      last_size_amount.set('100')
      last_size_unit.find(:option, unit.name).select_option

      click_add_size
      last_size_name.set('Can')
      last_size_amount.set('20')
      last_size_unit.find(:option, unit.name).select_option

      assert_difference -> { OrderingSize.count }, 2 do
        click_on 'Save'
        assert page.has_content? "Material was updated"
      end

      material_sizes = material.ordering_sizes.ordered
      can = material_sizes.first
      assert_equal unit, can.unit
      assert_equal 20, can.amount
      assert_equal 'Can', can.name

      barrel = material_sizes.last
      assert_equal unit, barrel.unit
      assert_equal 100, barrel.amount
      assert_equal 'Barrel', barrel.name
    end

    it 'allows removing ordering sizes' do
      create :ordering_size, material: material, amount: 5, unit: unit
      create :ordering_size, material: material, amount: 7, unit: unit

      visit material_edit_sizes_path(material)
      last_size_delete_button.click
      last_size_delete_button.click

      assert_difference -> { OrderingSize.count }, -2 do
        click_on 'Save'
        assert page.has_content? "Material was updated"
      end
    end

    private

    def click_add_size
      find(".add_nested_fields_link").click
    end

    def last_size_name
      last_size_row.find("input.size-name")
    end

    def last_size_amount
      last_size_row.find("input.size-amount")
    end

    def last_size_unit
      last_size_row.find("select.size-unit")
    end

    def last_size_delete_button
      last_size_row.find(".remove_nested_fields_link")
    end

    def last_size_row
      all(".nested_material_ordering_sizes").last
    end
  end
end
