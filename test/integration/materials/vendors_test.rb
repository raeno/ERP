# frozen_string_literal: true

require 'test_helper'

module Materials
  class VendorsTest < ActionDispatch::IntegrationTest
    include Logging
    let(:material) { create :material }
    let(:vendor1) { create :vendor, name: 'Vendor 1' }
    let(:vendor2) { create :vendor, name: 'Vendor 2' }

    before do
      vendor1
      vendor2
      find_or_create_material_inventory_item(material, production_zone: Zone.make_zone)
    end

    it 'allows adding vendors to a material' do
      visit edit_material_path material

      click_add_vendor
      last_vendor_name.find(:option, vendor1.name).select_option
      last_vendor_url.set('http://vendor1.example.com')

      click_add_vendor
      last_vendor_name.find(:option, vendor2.name).select_option
      last_vendor_url.set('http://vendor2.example.com')

      assert_difference -> { MaterialVendor.count }, 2 do
        click_on 'Save'
        assert page.has_content? "Material was updated"
      end

      vendors = material.reload.vendors
      assert vendors.include?(vendor1)
      assert vendors.include?(vendor2)

      first = material.material_vendors.order(:vendor_id).first
      assert_equal 'http://vendor1.example.com', first.url
    end

    it 'allows removing vendors' do
      create :material_vendor, material: material, vendor: vendor1

      visit edit_material_path material
      last_vendor_delete_button.click

      assert_difference -> { MaterialVendor.count }, -1 do
        click_on 'Save'
        assert page.has_content? "Material was updated"
      end
    end

    private

    def click_add_vendor
      find(".add_nested_fields_link").click
    end

    def last_vendor_name
      last_vendor_row.find("select.vendor")
    end

    def last_vendor_url
      last_vendor_row.find("input.vendor-url")
    end

    def last_vendor_delete_button
      last_vendor_row.find(".remove_nested_fields_link")
    end

    def last_vendor_row
      all(".nested_material_material_vendors").last
    end
  end
end
