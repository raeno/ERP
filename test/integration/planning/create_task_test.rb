# frozen_string_literal: true

require 'test_helper'

module Planning
  class CreateTaskTest < ActionDispatch::IntegrationTest
    let(:product) { create :product, :with_shipment_report, items_per_case: 111 }
    let(:inventory_item) { create :inventory_item, product: product, inventoriable: product }
    let(:production_report) { create :production_report, inventory_item: inventory_item, can_cover_optimal: 333 }
    before { production_report }

    describe 'on the Make page' do
      let(:user) { create :user, first_name: "Hanna", zones: [Zone.make_zone] }

      before do
        inventory_item.update_column(:zone_id, Zone.make_zone.id)
        user
      end

      it 'allows creation of a task' do
        visit planning_make_path
        click_link 'Assign'
        select user.display_name, from: 'task_user_id'
        assert_equal "333", find_field("task_quantity_in_zone_units").value
        fill_in "task_quantity_in_zone_units", with: "299"
        click_on 'Save'

        assert_text "Assigned"
        assert_text "299"

        new_task = Task.order(:id).last
        assert_equal 299, new_task.quantity
        assert_equal user.id, new_task.user_id
        assert_equal Time.zone.today, new_task.date
      end

      it 'displays an error when the record is invalid' do
        visit planning_make_path
        click_link 'Assign'
        select user.display_name, from: 'task_user_id'
        fill_in "task_quantity_in_zone_units", with: "0" # invalid value
        click_on 'Save'

        assert_text "Cannot create task. Record is invalid"

        fill_in "task_quantity_in_zone_units", with: "333"
        click_on 'Save'
        assert_text "Assigned"
      end
    end

    describe 'on the Pack page' do
      let(:user) { create :user, first_name: "Hanna", zones: [Zone.pack_zone] }

      before do
        inventory_item.update_column(:zone_id, Zone.pack_zone.id)
        user
      end

      it 'allows creation of a task' do
        visit planning_pack_path
        click_link 'Assign'
        select user.display_name, from: 'task_user_id'
        # 3 cases 111 units each, 333 units in total
        assert_equal "3", find_field("task_quantity_in_zone_units").value
        fill_in "task_quantity_in_zone_units", with: "4"
        click_on 'Save'

        assert_text "Assigned"

        new_task = Task.order(:id).last
        assert_equal 444, new_task.quantity
        assert_equal user.id, new_task.user_id
        assert_equal Time.zone.today, new_task.date
      end

      it 'displays an error when the record is invalid' do
        visit planning_pack_path
        click_link 'Assign'
        select user.display_name, from: 'task_user_id'
        fill_in "task_quantity_in_zone_units", with: "0" # invalid value
        click_on 'Save'

        assert_text "Cannot create task. Record is invalid"

        fill_in "task_quantity_in_zone_units", with: "1"
        click_on 'Save'
        assert_text "Assigned"
      end
    end
  end
end
