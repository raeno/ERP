# frozen_string_literal: true

require 'test_helper'

module Planning
  class DateFilterTest < ActionDispatch::IntegrationTest
    let(:inventory_item) { create :inventory_item, :for_product_with_shipment_report, :with_production_report }

    let(:task1) do
      create :task,
        user: current_user, inventory_item: inventory_item,
        date: 5.days.ago.to_date, quantity: 1_111
    end

    let(:task2) do
      create :task,
        user: current_user, inventory_item: inventory_item,
        date: 10.days.from_now.to_date, quantity: 2_222
    end

    let(:task3) do
      create :task,
        user: current_user, inventory_item: inventory_item,
        date: 30.days.from_now.to_date, quantity: 3_333
    end

    before do
      task1
      task2
      task3
    end

    it 'filters cards by date' do
      visit current_tasks_path
      assert_text    "1,111"
      assert_text    "2,222"
      assert_no_text "3,333"

      find("input.js-start-date").set(1.day.from_now.to_date)
      assert_no_text "1,111"
      assert_text    "2,222"
      assert_no_text "3,333"

      find("input.js-end-date").set('')
      assert_no_text "1,111"
      assert_text    "2,222"
      assert_text    "3,333"

      find("input.js-start-date").set('')
      assert_text "1,111"
      assert_text "2,222"
      assert_text "3,333"
    end
  end
end
