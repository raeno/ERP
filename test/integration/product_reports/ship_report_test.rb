# frozen_string_literal: true
require 'test_helper'

module ProductReports
  class ShipReportTest < ActionDispatch::IntegrationTest
    before { Zone.delete_all }
    let(:make_zone) { create :zone, :make }
    let(:pack_zone) { create :zone, :pack }
    let(:ship_zone) { create :zone, :ship }

    let(:lavender_oil) { create_product_in_zones ship_zone, items_per_case: 150, internal_title: "Lavender Oil" }
    let(:lemon_oil)    { create_product_in_zones ship_zone, items_per_case: 150, internal_title: "Lemon Oil" }

    let(:abc_warehouse) { create :fba_warehouse, name: "ABC" }
    let(:xyz_warehouse) { create :fba_warehouse, name: "XYZ" }
    let(:lavender_abc_allocation) { create :fba_allocation, product: lavender_oil, fba_warehouse: abc_warehouse }
    let(:lavender_xyz_allocation) { create :fba_allocation, product: lavender_oil, fba_warehouse: xyz_warehouse }
    let(:lemon_xyz_allocation)    { create :fba_allocation, product: lemon_oil,    fba_warehouse: xyz_warehouse }

    before do
      [make_zone, pack_zone, ship_zone,
       lavender_abc_allocation, lavender_xyz_allocation, lemon_xyz_allocation]

      create :shipment_report, product: lavender_oil
      create :shipment_report, product: lemon_oil

      add_user_to_all_zones(current_user)
    end

    describe 'case switcher' do
      it 'set to case by default' do
        visit_ship_page
        assert case_view?
      end

      it 'switches between item/case view' do
        visit_ship_page

        switch_to_item_view
        assert item_view?

        switch_to_case_view
        assert case_view?
      end
    end

    describe 'shipment name' do
      it 'is hidden when no warehouse selected' do
        visit_ship_page
        assert page.has_no_selector?(".new-shipment")
      end

      it 'is auto populated upon warehouse select' do
        visit_ship_page
        date_prefix = Time.zone.now.utc.strftime('%Y/%0m/%-d')

        select_warehouse(abc_warehouse.name)
        field_text = shipment_name_field.value
        expected_text = "#{date_prefix} #{abc_warehouse.name}"
        assert_equal expected_text, field_text

        select_warehouse(xyz_warehouse.name)
        field_text = shipment_name_field.value
        expected_text = "#{date_prefix} #{xyz_warehouse.name}"
        assert_equal expected_text, field_text
      end
    end

    protected

    def select_all_products_checkbox
      find(".js-select-all").set(true)
    end

    def unselect_all_products_checkbox
      find(".js-select-all").set(false)
    end

    def product_cheked?(product)
      product_tr(product).find("input[type='checkbox']").checked?
    end

    def select_warehouse(option_text)
      find(".js-warehouse").find(:option, option_text).select_option
    end

    def product_tr(product)
      find("tr[data-product-id='#{product.id}']")
    end

    def product_visible?(product)
      within 'table' do
        assert page.has_content? product.internal_title
      end
    end

    def product_hidden?(product)
      within 'table' do
        page.has_no_content? product.internal_title
      end
    end

    def switch_to_case_view
      find(".items-cases-selector").click
      within('.items-cases-selector') do
        click_on 'Cases'
      end
    end

    def switch_to_item_view
      find(".items-cases-selector").click
      within('.items-cases-selector') do
        click_on 'Units'
      end
    end

    def case_view?
      within "table thead" do
        page.has_content? "Cases to ship"
      end
    end

    def item_view?
      within "table thead" do
        page.has_content? "Units to ship"
      end
    end

    def shipment_name_field
      find(".new-shipment")
    end
  end
end
