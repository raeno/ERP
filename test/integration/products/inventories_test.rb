# frozen_string_literal: true
require 'test_helper'

module Products
  class InventoriesTest < ActionDispatch::IntegrationTest
    describe "for a gift box" do
      let(:make_zone) { build :zone, :make }
      let(:pack_zone) { build :zone, :pack }
      let(:giftbox) { create :product, zones: [pack_zone] }
      let(:inner_product) { create :product, internal_title: "Lavender Oil" }

      before do
        create :unit, :each

        product_ii = find_or_create_product_inventory_item inner_product,
          passed_zone: make_zone, production_zone: pack_zone, with_report: true

        find_or_create_product_inventory_item giftbox,
          passed_zone: pack_zone, with_report: true

        include_inventory_item_to_product(inventory_item: product_ii, product: giftbox)
      end

      it 'does not throw any errors, and contains inner product name' do
        visit product_inventory_path(product_id: giftbox.id)

        within '.show-product-ingredients table' do
          assert page.has_content? "Lavender Oil"
        end
      end
    end
  end
end
