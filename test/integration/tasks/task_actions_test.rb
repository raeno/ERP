# frozen_string_literal: true

require 'test_helper'

module Tasks
  class TaskActionsTest < ActionDispatch::IntegrationTest
    let(:inventory_item) { create :inventory_item, :for_product_with_shipment_report, :with_production_report }
    let(:task) do
      create :task, user: current_user, inventory_item: inventory_item, date: Time.zone.today, duration_estimate: 1000
    end
    before { task }

    it 'allows starting a task' do
      visit current_tasks_path
      click_on 'Start'
      assert_text "FNSKU label"
      assert task.reload.started?
    end

    it 'allows cancelling a started task' do
      task.update_column(:started_at, Time.zone.now)

      visit current_tasks_path
      click_on 'Cancel'
      assert_text "Work on the task was cancelled"
      assert_equal current_tasks_path, current_path
      refute task.reload.started?
    end

    it 'allows finishing a started task' do
      task.update_column(:started_at, Time.zone.now)

      visit current_tasks_path
      click_on 'Finish'

      assert_equal new_batch_path, current_path
      click_on 'Save'
      assert_text "Batch successfully created"

      assert task.reload.finished?
      assert task.batch
    end
  end
end
