require 'test_helper'

class PullAmazonShipmentsJobTest < ActiveJob::TestCase

  subject { described_class.new }

  before do
    fetcher = stub(call: success_result)
    subject.stubs(:shipments_fetcher).returns(fetcher)
    Timecop.freeze
  end

  it 'by default pulls shipments for last 60 days using amazon fetcher' do
    amazon_fetcher = stub
    amazon_fetcher.expects(:call).once.with(start_date: 60.days.ago)
    subject.stubs(:shipments_fetcher).returns(amazon_fetcher)

    subject.perform
  end

  it 'uses passed interval to limit shipments range' do
    amazon_fetcher = stub
    amazon_fetcher.expects(:call).once.with(start_date: 20.days.ago)
    subject.stubs(:shipments_fetcher).returns(amazon_fetcher)

    subject.perform days_to_fetch: 20
  end
end
