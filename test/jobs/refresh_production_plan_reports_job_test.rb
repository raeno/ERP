# frozen_string_literal: true
require 'test_helper'

class RefreshProductionPlanReportsJobTest < ActiveJob::TestCase
  subject { RefreshProductionPlanReportsJob.new }
  let(:product) { create :product }
  let(:inventory_item) { create :inventory_item, inventoriable: product }

  describe "#dependent_inventoriables" do
    it 'includes the inventoriable itself' do
      inventoriable = inventory_item.inventoriable
      assert inventoriable

      collection = subject.send(:dependent_inventoriables, inventory_item)
      assert collection.include?(inventoriable)
    end

    it 'includes parent products' do
      giftbox = create :product
      include_product_to_giftbox(
        product: product, giftbox_product: giftbox,
        production_zone: inventory_item.zone, passed_zone: inventory_item.zone
      )

      collection = subject.send(:dependent_inventoriables, inventory_item)
      assert collection.include?(giftbox)
    end

    it 'includes ingredients of all zones' do
      material = create :material
      any_zone = create(:zone)
      include_material_to_product(material: material, product: product, production_zone: any_zone)

      collection = subject.send(:dependent_inventoriables, inventory_item)
      assert collection.include?(material)
    end
  end

  describe "#perform" do
    it 'calls refreshing actions for the #dependent_inventoriables' do
      material = create :material

      subject.stubs(:dependent_inventoriables).returns([product, material])
      RefreshProductReportsJob.expects(:perform_later).with(product)
      RefreshMaterialReportsJob.expects(:perform_later).with(material)

      subject.perform inventory_item
    end
  end
end
