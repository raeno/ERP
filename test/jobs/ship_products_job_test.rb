# frozen_string_literal: true
require 'test_helper'

class ShipProductsJobTest < ActiveJob::TestCase

  let(:amazon_shipment) { create :amazon_shipment }
  subject { described_class.new amazon_shipment }

  it 'calls ship action with deserialized data' do
    action = mock
    action.expects(:call).with(amazon_shipment).once

    subject.stubs(:ship_action).returns(action)
    subject.perform_now
  end

  it 'enqueues job to pull shipment status from Amazon' do
    refresh_shipments_job = stub
    refresh_shipments_job.expects(:perform_later).with(days_to_fetch: 2).once

    subject.stubs(refresh_shipments_job: refresh_shipments_job)
    subject.perform amazon_shipment
  end
end
