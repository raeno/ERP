require 'test_helper'

class UpdateExistingShipmentJobTest < ActiveJob::TestCase

  let(:amazon_shipment) { build :amazon_shipment }
  subject { described_class.new }

  let(:refresh_shipments_job) { stub(perform_later: success_result) }
  let(:update_shipment_action) { stub(call: success_result) }

  it 'updates amazon shipment' do
    update_shipment_action = stub
    update_shipment_action.expects(:call).once.returns(success_result)
    subject.stubs(update_shipment_action: update_shipment_action)
    subject.stubs(refresh_shipments_job: refresh_shipments_job)

    subject.perform amazon_shipment
  end

  it 'enqueues job to pull shipment status from Amazon' do
    refresh_shipments_job = stub
    refresh_shipments_job.expects(:perform_later).with(days_to_fetch: 2).once
    subject.stubs(refresh_shipments_job: refresh_shipments_job)
    subject.stubs(update_shipment_action: update_shipment_action)

    subject.perform amazon_shipment
  end
end
