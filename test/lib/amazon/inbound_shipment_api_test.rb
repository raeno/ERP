# frozen_string_literal: true
require 'test_helper'

module Amazon
  class InboundShipmentApiTest < ActiveSupport::TestCase
    describe '.build' do
      it 'builds products api instance' do
        amazon_config = {}
        api = described_class.build amazon_config
        assert_kind_of described_class, api
      end
    end

    describe '#create_shipment_plan' do
      let(:result) { [{ shipment_id: 123, size: 10 }] }
      let(:response) { stub(parse: result) }
      let(:items) { [{ sku: 12_345, quantity: 5 }] }
      let(:inbound_shipment_client) { stub(create_inbound_shipment_plan: response) }

      it 'requests external API for create shipment plan' do
        inbound_shipment_client.expects(:create_inbound_shipment_plan).with(anything, items).returns(response)
        api = described_class.new(inbound_shipment_client)
        api.create_shipment_plan items
      end

      it 'parses received response and returns result' do
        api = described_class.new(inbound_shipment_client)
        info = api.create_shipment_plan items
        assert_equal result, info
      end
    end

    describe '#fetch_shipments' do
      let(:shipments_response) { { 'ShipmentData' => stub } }
      let(:response_with_parser) { stub(parse: shipments_response) }
      let(:inbound_shipment_client) { stub(list_inbound_shipments: shipments_response) }

      it 'fetches inbound shipments via Amazon client' do
        inbound_shipment_client.expects(:list_inbound_shipments).returns(response_with_parser)
        api = described_class.new inbound_shipment_client
        api.fetch_shipments
      end

      it 'fetches only active shipments if required statuses not specified' do
        inbound_shipment_client.expects(:list_inbound_shipments)
                               .returns(response_with_parser)
                               .with do |params|
          statuses = params[:shipment_status_list]
          statuses == described_class::ACTIVE_SHIPMENT_STATUSES
        end
        api = described_class.new inbound_shipment_client
        api.fetch_shipments
      end
    end

    describe '#create_shipment' do
      let(:amazon_data) { { shipment_id: 123, size: 10 } }
      let(:response) { stub(parse: amazon_data) }
      let(:items) { [{ sku: 12_345, quantity: 5 }] }
      let(:shipment_id) { 12_345 }
      let(:inbound_shipment_header) { {} }
      let(:inbound_shipment_client) { stub(create_inbound_shipment: response) }

      it 'requests external API for create shipment' do
        inbound_shipment_client.expects(:create_inbound_shipment).with(shipment_id, inbound_shipment_header, inbound_shipment_items: items).returns(response)
        api = described_class.new(inbound_shipment_client)
        api.create_shipment shipment_id, inbound_shipment_header, items
      end

      it 'returns successful result' do
        api = described_class.new(inbound_shipment_client)
        result = api.create_shipment shipment_id, inbound_shipment_header, items
        assert result.success?
      end

      it 'has parsed response in the result' do
        api = described_class.new(inbound_shipment_client)
        result = api.create_shipment shipment_id, inbound_shipment_header, items
        info = result.parsed_response
        assert_equal amazon_data, info
      end
    end

    describe '#update_shipment' do
      let(:parsed_response) { { shipment_id: 123 } }
      let(:response) { stub(parse: parsed_response) }
      let(:items) { [sku: 123, quantity: 10] }
      let(:shipment_id) { 123 }
      let(:header) { Object.new }
      let(:shipment_client) { stub(update_inbound_shipment: response) }

      it 'requests Amazon API to update existing shipment' do
        shipment_client.expects(:update_inbound_shipment).with(shipment_id, header, inbound_shipment_items: items).returns(response)
        api = described_class.new shipment_client
        api.update_shipment shipment_id, header, items
      end

      it 'parses received response and returns result' do
        api = described_class.new(shipment_client)
        result = api.update_shipment shipment_id, header, items
        assert result.success?
        assert_equal parsed_response, result.parsed_response
      end
    end
  end
end
