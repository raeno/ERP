# frozen_string_literal: true
require 'test_helper'

module Amazon
  module Parsers
    class AddressParserTest < ActiveSupport::TestCase
      let(:amazon_response) do
        { 'Name' => 'Factory', 'AddressLine1' => 'Some Address',
          'City' => 'LA', 'StateOrProvinceCode' => 'CA',
          'PostalCode' => '10023', 'CountryCode' => 'US' }
      end

      subject { described_class.new }

      describe '#parse' do
        it 'builds correct address from amazon parsed response' do
          address = subject.parse  amazon_response
          assert_kind_of Amazon::Structs::Address, address
          assert_equal 'Factory', address.name
          assert_equal 'Some Address', address.address_line_1
          assert_equal 'LA', address.city
          assert_equal 'CA', address.state_or_province_code
          assert_equal '10023', address.postal_code
          assert_equal 'US', address.country_code
        end
      end
    end
  end
end
