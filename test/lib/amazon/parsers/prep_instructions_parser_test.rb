# frozen_string_literal: true
require 'test_helper'

module Amazon
  module Parsers
    class PrepInstructionsParserTest < ActiveSupport::TestCase
      subject { described_class.new }

     def prep_instructions_info
        {
          "SKUPrepInstructionsList" => {
            "SKUPrepInstructions" => [
              {
                "SellerSKU" => "KIS-30ML-1234",
                "PrepInstructionList" => {
                  "PrepInstruction" => [
                    "Labeling",
                    "Boxing"
                  ]
                }
              },
              {
                "SellerSKU" => 'KIS-10ML-1234',
                "PrepInstructionList" => {
                  "PrepInstruction" => [
                    "Boxing"
                  ]
                }
              }
            ]
          }
        }
      end

      def expected_details_per_sku
        {
          "KIS-30ML-1234" => {
            "PrepDetails.1.PrepInstruction" => "Labeling",
            "PrepDetails.1.PrepOwner" => "SELLER",
            "PrepDetails.2.PrepInstruction" => "Boxing",
            "PrepDetails.2.PrepOwner" => "SELLER"
          },
          "KIS-10ML-1234" => {
            "PrepDetails.1.PrepInstruction" => "Boxing",
            "PrepDetails.1.PrepOwner" => "SELLER",
          }
        }
      end

      it 'correctly parses prep details list' do
        result = subject.parse(prep_instructions_info)
        assert_equal expected_details_per_sku, result
      end

      it 'parses prep details list with wrong response' do
        result = subject.parse nil
        blank_result = {}
        assert_equal blank_result, result
      end
    end
  end
end
