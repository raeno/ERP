require 'test_helper'

module Amazon
  module Parsers
    class ShipmentParserTest < ActiveSupport::TestCase

      # Amazon also has ShipFromAddress and few other fields in the
      # response that we don't use at the moment.
      # that's why we don't parse them in parser and don't test them here
      #
      # When Amazon has more than 50 elements, API has to fetch them in chunks by 50. All responses are grouped in array 
      AMAZON_RESPONSES_EXAMPLE = [
        {
          "ShipmentData" => {
            "member" => [
              {
                "ShipmentId" => 'ABCD',
                "AreCasesRequired" => false,
                "ShipmentName" => "ABCD Shipment",
                "DestinationFulfillmentCenterId" => "ABC1",
                "LabelPrepType" => 'BOXING',
                "ShipmentStatus" => 'RECEIVING',
                'BoxContentSource' => 'INTERACTIVE'
              }
              # can be more elements here, up to 50 in real response 
            ]
          }
        },
        {
          "ShipmentData" => {
            "member" => [
              {
                "ShipmentId" => 'XYZ',
                'AreCasesRequired' => true,
                'ShipmentName' => 'XYZ Shipment',
                'DestinationFulfillmentCenterId' => 'KLM2',
                'LabelPrepType' => '',
                'ShipmentStatus' => 'WORKING',
                'BoxContentSource' => 'INTERACTIVE'
              }
            ]
          }
        }
      ].freeze

      EXPECTED_PARSED_SHIPMENTS = [
        {
          shipment_id: 'ABCD', cases_required: false,
          name: 'ABCD Shipment', fba_warehouse: 'ABC1',
          label_prep_type: 'BOXING', status: 'RECEIVING',
          box_content_source: 'INTERACTIVE'
        },
        {
          shipment_id: 'XYZ', cases_required: true,
          name: 'XYZ Shipment', fba_warehouse: 'KLM2',
          label_prep_type: '', status: 'WORKING',
          box_content_source: 'INTERACTIVE'
        }
      ].freeze

      subject { described_class.new }

      describe '#parse' do
        it 'parses all shipments from Amazon response' do
          parsed = subject.parse AMAZON_RESPONSES_EXAMPLE
          assert_equal EXPECTED_PARSED_SHIPMENTS, parsed
        end

        it 'returns empty array when ShipmentData is empty' do
          empty_data = { "ShipmentData" => nil }
          parsed = subject.parse empty_data
          assert_equal [], parsed
        end
      end
    end
  end
end
