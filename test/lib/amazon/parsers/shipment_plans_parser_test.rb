# frozen_string_literal: true
require 'test_helper'

module Amazon
  module Parsers
    class ShipmentPlansParserTest < ActiveSupport::TestCase
      def shipment_plans_info
        {
          "InboundShipmentPlans" => {
            "member" => [
              {
                "DestinationFulfillmentCenterId" => "PHX7",
                "LabelPrepType" => "SELLER_LABEL",
                "ShipToAddress" => {
                  "City" => "city",
                  "CountryCode" => "US",
                  "PostalCode" => "12345",
                  "Name" => "name",
                  "AddressLine1" => "address line 1",
                  "StateOrProvinceCode" => "CA"
                },
                "Items" => {
                  "member" => [
                    {
                      "FulfillmentNetworkSKU" => "test",
                      "Quantity" => 50,
                      "SellerSKU" => "12345",
                      "PrepDetailsList" => {
                        "PrepDetails" => [
                          "PrepOwner" => "SELLER",
                          "PrepInstruction" => "Labeling"
                        ]
                      }
                    }
                  ]
                },
                "ShipmentId" => "12345"
              }
            ]
          }
        }
      end

      def ship_address
        OpenStruct.new city: "city", country_code: "US",
                       postal_code: "12345", name: "name",
                       address_line1: "address line 1",
                       state_or_province_code: "CA"
      end

      def expected_shipment_plans_result
        result = OpenStruct.new(
          destination_fulfillment_center_id: "PHX7",
          label_prep_type: "SELLER_LABEL",
          ship_to_address: ship_address,
          items: [
            OpenStruct.new(
              fulfillment_network_sku: "test",
              quantity: 50,
              seller_sku: "12345",
              prep_details_list: [
                OpenStruct.new(prep_owner: "SELLER",
                               prep_instruction: "Labeling")
              ]
            )
          ],
          shipment_id: "12345"
        )
        ShipmentPlansResult.new([result], true, [])
      end

      let(:address_parser) { stub(parse: ship_address) }
      subject { described_class.new(address_parser) }

      it 'correctly parses inbound shipment plans' do
        result = subject.parse shipment_plans_info
        assert result.success?
        assert_equal expected_shipment_plans_result.entities, result.entities
        assert_equal '', result.error_message
      end

      it 'parses inbound shipment plans with wrong response' do
        result = subject.parse nil
        refute result.success?
        assert_equal 'Invalid response in creating shipment plan', result.error_message
      end
    end
  end
end
