# frozen_string_literal: true
require 'test_helper'

module Amazon
  module Structs
    class BaseStructTest < ActiveSupport::TestCase
      before do
        described_class.const_set 'ATTRIBUTES', [:shipment_name, :shipment_status,
                                                 :are_cases_required, :box_content_source]
      end

      describe '#initialize' do
        it 'creates header from keys in hash' do
          hash = { shipment_name: 'Test', shipment_status: 'working', are_cases_required: true }
          header = described_class.new hash
          assert_equal 'Test', header.shipment_name
          assert_equal 'working', header.shipment_status
          assert header.are_cases_required
        end

        it 'uses only keys listed as attributes' do
          hash = { shipment_name: 'Test', some_other_attr: 'Boo' }
          header = described_class.new hash
          assert_equal 'Test', header.shipment_name
          assert_nil header.some_other_attr
        end
      end

      describe '#to_h' do
        it 'presents all non-nil fields as hash' do
          header = described_class.new shipment_name: '123', shipment_status: 'ready',
                                       are_cases_required: nil
          header.box_content_source = '2D_BARCODE'
          expected_hash = { shipment_name: '123', shipment_status: 'ready',
                            box_content_source: '2D_BARCODE' }
          assert_equal expected_hash, header.to_h
        end
      end
    end
  end
end
