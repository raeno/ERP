# frozen_string_literal: true
require 'test_helper'

module Amazon
  module Structs
    class InboundShipmentHeaderTest < ActiveSupport::TestCase
      describe '.build' do

        let(:fba_warehouse) { create :fba_warehouse, name: 'ABC1' }
        let(:amazon_shipment) { create :amazon_shipment, fba_warehouse: fba_warehouse, cases_required: false }
        let(:shipment_address) { build :address }

        subject { described_class.build amazon_shipment, shipment_address }

        it 'builds instance of inbound shipmen header from params' do
          instance = described_class.build amazon_shipment, shipment_address
          assert_kind_of described_class, instance
        end

        it 'fills destination fulfillment center id from amazon shipment' do
          assert_equal 'ABC1', subject.destination_fulfillment_center_id
        end

        it 'fetches cases requirement from amazon shipment' do
          refute subject.cases_required?
        end
      end
    end
  end
end
