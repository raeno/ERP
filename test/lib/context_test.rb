# frozen_string_literal: true
require 'test_helper'

class ContextTest < ActiveSupport::TestCase
  describe '#empty?' do
    it 'is true when context has no info stored' do
      context = described_class.new
      assert context.empty?, 'New context should be empty'
      context.some_info = 'boo'
      refute context.empty?, 'Context with some info should not be empty'
    end
  end
end
