# frozen_string_literal: true
require 'test_helper'

class FilestackApiTest < ActiveSupport::TestCase
  let(:file_handler) { 'abcdefgh' }
  let(:url) { "http://cdn.filestack.com/some_params/#{file_handler}" }
  subject { described_class.new url }

  describe '#preview_url' do
    it 'generates preview url for image of default height' do
      expected = 'https://www.filestackapi.com/api/file/abcdefgh/convert?format=jpg&h=150'
      assert_equal expected, subject.preview_url
    end

    it 'is nil when url is empty' do
      api = described_class.new ''
      assert_nil api.preview_url
    end

    describe 'when height specified' do
      it 'generates preview url to image of given height' do
        expected = 'https://www.filestackapi.com/api/file/abcdefgh/convert?format=jpg&h=300'
        assert_equal expected, subject.preview_url(height: 300)
      end
    end
  end

  describe 'viewer_url' do
    it 'generates url to filestack viewer' do
      expected = 'https://www.filestackapi.com/api/preview/abcdefgh'
      assert_equal expected, subject.viewer_url
    end

    it 'is nil when url is empty' do
      api = described_class.new ''
      assert_nil api.preview_url
    end
  end
end
