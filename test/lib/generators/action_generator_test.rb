require 'test_helper'
require 'generators/action/action_generator'

class ActionGeneratorTest < Rails::Generators::TestCase
  tests ActionGenerator
  destination Rails.root.join('tmp/generators')
  setup :prepare_destination

  it 'runs without errors' do
    assert_nothing_raised do
      run_generator ['products/create_product']
    end
  end

  it 'creates action file under given namespace' do
    run_generator ['products/create_product']
    assert_file 'app/actions/products/create_product.rb', /call/
  end

  it 'creates test for created action' do
    run_generator ['products/create_product']
    assert_file 'test/actions/products/create_product_test.rb', /test_helper/
  end
end
