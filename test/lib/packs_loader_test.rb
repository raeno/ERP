# frozen_string_literal: true

require 'test_helper'

class PacksLoaderTest < ActiveSupport::TestCase
  describe '.lookup' do
    it 'returns PacksRegistry.products for product paths' do
      assert_routes '/products/inventories', PacksRegistry.products
      assert_routes '/products/134/edit_ingredients', PacksRegistry.products
    end

    it 'returns PacksRegistry.shipping for shipping paths' do
      assert_routes '/product_reports/ship', PacksRegistry.shipping
    end

    it 'returns PacksRegistry.planning for planning paths' do
      assert_routes '/planning/make', PacksRegistry.planning
      assert_routes '/planning/pack', PacksRegistry.planning
    end

    it 'returns PacksRegistry.tasks for task card paths' do
      assert_routes '/tasks/make', PacksRegistry.tasks
      assert_routes '/tasks/pack', PacksRegistry.tasks
    end

    it 'returns PacksRegistry.task for task#show paths' do
      assert_routes '/tasks/1', PacksRegistry.task
    end
  end

  def assert_routes(path, *expected_routes)
    matched_routes = PacksLoader.lookup(path)
    assert expected_routes.count, matched_routes.count
    assert_include matched_routes, *expected_routes
  end
end
