# frozen_string_literal: true
require 'test_helper'

class ShipmentApiResultTest < ActiveSupport::TestCase
  describe '#success?'  do
    it 'depends on success param in constructor' do
      instance = described_class.new(success: false, response: 1)
      refute instance.success?
      instance = described_class.new(success: true, response: 1)
      assert instance.success?
    end
  end

  describe '#parsed_response' do
    it 'is nil if response is nil' do
      instance = described_class.new(success: true, response: nil)
      assert_nil instance.parsed_response
    end

    it 'returns parsed response' do
      response = stub(parse: 'some_response')
      instance = described_class.new(success: true, response: response)
      assert_equal 'some_response', instance.parsed_response
    end
  end

  describe '#errors' do
    let(:response) { stub(parse: {}) }

    it 'is empty for successful result' do
      instance = described_class.new(response: response, success: true, errors: 'some_error')
      assert_equal [], instance.errors
    end

    it 'accepts single error passed instead of array' do
      instance = described_class.new(success: false, response: response, errors: 'some_error')
      assert_equal ['some_error'], instance.errors
    end

    it 'contains errors added via constructor' do
      response = stub(parse: {})
      instance = described_class.new(response: response, success: false, errors: %w(error_1 error_2))
      assert_equal %w(error_1 error_2), instance.errors
    end

    it 'adds parsed error message from amazon response' do
      parsed = { 'Message' => 'Shipment is locked' }
      response = stub(parse: parsed)
      instance = described_class.new(response: response, success: false, errors: 'Failed to ship')
      assert_equal ['Failed to ship', 'Shipment is locked'], instance.errors
    end
  end
end
