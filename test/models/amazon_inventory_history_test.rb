# frozen_string_literal: true
# == Schema Information
#
# Table name: amazon_inventory_histories
#
#  id                  :integer          not null, primary key
#  fulfillable         :integer
#  reserved            :integer
#  inbound_working     :integer
#  inbound_shipped     :integer
#  reported_at         :datetime
#  amazon_inventory_id :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

require 'test_helper'

class AmazonInventoryHistoryTest < ActiveSupport::TestCase
  let(:amazon_inventory) { create :amazon_inventory }

  def create_history(params = {})
    params = { amazon_inventory: amazon_inventory }.merge(params)
    create :amazon_inventory_history, params
  end

  describe '.smooth_reserved' do
    it 'smoothes new value by calculating average with last 2 entries' do
      create_history reserved: 100
      create_history reserved: 200
      smoothed_value = described_class.smooth_reserved amazon_inventory, 500
      assert_equal (100 + 200 + 500) / 3.to_f, smoothed_value
    end

    it 'uses histories that belongs to selected amazon inventory' do
      another_inventory = create :amazon_inventory
      create_history reserved: 100
      create_history reserved: 300, amazon_inventory: another_inventory
      create_history reserved: 400
      create_history reserved: 600, amazon_inventory: another_inventory

      smoothed = described_class.smooth_reserved amazon_inventory, 700
      assert_equal (100 + 400 + 700) / 3.to_f, smoothed
    end

    it 'tries to convert provided value to number' do
      create_history reserved: 100
      create_history reserved: 200
      smoothed_value = described_class.smooth_reserved amazon_inventory, '123.5'
      assert_equal (100 + 200 + 123.5) / 3.to_f, smoothed_value
    end

    describe 'when history is not available' do
      it 'returns the value itself' do
        smoothed_value = described_class.smooth_reserved amazon_inventory, 500
        assert_equal 500, smoothed_value
      end
    end
  end

  describe '.old_histories' do
    it 'gets old history ids' do
      inventories = create_list :amazon_inventory, 2
      old_histories = inventories.map do |inventory|
        create_list :amazon_inventory_history, 2, amazon_inventory: inventory
      end.flatten
      inventories.map do |inventory|
        create_list :amazon_inventory_history, 2, amazon_inventory: inventory
      end
      assert_equal old_histories, described_class.old_histories
    end

    it 'is empty list when no old histories exist' do
      inventories = create_list :amazon_inventory, 2
      inventories.map do |inventory|
        create_list :amazon_inventory_history, 2, amazon_inventory: inventory
      end
      assert_equal [], described_class.old_histories
    end
  end
end
