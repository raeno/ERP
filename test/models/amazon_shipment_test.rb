# == Schema Information
#
# Table name: amazon_shipments
#
#  id               :integer          not null, primary key
#  shipment_id      :text
#  name             :text
#  status           :string
#  cases_required   :boolean
#  fba_warehouse_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  user_id          :integer
#

require 'test_helper'

class AmazonShipmentTest < ActiveSupport::TestCase
  describe '#initialized?' do
    it 'is truthy when status equals to "INITIALIZED"' do
      shipment = described_class.new status: 'INITIALIZED'
      assert shipment.initialized?
      shipment.status = 'AMAZON_SYNCED'
      refute shipment.initialized?
    end
  end

  describe '#synced?' do
    it 'is truthy when status equals to "AMAZON_SYNCED"' do
      shipment = described_class.new status: 'AMAZON_SYNCED'
      assert shipment.synced?
      shipment.status = 'FINISHED'
      refute shipment.synced?
    end
  end
end
