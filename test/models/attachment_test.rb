# frozen_string_literal: true
# == Schema Information
#
# Table name: attachments
#
#  id         :integer          not null, primary key
#  url        :text             not null
#  mimetype   :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'test_helper'

class AttachmentTest < ActiveSupport::TestCase
  let(:url) { 'http://som_url.com' }
  subject { described_class.new(url: url) }

  describe '#preview_url' do
    it 'prepairs preview url from url' do
      FilestackApi.any_instance.stubs(:preview_url).returns("preview_url")
      assert_equal 'preview_url', subject.preview_url
    end

    it 'caches prepaired url for future use' do
      FilestackApi.any_instance.expects(:preview_url).once.returns('preview_url')
      3.times.each { subject.preview_url }
    end
  end

  describe '#viewer_url' do
    it 'prepairs viewer url from url' do
      FilestackApi.any_instance.stubs(:viewer_url).returns("viewer_url")
      assert_equal 'viewer_url', subject.viewer_url
    end

    it 'caches prepaired url for future use' do
      FilestackApi.any_instance.expects(:viewer_url).once.returns('viewer_url')
      3.times.each { subject.viewer_url }
    end
  end

  describe '#image?' do
    it 'checks whether attachment has image type' do
      image_attachment = described_class.new mimetype: 'image/png'
      assert image_attachment.image?

      pdf_attachment = described_class.new mimetype: 'application/pdf'
      refute pdf_attachment.image?
    end

    it 'is false when mimetype not defined' do
      unknown_attachment = described_class.new mimetype: nil
      refute unknown_attachment.image?
    end
  end

  describe '#pdf?' do
    it 'checks whether attachment has pdf type' do
      image_attachment = described_class.new mimetype: 'image/png'
      refute image_attachment.pdf?

      pdf_attachment = described_class.new mimetype: 'application/pdf'
      assert pdf_attachment.pdf?
    end

    it 'is false when mimetype not defined' do
      unknown_attachment = described_class.new mimetype: nil
      refute unknown_attachment.pdf?
    end
  end
end
