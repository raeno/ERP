# frozen_string_literal: true
# == Schema Information
#
# Table name: batches
#
#  id                 :integer          not null, primary key
#  product_id         :integer          not null
#  quantity           :integer          not null
#  notes              :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  completed_on       :date
#  zone_id            :integer          not null
#  auxillary_info     :hstore
#  task_id            :integer
#  amazon_shipment_id :integer
#

require 'test_helper'

class BatchTest < ActiveSupport::TestCase
  describe '#shipment_id' do
    it 'gets shipment id from auxillary info' do
      batch = Batch.new auxillary_info: { 'shipment_id' => 'ABCD' }
      assert_equal 'ABCD', batch.shipment_id
    end

    it 'is empty string when no such key in auxillary info' do
      batch = Batch.new auxillary_info: { 'some-other-key' => '1234' }
      assert_nil batch.shipment_id
    end
  end
end
