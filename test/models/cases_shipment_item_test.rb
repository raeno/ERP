require 'test_helper'

class CasesShipmentItemTest < ActiveSupport::TestCase
  let(:product) { build :product }
  let(:quantity) { 10 }
  subject { described_class.new product, quantity }

  describe '#shipment_in_cases?' do
    it 'is always true' do
      assert subject.shipment_in_cases?
    end
  end

  describe '#shipment_type' do
    it 'is always equal to cases' do
      assert_equal 'cases', subject.shipment_type
    end
  end
end
