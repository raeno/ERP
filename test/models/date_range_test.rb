# frozen_string_literal: true

require 'test_helper'

class DateRangeTest < ActiveSupport::TestCase
  describe '#initialize' do
    it 'accepts start/end dates as string params' do
      range = described_class.new("start_date": "Sep 13, 2017", "end_date": "Nov 27, 2018")
      assert_equal Date.new(2017, 9, 13), range.start_date
      assert_equal Date.new(2018, 11, 27), range.end_date
    end

    it 'accepts dates as Date type' do
      date = Date.new(2017, 9, 18)
      range = described_class.new(start_date: date)
      assert_equal date, range.start_date
    end

    it 'both dates are optional' do
      range = described_class.new
      assert_nil range.start_date
      assert_nil range.end_date
    end
  end

  describe '#start?' do
    it 'is true when start_date is present' do
      range = described_class.new
      range.stubs(:start_date).returns(Time.zone.today)
      assert range.start?
    end

    it 'is false otherwise' do
      range = described_class.new
      refute range.start?
    end
  end

  describe '#end?' do
    it 'is true when end_date is present' do
      range = described_class.new
      range.stubs(:end_date).returns(Time.zone.today)
      assert range.end?
    end

    it 'is false otherwise' do
      range = described_class.new
      refute range.end?
    end
  end
end
