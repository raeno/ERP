# frozen_string_literal: true
# == Schema Information
#
# Table name: fba_allocations
#
#  id                   :integer          not null, primary key
#  fba_warehouse_id     :integer          not null
#  product_id           :integer          not null
#  quantity             :integer          not null
#  quantity_to_allocate :integer
#  created_at           :datetime
#  updated_at           :datetime
#  shipment_type        :text
#

require 'test_helper'

class FbaAllocationTest < ActiveSupport::TestCase
  describe '#clear_for_products' do
    it 'destroys all allocations for specified products' do
      product_1, product_2, product_3 = create_list :product, 3
      [product_1, product_2, product_3].each do |p|
        create :fba_allocation, product: p
      end
      assert_difference 'FbaAllocation.count', -2 do
        FbaAllocation.clear_for_products [product_1, product_2]
      end
    end
  end

  describe '#empty?' do
    it 'is true when quantity is zero' do
      empty_allocation = described_class.new quantity: 0
      assert empty_allocation.empty?
      non_empty = described_class.new quantity: 100
      refute non_empty.empty?
    end
  end

  describe '#could_be_fulfilled?' do
    let(:product) { create :product }
    let(:warehouse) { create :fba_warehouse }
    let(:product) { create :product }

    it 'is truthy when product has enough in inventory' do
      product_stats = stub(packed_inventory_quantity: 30)
      allocation = FbaAllocation.new quantity: 10, shipment_type: 'items', product: product
      allocation.stubs(:product_stats).returns(product_stats)
      assert allocation.could_be_fulfilled?
    end

    it 'is false when not enough in inventory' do
      product_stats = stub(packed_inventory_quantity: 10)
      allocation = FbaAllocation.new quantity: 30, shipment_type: 'items', product: product
      allocation.stubs(:product_stats).returns(product_stats)
      refute allocation.could_be_fulfilled?
    end

    describe 'for cases shipments it checks cases available' do
      # i.e. we have 2 full cases
      let(:product) { create :product, items_per_case: 20 }
      let(:product_stats) { stub(packed_inventory_quantity: 65) }

      it 'can fulfill allocation for 2 full cases' do
        allocation = FbaAllocation.new quantity: 2, shipment_type: 'cases', product: product

        allocation.stubs(:product_stats).returns(product_stats)
        assert allocation.could_be_fulfilled?
      end

      it 'cannot fulfill allocation with 3 cases' do
        allocation = FbaAllocation.new quantity: 3, shipment_type: 'cases', product: product
        allocation.stubs(:product_stats).returns(product_stats)
        assert allocation.could_be_fulfilled?
      end
    end
  end

  describe '.fetch_allocations' do
    let(:product_1) { create :product }
    let(:product_2 ) { create :product }

    before do
      [product_1, product_2].each do |product|
        [100, 200].each do |quantity|
          ['items', 'cases'].each do |shipment_type|
            create :fba_allocation, product: product, quantity_to_allocate: quantity, shipment_type: shipment_type
            create :fba_allocation, product: product, quantity_to_allocate: quantity, shipment_type: shipment_type, updated_at: 1.year.ago
          end
        end
      end
    end

    it 'fetches allocations for given product, quantity and shipment type' do
      allocations = described_class.fetch_allocations product_1, 100, 'cases'
      assert_equal 1, allocations.count

      allocation = allocations.first
      assert_equal 'cases', allocation.shipment_type
      assert_equal 100, allocation.quantity_to_allocate
      assert_equal product_1, allocation.product
    end
  end

  describe '.shipment_item_allocations' do
    let(:product) { build :product }
    let(:shipment_item) { build :cases_shipment_item, product: product, quantity: 100 }

    it 'fetches allocations for shipment item' do
      described_class.expects(:fetch_allocations).with(product, 100, 'cases' ).once
      described_class.shipment_item_allocations shipment_item
    end
  end
end
