# frozen_string_literal: true
# == Schema Information
#
# Table name: fba_warehouses
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  created_at :datetime
#  updated_at :datetime
#

require 'test_helper'

class FbaWarehouseTest < ActiveSupport::TestCase
  describe '#priority' do
    it 'gets warehouse priority based on their name' do
      warehouse_1 = described_class.new name: 'PH7'
      assert_equal 5, warehouse_1.priority
      warehouse_2 = described_class.new name: 'BNA2'
      assert_equal 1, warehouse_2.priority
    end

    it 'is lowest priority when name is unusual for system' do
      unusual_warehouse = described_class.new name: 'ABC'
      assert_equal 0, unusual_warehouse.priority
    end
  end

  describe '.retrieve_by_name' do
    it 'fetches warehouse by name' do
      create :fba_warehouse, name: 'warehouse_name'
      described_class.retrieve_by_name 'warehouse_name'
    end

    it 'creates warehouse if not exists' do
      assert_difference 'FbaWarehouse.count', 1 do
        described_class.retrieve_by_name 'non_existing_warehouse'
      end
    end
  end
end
