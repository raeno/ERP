# frozen_string_literal: true
# == Schema Information
#
# Table name: ingredients
#
#  id                      :integer          not null, primary key
#  product_id              :integer          not null
#  quantity_obsolete       :decimal(, )      default(1.0), not null
#  created_at              :datetime
#  updated_at              :datetime
#  inventory_item_id       :integer          not null
#  quantity_in_small_units :decimal(, )      default(1.0), not null
#
# Foreign Keys
#
#  fk_rails_...  (product_id => products.id)
#

require 'test_helper'

class IngredientTest < ActiveSupport::TestCase
  let(:material) { create :material, unit_price: 3.01 }

  describe '#cost' do
    it 'returns the cost of the ingredient' do
      inventory_item = create :inventory_item, inventoriable: material
      ingredient = build :ingredient, inventory_item: inventory_item, quantity_in_small_units: 9
      assert_equal 27.09, ingredient.cost
    end

    it 'works fine with default values and nils' do
      ingredient = create :ingredient
      assert ingredient.cost
    end
  end

  describe '#cost_percent' do
    it 'is the part of this ingredient cost in the total product prime cost' do
      ingredient = build :ingredient
      ingredient.stubs(:cost).returns 5.00
      ingredient.product.stubs(:prime_cost).returns(25.00)
      assert_equal 5.0 / 25 * 100, ingredient.cost_percent
    end

    it 'returns zero in case product prime cost is zero' do
      ingredient = build :ingredient
      ingredient.stubs(:cost).returns 5.00
      ingredient.product.stubs(:prime_cost).returns(0)
      assert_equal 0, ingredient.cost_percent
    end

    it 'works fine with default values and nils' do
      ingredient = create :ingredient
      assert ingredient.cost_percent
    end
  end

  describe '#quantity' do
    describe 'for material ingredients' do
      let(:ml) { create :unit }
      let(:litre) { create :unit }

      let(:material) { create :material, unit: litre, ingredient_unit: ml }
      let(:inventory_item) { create :inventory_item, inventoriable: material }

      let(:ingredient) { build :ingredient, inventory_item: inventory_item, quantity_in_small_units: 30 }

      before { stub_unit_conversion(ml, litre, from_amount: 30, to_amount: 0.03) }

      it 'is measured in main material units' do
        assert_equal 0.03, ingredient.quantity
      end
    end

    describe 'for product ingredients' do
      let(:product) { create :product }
      let(:inventory_item) { create :inventory_item, inventoriable: product }
      let(:ingredient) { build :ingredient, inventory_item: inventory_item, quantity_in_small_units: 98 }

      it 'is the same as #quantity_in_small_units' do
        assert_equal 98, ingredient.quantity
      end
    end
  end

  describe '#zones_available' do
    let(:zone_1) { create :zone, production_order: 1 }
    let(:zone_2) { create :zone, production_order: 2 }
    let(:zone_3) { create :zone, production_order: 3 }
    describe 'when product is empty' do
      it 'equals to all zones' do
        ingredient = described_class.new
        assert_equal Zone.all, ingredient.zones_available
      end
    end

    describe 'when product present' do
      let(:product) { create :product }
      before do
        [zone_3, zone_2].each { |z| create :product_zone_availability, product: product, zone: z }
      end

      it 'equals to ordered product zones' do
        ingredient = described_class.new product: product
        assert_equal [zone_2, zone_3], ingredient.zones_available
      end
    end
  end

  describe '.for_zone' do
    it 'returns ingredients with materials related to the zone' do
      make_zone = create :zone, :make
      make_ingredient = create_ingredient_in_zone(make_zone)

      pack_zone = create :zone, :pack
      pack_ingredient = create_ingredient_in_zone(pack_zone)

      make_ingredients = Ingredient.for_zone(make_zone)
      assert make_ingredients.include?(make_ingredient)
      refute make_ingredients.include?(pack_ingredient)

      pack_ingredients = Ingredient.for_zone(pack_zone)
      assert pack_ingredients.include?(pack_ingredient)
      refute pack_ingredients.include?(make_ingredient)
    end
  end

  describe '.up_to_zone' do
    it 'returns ingredients with materials related to the zone specified, and all zones before the specified' do
      make_zone = create :zone, :make
      make_ingredient = create_ingredient_in_zone(make_zone)

      pack_zone = create :zone, :pack
      pack_ingredient = create_ingredient_in_zone(pack_zone)

      ship_zone = create :zone, :ship
      ship_ingredient = create_ingredient_in_zone(ship_zone)

      make_ingredients = Ingredient.up_to_zone(make_zone)
      assert make_ingredients.include?(make_ingredient)
      refute make_ingredients.include?(pack_ingredient)
      refute make_ingredients.include?(ship_ingredient)

      pack_ingredients = Ingredient.up_to_zone(pack_zone)
      assert pack_ingredients.include?(make_ingredient)
      assert pack_ingredients.include?(pack_ingredient)
      refute pack_ingredients.include?(ship_ingredient)

      ship_ingredients = Ingredient.up_to_zone(ship_zone)
      assert ship_ingredients.include?(make_ingredient)
      assert ship_ingredients.include?(pack_ingredient)
      assert ship_ingredients.include?(ship_ingredient)
    end
  end
end
