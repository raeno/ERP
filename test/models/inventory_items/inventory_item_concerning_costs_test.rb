# frozen_string_literal: true
require 'test_helper'

module InventoryItems
  class InventoryItemConcerningCostsTest < ActiveSupport::TestCase
    describe '#prime_cost' do
      describe 'when material' do
        it 'delegates call to the material' do
          material = build :material
          material.stubs(:prime_cost).returns(30)
          inventory_item = build :inventory_item, inventoriable: material
          assert_equal 30, inventory_item.prime_cost
        end
      end

      describe 'when product' do
        let(:zone) { create :zone }

        it 'returns the product cumulative prime cost up to the inventory item zone' do
          product = create :product
          product.stubs(:cumulative_cogs).with(zone).returns(5.99)
          inventory_item = create :inventory_item, inventoriable: product, zone: zone
          assert_equal 5.99, inventory_item.prime_cost
        end
      end
    end

    describe '#total_prime_cost' do
      it 'is prime_cost * quantity' do
        inventory_item = InventoryItem.new
        inventory_item.stubs(:inventory_quantity).returns(10)
        inventory_item.stubs(:prime_cost).returns(3.15)
        assert_equal 10 * 3.15, inventory_item.total_prime_cost
      end
    end

    describe "#previous_prime_cost" do
      describe "for materials" do
        it 'delegates call to the material' do
          material = build :material
          material.stubs(:previous_container_unit_price).returns(1.78)
          inventory_item = build :inventory_item, material: material, inventoriable: material
          assert_equal 1.78, inventory_item.previous_prime_cost
        end
      end

      describe "for products" do
        it 'delegates call to the product' do
          product = build :product
          inventory_item = build :inventory_item, product: product, inventoriable: product
          cogs_history = stub(previous_prime_cost: 3.88)
          ProductCogsHistory.stubs(:new).with(product, inventory_item.zone).returns(cogs_history)
          assert_equal 3.88, inventory_item.previous_prime_cost
        end
      end
    end
    describe "#prime_cost_change_percent" do
      describe "for materials" do
        it 'delegates call to the material' do
          material = build :material
          material.stubs(:price_change_percent).returns(11.11)
          inventory_item = build :inventory_item, material: material, inventoriable: material
          assert_equal 11.11, inventory_item.prime_cost_change_percent
        end
      end

      describe 'for products' do
        it 'delegates call to product' do
          product = build :product
          inventory_item = build :inventory_item, product: product, inventoriable: product
          cogs_history = stub(prime_cost_change_percent: 22.22)
          ProductCogsHistory.stubs(:new).with(product, inventory_item.zone).returns(cogs_history)
          assert_equal 22.22, inventory_item.prime_cost_change_percent
        end
      end
    end

    describe "#latest_container_unit_price" do
      describe "for materials" do
        it 'delegates call to the material' do
          material = build :material
          material.stubs(:latest_container_unit_price).returns(2.22)
          inventory_item = build :inventory_item, material: material, inventoriable: material
          assert_equal 2.22, inventory_item.latest_container_unit_price
        end
      end

      describe 'for products' do
        it 'returns nil' do
          product = build :product
          inventory_item = build :inventory_item, product: product
          assert_nil inventory_item.latest_container_unit_price
        end
      end
    end

    describe "#zone_prime_cost" do
      describe "for materials" do
        it 'is material prime cost' do
          material = build :material, unit_price: 1.78
          inventory_item = build :inventory_item, inventoriable: material
          assert_equal 1.78, inventory_item.zone_prime_cost
        end
      end

      describe "for products" do
        it 'the cost of ingredients involved in this zone' do
          product = build :product
          inventory_item = build :inventory_item, inventoriable: product
          product.stubs(:zone_cogs).with(inventory_item.zone).returns(99.99)
          assert_equal 99.99, inventory_item.zone_prime_cost
        end
      end
    end

    describe "#labour_cost" do
      subject { build :inventory_item }

      it 'returns last task unit labour cost' do
        task = build :task
        task.stubs(:unit_labour_cost).returns(1.23)
        subject.stubs(:last_finished_task).returns(task)
        assert_equal 1.23, subject.labour_cost
      end

      it 'returns nil if there is no tasks' do
        assert_nil subject.labour_cost
      end
    end
  end
end
