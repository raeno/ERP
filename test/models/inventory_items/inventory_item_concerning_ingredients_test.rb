# frozen_string_literal: true
# == Schema Information
#
# Table name: inventory_items
#
#  id                 :integer          not null, primary key
#  zone_id            :integer
#  inventoriable_id   :integer
#  inventoriable_type :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  production_zone_id :integer
#

require 'test_helper'

module InventoryItems
  class InventoryItemConcerningIngredientsTest < ActiveSupport::TestCase
    describe "#contained_ingredients" do
      describe "for product inventoriables" do
        let(:product) { build :product }
        let(:inventory_item) { build :inventory_item, inventoriable: product }

        it 'is a set of Ingredients used for the product in this zone' do
          assert inventory_item.zone
          ingredients = stub
          product.stubs(:ingredients_for_zone).with(inventory_item.zone).returns(ingredients)

          assert_equal ingredients, inventory_item.contained_ingredients
        end
      end

      describe "for material inventoriables" do
        let(:material) { build :material }
        let(:inventory_item) { build :inventory_item, inventoriable: material }

        it 'is an empty Ingredients relation' do
          assert inventory_item.contained_ingredients.empty?
        end
      end
    end

    describe "#contained_ingredients_ordered" do
      describe "for product inventoriables" do
        let(:product) { create :product }
        let(:inventory_item) { create :inventory_item, inventoriable: product }

        # Add raw oil material to the product
        let(:oil_category) { create :material_category, name: "oil" }
        let(:oil_material) { create :material, category: oil_category }
        let(:oil_ingredient) do
          include_material_to_product(material: oil_material, product: product, production_zone: inventory_item.zone)
        end
        before { oil_ingredient }

        # Add inner product
        let(:inner_product) { create :product }
        let(:product_ingredient) do
          include_product_to_giftbox(
            product: inner_product, giftbox_product: product,
            production_zone: inventory_item.zone, passed_zone: build(:zone)
          )
        end
        before { product_ingredient }

        # Add bottle material to the product
        let(:bottle_category) { create :material_category, name: "bottle" }
        let(:bottle_material) { create :material, category: bottle_category }
        let(:bottle_ingredient) do
          include_material_to_product(material: bottle_material, product: product, production_zone: inventory_item.zone)
        end
        before { bottle_ingredient }

        it 'sorts contained_ingredients so that products go first, then materials sorted by category' do
          ingredients = inventory_item.contained_ingredients_ordered
          assert_equal product_ingredient, ingredients[0]
          assert_equal bottle_ingredient,  ingredients[1]
          assert_equal oil_ingredient,     ingredients[2]
        end
      end

      describe "for material inventoriables" do
        let(:material) { build :material }
        let(:inventory_item) { build :inventory_item, inventoriable: material }

        it 'is an empty Ingredients relation' do
          assert inventory_item.contained_ingredients_ordered.empty?
        end
      end
    end

    describe "#all_inventoriable_components" do
      describe "for product inventoriables" do
        let(:product) { build :product }
        let(:inventory_item) { build :inventory_item, inventoriable: product }

        it 'redirects call to inventoriable.components' do
          components = stub
          product.stubs(:components).returns(components)

          assert_equal components, inventory_item.all_inventoriable_components
        end
      end

      describe "for material inventoriables" do
        let(:material) { build :material }
        let(:inventory_item) { build :inventory_item, inventoriable: material }

        it 'is an empty relation' do
          assert inventory_item.all_inventoriable_components.empty?
        end
      end
    end
  end
end
