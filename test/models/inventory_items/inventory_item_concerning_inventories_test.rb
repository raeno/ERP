# frozen_string_literal: true

# == Schema Information
#
# Table name: inventory_items
#
#  id                 :integer          not null, primary key
#  zone_id            :integer
#  inventoriable_id   :integer
#  inventoriable_type :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  production_zone_id :integer
#

require 'test_helper'

module InventoryItems
  class InventoryItemConcerningInventoriesTest < ActiveSupport::TestCase
    describe "#inventory_quantity" do
      describe "for product inventoriable" do
        let(:product) { build :product }
        let(:inventory_item) { create :inventory_item, inventoriable: product }
        before do
          inventory_item.stubs(:inventory).returns(stub(quantity: 99_999))

          product_stats = stub(total_amazon_inventory: 555)
          Products::ProductInventoryStats.stubs(:new).with(inventory_item.product).returns(product_stats)
        end

        describe "for ship zone" do
          it 'returns quantity fetched from amazon' do
            inventory_item.zone = build :zone, :ship
            assert_equal 555, inventory_item.inventory_quantity
          end
        end

        describe "for other zones" do
          it 'returns quantity stored in our DB' do
            inventory_item.zone = build :zone
            assert_equal 99_999, inventory_item.inventory_quantity
          end
        end
      end

      describe "for material inventoriable" do
        let(:material) { build :material }
        let(:inventory_item) { build :inventory_item, inventoriable: material }
        before do
          inventory_item.stubs(:inventory).returns(stub(quantity: 99_999))
        end

        it 'returns quantity stored in our DB' do
          assert_equal 99_999, inventory_item.inventory_quantity
        end
      end
    end
  end
end
