# frozen_string_literal: true

# == Schema Information
#
# Table name: inventory_items
#
#  id                 :integer          not null, primary key
#  zone_id            :integer
#  inventoriable_id   :integer
#  inventoriable_type :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  production_zone_id :integer
#

require 'test_helper'

module InventoryItems
  class InventoryItemConcerningProductionTest < ActiveSupport::TestCase
    subject { create :inventory_item }

    describe '#last_finished_task' do
      it 'is the latest finished task for this item' do
        task1 = create :task, inventory_item: subject, finished_at: 1.day.ago
        task2 = create :task, inventory_item: subject, finished_at: Time.zone.now
        task3 = create :task, inventory_item: subject, finished_at: nil

        assert_equal task2, subject.last_finished_task
      end
    end
  end
end
