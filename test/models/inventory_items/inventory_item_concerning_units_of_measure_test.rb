# frozen_string_literal: true

require 'test_helper'

module InventoryItems
  class InventoryItemConcerningUnitsOfMeasureTest < ActiveSupport::TestCase
    describe "#zone_unit_name" do
      describe "for product inventoriable" do
        let(:product) { build :product }
        let(:inventory_item) { create :inventory_item, inventoriable: product }

        describe "for make zone" do
          it 'returns units' do
            inventory_item.zone = build :zone, :make
            assert_equal 'units', inventory_item.zone_unit_name
          end
        end

        describe "for other zones" do
          it 'returns cases' do
            inventory_item.zone.name = 'some generic zone'
            assert_equal 'cases', inventory_item.zone_unit_name
          end
        end
      end

      describe "for material inventoriable" do
        let(:unit) { build :unit, name: 'parrot' }
        let(:material) { build :material, unit: unit }
        let(:inventory_item) { build :inventory_item, inventoriable: material }

        it 'returns main_unit name' do
          assert_equal 'parrot', inventory_item.zone_unit_name
        end
      end
    end
  end
end
