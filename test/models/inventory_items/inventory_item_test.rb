# frozen_string_literal: true
require 'test_helper'

module InventoryItems
  class InventoryItemTest < ActiveSupport::TestCase
    describe '#destroy' do
      let(:inventory_item) { create :inventory_item }

      it 'also destroys associated ingredients' do
        create :ingredient, inventory_item: inventory_item
        assert_difference [-> { InventoryItem.count }, -> { Ingredient.count }], -1 do
          inventory_item.destroy
        end
      end
    end

    describe '.materials' do
      it 'gets inventory_items of "Material" type' do
        material1, material2 = create_list :material, 2
        inventory_item1 = create :inventory_item, inventoriable: material1
        inventory_item2 = create :inventory_item, inventoriable: material2
        create :inventory_item, inventoriable: create(:product)
        assert_equal [inventory_item1, inventory_item2], InventoryItem.materials
      end
    end

    describe '.products' do
      it 'gets inventory_items of "Product" type' do
        product1, product2 = create_list :product, 2
        inventory_item1 = create :inventory_item, inventoriable: product1
        inventory_item2 = create :inventory_item, inventoriable: product2
        create :inventory_item, inventoriable: create(:material)
        assert_equal [inventory_item1, inventory_item2], InventoryItem.products
      end
    end

    describe '.active_products' do
      it 'get inventory_items of "Product" type only with active products' do
        active_product = create :product, is_active: true
        inactive_product = create :product, is_active: false
        inventory_item1 = create :inventory_item, inventoriable: active_product
        create :inventory_item, inventoriable: inactive_product
        create :inventory_item, inventoriable: create(:material)
        assert_equal [inventory_item1], InventoryItem.active_products
      end
    end

    describe '#nature' do
      it 'is nil when type is not defined' do
        inventory_item = build :inventory_item, inventoriable: nil
        assert_nil inventory_item.nature
      end

      it 'returns inventoriable type as symbol' do
        inventory_item = build :inventory_item, inventoriable_type: 'Material'
        assert_equal :material, inventory_item.nature

        inventory_item = build :inventory_item, inventoriable_type: 'Product'
        assert_equal :product, inventory_item.nature
      end
    end

    describe '#product?' do
      it 'true wnen inventoriable is product' do
        inventory_item = InventoryItem.new inventoriable: build(:product)
        assert inventory_item.product?

        inventory_item = InventoryItem.new inventoriable: build(:material)
        refute inventory_item.product?
      end
    end

    describe '#material?' do
      it 'true wnen inventoriable is material' do
        inventory_item = InventoryItem.new inventoriable: build(:material)
        assert inventory_item.material?

        inventory_item = InventoryItem.new inventoriable: build(:product)
        refute inventory_item.material?
      end
    end

    describe '#active_product?' do
      let(:active_product) { build :product, is_active: true }
      let(:not_active_product) { build :product, is_active: false }
      it 'is true wnen inventoriable is active product' do
        inventory_item = InventoryItem.new inventoriable: active_product
        assert inventory_item.active_product?

        inventory_item = InventoryItem.new inventoriable: not_active_product
        refute inventory_item.active_product?
      end
    end

    describe "#in_ship_zone?" do
      it 'is true wnen zone is ship' do
        inventory_item = InventoryItem.new zone: build(:zone)
        refute inventory_item.in_ship_zone?

        ship_zone = build :zone, :ship
        inventory_item = InventoryItem.new zone: ship_zone
        assert inventory_item.in_ship_zone?
      end
    end

    describe '#build_inventoriable' do
      let(:params) { { name: 'Cap', unit_price: 10 } }
      it 'raises an error when type is not right' do
        inventory_item = build :inventory_item, inventoriable_type: 'SOME_WRONG_TYPE'
        assert_raise do
          inventory_item.build_inventoriable params
        end
      end

      describe 'when params and type are OK' do
        it 'build proper inventoriable instance of given type' do
          inventory_item = build :inventory_item, inventoriable_type: 'Material'
          inventory_item.build_inventoriable params
          inventoriable = inventory_item.inventoriable
          refute_nil inventoriable
          assert_kind_of Material, inventoriable
          assert_equal 'Cap', inventoriable.name
          assert_equal 10, inventoriable.prime_cost
        end
      end
    end

    describe '#name' do
      it 'delegates call to underlying inventoriable' do
        material = build :material, name: 'Cap 123'
        inventory_item = build :inventory_item, inventoriable: material
        assert_equal 'Cap 123', inventory_item.name
      end
    end

    describe '#main_unit_name' do
      describe 'when inventoriable is material' do
        it 'delegates to material main unit name' do
          material = build :material
          material.stubs(:main_unit_name).returns('ML')
          inventory_item = build :inventory_item, inventoriable: material
          assert_equal "ML", inventory_item.main_unit_name
        end
      end

      describe 'when inventoriable is product' do
        it 'is always "Each" unit name' do
          create :unit, name: 'Each'
          product = build :product
          inventory_item = build :inventory_item, inventoriable: product
          assert_equal 'Each', inventory_item.main_unit_name
        end
      end
    end

    describe '#image_url' do
      let(:url) { 'http://some_image_url.com' }
      describe 'when inventoriable is product' do
        it "delegates to product small image url" do
          product = build :product, small_image_url: url
          inventory_item = build :inventory_item, inventoriable: product
          assert_equal url, inventory_item.image_url
        end
      end

      describe 'when inventoriable is material' do
        it 'is empty string' do
          material = build :material, image_url: url
          inventory_item = build :inventory_item, inventoriable: material
          assert_equal url, inventory_item.image_url
        end
      end
    end

    describe 'set production zone before create' do
      let(:zone) { create :zone }
      let(:material) { create :material }
      let(:product) { create :product }
      describe 'when inventoriable is material' do
        it 'sets production zone equal to inventory zone' do
          item = InventoryItem.new zone: zone, inventoriable: material
          assert_nil item.production_zone_id
          item.save
          assert_equal item.production_zone_id, zone.id
        end
      end

      describe 'when inventoriable is product' do
        let(:next_zone) { create :zone }
        before do
          ZoneWorkflow.any_instance.stubs(:next_zone).returns(next_zone)
        end

        it 'sets production zone equal to next after inventory zone' do
          item = InventoryItem.new zone: zone, inventoriable: product
          assert_nil item.production_zone_id
          item.save
          assert_equal item.production_zone_id, next_zone.id
        end
      end
    end

    describe "#product_items_per_case" do
      it 'delegates call to product' do
        product = build :product, items_per_case: 77
        ii = create :inventory_item, inventoriable: product
        assert_equal 77, ii.product_items_per_case
      end
    end
  end
end
