# frozen_string_literal: true
# == Schema Information
#
# Table name: inventory_items_production_reports
#
#  id                         :integer          not null, primary key
#  inventory_item_id          :integer          not null
#  in_stock                   :decimal(, )      not null
#  demand                     :decimal(, )      not null
#  demand_coverage            :decimal(, )      not null
#  supply                     :decimal(, )      not null
#  to_cover                   :decimal(, )      not null
#  can_cover                  :decimal(, )      not null
#  can_cover_in_cases         :decimal(, )
#  previous_zone_deficit      :boolean
#  deficit_inventoriable_id   :integer
#  deficit_inventoriable_type :string
#  deficit_ingredient_name    :string
#  can_cover_optimal          :decimal(, )      not null
#  assigned                   :decimal(, )      default(0.0), not null
#

require 'test_helper'

module InventoryItems
  class ProductionReportTest < ActiveSupport::TestCase
    subject { build(:production_report) }

    describe "#fully_supplied?" do
      it 'is true if to_cover value is fully supplied' do
        subject.to_cover = 500
        subject.supply = 500
        assert subject.fully_supplied?

        subject.supply = 700
        assert subject.fully_supplied?
      end

      it 'is false when to_cover cannot be fully supplied' do
        subject.to_cover = 500
        subject.supply = 100
        refute subject.fully_supplied?
      end
    end

    describe '#in_stock_cases' do
      it 'rounds #in_stock quantity down to cases' do
        subject.stubs(:product?).returns(true)
        subject.in_stock = 750
        subject.stubs(:product_items_per_case).returns(100)
        assert_equal 7, subject.in_stock_cases
      end
    end

    describe '#unassigned' do
      it 'is the difference between optimal :can_cover and :assigned' do
        subject.can_cover_optimal = 100
        subject.assigned = 70
        assert_equal 30, subject.unassigned

        subject.assigned = 100
        assert_equal 0, subject.unassigned
      end

      it 'is 0 when assigned > can_cover (i.e. does not go negative)' do
        subject.can_cover_optimal = 100
        subject.assigned = 200
        assert_equal 0, subject.unassigned
      end
    end

    describe "#time_estimate" do
      it 'delegates call ProductionEstimate with can_cover_optimal quantity' do
        subject.can_cover_optimal = 777
        stub_duration_estimate(subject.inventory_item, 777, 3333)

        assert_equal 3333, subject.time_estimate
      end
    end

    describe '.low_supply scope' do
      it 'returns records where can_cover < to_cover' do
        report1 = create :production_report, to_cover: 100, can_cover: 90
        report2 = create :production_report, to_cover: 100, can_cover: 80
        report3 = create :production_report, to_cover: 100, can_cover: 100
        report4 = create :production_report, to_cover: 100, can_cover: 200
        collection = described_class.low_supply

        assert_include collection, report1, report2
        refute_include collection, report3, report4
      end
    end

    describe '.unassigned scope' do
      it 'returns records where assigned < can_cover_optimal' do
        report1 = create :production_report, can_cover_optimal: 100, assigned: 90
        report2 = create :production_report, can_cover_optimal: 100, assigned: 80
        report3 = create :production_report, can_cover_optimal: 100, assigned: 100
        report4 = create :production_report, can_cover_optimal: 100, assigned: 200
        collection = described_class.unassigned

        assert_include collection, report1, report2
        refute_include collection, report3, report4
      end
    end

    describe '.managed scope' do
      it 'excludes .low_supply records' do
        report1 = create :production_report,
          can_cover_optimal: 50, assigned: 50, to_cover: 100, can_cover: 100
        report2 = create :production_report,
          can_cover_optimal: 50, assigned: 50, to_cover: 100, can_cover: 100
        report3 = create :production_report,
          can_cover_optimal: 50, assigned: 50, to_cover: 100, can_cover: 90
        collection = described_class.managed

        assert_include collection, report1, report2
        refute_include collection, report3
      end

      it 'excludes .unassigned records' do
        report1 = create :production_report,
          to_cover: 100, can_cover: 100, can_cover_optimal: 100, assigned: 120
        report2 = create :production_report,
          to_cover: 100, can_cover: 100, can_cover_optimal: 100, assigned: 100
        report3 = create :production_report,
          to_cover: 100, can_cover: 100, can_cover_optimal: 100, assigned: 90
        collection = described_class.managed

        assert_include collection, report1, report2
        refute_include collection, report3
      end
    end

    describe '.with_status scope' do
      it 'returns .all relation for nill status' do
        all_relation = stub
        described_class.stubs(:all).returns(all_relation)
        collection = described_class.with_status(nil)

        assert_equal all_relation, collection
      end

      it 'returns .unassigned relation for :unassigned status' do
        unassigned_relation = stub
        described_class.stubs(:unassigned).returns(unassigned_relation)
        collection = described_class.with_status(:unassigned)

        assert_equal unassigned_relation, collection
      end

      it 'returns .low_supply relation for :low_supply status' do
        low_supply_relation = stub
        described_class.stubs(:low_supply).returns(low_supply_relation)
        collection = described_class.with_status(:low_supply)

        assert_equal low_supply_relation, collection
      end

      it 'returns .managed relation for :managed status' do
        managed_relation = stub
        described_class.stubs(:managed).returns(managed_relation)
        collection = described_class.with_status(:managed)

        assert_equal managed_relation, collection
      end
    end
  end
end
