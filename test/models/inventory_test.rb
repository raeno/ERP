# frozen_string_literal: true
# == Schema Information
#
# Table name: inventories
#
#  id                :integer          not null, primary key
#  inventory_item_id :integer          not null
#  quantity          :decimal(, )      not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Foreign Keys
#
#  fk_rails_...  (inventory_item_id => inventory_items.id)
#

require 'test_helper'

class InventoryTest < ActiveSupport::TestCase
  let(:product) { create :product }
  let(:inventory_item) { create :inventory_item }

  describe '#debit' do
    it 'decreases product inventory in this zone on specified amount' do
      inventory = described_class.new quantity: 10, inventory_item: inventory_item
      assert_difference 'inventory.quantity', -5 do
        inventory.debit 5
      end
    end

    it 'saves updated inventory' do
      inventory = described_class.new quantity: 10, inventory_item: inventory_item
      refute inventory.persisted?
      inventory.debit 5
      assert inventory.persisted?
    end

    it 'raises an exception for not positive amounts' do
      inventory = described_class.new quantity: 10, inventory_item: inventory_item

      assert_raise ArgumentError do
        inventory.debit(-1)
      end
    end
  end

  describe '#credit' do
    it 'increases product inventory in this zone on specified amount' do
      inventory = described_class.new quantity: 10, inventory_item: inventory_item
      assert_difference 'inventory.quantity', 5 do
        inventory.credit 5
      end
    end

    it 'saves updated inventory' do
      inventory = described_class.new quantity: 10, inventory_item: inventory_item
      refute inventory.persisted?
      inventory.credit 5
      assert inventory.persisted?
    end

    it 'raises an exception for non positive amounts' do
      inventory = described_class.new quantity: 10, inventory_item: inventory_item

      assert_raise ArgumentError do
        inventory.credit(-1)
      end
    end
  end

  describe '.positive' do
    it 'selects only inventory items with positive amount in inventory' do
      positive = create :inventory, quantity: 100
      create :inventory, quantity: 0
      assert_equal [positive], described_class.positive
    end
  end
end
