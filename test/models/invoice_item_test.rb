# frozen_string_literal: true
# == Schema Information
#
# Table name: invoice_items
#
#  id                    :integer          not null, primary key
#  invoice_id            :integer          not null
#  material_id           :integer
#  pkg_external_name     :string
#  pkg_form              :string
#  pkg_unit_id           :integer
#  pkg_amount            :float
#  pkg_price             :decimal(, )
#  quantity              :integer          default(1), not null
#  total_material_amount :decimal(, )      not null
#  total_price           :decimal(, )      not null
#  batch_number          :string
#  created_at            :datetime
#  updated_at            :datetime
#  received_date         :date
#  expiration_date       :date
#  material_container_id :integer
#
# Foreign Keys
#
#  fk_rails_...  (invoice_id => invoices.id)
#  fk_rails_...  (material_id => materials.id)
#

require 'test_helper'

class InvoiceItemTest < ActiveSupport::TestCase
  subject { build :invoice_item }
  let(:unit) { create :unit }

  describe '.before scope' do
    it 'returns all invoices before the specified date' do
      item_3_days_ago = create_invoice_item_with_date 3.days.ago
      item_2_days_ago = create_invoice_item_with_date 2.days.ago
      item_1_day_ago  = create_invoice_item_with_date 1.day.ago

      items = InvoiceItem.before(1.day.ago.to_date)
      assert_include items, item_3_days_ago, item_2_days_ago
      refute_include items, item_1_day_ago
    end
  end

  it "calculates total_material_amount on save" do
    container = build :material_container
    container.stubs(amount_in_material_units: 144)
    subject.stubs(material_container: container)
    subject.quantity = 2
    subject.save!
    assert_equal 288, subject.total_material_amount
  end

  it "calculates total_price on save" do
    material_container = build :material_container, price: 200
    subject.stubs(material_container: material_container)
    subject.quantity = 2
    subject.save!
    assert_equal 400, subject.total_price
  end

  describe "#locked?" do
    it "is true when the whole invoice is locked" do
      subject.invoice.stubs(:locked?).returns(true)
      subject.received_date = nil
      assert subject.locked?
    end

    it "is true when the item is individually received" do
      subject.become_received
      subject.invoice.stubs(:locked?).returns(false)
      assert subject.locked?
    end

    it "is false otherwise" do
      subject.received_date = nil
      subject.invoice.stubs(:locked?).returns(false)
      refute subject.locked?
    end
  end
end
