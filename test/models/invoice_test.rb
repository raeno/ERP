# == Schema Information
#
# Table name: invoices
#
#  id             :integer          not null, primary key
#  vendor_id      :integer
#  created_at     :datetime
#  updated_at     :datetime
#  number         :string           default(""), not null
#  attachment_url :text
#  date           :date             not null
#  total_price    :decimal(, )      not null
#  received_date  :date
#  notes          :text
#  paid_date      :date
#

require 'test_helper'

class InvoiceTest < ActiveSupport::TestCase
  subject { create :invoice }

  describe 'default fiels setting' do
    let(:vendor) { create :vendor }
    let(:invoice) { Invoice.new(vendor_id: vendor.id) }
    before do
      Invoice.delete_all
      invoice  # init the invoice
    end

    it 'produces a valid invoice' do
      assert invoice.valid?
    end

    it 'sets date' do
      assert_equal Time.zone.today, invoice.date
    end

    it 'sets sequential invoice number' do
      assert_equal '000001', invoice.number
      invoice.save!

      another_invoice = Invoice.new(vendor_id: vendor.id)
      assert_equal '000002', another_invoice.number
    end

    it 'creates freight and tax fees' do
      invoice.save!
      assert_equal 2, invoice.fees.count
    end

    it 'does not overwrite saved invoices' do
      saved_invoice = create :invoice, number: "ABC123", date: 2.years.ago
      fetched_invoice = Invoice.find(saved_invoice.id)
      assert_equal "ABC123", fetched_invoice.number
      assert_equal 2.years.ago.to_date, fetched_invoice.date
    end
  end

  describe '#total_item_price' do
    it 'sums up all item prices' do
      container1 = create :material_container, price: 100.99
      create :invoice_item, invoice: subject, quantity: 1, material_container: container1
      container2 = create :material_container, price: 15
      create :invoice_item, invoice: subject, quantity: 1, material_container: container2

      assert_equal 115.99, subject.reload.total_item_price
    end
  end

  describe "#total_price_matching?" do
    it 'indicates if total_price is equal to total_item_price' do
      subject.stubs(:total_item_price).returns(88.88)
      subject.total_price = 99.99
      refute subject.total_price_matching?

      subject.total_price = 88.88
      assert subject.total_price_matching?
    end
  end

  describe '#date_str' do
    describe 'getter' do
      it 'returns formatted date' do
        subject.date = Date.new(2016, 10, 27)
        assert_equal "10/27/2016", subject.date_str
      end
    end

    describe 'setter' do
      it 'parses formatted date' do
        subject.date_str = "10/27/2016"
        assert_equal Date.new(2016, 10, 27), subject.date
      end
    end
  end

  describe "#status" do
    describe 'when total prices mismatch' do
      before { subject.stubs(:total_price_matching?).returns(false) }

      it 'returns :error' do
        assert_equal :error, subject.status
      end
    end

    describe 'when total prices do match' do
      before { subject.stubs(:total_price_matching?).returns(true) }

      it 'is :received when :received_date is set' do
        subject.become_received
        assert_equal :received, subject.status
      end

      it 'is :paid when :paid_date is set' do
        subject.become_paid
        assert_equal :paid, subject.status
      end

      it 'is :complete when both paid and received' do
        subject.become_paid
        subject.become_received
        assert_equal :complete, subject.status
      end

      it 'is :pending otherwise' do
        subject.stubs(:total_price_matching?).returns(true)
        assert_equal :pending, subject.status
      end
    end
  end

  describe "#become_paid" do
    it "sets the paid_date" do
      refute subject.paid_date
      subject.become_paid
      assert_equal Time.zone.today, subject.paid_date
    end
  end

  describe "#become_received" do
    it "sets the received_date" do
      refute subject.received_date
      subject.become_received
      assert_equal Time.zone.today, subject.received_date
    end
  end

  describe "#paid?" do
    it "is true when paid_date is set" do
      subject.paid_date = Time.zone.now
      assert subject.paid?
    end

    it "is false otherwise" do
      refute subject.paid?
    end
  end

  describe "#received?" do
    it "is true when received_date is set" do
      subject.received_date = Time.zone.now
      assert subject.received?
    end

    it "is false otherwise" do
      refute subject.received?
    end
  end

  describe "all_items_received?" do
    let(:item1) { create :invoice_item, invoice: subject }
    let(:item2) { create :invoice_item, invoice: subject, received_date: 2.days.ago }
    before { item1; item2 }

    it 'returns true if all items have been received' do
      item1.update_column(:received_date, 1.day.ago)
      assert subject.all_items_received?
    end

    it 'returns false if some items have not been received' do
      refute subject.all_items_received?
    end
  end

  describe "#locked?" do
    it "is true when invoice is received" do
      subject.become_received
      subject.paid_date = nil
      assert subject.locked?
    end

    it "is true when invoice is paid" do
      subject.become_paid
      subject.received_date = nil
      assert subject.locked?
    end

    it "is false when invoice is neither paid nor received" do
      subject.received_date = nil
      subject.paid_date = nil
      refute subject.locked?
    end
  end

  describe "#lead_time_days" do
    before { subject.update_column(:date, 10.days.ago) }

    it 'is the number of days between the order date and the receiving date' do
      subject.received_date = 3.days.ago
      assert_equal 7, subject.lead_time_days
    end

    it 'returns nil if the invoice has not been received yet' do
      subject.received_date = nil
      assert_nil subject.lead_time_days
    end
  end
end
