# frozen_string_literal: true
require 'test_helper'

class LotNumberTest < ActiveSupport::TestCase
  let(:invoice) { create :invoice, number: "ABC" }

  describe '.generate' do
    it 'generates material container lot number' do
      assert_equal "ABC/1", LotNumber.generate(invoice)
    end

    it 'is increased for each new invoice item' do
      create :invoice_item, invoice: invoice
      assert_equal "ABC/2", LotNumber.generate(invoice)
    end
  end
end
