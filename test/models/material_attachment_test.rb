# frozen_string_literal: true
# == Schema Information
#
# Table name: material_attachments
#
#  id            :integer          not null, primary key
#  material_id   :integer          not null
#  attachment_id :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'test_helper'

class MaterialAttachmentTest < ActiveSupport::TestCase
  it 'can create an instance of class' do
    material = create :material
    attachment = create :attachment
    instance = described_class.create material: material, attachment: attachment
    refute_nil instance
  end
end
