# frozen_string_literal: true
# == Schema Information
#
# Table name: material_containers
#
#  id               :integer          not null, primary key
#  material_id      :integer          not null
#  external_name    :text
#  unit_id          :integer          not null
#  amount           :decimal(, )      default(1.0), not null
#  price            :decimal(, )      not null
#  lot_number       :text
#  expiration_date  :date
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  amount_left      :decimal(, )      default(0.0), not null
#  old_status       :integer          default(0)
#  ordering_size_id :integer
#  status           :enum             default("pending")
#
# Foreign Keys
#
#  fk_rails_...  (material_id => materials.id)
#

require 'test_helper'

class MaterialContainerTest < ActiveSupport::TestCase
  subject { build :material_container }

  describe "unit validation" do
    before { subject.unit = create :unit }

    it "is invalid when we do not know how to convert" do
      refute UnitConversion.new(subject.unit, subject.material_unit).valid?
      refute subject.valid?
      assert subject.errors[:unit_id].include? "is incompatible with material unit of measure"
    end

    it "is valid when we know how to convert pkg_unit to material's unit of measure" do
      stub_unit_conversion(subject.unit, subject.material_unit)
      assert subject.valid?
    end
  end

  describe '#material_unit_price' do
    before { subject.stubs(price: 99.99) }

    it 'is calculated from package numbers' do
      subject.stubs(amount_in_material_units: 3)
      assert_in_delta 33.33, subject.material_unit_price
    end

    it 'addresses zero numbers properly' do
      subject.stubs(amount_in_material_units: 0)
      assert_nil subject.material_unit_price
    end

    it 'addresses nils properly' do
      subject.stubs(amount_in_material_units: nil)
      assert_nil subject.material_unit_price
    end
  end

  describe '#unit_price' do
    before { subject.stubs(price: 99.99) }

    it 'is calculated from package numbers' do
      subject.amount = 3
      assert_in_delta 33.33, subject.unit_price
    end

    it 'addresses zero numbers properly' do
      subject.amount = 0
      assert_nil subject.unit_price
    end

    it 'addresses nils properly' do
      subject.amount = nil
      assert_nil subject.unit_price
    end
  end

  describe "#amount_in_material_units" do
    let(:unit) { stub }
    let(:material_unit) { stub }
    before { subject.stubs(unit: unit, material_unit: material_unit, amount: 500) }

    it 'converts amount with UnitConversion' do
      stub_unit_conversion(unit, material_unit, from_amount: 500, to_amount: 7777)
      assert_equal 7777, subject.amount_in_material_units
    end

    it 'returns nil if UnitConversion returns nil' do
      stub_unit_conversion(unit, material_unit, from_amount: 500, to_amount: nil)
      assert_nil subject.amount_in_material_units
    end
  end

  describe '#amount_left_in_material_units' do
    let(:unit) { stub }
    let(:material_unit) { stub }
    before { subject.stubs(unit: unit, material_unit: material_unit, amount_left: 300) }

    it 'converts amount left with UnitConversion' do
      stub_unit_conversion(unit, material_unit, from_amount: 300, to_amount: 400)
      assert_equal 400, subject.amount_left_in_material_units
    end
  end

  describe '#empty?' do
    it 'is true when amount left is less than epsilon' do
      subject.amount_left = 100
      refute subject.empty?
      subject.amount_left = 0
      assert subject.empty?
    end
  end

  describe "#previous" do
    let(:unit) { create :unit }
    let(:material) { create :material, unit: unit }

    it 'is the previous material container for the same material' do
      container_2_days_ago = create_container_with_invoice_item date: 2.days.ago, material: material, unit: unit
      container_3_days_ago = create_container_with_invoice_item date: 3.days.ago, material: material, unit: unit
      container_1_day_ago  = create_container_with_invoice_item date: 1.day.ago, material: material, unit: unit

      assert_equal container_2_days_ago, container_1_day_ago.previous
      assert_equal container_3_days_ago, container_2_days_ago.previous
    end

    it 'returns nil if there is no previous invoice item' do
      container = build :material_container
      assert_nil container.previous
    end
  end

  describe "#previous_container_material_unit_price" do
    let(:container) { build :material_container }

    it 'is the material unit price as of the previous invoice item' do
      prev_container = stub(material_unit_price: 0.55)
      container.stubs(:previous).returns(prev_container)
      assert_equal 0.55, container.previous_container_material_unit_price
    end

    it 'is nil if there is no previous item' do
      assert_nil container.previous_container_material_unit_price
    end
  end

  describe "#material_price_change_percent" do
    let(:container) { build :material_container }

    it "returns percent ratio of the previous and current invoice unit price" do
      container.stubs(:material_unit_price).returns(0.33)
      container.stubs(:previous_container_material_unit_price).returns(0.22)
      assert_equal 50, container.material_price_change_percent.round(5)
    end

    it "returns nil if previous unit price is nil or zero" do
      container.stubs(:material_unit_price).returns(0.33)

      container.stubs(:previous_container_material_unit_price).returns(0)
      assert_nil container.material_price_change_percent

      container.stubs(:previous_container_material_unit_price).returns(nil)
      assert_nil container.material_price_change_percent
    end

    it "returns nil if current unit price is nil or zero" do
      container.stubs(:previous_container_material_unit_price).returns(0.22)

      container.stubs(:material_unit_price).returns(0)
      assert_nil container.material_price_change_percent

      container.stubs(:material_unit_price).returns(nil)
      assert_nil container.material_price_change_percent
    end
  end

  describe "#container_form" do
    it 'refers to ordering_size name' do
      ordering_size = build :ordering_size, name: 'Large barrel'
      container = build :material_container, ordering_size: ordering_size
      assert_equal 'Large barrel', container.container_form
    end

    it 'returns nil if there is no ordering size' do
      container = build :material_container, ordering_size: nil
      assert_nil container.container_form
    end
  end
end
