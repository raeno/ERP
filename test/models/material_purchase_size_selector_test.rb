# frozen_string_literal: true
# == Schema Information
#
# Table name: material_containers
#
#  id              :integer          not null, primary key
#  material_id     :integer          not null
#  external_name   :text
#  container_form_obsolete  :text
#  unit_id         :integer          not null
#  amount          :float            default(1.0), not null
#  price           :decimal(, )      not null
#  lot_number      :text
#  expiration_date :date
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Foreign Keys
#
#  fk_rails_bc3d614920  (material_id => materials.id)
#

require 'test_helper'

class MaterialPurchaseSizeSelectorTest < ActiveSupport::TestCase
  let(:material) { create :material }
  let(:size100) { create :ordering_size, material: material, amount: 100 }
  let(:size200) { create :ordering_size, material: material, amount: 200 }
  let(:size300) { create :ordering_size, material: material, amount: 300 }
  subject { described_class.new material }

  describe '#best_size' do
    it 'is the minimum size that covers the given demand' do
      size100; size200; size300

      assert_equal size100, subject.best_size(80)
      assert_equal size200, subject.best_size(150)
      assert_equal size300, subject.best_size(220)
      assert_equal size300, subject.best_size(900)
    end

    it 'returns nil if there is no sizes for the material' do
      assert material.ordering_sizes.empty?
      assert_nil subject.best_size(80)
    end
  end
end
