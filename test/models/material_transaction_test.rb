# frozen_string_literal: true
# == Schema Information
#
# Table name: material_transactions
#
#  id                    :integer          not null, primary key
#  quantity              :float
#  inventory_amount      :float
#  batch_id              :integer
#  material_container_id :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

require 'test_helper'

class MaterialTransactionTest < ActiveSupport::TestCase
end
