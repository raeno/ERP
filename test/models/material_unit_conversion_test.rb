# frozen_string_literal: true
# == Schema Information
#
# Table name: unit_conversions
#
#  id           :integer          not null, primary key
#  from_unit_id :integer          not null
#  to_unit_id   :integer          not null
#  rate  :decimal(, )      not null
#

require 'test_helper'

class MaterialUnitConversionTest < ActiveSupport::TestCase
  let(:lbs)  { create :unit }
  let(:ml)   { create :unit }
  before { create :conversion_rate, from_unit: lbs, to_unit: ml, rate: 473.18 }
  let(:material) { create :material, unit: lbs, ingredient_unit: ml }
  let(:conversion) { MaterialUnitConversion.new(material) }

  describe "#main_to_ingredient_rate" do
    it "returns the material individual rate if specified" do
      material.unit_conversion_rate = 444.555
      assert_equal 444.555, conversion.main_to_ingredient_rate
    end

    it "returns the generic conversion rate otherwise" do
      assert_equal 473.18, conversion.main_to_ingredient_rate
    end
  end

  describe "#ingredient_to_main_rate" do
    it "returns the inversed material individual rate, if specified" do
      material.unit_conversion_rate = 444.555
      assert_equal 1 / 444.555, conversion.ingredient_to_main_rate.to_f
    end

    it "returns the generic conversion rate otherwise" do
      assert_equal 1 / 473.18, conversion.ingredient_to_main_rate.to_f
    end
  end

  describe ".main_to_ingredient" do
    it 'returns the given amount multiplied by the relevant rate' do
      assert_equal 4731.8, conversion.main_to_ingredient(10)
    end
  end

  describe ".ingredient_to_main" do
    it 'returns the given amount multiplied by the relevant rate' do
      assert_equal 10 / 473.18, conversion.ingredient_to_main(10).to_f
    end
  end
end
