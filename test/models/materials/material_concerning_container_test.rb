# frozen_string_literal: true
# == Schema Information
#
# Table name: materials
#
#  id                  :integer          not null, primary key
#  name                :string           not null
#  unit_price          :decimal(, )      default(0.0), not null
#  created_at          :datetime
#  updated_at          :datetime
#  unit_id             :integer
#  vendor_id           :integer
#  image_url           :string
#  category_id         :integer          default(1), not null
#  previous_unit_price :decimal(, )      default(0.0), not null
#
# Foreign Keys
#
#  fk_rails_e9ba423c07  (vendor_id => vendors.id)
#

require 'test_helper'

module Materials
  class MaterialTest < ActiveSupport::TestCase
    let(:material) { create :material }

    describe "#latest_container" do
      it "is the latest container for this material" do
        create_container_with_invoice_item date: 2.days.ago, material: material, unit: material.unit
        item_1_day_ago = create_container_with_invoice_item date: 1.day.ago, material: material, unit: material.unit
        create_container_with_invoice_item date: 3.days.ago, material: material, unit: material.unit

        assert_equal item_1_day_ago, material.latest_container
      end

      it 'returns nil if there is no containers' do
        assert_nil material.latest_container
      end
    end

    describe "#latest_container_unit_price" do
      it "is the latest container unit price" do
        material.stubs(:latest_container).returns(stub(material_unit_price: 5.99))
        assert_equal 5.99, material.latest_container_unit_price
      end

      it 'returns current material unit price if there is no containers' do
        material.unit_price = 0.77
        assert_nil material.latest_container
        assert_equal 0.77, material.latest_container_unit_price
      end
    end

    describe "#previous_container_unit_price" do
      it "delegates call to the latest container" do
        material.stubs(:latest_container).returns(stub(previous_container_material_unit_price: 5.99))
        assert_equal 5.99, material.previous_container_unit_price
      end

      it 'returns nil if there is no containers' do
        assert_nil material.previous_container_unit_price
      end
    end

    describe "#price_change_percent" do
      it "delegates call to the latest container" do
        material.stubs(:latest_container).returns(stub(material_price_change_percent: 11))
        assert_equal 11, material.price_change_percent
      end

      it 'returns nil if there is no containers' do
        assert_nil material.price_change_percent
      end
    end
  end
end
