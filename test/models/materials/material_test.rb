# frozen_string_literal: true
# == Schema Information
#
# Table name: materials
#
#  id                  :integer          not null, primary key
#  name                :string           not null
#  unit_price          :decimal(, )      default(0.0), not null
#  created_at          :datetime
#  updated_at          :datetime
#  unit_id             :integer
#  vendor_id           :integer
#  pdf_url             :string
#  image_url           :string
#  category_id         :integer          default(1), not null
#  previous_unit_price :decimal(, )      default(0.0), not null
#
# Foreign Keys
#
#  fk_rails_e9ba423c07  (vendor_id => vendors.id)
#

require 'test_helper'

module Materials
  class MaterialTest < ActiveSupport::TestCase
    let(:material) { create :material }

    describe "scope .in_category" do
      it 'filters materials by zone' do
        oils_category = create :material_category
        lemon_oil = create :material, category: oils_category

        packaging_category = create :material_category
        bubble_bag = create :material, category: packaging_category

        oil_materials = Material.in_category(oils_category.id)
        assert_equal 1, oil_materials.count
        assert oil_materials.include? lemon_oil

        packaging_materials = Material.in_category(packaging_category.id)
        assert_equal 1, packaging_materials.count
        assert packaging_materials.include? bubble_bag
      end
    end

    describe "scope .for_zone" do
      let(:make_zone) { create :zone }
      let(:pack_zone) { create :zone }

      it 'filters materials by zone' do
        lemon_oil = create :material
        find_or_create_material_inventory_item(lemon_oil, production_zone: make_zone)

        business_card = create :material
        find_or_create_material_inventory_item(business_card, production_zone: pack_zone)

        olive_oil = create :material
        find_or_create_material_inventory_item(olive_oil, production_zone: make_zone)

        make_materials = Material.for_zone(make_zone.id)
        assert_equal 2, make_materials.count
        assert make_materials.include? lemon_oil
        assert make_materials.include? olive_oil
      end
    end

    describe "scope .ordered_by_zone" do
      let(:ship_zone) { create :zone, production_order: 3 }
      let(:pack_zone) { create :zone, production_order: 2 }
      let(:make_zone) { create :zone, production_order: 1 }

      it 'orders materials by zone production order' do
        business_card = create :material
        find_or_create_material_inventory_item(business_card, production_zone: pack_zone)

        shipping_box = create :material
        find_or_create_material_inventory_item(shipping_box, production_zone: ship_zone)

        lemon_oil = create :material
        find_or_create_material_inventory_item(lemon_oil, production_zone: make_zone)

        materials = Material.ordered_by_zone
        assert_equal lemon_oil, materials.first
        assert_equal shipping_box, materials.last
      end
    end

    describe "ingredient_unit validation" do
      let(:ingredient_unit) { create :unit }
      let(:material) { build :material, ingredient_unit: ingredient_unit }

      it "is valid when we know how to convert to main units" do
        stub_unit_conversion(material.ingredient_unit, material.unit)
        assert material.valid?
      end

      it "is invalid otherwise" do
        refute UnitConversion.new(material.ingredient_unit, material.unit).valid?

        refute material.valid?
        assert material.errors[:ingredient_unit_id].include? "is incompatible with the main unit"
      end
    end

    describe "unit_conversion_rate validation" do
      it "is valid when unit_conversion_rate is nil" do
        material.unit_conversion_rate = nil
        assert material.valid?
      end

      it "is valid when unit_conversion_rate is > 0" do
        material.unit_conversion_rate = 5
        assert material.valid?
      end

      # to avoid division by zero errors when converting
      it "is invalid when unit_conversion_rate is 0" do
        material.unit_conversion_rate = 0
        refute material.valid?
      end
    end
  end
end
