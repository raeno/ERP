# frozen_string_literal: true
# == Schema Information
#
# Table name: ordering_sizes
#
#  id                            :integer          not null, primary key
#  material_id                   :integer          not null
#  unit_id                       :integer          not null
#  amount                        :float            not null
#  amount_in_main_material_units :float            not null
#  name                          :string
#
# Foreign Keys
#
#  fk_rails_...  (material_id => materials.id)
#  fk_rails_...  (unit_id => units.id)
#

require 'test_helper'

class OrderingSizeTest < ActiveSupport::TestCase
  subject { create :ordering_size, amount: 5.44 }

  describe '#to_s' do
    before { subject.unit = build :unit, :oz }

    it 'works fine when there is a name' do
      subject.name = 'Barrel'
      assert_equal 'Barrel (5.44 oz)', subject.to_s
    end

    it 'works fine when there is no name' do
      assert_equal '5.44 oz', subject.to_s
    end
  end

  describe '#estimated_price' do
    it 'returns the last purchased price if the material was purchased in this size' do
      subject.stubs(:last_purchased_price).returns(333)
      assert_equal 333, subject.estimated_price
    end

    it 'calculates price based on the material unit price otherwise' do
      material = subject.material
      material.update_column(:unit_price, 22.22)
      subject.amount_in_main_material_units = 0.2
      assert_equal 4.44, subject.estimated_price
    end
  end

  describe '#last_purchased_price' do
    it 'is the price of the last purchased container of this material and size' do
      create :material_container, ordering_size: subject, price: 100
      create :material_container, ordering_size: subject, price: 200
      assert_equal 200, subject.last_purchased_price
    end

    it 'returns nil if material has never been purchased in this size' do
      assert_nil subject.last_purchased_price
    end
  end

  describe 'amount_in_main_material_units' do
    before { subject }

    it 'is set on save' do
      stub_unit_conversion(subject.unit, subject.material_unit, from_amount: 5.44, to_amount: 555.77)
      subject.amount_in_main_material_units = nil

      subject.save!
      assert_equal 555.77, subject.amount_in_main_material_units
    end
  end

  describe 'unit' do
    it 'is validated against material unit compatibility' do
      assert subject.valid?
      subject.unit = create :unit
      refute subject.valid?
    end
  end
end
