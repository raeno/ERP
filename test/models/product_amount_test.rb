# frozen_string_literal: true
require 'test_helper'

class ProductAmountTest < ActiveSupport::TestCase
  describe '#days' do
    it 'converts items to days of/to cover' do
      assert_equal 3.3, described_class.new(10).days(3)
    end

    describe 'when items_per_day is 0' do
      it 'returns infinity' do
        assert_equal Float::INFINITY, described_class.new(10).days(0)
      end
    end
  end

  describe '#cases_floor' do
    it 'converts numbers to cases' do
      assert_equal 2, described_class.new(333).cases_floor(150)
    end

    describe 'when items_per_case is 0' do
      it 'returns 0' do
        assert_equal 0, described_class.new(10).cases_floor(0)
      end
    end
  end

  describe '#cases_ceil' do
    it 'converts (ceils) number of items to integral number of cases' do
      assert_equal 3, described_class.new(333).cases_ceil(150)
    end

    describe 'when quantity_limit is passed' do
      let(:product_amount) { described_class.new(230) }

      it 'ceils if quantity_limit alows ceiling' do
        # 230 items -> 4 * 100pc when limit >= 400
        assert_equal 3, product_amount.cases_ceil(100, 1000)
        assert_equal 3, product_amount.cases_ceil(100, 300)
      end

      it 'floors the quantity_limit otherwise' do
        assert_equal 2, product_amount.cases_ceil(100, 299)
        assert_equal 2, product_amount.cases_ceil(100, 200)
        assert_equal 1, product_amount.cases_ceil(100, 199)
        assert_equal 0, product_amount.cases_ceil(100,  99)
      end
    end

    describe 'when items_per_case is 0' do
      it 'returns 0' do
        assert_equal 0, described_class.new(10).cases_ceil(0)
      end
    end

    describe 'when items_count is nil' do
      it 'returns 0' do
        assert_equal 0, described_class.new(nil).cases_ceil(150)
      end
    end
  end
end
