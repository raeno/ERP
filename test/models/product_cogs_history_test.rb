# frozen_string_literal: true
require 'test_helper'

class ProductCostHistoryTest < ActiveSupport::TestCase
  let(:pack_zone) { create :zone, :pack }
  let(:ship_zone) { create :zone, :ship }

  let(:product) { build :product }

  let(:history_2y_ago) do
    create :product_cogs_snapshot, product: product, zone: ship_zone, created_at: 2.years.ago
  end
  let(:history_11mo_ago) do
    create :product_cogs_snapshot, product: product, zone: ship_zone, created_at: 11.months.ago
  end
  let(:history_5mo_ago) do
    create :product_cogs_snapshot, product: product, zone: pack_zone, created_at: 5.months.ago
  end
  let(:history_3d_ago) do
    create :product_cogs_snapshot, product: product, zone: ship_zone, created_at: 3.days.ago
  end
  before { history_2y_ago; history_11mo_ago; history_5mo_ago; history_3d_ago }

  describe '#latest_history_record' do
    it 'returns the latest history record for the specified zone' do
      record = ProductCogsHistory.new(product, pack_zone).latest_history_record
      assert_equal history_5mo_ago, record
    end

    describe "without arguments" do
      it 'returns the latest ship history record' do
        record = ProductCogsHistory.new(product).latest_history_record
        assert_equal history_3d_ago, record
      end
    end
  end

  describe "#previous_prime_cost" do
    it 'returns the #latest_history_record cost value' do
      history_5mo_ago.update_column(:cogs, 7.99)
      assert_equal 7.99, ProductCogsHistory.new(product, pack_zone).previous_prime_cost
    end

    describe "without arguments" do
      it 'assumes ship zone' do
        history_3d_ago.update_column(:cogs, 8.77)
        assert_equal 8.77, ProductCogsHistory.new(product).previous_prime_cost
      end
    end
  end

  describe "#prime_cost_change_percent" do
    it 'says how much COGS in certain zone have changed since the last time' do
      product.stubs(:cumulative_cogs).with(pack_zone).returns(3.33)
      history_record = ProductCogsHistory.new(product, pack_zone)
      history_record.stubs(:previous_prime_cost).returns(2.22)
      assert_equal 50, history_record.prime_cost_change_percent.round
    end

    describe "without arguments" do
      it 'assumes ship zone' do
        product.stubs(:prime_cost).returns(2.22)
        history_record = ProductCogsHistory.new(product)
        history_record.stubs(:previous_prime_cost).returns(4.44)
        assert_equal(-50, history_record.prime_cost_change_percent.round)
      end
    end

    it 'returns nil if current cost is undefined' do
      product.stubs(:cumulative_cogs).with(pack_zone).returns(nil)
      assert_nil ProductCogsHistory.new(product, pack_zone).prime_cost_change_percent
    end

    it 'returns nil if previous cost is undefined' do
      history_record = ProductCogsHistory.new(product)
      history_record.stubs(:previous_prime_cost).returns(nil)
      assert_nil history_record.prime_cost_change_percent
    end
  end

  describe '#a_year_ago_history_record' do
    it 'returns the oldest history record within 1 year period' do
      record = ProductCogsHistory.new(product).a_year_ago_history_record
      assert_equal history_11mo_ago, record
    end
  end

  describe "#a_year_ago_prime_cost" do
    it 'returns the #latest_history_record cost value' do
      history_11mo_ago.update_column(:cogs, 7.99)
      assert_equal 7.99, ProductCogsHistory.new(product).a_year_ago_prime_cost
    end
  end

  describe "#prime_cost_annual_change_percent" do
    it 'says how much COGS have changed since the past year' do
      product.stubs(:prime_cost).returns(3.33)
      history_record = ProductCogsHistory.new(product)
      history_record.stubs(:a_year_ago_prime_cost).returns(2.22)
      assert_equal 50, history_record.prime_cost_annual_change_percent.round
    end

    it 'returns nil if current cost is undefined' do
      product.stubs(:prime_cost).returns(nil)
      assert_nil ProductCogsHistory.new(product).prime_cost_annual_change_percent
    end

    it 'returns nil if previous cost is undefined' do
      history_record = ProductCogsHistory.new(product)
      history_record.stubs(:a_year_ago_prime_cost).returns(nil)
      assert_nil history_record.prime_cost_annual_change_percent
    end
  end
end
