# frozen_string_literal: true
# == Schema Information
#
# Table name: product_cogs_snapshots
#
#  id         :integer          not null, primary key
#  product_id :integer
#  batch_id   :integer
#  cogs       :decimal(, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  zone_id    :integer
#

require 'test_helper'

class ProductCogsSnapshotTest < ActiveSupport::TestCase
  describe '.past_year scope' do
    it 'returns records that are less than 1 year old' do
      record = create :product_cogs_snapshot, created_at: 3.months.ago
      irrelevant_record = create :product_cogs_snapshot, created_at: 2.years.ago

      collection = ProductCogsSnapshot.past_year
      assert collection.include? record
      refute collection.include? irrelevant_record
    end
  end
end
