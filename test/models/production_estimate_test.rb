# frozen_string_literal: true

require 'test_helper'

class ProductionEstimateTest < ActiveSupport::TestCase
  describe "#duration_for" do
    let(:ii) { create :inventory_item }

    it 'is production time estimate (in seconds) for  the given inventory item and given quantity' do
      ii.stubs(:production_seconds_per_unit).returns(1.2345)
      ii.stubs(:production_setup_minutes).returns(40)
      assert_equal 123 + 40*60, described_class.new(ii).duration_for(100)
    end

    it 'returns nil if seconds_per_unit is nil' do
      ii.stubs(:production_seconds_per_unit).returns(nil)
      assert_nil described_class.new(ii).duration_for(100)
    end

    it 'returns nil if production_setup_minutes is nil' do
      ii.stubs(:production_setup_minutes).returns(nil)
      assert_nil described_class.new(ii).duration_for(100)
    end

    it 'returns nil if passed quantity is nil' do
      ii.stubs(:production_seconds_per_unit).returns(1.2345)
      assert_nil described_class.new(ii).duration_for(nil)
    end
  end
end
