# frozen_string_literal: true
require 'test_helper'

module ProductionPlan
  class DemandTest < ActiveSupport::TestCase
    describe "#build" do
      let(:inventory_item) { stub }

      it "inits demand and supply objects" do
        ingredient_demand = stub
        ProductionPlan::IngredientDemand.expects(:new).with(inventory_item).returns(ingredient_demand)

        next_zone_demand = stub
        ProductionPlan::NextZoneDemand.expects(:new).with(inventory_item).returns(next_zone_demand)

        described_class.expects(:new).with(ingredient_demand, next_zone_demand).once
        described_class.build(inventory_item)
      end
    end

    describe "#value" do
      it 'sums ingredient demand and next zone demand values' do
        ingredient_demand = stub(value: 111)
        next_zone_demand  = stub(value: 222)
        demand = described_class.new(ingredient_demand, next_zone_demand)
        assert_equal 111 + 222, demand.value
      end
    end

    describe "#breakdown" do
      it 'combines ingredient demand and next zone demand breakdowns' do
        product_1 =  stub
        product_2 =  stub
        ingredient_breakdown = { product_1 => 111, product_2 => 222 }
        ingredient_demand = stub(breakdown: ingredient_breakdown)

        next_zone_breakdown = { "Pack" => 555 }
        next_zone_demand = stub(breakdown: next_zone_breakdown)

        expected_breakdown = {
          as_ingredient: ingredient_breakdown,
          for_next_zone: next_zone_breakdown
        }
        demand = described_class.new(ingredient_demand, next_zone_demand)
        assert_equal expected_breakdown, demand.breakdown
      end
    end
  end
end
