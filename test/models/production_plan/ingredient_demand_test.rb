# frozen_string_literal: true
require 'test_helper'

module ProductionPlan
  class IngredientDemandTest < ActiveSupport::TestCase
    before do
      create :zone, :make
      create :zone, :pack
      create :zone, :ship
    end

    describe "#value" do
      it "sums up demand breakdown" do
        demand = described_class.new(stub)
        demand.stubs(:breakdown).returns({ 'a' => 2, 'b' => 3, 'c' => 4 })
        assert_equal 2 + 3 + 4, demand.value
      end
    end

    describe "#breakdown" do
      describe "for material inventoriables" do
        describe "for Make zone" do
          # Init the material
          let(:orange_oil_material) { create :material, name: "Orange oil" }
          let(:orange_oil_material_inventory_item) do
            find_or_create_material_inventory_item orange_oil_material, production_zone: Zone.make_zone
          end

          # Init a product that includes the material
          before do
            @orange_oil_product = create_product_with_data(
              ingredient: { inventory_item: orange_oil_material_inventory_item, quantity: 10 },
              to_cover: { in_zone: Zone.make_zone, quantity: 55 }
            )
          end

          # Init another product that includes the material
          before do
            @citrus_mix_product = create_product_with_data(
              ingredient: { inventory_item: orange_oil_material_inventory_item, quantity: 3.3 },
              to_cover: { in_zone: Zone.make_zone, quantity: 44 }
            )
          end

          # Init another product that is in demand but does NOT include the material
          before do
            create_product_with_data(
              to_cover: { in_zone: Zone.make_zone, quantity: 1000 }
            )
          end

          it "includes all products that include the material as component" do
            expected_breakdown = {
              @orange_oil_product => 55 * 10,
              @citrus_mix_product => 44 * 3.3
            }

            calculated_breakdown = described_class.new(orange_oil_material_inventory_item).breakdown

            assert_equal expected_breakdown.count, calculated_breakdown.count
            assert_equal  expected_breakdown[@orange_oil_product].round(10),
                          calculated_breakdown[@orange_oil_product].round(10)
            assert_equal  expected_breakdown[@citrus_mix_product].round(10),
                          calculated_breakdown[@citrus_mix_product].round(10)
          end
        end

        describe "for Pack zone" do
          # Init a material
          let(:bubble_bag_material) { create :material, name: "Orange oil" }
          let(:bubble_bag_material_inventory_item) do
            find_or_create_material_inventory_item bubble_bag_material, production_zone: Zone.pack_zone
          end

          # Init a product that includes the material
          before do
            @orange_oil_product = create_product_with_data(
              ingredient: { inventory_item: bubble_bag_material_inventory_item, quantity: 1 },
              to_cover: { in_zone: Zone.pack_zone, quantity: 120 }
            )
          end

          # Init another product that includes the material
          before do
            @citrus_mix_product = create_product_with_data(
              ingredient: { inventory_item: bubble_bag_material_inventory_item, quantity: 2 },
              to_cover: { in_zone: Zone.pack_zone, quantity: 22 }
            )
          end

          # Init another product that is in demand but does NOT include the material
          before do
            create_product_with_data(
              to_cover: { in_zone: Zone.pack_zone, quantity: 1000 }
            )
          end

          it "includes all products that include the material as component" do
            expected_breakdown = {
              @orange_oil_product => BigDecimal.new(120),
              @citrus_mix_product => BigDecimal.new(22 * 2)
            }

            calculated_breakdown = described_class.new(bubble_bag_material_inventory_item).breakdown

            assert_equal expected_breakdown, calculated_breakdown
          end
        end

        describe "for Ship zone" do
          # Init the material
          let(:carton_box_material) { create :material }
          let(:carton_box_material_inventory_item) do
            find_or_create_material_inventory_item carton_box_material, production_zone: Zone.ship_zone
          end

          # Init a product that includes the material
          before do
            @orange_oil_10ml_product = create_product_with_data(
              ingredient: { inventory_item: carton_box_material_inventory_item, quantity: 1.0 / 150 },
              to_cover: { in_zone: Zone.ship_zone, quantity: 550 }
            )
          end

          # Init another product that includes the material
          before do
            @orange_oil_30ml_product = create_product_with_data(
              ingredient: { inventory_item: carton_box_material_inventory_item, quantity: 1.0 / 70 },
              to_cover: { in_zone: Zone.ship_zone, quantity: 700 }
            )
          end

          # Init another product that is in demand but does NOT include the material
          before do
            create_product_with_data(
              to_cover: { in_zone: Zone.ship_zone, quantity: 1000 }
            )
          end

          it "includes all products that include the material as component" do
            expected_breakdown = {
              @orange_oil_10ml_product => 550.0 / 150,
              @orange_oil_30ml_product => 700.0 / 70
            }

            calculated_breakdown = described_class.new(carton_box_material_inventory_item).breakdown

            assert_equal expected_breakdown.count, calculated_breakdown.count
            assert_equal  expected_breakdown[@orange_oil_10ml_product].round(10),
                          calculated_breakdown[@orange_oil_10ml_product].round(10)
            assert_equal  expected_breakdown[@orange_oil_30ml_product].round(10),
                          calculated_breakdown[@orange_oil_30ml_product].round(10)
          end
        end
      end

      describe "for product inventoriables" do
        describe "for made products" do
          # Init made bottle product
          let(:bottle_product) { create_product_in_zones Zone.make_zone }
          let(:bottle_inventory_item) { find_inventory_item(bottle_product, Zone.make_zone) }

          describe 'when the product is not included in any giftboxes' do
            it "is empty" do
              calculated_breakdown = described_class.new(bottle_inventory_item).breakdown
              assert calculated_breakdown.empty?
            end
          end

          describe 'when the product is part of giftboxes' do
            # Create a gift box that includes the bottle, with 187pc to cover
            let(:citrus_mix_gift_box) do
              create_product_with_data(
                ingredient: { inventory_item: bottle_inventory_item, quantity: 1 },
                to_cover: { in_zone: Zone.pack_zone, quantity: 187 }
              )
            end

            # Create another gift box by analogy, with 900pc to cover
            let(:top_oils_gift_box) do
              create_product_with_data(
                ingredient: { inventory_item: bottle_inventory_item, quantity: 1 },
                to_cover: { in_zone: Zone.pack_zone, quantity: 900 }
              )
            end

            it "includes the gift box 'to_cover' value" do
              expected_breakdown = {
                citrus_mix_gift_box => BigDecimal.new(187),
                top_oils_gift_box => BigDecimal.new(900)
              }
              calculated_breakdown = described_class.new(bottle_inventory_item).breakdown
              assert_equal expected_breakdown, calculated_breakdown
            end
          end
        end
      end
    end
  end
end
