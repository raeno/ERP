# frozen_string_literal: true
require 'test_helper'

module ProductionPlan
  class LabourTest < ActiveSupport::TestCase
    let(:inventory_item) { create :inventory_item }
    let(:subject) { described_class.new(inventory_item) }
  end
end
