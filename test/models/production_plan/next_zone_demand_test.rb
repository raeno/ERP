# frozen_string_literal: true
require 'test_helper'

module ProductionPlan
  class NextZoneDemandTest < ActiveSupport::TestCase
    before do
      create :zone, :make
      create :zone, :pack
      create :zone, :ship
    end

    describe "#value" do
      it "sums up demand breakdown" do
        demand = described_class.new(stub)
        demand.stubs(:breakdown).returns({ 'a' => 2, 'b' => 3, 'c' => 4 })
        assert_equal 2 + 3 + 4, demand.value
      end
    end

    describe "#breakdown" do
      describe "for material inventoriables" do
        let(:material) { create :material, name: "Orange oil" }
        let(:material_inventory_item) do
          find_or_create_material_inventory_item material, production_zone: (create :zone)
        end

        it 'returns an empty hash' do
          breakdown = described_class.new(material_inventory_item).breakdown
          assert breakdown.empty?
        end
      end

      describe "for product inventoriables" do
        describe "for made products" do
          # Init a product that has 120pc packed items to cover
          let(:product) { create_product_in_zones Zone.make_zone }
          let(:made_inventory_item) { find_inventory_item(product, Zone.make_zone) }
          before do
            stub_product_to_cover_plan(product: product, in_zone: Zone.pack_zone, to_cover: 120)
          end

          it "returns pack 'to_cover' value" do
            expected_breakdown = { "Pack" => 120 }
            calculated_breakdown = described_class.new(made_inventory_item).breakdown
            assert_equal expected_breakdown, calculated_breakdown
          end
        end

        describe "for packed products" do
          # Init a product that has 222pc ship demand
          let(:product) { create_product_in_zones Zone.pack_zone }
          let(:packed_inventory_item) do
            find_or_create_product_inventory_item product,
              production_zone: Zone.ship_zone, passed_zone: Zone.pack_zone
          end
          before do
            stub_product_to_cover_plan(product: product, in_zone: Zone.ship_zone, to_cover: 222)
          end

          it "is equal to ship demand" do
            expected_breakdown = { "Ship" => 222 }
            calculated_breakdown = described_class.new(packed_inventory_item).breakdown
            assert_equal expected_breakdown, calculated_breakdown
          end
        end

        describe "for shipped (FBA) products" do
          let(:product) { create :product, items_per_case: 149, production_buffer_days: 30 }
          let(:shipped_inventory_item) do
            find_or_create_product_inventory_item product,
              production_zone: nil, passed_zone: Zone.ship_zone
          end

          describe "when we completely ran out of product on Amazon" do
            describe "when demand is less or equal to product case capacity" do
              it "suggests to ship at least 1 full case" do
                product.amazon_inventory = create :amazon_inventory, :zero_inventory, reserved: 2
                product.save!

                expected_breakdown = { "FBA" => 149 }
                calculated_breakdown = described_class.new(shipped_inventory_item).breakdown

                assert_equal expected_breakdown, calculated_breakdown
              end
            end

            describe "when demand is higher than product case capacity" do
              it "suggests to ship the demand" do
                product.amazon_inventory = create :amazon_inventory, :zero_inventory, reserved: 60
                product.save!

                expected_breakdown = { "FBA" => 60 * 30 }
                calculated_breakdown = described_class.new(shipped_inventory_item).breakdown

                assert_equal expected_breakdown, calculated_breakdown
              end
            end
          end

          describe "when there is some product (at least 1 item) on Amazon" do
            it "is equal to reserved * days_bufer" do
              product.amazon_inventory = create :amazon_inventory, reserved: 2
              product.save!

              # Intentionally testing the case when demand < items_per_case in order to avoid false positives.
              demand = product.reserved * product.production_buffer_days
              assert demand < product.items_per_case

              expected_breakdown = { "FBA" => 2 * 30 }
              calculated_breakdown = described_class.new(shipped_inventory_item).breakdown

              assert_equal expected_breakdown, calculated_breakdown
            end
          end
        end
      end
    end
  end
end
