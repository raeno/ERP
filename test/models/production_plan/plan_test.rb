# frozen_string_literal: true
require 'test_helper'

module ProductionPlan
  class PlanTest < ActiveSupport::TestCase
    before do
      create :zone, :make
      create :zone, :pack
      create :zone, :ship
    end

    describe "#build" do
      let(:demand_calc) { stub }
      before { ProductionPlan::Demand.expects(:build).with(inventory_item).returns(demand_calc) }

      let(:labour_calc) { stub }
      before { ProductionPlan::Labour.expects(:new).with(inventory_item).returns(labour_calc) }

      describe "for material inventory" do
        let(:inventory_item) { stub(:product? => false) }
        it "inits demand calc object, sets supply/labour calc to nil, calls intializer" do
          described_class.expects(:new).with(inventory_item, demand_calc, nil, labour_calc).once
          described_class.build(inventory_item)
        end
      end

      describe "for product inventory" do
        let(:product) { stub }
        let(:zone) { stub }
        let(:inventory_item) { stub(:product? => true, product: product, zone: zone) }

        let(:supply_calc) { stub }
        before { ProductionPlan::Supply.expects(:new).with(product, zone).returns(supply_calc) }

        it "inits demand, supply and labour objects and calls intializer" do
          described_class.expects(:new).with(inventory_item, demand_calc, supply_calc, labour_calc).once
          described_class.build(inventory_item)
        end
      end
    end

    describe "#demand" do
      let(:demand_calc) { stub(value: 555.5) }

      describe "for material inventory" do
        let(:inventory_item) { stub(:product? => false) }
        let(:plan) { described_class.new(inventory_item, demand_calc, stub, stub) }

        it "redirects call to demand_calc" do
          assert_equal 555.5, plan.demand
        end
      end

      describe "for product inventory" do
        let(:inventory_item) { stub(:product? => true) }
        let(:plan) { described_class.new(inventory_item, demand_calc, stub, stub) }

        it "redirects call to demand_calc and converts the result to integer" do
          assert_equal 555.5, plan.demand
        end
      end
    end

    describe "#supply" do
      describe "for material inventory, i.e. where supply_calc is nil" do
        let(:inventory_item) { stub(:product? => false) }
        let(:plan) { described_class.new(inventory_item, stub, nil, stub) }

        it "retruns a very big number to mimic infinite material supply" do
          assert plan.supply > 999_999_999_999_999
        end
      end

      describe "for product inventory" do
        let(:supply_calc) { stub(value: 777.7) }
        let(:inventory_item) { stub(:product? => true) }
        let(:plan) { described_class.new(inventory_item, stub, supply_calc, stub) }

        it "redirects call to supply_calc" do
          assert_equal 777.7, plan.supply
        end
      end
    end

    describe "#current_inventory" do
      it "redirects call to inventory_item" do
        inventory_item = stub(inventory_quantity: 888)
        plan = described_class.new(inventory_item, stub, stub, stub)
        assert_equal 888, plan.current_inventory
      end

      # This is tested in InventoryItem already, but is being tested here as well
      # to prevent ProductionPlan from breaking in case of InventoryItem.inventory_quantity calc changes.
      describe "for product inventory on ship zone" do
        it 'returns amazon inventory quantity' do
          product = build :product
          inventory_item = create :inventory_item, inventoriable: product, zone: Zone.ship_zone
          inventory_item.stubs(:inventory).returns(stub(quantity: 99_999))  # this should not be returned
          product_stats = stub(total_amazon_inventory: 555)
          Products::ProductInventoryStats.stubs(:new).with(product).returns(product_stats)

          plan = described_class.new(inventory_item, stub, stub, stub)
          assert_equal 555, plan.current_inventory
        end
      end
    end

    describe "#to_cover" do
      let(:inventory_item) { stub :product? => true, :in_ship_zone? => false }
      let(:plan) { described_class.new(inventory_item, stub, stub, stub) }

      describe "when demand is higher than the current inventory" do
        it "returns the difference" do
          plan.stubs(:demand).returns(999)
          plan.stubs(:current_inventory).returns(555)
          assert_equal 999 - 555, plan.to_cover
        end
      end

      describe "when overstocked, ie demand is lesser or equal to the current inventory" do
        it "returns zero rather than negative" do
          plan.stubs(:demand).returns(500)
          plan.stubs(:current_inventory).returns(500)
          assert_equal 0, plan.to_cover

          plan.stubs(:demand).returns(100)
          assert_equal 0, plan.to_cover
        end
      end
    end

    describe "#can_cover" do
      let(:plan) { described_class.new(stub, stub, stub, stub) }

      describe "when to_cover value can be supplied" do
        it "returns to_cover value" do
          plan.stubs(:to_cover).returns(4444)
          plan.stubs(:supply).returns(999_999)
          assert_equal 4444, plan.can_cover

          plan.stubs(:to_cover).returns(333)
          plan.stubs(:supply).returns(333)
          assert_equal 333, plan.can_cover
        end
      end

      describe "when to_cover value canNOT be supplied" do
        it "returns the supply value (maximum items that can be supplied)" do
          plan.stubs(:to_cover).returns(88)
          plan.stubs(:supply).returns(33)
          assert_equal 33, plan.can_cover
        end
      end
    end

    describe "#can_cover_in_cases" do
      describe "for products" do
        let(:inventory_item) { stub(product?: true, product_items_per_case: 70) }
        let(:plan) { described_class.new(inventory_item, stub, stub, stub) }

        describe "when there is enough inventory to round cases up" do
          it 'converts #can_cover to integer cases by rounding up' do
            plan.stubs(:can_cover).returns(33 * 70 + 1)
            plan.stubs(:supply).returns(99_999_999)
            assert_equal 34, plan.can_cover_in_cases
          end
        end

        describe "otherwise" do
          it 'converts #can_cover to integer cases by rounding down' do
            plan.stubs(:can_cover).returns(33 * 70 + 1)
            plan.stubs(:supply).returns(33 * 70 + 69)
            assert_equal 33, plan.can_cover_in_cases
          end
        end
      end

      describe "for materials" do
        let(:inventory_item) { stub(product?: false, product_items_per_case: nil) }
        let(:plan) { described_class.new(inventory_item, stub, stub, stub) }

        it 'returns nil' do
          assert_nil plan.can_cover_in_cases
        end
      end
    end

    describe "#can_cover_optimal" do
      describe "for products" do
        let(:inventory_item) { stub(material?: false, product_items_per_case: 70) }
        let(:plan) { described_class.new(inventory_item, stub, stub, stub) }

        it 'is can_cover_in_cases * items_per_case and in general not equal to can_cover' do
          plan.stubs(:in_make_zone?).returns(false)
          plan.stubs(:can_cover_in_cases).returns(7)
          assert_equal 490, plan.can_cover_optimal
        end

        it 'for make zone, returns can_cover because cases are irrelevant' do
          plan.stubs(:in_make_zone?).returns(true)
          plan.stubs(:can_cover).returns(444)
          assert_equal 444, plan.can_cover_optimal
        end
      end

      describe "for materials" do
        let(:inventory_item) { stub(material?: true) }
        let(:plan) { described_class.new(inventory_item, stub, stub, stub) }

        it 'is the same as can_cover' do
          plan.stubs(:can_cover).returns(555)
          assert_equal 555, plan.can_cover_optimal
        end
      end
    end

    describe "#fully_supplied?" do
      let(:plan) { described_class.new(stub, stub, stub, stub) }

      describe "when to_cover value can be supplied" do
        it "returns true" do
          plan.stubs(:to_cover).returns(4444)
          plan.stubs(:supply).returns(999_999)
          assert plan.fully_supplied?

          plan.stubs(:to_cover).returns(333)
          plan.stubs(:supply).returns(333)
          assert plan.fully_supplied?
        end
      end

      describe "when to_cover value canNOT be supplied" do
        it "returns false" do
          plan.stubs(:to_cover).returns(88)
          plan.stubs(:supply).returns(33)
          refute plan.fully_supplied?
        end
      end
    end

    describe "#deficit_in_previous_zone?" do
      it 'returns false if there is no ingredient shortfall' do
        plan = described_class.new(stub, stub, stub, stub)
        plan.stubs(:fully_supplied?).returns(true)
        refute plan.deficit_in_previous_zone?
      end

      it 'returns true if there is a shortfall and it is the previous zone' do
        supply_calc = stub(previous_zone_is_least_supplied?: true)
        plan = described_class.new(stub, stub, supply_calc, stub)
        plan.stubs(:fully_supplied?).returns(false)

        assert plan.deficit_in_previous_zone?
      end
    end

    describe "#deficit_inventoriable" do
      it 'returns nil if there is no ingredient shortfall' do
        plan = described_class.new(stub, stub, stub, stub)
        plan.stubs(:fully_supplied?).returns(true)
        assert_nil plan.deficit_inventoriable
      end

      it 'returns nil if there is a shortfall but it is the previous zone' do
        plan = described_class.new(stub, stub, stub, stub)
        plan.stubs(:fully_supplied?).returns(false)
        plan.stubs(:deficit_in_previous_zone?).returns(true)

        assert_nil plan.deficit_inventoriable
      end

      describe 'when there is a shortfall of an ingredient other than the prev. zone' do
        before do
          @product = create :product
          inventory_item = create :inventory_item, inventoriable: @product
          @ingredient = create :ingredient, inventory_item: inventory_item
        end

        it 'returns this ingredient inventoriable' do
          supply_calc = stub(least_supplied_ingredient_id: @ingredient.id)
          plan = described_class.new(stub, stub, supply_calc, stub)
          plan.stubs(:fully_supplied?).returns(false)
          plan.stubs(:deficit_in_previous_zone?).returns(false)

          assert_equal @product, plan.deficit_inventoriable
        end
      end
    end
  end
end
