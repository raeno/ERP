# frozen_string_literal: true
require 'test_helper'

module ProductionPlan
  class SupplyTest < ActiveSupport::TestCase
    before do
      create :zone, :make
      create :zone, :pack
      create :zone, :ship
    end

    describe "#value" do
      let(:supply) { described_class.new(stub, stub) }
      it "is a minimum value from supply breakdown (ingredients case)" do
        breakdown =
          {
            ingredients: { 1 => 2.0, 2 => 3 },
            previous_zone: { 'Make' => 4.5 }
          }
        supply.stubs(:breakdown).returns(breakdown)
        assert_equal 2, supply.value
      end

      it "is a minimum value from supply breakdown (previous_zone case)" do
        breakdown =
          {
            ingredients: { 1 => 100, 2 => 200 },
            previous_zone: { 'Make' => 4.5 }
          }
        supply.stubs(:breakdown).returns(breakdown)
        assert_equal 4.5, supply.value
      end
    end

    describe "#value_for_ingredient" do
      let(:ingredient) { stub(id: 22) }
      let(:breakdown) do
        { ingredients: { 11 => 11.1, 22 => 22.2 } }
      end
      let(:supply) { described_class.new(stub, stub) }
      before { supply.stubs(:breakdown).returns(breakdown) }

      it "returns breakdown value for the ingredent" do
        assert_equal 22.2, supply.value_for_ingredient(ingredient)
      end

      it "returns nil if ingredent is not in the breakdown" do
        another_ingredient = stub(id: 33)
        assert_nil supply.value_for_ingredient(another_ingredient)
      end
    end

    describe "#breakdown" do
      describe "for single-bottle products" do
        # Init "Citrus Mix" product that consists of lemon and orange oil,
        # empty bottle and bubble bag.
        let(:citrus_mix_product) { create_product_in_zones Zone.make_zone }

        # Init Lemon Oil material.
        # We need 3ml of Lemon Oil to make Citrus Mix,
        # and we've got 600ml in stock, which is enough for 200 bottles of Citrus Mix.
        let(:lemon_oil_material) { create :material }
        let(:lemon_oil_inventory_item) do
          find_or_create_material_inventory_item lemon_oil_material,
            production_zone: Zone.make_zone, quantity: 600
        end
        let(:lemon_oil_ingredient) do
          include_inventory_item_to_product(
            inventory_item: lemon_oil_inventory_item,
            product: citrus_mix_product,
            ingredient_quantity: 3
          )
        end

        # Init Orange Oil material.
        # We need 7ml of Orange Oil to make Citrus Mix,
        # and we've got 700ml in stock, which is enough for 100 bottles of Citrus Mix.
        let(:orange_oil_material) { create :material }
        let(:orange_oil_inventory_item) do
          find_or_create_material_inventory_item orange_oil_material,
            production_zone: Zone.make_zone, quantity: 700
        end
        let(:orange_oil_ingredient) do
          include_inventory_item_to_product(
            inventory_item: orange_oil_inventory_item,
            product: citrus_mix_product,
            ingredient_quantity: 7
          )
        end

        # Init Empty Bottle material.
        # We need 1pc to make Citrus Mix,
        # and we've got 300pc in stock, which is enough for 300 bottles of Citrus Mix.
        let(:empty_bottle_material) { create :material }
        let(:empty_bottle_inventory_item) do
          find_or_create_material_inventory_item empty_bottle_material,
            production_zone: Zone.make_zone, quantity: 300
        end
        let(:empty_bottle_ingredient) do
          include_inventory_item_to_product(
            inventory_item: empty_bottle_inventory_item,
            product: citrus_mix_product,
            ingredient_quantity: 1
          )
        end

        # Init Bobble Bag material.
        # We need 1pc to PACK Citrus Mix,
        # and we've got 50pc in stock, which is enough for 500 bottles of Citrus Mix.
        let(:bubble_bag_material)   { create :material }
        let(:bubble_bag_inventory_item) do
          find_or_create_material_inventory_item bubble_bag_material,
            production_zone: Zone.pack_zone, quantity: 50
        end
        let(:bubble_bag_ingredient) do
          include_inventory_item_to_product(
            inventory_item: bubble_bag_inventory_item,
            product: citrus_mix_product,
            ingredient_quantity: 1
          )
        end

        before do
          [lemon_oil_ingredient, orange_oil_ingredient, empty_bottle_ingredient, bubble_bag_ingredient]
        end

        describe "for Make zone" do
          it "says how many product we've got Make-zone ingredients for" do
            expected_breakdown = {
              ingredients: {
                lemon_oil_ingredient.id    => BigDecimal.new(200),
                orange_oil_ingredient.id   => BigDecimal.new(100),
                empty_bottle_ingredient.id => BigDecimal.new(300)
              },
              previous_zone: {}
            }
            calculated_breakdown = described_class.new(citrus_mix_product, Zone.make_zone).breakdown

            assert_equal expected_breakdown, calculated_breakdown
          end
        end

        describe "for Pack zone" do
          it "says how many product we've got Pack-zone ingredients AND made product for" do
            find_or_create_product_inventory_item citrus_mix_product,
              production_zone: Zone.pack_zone, passed_zone: Zone.make_zone, quantity: 555

            expected_breakdown = {
              ingredients: { bubble_bag_ingredient.id => BigDecimal.new(50) },
              previous_zone: { "Make" => 555 }
            }
            calculated_breakdown = described_class.new(citrus_mix_product, Zone.pack_zone).breakdown

            assert_equal expected_breakdown, calculated_breakdown
          end
        end
      end

      describe "for gift box Pack zone" do
        # Init a product that consists of lavender and eucalyptus oils
        # bound with 25inch band.
        let(:gift_box_product) { create_product_in_zones Zone.pack_zone }

        # Init Lavender Oil product and include it to the gift box.
        # We've got 111 made bottles in stock.
        let(:lavender_oil_product) { create :product }
        let(:lavender_oil_inventory_item) do
          find_or_create_product_inventory_item lavender_oil_product,
            production_zone: Zone.pack_zone, passed_zone: Zone.make_zone, quantity: 111
        end
        let(:lavender_oil_ingredient) do
          include_inventory_item_to_product(
            inventory_item: lavender_oil_inventory_item,
            product: gift_box_product,
            ingredient_quantity: 1
          )
        end

        # Init Euclyptus Oil product and include it to the gift box.
        # We've got 222 made bottles in stock.
        let(:eucalyptus_oil_product) { create :product }
        let(:eucalyptus_oil_inventory_item) do
          find_or_create_product_inventory_item eucalyptus_oil_product,
            production_zone: Zone.pack_zone, passed_zone: Zone.make_zone, quantity: 222
        end
        let(:eucalyptus_oil_ingredient) do
          include_inventory_item_to_product(
            inventory_item: eucalyptus_oil_inventory_item,
            product: gift_box_product,
            ingredient_quantity: 1
          )
        end

        # Init Band material and include it to the gift box.
        # We've got 125inch in stock, which is enough for 5 gift boxes.
        let(:band_material) { create :material }
        let(:band_inventory_item) do
          find_or_create_material_inventory_item band_material,
            production_zone: Zone.pack_zone, quantity: 125
        end
        let(:band_ingredient) do
          include_inventory_item_to_product(
            inventory_item: band_inventory_item,
            product: gift_box_product,
            ingredient_quantity: 25
          )
        end

        before do
          [lavender_oil_ingredient, eucalyptus_oil_ingredient, band_ingredient]
        end

        it "includes 'made' products as well as package materials" do
          expected_breakdown = {
            ingredients: {
              lavender_oil_ingredient.id   => BigDecimal.new(111),
              eucalyptus_oil_ingredient.id => BigDecimal.new(222),
              band_ingredient.id => BigDecimal.new(125 / 25)
            },
            previous_zone: {}
          }
          calculated_breakdown = described_class.new(gift_box_product, Zone.pack_zone).breakdown

          assert_equal expected_breakdown, calculated_breakdown
        end
      end
    end

    describe "#previous_zone_is_least_supplied?" do
      let(:breakdown) { { "Make" => 300 } }
      let(:supply) { described_class.new(stub, stub) }
      before do
        supply.stubs(:previous_zone_supply_breakdown).returns(breakdown)
      end

      it 'returns true if value is the previous zone inventory' do
        supply.stubs(:value).returns(300)
        assert supply.previous_zone_is_least_supplied?
      end

      it 'returns false otherwise' do
        supply.stubs(:value).returns(200)
        refute supply.previous_zone_is_least_supplied?
      end
    end

    describe "#least_supplied_ingredient_id" do
      let(:breakdown) { { 1 => 300, 2 => 100, 3 => 500 } }
      let(:supply) { described_class.new(stub, stub) }
      before do
        supply.stubs(:ingredient_supply_breakdown).returns(breakdown)
      end

      it 'returns nil if value is the previous zone inventory' do
        supply.stubs(:previous_zone_is_least_supplied?).returns(true)
        refute supply.least_supplied_ingredient_id
      end

      it 'returns ingredient ID if value is an ingredient supply' do
        supply.stubs(:previous_zone_is_least_supplied?).returns(false)
        supply.stubs(:value).returns(100)
        assert_equal 2, supply.least_supplied_ingredient_id
      end
    end
  end
end
