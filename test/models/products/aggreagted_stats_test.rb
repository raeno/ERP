# frozen_string_literal: true
require 'test_helper'

module Products
  class AggregatedStatsTest < ActiveSupport::TestCase
    before { Product.delete_all }
    let(:total_stats) { Products::AggregatedStats.new(Product.all) }

    describe '#average_selling_price' do
      it 'is an average selling prices of the given products' do
        create :product, selling_price: 1
        create :product, selling_price: 3
        create :product, selling_price: 5

        assert_equal 3.0, total_stats.average_selling_price
      end
    end

    describe "#total_inventory" do
      it 'is the total inventory quantity among all the given products' do
        stub_stats(:total_inventory)
        assert_equal 251, total_stats.total_inventory
      end
    end

    describe "#total_reserved" do
      it 'is the total inventory quantity among all the given products' do
        stub_stats(:reserved)
        assert_equal 251, total_stats.total_reserved
      end
    end

    describe "#total_inventory_price" do
      it 'is the total inventory quantity among all the given products' do
        stub_stats(:total_inventory_price)
        assert_equal 251, total_stats.total_inventory_price
      end
    end

    describe "#total_inventory_cogs" do
      it 'is the total inventory quantity among all the given products' do
        stub_stats(:total_inventory_cogs)
        assert_equal 251, total_stats.total_inventory_cogs
      end
    end

    describe "#total_inventory_profit" do
      it 'is the total inventory quantity among all the given products' do
        stub_stats(:total_inventory_profit)
        assert_equal 251, total_stats.total_inventory_profit
      end
    end

    describe "#total_reserved_quantity_profit" do
      it 'is the total inventory quantity among all the given products' do
        stub_stats(:reserved_quantity_profit)
        assert_equal 251, total_stats.total_reserved_quantity_profit
      end
    end

    private

    def stub_stats(method)
      stats = [
        stub(method => 200),
        stub(method => 50),
        stub(method => 1)
      ]
      total_stats.stubs(:stats).returns(stats)
    end
  end
end
