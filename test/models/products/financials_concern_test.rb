# frozen_string_literal: true
require 'test_helper'

module Products
  class FinancialsConcernTest < ActiveSupport::TestCase
    let(:product) { create :product }
    let(:zone) { create :zone }

    describe "#profit" do
      it "calculates profit" do
        product.fee = 1.01
        product.stubs(:prime_cost).returns(3.03)
        product.selling_price = 7.99
        assert_equal (7.99 - 3.03 - 1.01).round(2), product.profit.round(2)
      end

      it "returns nil if selling price is undefined" do
        product.selling_price = nil
        assert_nil product.profit
      end

      it "works fine with other default values" do
        product.selling_price = 7.99
        assert product.profit
      end
    end

    describe "#gross_margin" do
      it "calculates gross margin" do
        product.stubs(:profit).returns(2.02)
        product.selling_price = 4.99
        assert_equal (2.02 / 4.99 * 100).round(2), product.gross_margin
      end

      it "returns nil if selling price is undefined or zero" do
        product.stubs(:profit).returns(2.02)
        product.selling_price = nil
        assert_nil product.gross_margin

        product.selling_price = 0
        assert_nil product.gross_margin
      end

      it "returns nil if profit is nil" do
        product.stubs(:profit).returns(nil)
        assert_nil product.gross_margin
      end

      it "works fine with other default values" do
        product.selling_price = 7.99
        assert product.gross_margin
      end
    end

    describe "#roi" do
      it "calculates ROI" do
        product.stubs(:profit).returns(10.0)
        product.stubs(:prime_cost).returns(3.0)
        assert_equal 3.33, product.roi
      end

      it "returns nil if prime_cost is undefined or zero" do
        product.stubs(:prime_cost).returns(nil)
        assert_nil product.roi

        product.stubs(:prime_cost).returns(0)
        assert_nil product.roi
      end

      it "returns nil if profit is nil" do
        product.stubs(:profit).returns(nil)
        assert_nil product.roi
      end
    end

    describe '#prime_cost' do
      it 'returns total ingredient cost' do
        create_ingredient product, zone, unit_price: 2.10, quantity: 5
        create_ingredient product, zone, unit_price: 0.49, quantity: 1

        assert_equal 10.99, product.prime_cost
      end

      it 'returns 0 when there is no ingredients' do
        assert product.ingredients.empty?
        assert_equal 0, product.prime_cost
      end

      it 'works fine on default values and nils' do
        create :ingredient, product: product
        assert product.ingredients.any?
        assert product.prime_cost
      end
    end

    describe '#zone_cogs' do
      it 'returns ingredient cost spendings on single unit of this product in this zone' do
        create_ingredient product, zone, unit_price: 2.10, quantity: 5
        create_ingredient product, zone, unit_price: 0.49, quantity: 1

        irrelevant_zone = create :zone
        irrelevant_ingredient = create_ingredient product, irrelevant_zone, unit_price: 1000, quantity: 1
        assert product.ingredients.include?(irrelevant_ingredient)
        assert_equal 2.10 * 5 + 0.49, product.zone_cogs(zone)
      end

      it 'returns 0 when there is no ingredients' do
        assert product.ingredients.empty?
        assert_equal 0, product.zone_cogs(zone)
      end
    end

    describe '#cumulative_cogs' do
      let(:product) { create :product }
      let(:make_zone) { create :zone, :make }
      let(:pack_zone) { create :zone, :pack }
      let(:ship_zone) { create :zone, :ship }

      it 'returns ingredient cost spendings to produce single unit of this product up to this zone' do
        create_ingredient product, make_zone, quantity: 5, unit_price: 3.09

        create_ingredient product, pack_zone, quantity: 4, unit_price: 1.99
        create_ingredient product, pack_zone, quantity: 2, unit_price: 0.85

        create_ingredient product, ship_zone, quantity: 1, unit_price: 1.11

        assert_equal 3.09 * 5, product.cumulative_cogs(make_zone)
        assert_equal 3.09 * 5 + 1.99 * 4 + 0.85 * 2, product.cumulative_cogs(pack_zone)
        assert_equal 3.09 * 5 + 1.99 * 4 + 0.85 * 2 + 1.11, product.cumulative_cogs(ship_zone)
      end

      it 'returns 0 when there is no ingredients' do
        assert product.ingredients.empty?
        assert_equal 0, product.cumulative_cogs(pack_zone)
      end
    end

    describe "#make_labour_cost" do
      let(:product) { create :product }
      let(:make_zone) { create :zone, :make }

      it "delegates call to #make_inventory_item" do
        inventory_item = create :inventory_item
        inventory_item.stubs(:labour_cost).returns(77.77)
        product.stubs(:make_inventory_item).returns(inventory_item)
        assert_equal 77.77, product.make_labour_cost
      end

      describe "when #make_inventory_item is not found" do
        it "returns nil" do
          assert_nil product.make_labour_cost
        end
      end
    end

    describe "#pack_labour_cost" do
      let(:product) { create :product }
      let(:pack_zone) { create :zone, :pack }

      it "delegates call to #pack_inventory_item" do
        inventory_item = create :inventory_item
        inventory_item.stubs(:labour_cost).returns(77.77)
        product.stubs(:pack_inventory_item).returns(inventory_item)
        assert_equal 77.77, product.pack_labour_cost
      end

      describe "when #pack_inventory_item is not found" do
        it "returns nil" do
          assert_nil product.pack_labour_cost
        end
      end
    end

    describe "#labour_cost" do
      let(:product) { create :product }

      it "sums labour costs by all inventory items" do
        ii1 = create :inventory_item
        ii1.stubs(:labour_cost).returns(11)
        ii2 = create :inventory_item
        ii2.stubs(:labour_cost).returns(22)
        ii3 = create :inventory_item
        ii3.stubs(:labour_cost).returns(nil)
        product.stubs(:inventory_items).returns([ii1, ii2, ii3])
        assert_equal 33, product.labour_cost
      end
    end

    private

    def create_ingredient(product, zone, unit_price:, quantity:)
      material = create :material, unit_price: unit_price
      inventory_item = create :inventory_item, inventoriable: material, production_zone: zone, zone: zone
      create :ingredient, product: product, inventory_item: inventory_item, quantity_in_small_units: quantity
    end
  end
end
