# frozen_string_literal: true
require 'test_helper'

module Products
  class GiftBoxesConcernTest < ActiveSupport::TestCase
    let(:make_zone) { create :zone, :make}
    let(:pack_zone) { create :zone, :pack}

    describe "#gift_boxes" do
      it "returns all gift boxes the bottle product is included in" do
        bottle_product = create :product

        gift_box_product_1 = create :product
        include_product_to_giftbox(
          product: bottle_product, giftbox_product: gift_box_product_1,
          production_zone: pack_zone, passed_zone: make_zone
        )

        gift_box_product_2 = create :product
        include_product_to_giftbox(
          product: bottle_product, giftbox_product: gift_box_product_2,
          production_zone: pack_zone, passed_zone: make_zone
        )

        assert_equal 2, bottle_product.gift_boxes.count
        assert_includes bottle_product.gift_boxes, gift_box_product_1
        assert_includes bottle_product.gift_boxes, gift_box_product_2
      end

      it "returns empty relation otherwise" do
        product = create :product
        assert_empty product.gift_boxes
      end
    end

    describe "#gift_box?" do
      let(:product) { create :product }

      it "returns true if there are any product ingredients" do
        product_component = create :product
        inventory_item = create :inventory_item, inventoriable: product_component
        include_inventory_item_to_product(inventory_item: inventory_item, product: product)

        assert product.gift_box?
      end

      it "returns false otherwise" do
        refute product.gift_box?

        material_component = create :material
        inventory_item = create :inventory_item, inventoriable: material_component
        include_inventory_item_to_product(inventory_item: inventory_item, product: product)

        refute product.gift_box?
      end
    end
  end
end
