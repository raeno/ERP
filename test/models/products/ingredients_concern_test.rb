# frozen_string_literal: true
require 'test_helper'

module Products
  class IngredientsConcernTest < ActiveSupport::TestCase
    let(:product) { create :product }
    let(:zone) { create :zone }

    describe '#replace_ingredients' do
      let(:inventory_item_1) { create :inventory_item }
      let(:inventory_item_2) { create :inventory_item }

      it 'creates product ingredients from hash and returns true' do
        hash = [{ inventory_item_id: inventory_item_1.id, quantity_in_small_units: 30 },
                { inventory_item_id: inventory_item_2.id, quantity_in_small_units: 1  }]

        assert product.ingredients.empty?
        assert_difference 'Ingredient.count', 2 do
          assert product.replace_ingredients(hash)
        end

        ingredient_1 = product.ingredients.order(:id).first
        ingredient_2 = product.ingredients.order(:id).second
        assert_equal 30, ingredient_1.quantity_in_small_units
        assert_equal inventory_item_1, ingredient_1.inventory_item
        assert_equal 1, ingredient_2.quantity_in_small_units
        assert_equal inventory_item_2, ingredient_2.inventory_item
      end

      it 'works fine with string-value hashes' do
        hash = [{ "inventory_item_id" => inventory_item_1.to_param, "quantity_in_small_units" => "29" }]

        assert_difference 'Ingredient.count', 1 do
          assert product.replace_ingredients(hash)
        end

        ingredient = product.ingredients.first
        assert_equal 29, ingredient.quantity_in_small_units
        assert_equal inventory_item_1, ingredient.inventory_item
      end

      it 'replaces previous ingredients' do
        create :ingredient, product: product
        assert_equal 1, product.ingredients.count

        hash = [{ inventory_item_id: inventory_item_2.id, quantity_in_small_units: 1111 }]
        assert product.replace_ingredients(hash)
        assert_equal 1, product.ingredients.count

        ingredient = product.ingredients.first
        assert_equal 1111, ingredient.quantity_in_small_units
        assert_equal inventory_item_2, ingredient.inventory_item
      end

      describe 'when there are duplicated inventory_items' do
        let(:material) { build :material, name: 'NewMaterial' }
        let(:inventory_item_1) { create :inventory_item, inventoriable: material }

        it 'returns false + error message' do
          hash = [{ inventory_item_id: inventory_item_1.id, quantity_in_small_units: 30 },
                  { inventory_item_id: inventory_item_1.id, quantity_in_small_units: 1 }]

          assert_no_difference -> { Ingredient.count } do
            refute product.replace_ingredients(hash)
          end

          error_message = product.errors.full_messages.first
          assert_equal "Ingredient \"NewMaterial\" cannot be saved: Inventory item is a duplicate", error_message
        end
      end

      describe 'when data is invalid' do
        it 'in case of invalid data, leaves previous ingredients without changes' do
          # assuming the product aleady has an ingredient
          ingredient = create :ingredient, product: product
          assert_equal 1, product.ingredients.count

          hash = [{ inventory_item_id: inventory_item_1.id, quantity_in_small_units: 30 },
                  { inventory_item_id: nil, quantity_in_small_units: 1 }]

          assert_no_difference 'Ingredient.count' do
            refute product.replace_ingredients(hash)
          end

          # check the ingredient remains there (not removed)
          product.ingredients.reload
          assert_equal 1, product.ingredients.count
          assert_equal ingredient.id, product.ingredients.first.id
        end
      end
    end
  end
end
