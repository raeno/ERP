# frozen_string_literal: true
# == Schema Information
#
# Table name: products
#
#  id                       :integer          not null, primary key
#  title                    :string
#  seller_sku               :string
#  asin                     :text
#  fnsku                    :string
#  size                     :string
#  list_price_amount        :string
#  list_price_currency      :string
#  total_supply_quantity    :integer
#  in_stock_supply_quantity :integer
#  small_image_url          :string
#  is_active                :boolean          default(TRUE)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  inbound_qty              :float            default(0.0)
#  sold_last_24_hours       :float            default(0.0)
#  weeks_of_cover           :float            default(0.0)
#  sellable_qty             :float            default(0.0)
#  internal_title           :string
#  sales_rank               :integer
#  selling_price     :float
#  selling_price_currency   :string
#  items_per_case           :integer          default(0), not null
#  production_buffer_days   :integer          default(35), not null
#  batch_min_quantity       :integer          default(0), not null
#  batch_max_quantity       :integer
#  fee                      :decimal(, )      default(0.0), not null
#

require 'test_helper'

module Products
  class ProductTest < ActiveSupport::TestCase
    describe '.active' do
      it 'returns only active products' do
        active_product = create :product, :active
        create :product, is_active: false

        assert_equal [active_product], Product.active
      end
    end

    describe '.inactive' do
      it 'returns only inactive products' do
        create :product, :active
        not_active_product = create :product, is_active: false
        assert_equal [not_active_product], Product.inactive
      end
    end

    describe '#name' do
      it 'is internal title if available' do
        product = Product.new title: 'ann', internal_title: 'bob'
        assert_equal 'bob', product.name
      end

      it 'is title if internal title is empty' do
        product = Product.new title: 'ann', internal_title: ''
        assert_equal 'ann', product.name
      end

      it 'is "Untitled" if both titles blank' do
        product = Product.new title: '', internal_title: ''
        assert_equal 'Untitled', product.name
      end
    end

    describe '#destroy' do
      let(:product) { create :product }

      it 'also destroys associated ingredients' do
        create :ingredient, product: product

        assert_difference [-> { Product.count }, -> { Ingredient.count }], -1 do
          product.destroy
        end
      end
    end
  end
end
