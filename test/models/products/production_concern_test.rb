# frozen_string_literal: true
require 'test_helper'

module Products
  class ProductionConcernTest < ActiveSupport::TestCase
    describe '#production_urgency' do
      before { Setting.create!(red_urgency_level_days: 8, yellow_urgency_level_days: 16) }
      let(:product) { Product.new }

      describe 'when shipment report is prepaired for the product' do
        let(:product) { create :product }
        before do
          shipment_report = stub
          shipment_report.expects(:days_of_cover).once.returns(25)
          product.stubs(:shipment_report).returns(shipment_report)
        end
        it 'uses calculated days of cover value from the report' do
          assert_equal :green, product.production_urgency
        end
      end

      it 'returns :red when days_of_cover are less than or equal to the related global setting' do
        product.stubs(:days_of_cover).returns(0)
        assert_equal :red, product.production_urgency

        product.stubs(:days_of_cover).returns(1)
        assert_equal :red, product.production_urgency

        product.stubs(:days_of_cover).returns(8)
        assert_equal :red, product.production_urgency
      end

      it 'returns :yellow when days_of_cover are less than or equal to the related global setting' do
        product.stubs(:days_of_cover).returns(9)
        assert_equal :yellow, product.production_urgency

        product.stubs(:days_of_cover).returns(15)
        assert_equal :yellow, product.production_urgency

        product.stubs(:days_of_cover).returns(16)
        assert_equal :yellow, product.production_urgency
      end

      it 'returns :green when days_of_cover is higher' do
        product.stubs(:days_of_cover).returns(17)
        assert_equal :green, product.production_urgency

        product.stubs(:days_of_cover).returns(100)
        assert_equal :green, product.production_urgency
      end
    end

    describe '#reserved' do
      let(:product) { build :product }

      it 'delegates call to amazon inventory' do
        product.amazon_inventory = build :amazon_inventory, reserved: 222
        assert_equal 222, product.reserved
      end

      it 'is 0 if amazon inventory is nil' do
        product.amazon_inventory = nil
        assert_equal 0, product.reserved
      end
    end

    describe '#days_of_cover' do
      let(:product) { build :product }
      before do
        amazon_inventory = build :amazon_inventory
        amazon_inventory.stubs(:days_of_cover).returns(12.3)
        product.amazon_inventory = amazon_inventory
      end

      it 'delegates call to amazon inventory' do
        assert_equal 12.3, product.days_of_cover
      end

      it 'is 0 if amazon inventory is nil' do
        product.amazon_inventory = nil
        assert_equal 0, product.days_of_cover
      end

      describe "when the product is part of giftboxes" do
        it 'is the minimum days_of_cover among the product and the giftboxes' do
          gift_boxes = [stub(days_of_cover: 10), stub(days_of_cover: 30)]
          product.stubs(:gift_boxes).returns(gift_boxes)
          assert_equal 10, product.days_of_cover

          gift_boxes = [stub(days_of_cover: 50), stub(days_of_cover: 60)]
          product.stubs(:gift_boxes).returns(gift_boxes)
          assert_equal 12.3, product.days_of_cover
        end
      end
    end

    describe '#days_to_cover' do
      let(:product) { build :product }

      it 'delegates call to amazon inventory' do
        amazon_inventory = build :amazon_inventory
        amazon_inventory.stubs(:days_to_cover).returns(23.5)
        product.amazon_inventory = amazon_inventory
        assert_equal 23.5, product.days_to_cover
      end
    end

    describe '#amazon_coverage_ratio' do
      let(:product) { build :product }

      it 'delegates call to amazon inventory' do
        amazon_inventory = build :amazon_inventory
        amazon_inventory.stubs(:coverage_ratio).returns(90)
        product.amazon_inventory = amazon_inventory
        assert_equal 90, product.amazon_coverage_ratio
      end
    end
  end
end
