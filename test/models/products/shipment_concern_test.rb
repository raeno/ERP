# frozen_string_literal: true
require 'test_helper'

module Products
  class ShipmentConcernTest < ActiveSupport::TestCase
    describe '#shipping_priority' do
      let(:product) { Product.new }

      it 'delegates calculation to ShippingPriority' do
        ShippingPriority.any_instance.stubs(:priority).returns(10)
        assert_equal 10, product.shipping_priority

        ShippingPriority.any_instance.stubs(:priority).returns(35)
        assert_equal 35, product.shipping_priority
      end
    end

    describe 'need_to_allocate?' do
      let(:product) { Product.new }

      it 'delegates decisiion to ShippingPriority instance' do
        ShippingPriority.any_instance.stubs(:allocate?).returns(false)
        refute product.need_to_allocate?
        ShippingPriority.any_instance.stubs(:allocate?).returns(true)
        assert product.need_to_allocate?
      end
    end
  end
end
