# frozen_string_literal: true
# == Schema Information
#
# Table name: settings
#
#  id                        :integer          not null, primary key
#  aws_access_key_id         :string
#  aws_secret_key            :string
#  mws_marketplace_id        :string
#  mws_merchant_id           :string
#  address_name              :string
#  address_line1             :string
#  address_line2             :string
#  address_city              :string
#  address_state             :string
#  address_zip_code          :string
#  address_country           :string
#  created_at                :datetime
#  updated_at                :datetime
#  red_urgency_level_days    :integer          default(10), not null
#  yellow_urgency_level_days :integer          default(20), not null
#

require 'test_helper'

class SettingTest < ActiveSupport::TestCase
  describe '.instance' do
    it 'gets first entry of settings' do
      first, = Array.new(2) { Setting.create }
      assert_equal first, Setting.instance
    end

    it 'initiates new instance if empty' do
      Setting.destroy_all
      refute_nil Setting.instance
    end
  end

  describe '#amazon_config' do
    it 'provides hash with all amazon parameters' do
      Setting.create aws_access_key_id: 'key_id', aws_secret_key: 'secret_key', mws_marketplace_id: 'marketplace', mws_merchant_id: 'merchant'

      config = described_class.instance.amazon_config
      assert_equal 'key_id', config[:aws_access_key_id]
      assert_equal 'secret_key', config[:aws_secret_access_key]
      assert_equal 'merchant', config[:merchant_id]
      assert_equal 'marketplace', config[:primary_marketplace_id]
    end
  end

  describe '#shipment_address' do
    let(:params) do
      { address_name: 'Factory', address_line1: 'First Street',
        address_city: 'NY', address_state: 'WA',
        address_zip_code: '10002', address_country: 'US' }
    end
    it 'builds amazon address structs from setting info' do
      settings = Setting.new params
      address = settings.shipment_address
      assert_kind_of Amazon::Structs::Address, address
      assert_equal 'Factory', address.name
      assert_equal 'First Street', address.address_line_1
      assert_equal 'NY', address.city
      assert_equal 'WA', address.state_or_province_code
      assert_equal '10002', address.postal_code
      assert_equal 'US', address.country_code
    end
  end
end
