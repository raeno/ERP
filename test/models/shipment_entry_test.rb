# == Schema Information
#
# Table name: shipment_entries
#
#  id                 :integer          not null, primary key
#  amazon_shipment_id :integer
#  product_id         :integer
#  quantity           :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  status             :string           default("NEW")
#  batch_id           :integer
#

require 'test_helper'

class ShipmentEntryTest < ActiveSupport::TestCase
  let(:items_per_case) { 20 }
  let(:quantity) { 10 }

  let(:warehouse) { create :fba_warehouse }
  let(:amazon_shipment_cases) { create :amazon_shipment, fba_warehouse: warehouse, cases_required: true }
  let(:amazon_shipment_items) { create :amazon_shipment, fba_warehouse: warehouse, cases_required: false }
  let(:product) { create :product, items_per_case: items_per_case, seller_sku: 'ABCD' }

  subject { described_class.new product: product, amazon_shipment: amazon_shipment_cases, quantity: quantity }

  describe '#quantity' do
    it 'is always exact number of cases or items' do
      assert_equal quantity, subject.quantity
      assert subject.cases_required?

      subject.stubs(cases_required?: false)
      assert_equal quantity, subject.quantity
    end
 end

  describe '#shipment_type' do
    it 'is "items" when shipping by items' do
      subject.stubs(cases_required?: false)
      assert_equal 'items', subject.shipment_type
    end

    it 'is "cases" when shipping by cases' do
      subject.stubs(cases_required?: true)
      assert_equal 'cases', subject.shipment_type
    end
  end

  describe '#quantity_in_case' do
    describe 'for shipments by items' do
      subject { described_class.new product: product, amazon_shipment: amazon_shipment_items, quantity: 10 }

      it 'is always nil' do
        assert_nil subject.quantity_in_case
      end
    end

    describe 'for shipments by case' do
      subject { described_class.new product: product, amazon_shipment: amazon_shipment_cases, quantity: 10 }

      it 'equal product items per case value' do
        assert_equal 20, subject.quantity_in_case
      end
    end
  end

  describe '#seller_sku' do
    it 'delegates call to product' do
      assert_equal product.seller_sku, subject.seller_sku
    end
  end

  describe '#to_h' do
    it 'contains product sku' do
      hash = subject.to_h
      assert_equal product.seller_sku, hash[:seller_sku]
    end

    describe 'when shipping by cases' do
      it 'contains positive quantity in case' do
        hash = subject.to_h
        assert_equal 20, hash[:quantity_in_case]
      end

      it 'contains quantity in items' do
        hash = subject.to_h
        assert_equal 10 * 20, hash[:quantity]
      end
    end
  end
end
