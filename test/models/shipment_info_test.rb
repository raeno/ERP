require 'test_helper'

class ShipmentInfoTest < ActiveSupport::TestCase

  describe '.from_shipment' do
    let(:shipment) { create :amazon_shipment, shipment_id: 'ABC' }
    let(:product) { create :product, seller_sku: 'SKU_1', items_per_case: 20 }
    let(:address) { build :address }
    before do
      create :shipment_entry, :new, amazon_shipment: shipment, product: product, quantity: 5
      create :shipment_entry, :changed, amazon_shipment: shipment
      create :shipment_entry, :amazon_synced, amazon_shipment: shipment
    end

    it 'builds shipment info to sync amazon shipment' do
      instance = described_class.from_shipment shipment, address

      assert_equal 'ABC', instance.shipment_id
      assert_kind_of Amazon::Structs::InboundShipmentHeader, instance.header
      assert_equal 2, instance.items.count
      expected_first_item = { quantity_shipped: 100, quantity_in_case: 20, seller_sku: 'SKU_1'}
      assert_equal expected_first_item, instance.items.first
    end
  end
end
