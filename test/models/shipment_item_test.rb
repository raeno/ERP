require 'test_helper'

class ShipmentItemTest < ActiveSupport::TestCase
  let(:sku) { 'ABC-10ML' }
  let(:product) { build :product, seller_sku: sku }
  let(:quantity) { 10 }

  subject { described_class.new product, quantity } 

  describe '#seller_sku' do
    it 'delegates sku to product' do
      assert_equal sku, subject.seller_sku
    end
  end

  describe '#shipment_type' do
    it 'is alwyas "items"' do
      assert_equal 'items', subject.shipment_type
    end
  end

  describe '#shipment_in_cases?' do
    it 'is always false' do
      refute subject.shipment_in_cases?
    end
  end

  describe '.build' do
    it 'builds shipment item for items shipment' do
      instance = described_class.build product, quantity, 'items'
      assert_kind_of ShipmentItem, instance
    end

    it 'builds cases shipment item for cases shipment' do
      instance = described_class.build product, quantity, 'cases'
      assert_kind_of CasesShipmentItem, instance
    end
  end

  describe '.from_params' do
    let(:params) { { product_id: product.id, quantity: 30, shipment_type: 'items'} }
    let(:product) { create :product, seller_sku: sku }

    it 'builds product with proper fields from params' do
      instance = described_class.from_params params
      assert_kind_of described_class, instance
      assert_equal product, instance.product
      assert_equal 30, instance.quantity
    end

    it 'returns nil if product cannot be found' do
      params = { quantity: 30, shipment_type: 'items' }
      instance = described_class.from_params params
      assert_nil instance
    end

    it 'builds product with default properties' do
      minimal_params = { product_id: product.id }
      instance = described_class.from_params minimal_params
      assert_kind_of CasesShipmentItem, instance
      assert_equal 0, instance.quantity
      assert_equal product, instance.product
    end
  end
end
