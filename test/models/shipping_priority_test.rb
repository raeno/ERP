# frozen_string_literal: true
require 'test_helper'

class ShippingPriorityTest < ActiveSupport::TestCase
  let(:product) { create :product }
  let(:inventory_item) { build :inventory_item, inventoriable: product }
  subject { described_class.new(product) }

  def stub_production_plan(amount)
    production_plan = stub(can_cover_in_cases: amount)
    ProductionPlan::Plan.stubs(:build).returns(production_plan)
  end

  before do
    product.stubs(:ship_inventory_item).returns(inventory_item)
  end

  describe '#priority' do
    describe 'when product does not have ship inventory item' do
      before do
        product.stubs(:ship_inventory_item).returns(nil)
      end

      it 'has zero priority' do
        assert_equal 0, subject.priority
      end
    end

    describe 'when product has items to ship' do
      before { stub_production_plan 10 }

      describe 'when amazon coverage is zero' do
        before { product.stubs(:amazon_coverage_ratio).returns(0) }

        it 'has maximum priority' do
          assert_equal 1_000_000, subject.priority
        end

        it 'also at the allocation upper limit' do
          assert_equal ShippingPriority::ALLOCATION_UPPER_LIMIT, subject.priority
        end
      end

      describe 'when amazon coverage is negative' do
        before { product.stubs(:amazon_coverage_ratio).returns(-10) }

        it 'does not go negative' do
          assert_equal ShippingPriority::ALLOCATION_UPPER_LIMIT, subject.priority
        end
      end

      describe 'when amazon coverage is 100%' do
        before { product.stubs(:amazon_coverage_ratio).returns(100) }

        it 'has priority set at allocation lower limit' do
          assert_equal ShippingPriority::ALLOCATION_LOWER_LIMIT, subject.priority
        end
      end

      describe 'when amazon coverage is very high' do
        before { product.stubs(:amazon_coverage_ratio).returns(1_000_000) }

        it 'still limits lowest priority' do
          assert_equal 1000, subject.priority
        end
      end

      describe 'when amazon coverage somewhere in between of 0 and 100' do
        before { product.stubs(:amazon_coverage_ratio).returns(40) }

        it 'has priority between lower and upper allocation limit' do
          priority = subject.priority
          assert priority > ShippingPriority::ALLOCATION_LOWER_LIMIT
          assert priority < ShippingPriority::ALLOCATION_UPPER_LIMIT
        end
      end
    end

    describe 'when product does not have items to ship' do
      before { stub_production_plan 0 }

      it 'has priority lower than allocation limit' do
        assert subject.priority < ShippingPriority::ALLOCATION_LOWER_LIMIT
      end

      describe 'when product overcovered' do
        before { product.stubs(:amazon_coverage_ratio).returns(1_000_000_000) }

        it 'goes as low as to 1' do
          assert_equal 1, subject.priority
        end
      end
    end
  end
end
