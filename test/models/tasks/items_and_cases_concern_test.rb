# frozen_string_literal: true

require 'test_helper'

module Tasks
  class ItemsAndCasesConcernTest < ActiveSupport::TestCase
    subject { create :task }
    before do
      subject.quantity = 900
      subject.product_items_per_case = 100
    end

    describe '#quantity_in_zone_units' do
      describe 'getter' do
        it 'returns quantity_cases for pack zone' do
          subject.stubs(:pack?).returns(true)
          assert_equal 9, subject.quantity_in_zone_units
        end

        it 'returns quantity for other zones' do
          subject.stubs(:pack?).returns(false)
          assert_equal 900, subject.quantity_in_zone_units
        end
      end

      describe 'setter' do
        it 'sets quantity_cases for pack zone' do
          subject.stubs(:pack?).returns(true)
          subject.quantity_in_zone_units = 2
          assert_equal 200, subject.quantity
        end

        it 'sets quantity for other zones' do
          subject.stubs(:pack?).returns(false)
          subject.quantity_in_zone_units = 2
          assert_equal 2, subject.quantity
        end
      end
    end

    describe '#quantity_cases' do
      describe 'getter' do
        it 'converts task quantity to cases' do
          assert_equal 9, subject.quantity_cases
        end
      end

      describe 'setter' do
        it 'sets task quantity based on the given case quantity' do
          subject.quantity_cases = 7
          assert_equal 700, subject.quantity
        end

        it 'for new tasks with undefined items_per_case, sets quantity based on inventory_item.product_items_per_case' do
          subject = build :task
          refute subject.product_items_per_case
          subject.inventory_item.stubs(:product_items_per_case).returns(70)

          subject.quantity_cases = 7
          assert_equal 490, subject.quantity
        end
      end
    end
  end
end
