# frozen_string_literal: true

require 'test_helper'

module Tasks
  class StatusAndDurationConcernTest < ActiveSupport::TestCase
    let(:task) { create :task }

    describe '.in_progress scope' do
      it 'returns all started but not finished tasks' do
        task1 = create :task, started_at: 1.hour.ago
        task2 = create :task, started_at: 1.hour.ago, finished_at: 5.minutes.ago
        collection = Task.in_progress
        assert collection.include?(task1)
        refute collection.include?(task2)
      end
    end

    describe "#in_progress_by?" do
      let(:sam)  { create :user }
      before { task.update_column(:user_id, sam.id) }

      it 'returns true if the task is assigned to the given user and is in progress' do
        task.stubs(:in_progress?).returns(true)
        assert task.in_progress_by?(sam)
      end

      it 'returns false if the task is not assigned to the given user' do
        john = create :user
        task.stubs(:in_progress?).returns(true)
        refute task.in_progress_by?(john)
      end

      it 'returns false if the task is not in progress' do
        task.stubs(:in_progress?).returns(false)
        refute task.in_progress_by?(sam)
      end
    end

    describe '#status?' do
      it 'is pending when there is no start time' do
        task.started_at = nil
        assert task.pending?
      end

      it 'is started when started, no matter if finished or not' do
        task.started_at = 1.hour.ago
        assert task.started?

        task.finished_at = 1.minute.ago
        assert task.started?
      end

      it 'is in progress when started but not finished' do
        task.started_at = 1.hour.ago
        task.finished_at = nil
        assert task.in_progress?
      end

      it 'is finished when finished time is set' do
        task.finished_at = 1.hour.ago
        assert task.finished?
      end
    end

    describe '#duration' do
      before { task.started_at = 3.minutes.ago }

      it 'returns duration in seconds between started_at time and finished_at (if set)' do
        task.finished_at = 1.minute.ago
        assert_equal 120, task.duration
      end

      it 'returns duration between started_at and now otherwise' do
        task.finished_at = nil
        assert_equal 180, task.duration
      end

      it 'returns 0 when started_at is blank' do
        task.started_at = nil
        assert_equal 0, task.duration
      end
    end

    describe '#duration_hours' do
      it 'converts #duration from seconds to hours' do
        task.stubs(:duration).returns(9180)
        assert_equal 2.55, task.duration_hours
      end
    end

    describe '#duration_estimate_hours' do
      it 'converts #duration_estimate from seconds to hours' do
        task.duration_estimate = 9180
        assert_equal 2.55, task.duration_estimate_hours
      end
    end

    describe '#seconds_left' do
      it 'is the difference between duration_estimate and actual duration' do
        task.duration_estimate = 500
        task.stubs(:duration).returns(300)
        assert_equal 200, task.seconds_left
      end

      it 'can be negative' do
        task.duration_estimate = 500
        task.stubs(:duration).returns(700)
        assert_equal(-200, task.seconds_left)
      end
    end
  end
end
