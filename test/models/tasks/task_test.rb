# frozen_string_literal: true
# == Schema Information
#
# Table name: tasks
#
#  id                      :integer          not null, primary key
#  inventory_item_id       :integer          not null
#  started_at              :datetime
#  finished_at             :datetime
#  quantity                :integer
#  created_at              :datetime
#  updated_at              :datetime
#  quantity_cases_obsolete :integer
#  labour_cost             :decimal(, )
#  user_id                 :integer          not null
#  duration_estimate       :integer
#  date                    :date
#  product_items_per_case  :integer
#
# Foreign Keys
#
#  fk_rails_...  (inventory_item_id => inventory_items.id)
#

require 'test_helper'

module Tasks
  class TaskTest < ActiveSupport::TestCase
    subject { create :task }
    before { Timecop.freeze }
    let(:may_first) { Date.new(2017, 5, 1) }
    let(:may_fourth) { Date.new(2017, 5, 4) }
    let(:may_tenth) { Date.new(2017, 5, 10) }

    describe '.till_date scope' do
      it 'returns tasks with the date before or equal to the given one' do
        task1 = create :task, date: may_first
        task2 = create :task, date: may_fourth
        task3 = create :task, date: may_tenth

        collection = Task.till_date(may_fourth)
        assert collection.include? task1
        assert collection.include? task2
        refute collection.include? task3
      end
    end

    describe '.after_date scope' do
      it 'returns tasks with the date after or equal to the given one' do
        task1 = create :task, date: may_fourth
        task2 = create :task, date: may_tenth
        task3 = create :task, date: may_first

        collection = Task.after_date(may_fourth)
        assert collection.include? task1
        assert collection.include? task2
        refute collection.include? task3
      end
    end

    describe '.within_date_range scope' do
      before do
        @task1 = create :task, date: 1.day.from_now
        @task2 = create :task, date: 2.days.from_now
        @task3 = create :task, date: 3.days.from_now
        @task4 = create :task, date: 4.days.from_now
        @task5 = create :task, date: 5.days.from_now
      end

      it 'returns tasks within the given date range' do
        date_range = DateRange.new(start_date: 2.days.from_now, end_date: 4.days.from_now)
        collection = Task.within_date_range(date_range)
        assert_include collection, @task2, @task3, @task4
        refute_include collection, @task1, @task5
      end

      it 'returns all tasks when date range is missing' do
        date_range = nil
        collection = Task.within_date_range(date_range)
        assert_include collection, @task1, @task2, @task3, @task4, @task5
      end

      it 'works without the end date' do
        date_range = DateRange.new(start_date: 2.days.from_now)
        collection = Task.within_date_range(date_range)
        assert_include collection, @task2, @task3, @task4, @task5
        refute_include collection, @task1
      end

      it 'works without the start date' do
        date_range = DateRange.new(end_date: 4.days.from_now)
        collection = Task.within_date_range(date_range)
        assert_include collection, @task1, @task2, @task3, @task4
        refute_include collection, @task5
      end
    end

    describe "#quantity_remainder" do
      it 'is difference between the aimed and the real quantity' do
        subject.quantity = 100
        subject.stubs(:batch_quantity).returns(80)
        assert_equal 20, subject.quantity_remainder
      end

      it 'returns nil if quantity is undefined' do
        subject.stubs(:batch_quantity).returns(120)

        subject.quantity = nil
        assert_nil subject.quantity_remainder
      end
    end

    describe "#coverage" do
      it 'is a proportion of the really produced qty and the demanded qty' do
        subject.quantity = 100
        subject.stubs(:batch_quantity).returns(120)
        assert_equal 1.2, subject.coverage
      end

      it 'works fine if there is no batch' do
        subject.quantity = 100
        refute subject.batch
        assert_equal 0, subject.coverage
      end

      it 'returns nil if quantity is zero or undefined' do
        subject.stubs(:batch_quantity).returns(120)

        subject.quantity = nil
        assert_nil subject.coverage

        subject.quantity = 0
        assert_nil subject.coverage
      end
    end

    describe '#unit_labour_cost' do
      it 'calculates single unit labour cost based on the total labour cost' do
        subject.stubs(:labour_cost).returns(100.0)
        subject.stubs(:batch_quantity).returns(2000)
        assert_equal 0.05, subject.unit_labour_cost
      end

      it 'returns nil when labour_cost is undefined' do
        subject.stubs(:labour_cost).returns(nil)
        assert_nil subject.unit_labour_cost
      end

      it 'returns nil when batch_quantity is undefined or zero' do
        subject.stubs(:batch_quantity).returns(nil)
        assert_nil subject.unit_labour_cost

        subject.stubs(:batch_quantity).returns(0)
        assert_nil subject.unit_labour_cost
      end
    end

    describe '#set_labour_cost' do
      before do
        subject.stubs(:duration_hours).returns(2.55)
        subject.user.stubs(:labour_rate).returns(10)
      end

      it 'sets total labour cost vased on the user labour rate and the task duration' do
        subject.set_labour_cost
        assert_equal 25.50, subject.labour_cost
      end

      it 'produces no effect if task duration is undefined' do
        subject.stubs(:duration_hours).returns(nil)
        subject.set_labour_cost
        assert_nil subject.labour_cost
      end

      it 'produces no effect if labour rate is undefined' do
        subject.stubs(:labour_rate).returns(nil)
        subject.set_labour_cost
        assert_nil subject.labour_cost
      end
    end

    describe '#batch_default_attributes' do
      let(:zone) { create :zone }
      let(:product) { create :product }
      let(:inventory_item) { create :inventory_item, inventoriable: product, zone: zone }
      let(:user) { create :user }
      let(:task) { create :task, inventory_item: inventory_item, user: user, quantity: 234 }

      it 'returns default attributes for a batch' do
        expected_attributes = {
          product_id: product.id,
          zone_id: zone.id,
          quantity: 234,
          task_id: task.id,
          user_ids: [user.id]
        }
        assert_equal expected_attributes, task.batch_default_attributes
      end
    end
  end
end
