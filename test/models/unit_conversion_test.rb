# frozen_string_literal: true
# == Schema Information
#
# Table name: unit_conversions
#
#  id           :integer          not null, primary key
#  from_unit_id :integer          not null
#  to_unit_id   :integer          not null
#  rate  :decimal(, )      not null
#

require 'test_helper'

class UnitConversionTest < ActiveSupport::TestCase
  let(:ml)   { create :unit }
  let(:lbs)  { create :unit }
  let(:oz)   { create :unit }
  let(:inch) { create :unit }

  before do
    create :conversion_rate, from_unit: lbs, to_unit: ml, rate: 473.18
    create :conversion_rate, from_unit: oz,  to_unit: ml
    inch # make sure inch record is present
  end

  describe '.units_convertable_to' do
    it 'returns all units that are convertable to the given unit' do
      units = UnitConversion.units_convertable_to(ml)
      assert_equal 3, units.count
      assert_include units, ml, oz, lbs
    end

    it 'includes the given unit itself as a minumum' do
      units = UnitConversion.units_convertable_to(inch)
      assert_equal 1, units.count
      assert_include units, inch
    end

    it 'works fine with inverse conversion rules' do
      # For oz, we only have a reverse conversion oz -> ml.
      # The system should infer that ml is convertable to oz.
      units = UnitConversion.units_convertable_to(oz)
      assert_equal 2, units.count
      assert_include units, ml, oz
    end
  end

  describe '#rate' do
    it 'returns the rate if a direct conversion rule is there' do
      assert_equal 473.18, UnitConversion.new(lbs, ml).rate
    end

    it 'returns 1/rate if an inverse conversion rule is there' do
      assert_equal 1 / 473.18, UnitConversion.new(ml, lbs).rate.to_f
    end

    it 'returns 1 if from == to' do
      assert_equal 1, UnitConversion.new(lbs, lbs).rate
    end

    it 'returns nil if the record was not found' do
      assert_nil UnitConversion.new(lbs, inch).rate
    end
  end

  describe '.convert' do
    it 'returns amount * rate if the rate is present' do
      assert_equal 4731.8, UnitConversion.new(lbs, ml).convert(10).to_f
      assert_equal 111.0,  UnitConversion.new(lbs, lbs).convert(111).to_f
    end

    it 'throws an exception if the rate was not found' do
      assert_raise RuntimeError do
        UnitConversion.new(lbs, inch).convert(23)
      end
    end
  end

  describe '.valid?' do
    it 'returns true if there is a relevant conversion rule' do
      assert UnitConversion.new(lbs, ml).valid?
      assert UnitConversion.new(lbs, lbs).valid?
    end

    it 'returns false otherwise' do
      refute UnitConversion.new(lbs, inch).valid?
    end
  end
end
