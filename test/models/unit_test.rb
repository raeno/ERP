# frozen_string_literal: true
# == Schema Information
#
# Table name: units
#
#  id        :integer          not null, primary key
#  name      :text
#  full_name :text
#

require 'test_helper'

class UnitTest < ActiveSupport::TestCase
  describe '.each_piece' do
    it 'gets "Each" unit instance' do
      each_piece_unit = Unit.create name: 'Each'
      assert_equal each_piece_unit, Unit.each_piece
    end
  end
end
