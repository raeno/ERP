# frozen_string_literal: true

require 'test_helper'

class UserWorkloadTest < ActiveSupport::TestCase
  let(:alex) { create :user }
  let(:jane) { create :user }

  let(:today)    { Time.zone.today }
  let(:tomorrow) { today + 1.day }

  let(:workload) { UserWorkload.new }

  before do
    create :task, user: alex, date: today, duration_estimate: 60
    create :task, user: alex, date: today, duration_estimate: 120

    create :task, user: jane, date: tomorrow, duration_estimate: 100
    create :task, user: jane, date: tomorrow, duration_estimate: 60, finished_at: 1.day.ago

    workload
  end

  describe '#workload_for' do
    it 'is total unfinished tasks duration (in seconds) for the given user and date' do
      assert_equal 180, workload.workload_for(today, alex.id)
      assert_nil workload.workload_for(tomorrow, alex.id)

      assert_nil workload.workload_for(today, jane.id)
      assert_equal 100, workload.workload_for(tomorrow, jane.id)

      assert_nil workload.workload_for(10.days.ago, alex.id)
      assert_nil workload.workload_for(10.days.from_now, jane.id)
    end
  end

  describe '#dates' do
    it 'is the list of all dates included in workload stats' do
      assert_equal [today, tomorrow], workload.dates
    end
  end

  describe '#users' do
    it 'is the list of active users' do
      assert_include workload.users, alex, jane
    end
  end
end
