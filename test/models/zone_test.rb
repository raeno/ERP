# frozen_string_literal: true
# == Schema Information
#
# Table name: zones
#
#  id               :integer          not null, primary key
#  name             :string
#  production_order :integer
#  created_at       :datetime
#  updated_at       :datetime
#

require 'test_helper'

class ZoneTest < ActiveSupport::TestCase
  describe '#slug' do
    it 'is downcased name' do
      zone = Zone.new name: 'AbCdEF'
      assert_equal 'abcdef', zone.slug
    end
  end

  describe '#make?' do
    it 'checks if name of zone is "Make"' do
      zone = Zone.new name: 'Make'
      assert zone.make?
      other_zone = Zone.new name: 'Pack'
      refute other_zone.make?
    end
  end

  describe '#pack?' do
    it 'checks if name of zone is "Pack"' do
      zone = Zone.new name: 'Pack'
      assert zone.pack?
      other_zone = Zone.new name: 'Make'
      refute other_zone.pack?
    end
  end

  describe '#ship?' do
    it 'checks if name of zone is "Ship"' do
      zone = Zone.new name: 'Ship'
      assert zone.ship?
      other_zone = Zone.new name: 'Make'
      refute other_zone.ship?
    end
  end

  describe '#components_with_positive_amount' do
    it 'gets only components with positive amount' do
      zone = Zone.new name: 'Make'
      components = stub(with_positive_amount: [1, 2, 3])
      zone.stubs(:components).returns(components)
      assert_equal [1, 2, 3], zone.components_with_positive_amount
    end
  end

  describe '.fetch_or_default' do
    it 'fetches zone by id' do
      zone = Zone.create
      fetched_zone = Zone.fetch_or_default(zone.id)
      assert_equal zone, fetched_zone
    end

    it 'returns default zone if could not find zone by id' do
      default_zone = Zone.create name: 'some_default_zone'
      Zone.stub(:default_zone, -> { default_zone }) do
        non_existent_zone_id = default_zone.id + 100
        fetched_zone = Zone.fetch_or_default(non_existent_zone_id)
        assert_equal default_zone, fetched_zone
      end
    end
  end

  describe '.latest_change' do
    before do
      @recent, = [1.day.ago, 2.days.ago, 3.days.ago].map { |t| create :zone, updated_at: t }
    end

    it 'gets latest updated_at datetime' do
      assert_equal @recent.updated_at.utc.to_s(:iso8601), Zone.latest_change.utc.to_s(:iso8601)
    end
  end

  describe '.default_zone' do
    it 'is "Make" zone' do
      Zone.create(name: 'Pack')
      make_zone = Zone.create(name: 'Make')
      assert_equal make_zone, Zone.default_zone
    end

    it 'is nil if "Make" zone not exists' do
      assert_nil Zone.default_zone
    end
  end

  describe 'compare' do
    it 'is comarable based on production order' do
      zone1 = build :zone, production_order: 1
      zone2 = build :zone, production_order: 2
      zone3 = build :zone, production_order: 3

      assert zone1 <= zone2
      assert zone1 <  zone3
      refute zone2 >= zone3
    end
  end
end
