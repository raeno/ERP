# frozen_string_literal: true
require 'test_helper'

class BatchChartPresenterTest < ActiveSupport::TestCase
  describe '#chart_data' do
    let(:start_date) { 5.days.ago }
    let(:end_date) { Time.zone.today }
    let(:zone_names) { %w(Make Pack) }
    let(:zones) { zone_names.map { |name| create :zone, name: name } }
    let(:product) { create :product }
    let(:batches) do
      [Time.zone.now, 1.day.ago, 2.days.ago].each_with_index.map do |date, index|
        first_zone_batch = Batch.new product: product, zone: zones.first, completed_on: date, quantity: (index + 3) * 10
        last_zone_batch = Batch.new product: product, zone: zones.last, completed_on: date, quantity: (index + 5) * 50
        [first_zone_batch, last_zone_batch]
      end.flatten
    end

    subject do
      described_class.new start_date: start_date, end_date: end_date, zones: zones
    end

    before do
      Timecop.freeze
      ordered = stub(ordered_by_completion: batches)
      Batch.stubs(:for_interval).returns(ordered)
    end

    it 'provides days information for chart legend' do
      result = subject.chart_data
      days = result.days
      valid_days = [[Time.zone.today].to_s, [1.day.ago.to_date].to_s, [2.days.ago.to_date].to_s]
      assert_equal valid_days, days
    end

    it 'provides data for each zon' do
      result = subject.chart_data
      chart_data = result.chart_data
      chart_data.each do |data|
        assert zone_names.include?(data[:name])
      end
    end

    it 'accumulates amount of products made each day for each zone' do
      result = subject.chart_data
      chart_data = result.chart_data
      assert_equal [30, 40, 50], chart_data.first[:data]
      assert_equal [250, 300, 350], chart_data.last[:data]
    end
  end
end
