# frozen_string_literal: true
require 'test_helper'

class DailyTasksPresenterTest < ActiveSupport::TestCase
  let(:presenter) { described_class.new(Task.all, Time.zone.today) }

  describe "#total_hours" do
    it "is the total duration estimate (in hours) for the given tasks, rounded" do
      Task.delete_all
      create :task, duration_estimate: 2.22 * 3600
      create :task, duration_estimate: 3.33 * 3600

      assert_equal 6, presenter.total_hours
    end
  end

  describe "#date_title" do
    it "is Overdue if the collection is for overdue tasks" do
      date = 1.day.ago.to_date
      presenter = described_class.new(Task.all, date)
      assert_equal "Overdue: #{date.to_s(:date_month)}", presenter.date_title
    end

    it "is Today if the collection is for today's date" do
      assert_equal "Today: #{Time.zone.today.to_s(:date_month)}", presenter.date_title
    end

    it "is usually a formatted date" do
      date = 2.days.from_now.to_date
      presenter = described_class.new(Task.all, date)
      assert_equal date.to_s(:date_month), presenter.date_title
    end
  end
end
