# frozen_string_literal: true
require 'test_helper'

class IngredientPresenterTest < ActiveSupport::TestCase
  subject { described_class.new ingredient }
  let(:ingredient) { stub(zones_available: Zone.all) }
  let(:zone) { build :zone, name: 'Pack' }

  describe '#quantity_with_unit' do
    let(:unit) { build :unit, name: 'ml' }
    let(:material) { create :material, unit: unit, ingredient_unit: unit }
    let(:inventory_item) { create :inventory_item, inventoriable: material }
    let(:ingredient) { build :ingredient, inventory_item: inventory_item, quantity_in_small_units: 50 }

    it 'returns quantity of material with a unit of measure' do
      presenter = described_class.new ingredient
      assert_equal "50&nbsp;ml", presenter.quantity_with_unit
    end
  end

  describe '#components?' do
    it 'is true when have at least one component in selected zone' do
      components = [stub(material?: true, active_product?: false, name: 'Cap')]
      zone.stubs(:components).returns(components)
      assert subject.components?(zone)
    end

    it 'is false when no components in specified zone' do
      zone.stubs(:components).returns([])
      refute subject.components?(zone)
    end

    it 'is false when all components are not active products and not materials' do
      components = %w(Some Another Third).map { |name| stub(material?: false, active_product?: false, name: name) }
      zone.stubs(:components).returns(components)
      refute subject.components?(zone)
    end
  end

  describe '#zone_products' do
    it 'fetches components for specified zone' do
      zone.expects(:components).once.returns([])
      subject.zone_products zone
    end

    it 'is empty array for any zone except pack' do
      ship_zone = create :zone, name: 'Ship'
      assert_equal [], subject.zone_products(ship_zone)
    end

    it 'selects only active products' do
      product = build :product, is_active: false
      active_product = build :product, is_active: true
      inventoriables = [active_product, product]
      components = inventoriables.map { |inventoriable| create :inventory_item, inventoriable: inventoriable }
      zone.stubs(:components).returns(components)

      active, = components
      assert_equal [active], subject.zone_products(zone)
    end

    it 'sort components by name' do
      components = %w(Cap GiftBox Bottle).map { |name| stub(active_product?: true, name: name) }
      zone.stubs(:components).returns(components)
      zone_components_names = subject.zone_products(zone).map(&:name)
      assert_equal %w(Bottle Cap GiftBox), zone_components_names
    end
  end

  describe '#zone_materials' do
    it 'fetches components for specified zone' do
      zone.expects(:components).once.returns([])
      subject.zone_materials zone
    end

    it 'selects only materials' do
      material = build :material
      product = build :product, is_active: true
      inventoriables = [material, product]
      components = inventoriables.map { |inventoriable| create :inventory_item, inventoriable: inventoriable }
      zone.stubs(:components).returns(components)

      material_component, = components
      assert_equal [material_component], subject.zone_materials(zone)
    end

    it 'sort components by name' do
      components = %w(Cap GiftBox Bottle).map { |name| stub(material?: true, name: name) }
      zone.stubs(:components).returns(components)
      zone_components_names = subject.zone_materials(zone).map(&:name)
      assert_equal %w(Bottle Cap GiftBox), zone_components_names
    end
  end
end
