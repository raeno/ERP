# frozen_string_literal: true
require 'test_helper'

class InventoryItemPresenterTest < ActiveSupport::TestCase
  let(:inventory_item) { InventoryItem.new }
  subject { described_class.new(inventory_item) }

  describe '#product_presenter' do
    it 'returns ProductPresenter instance if inventory the item belongs to a product' do
      product = create :product
      inventory_item.inventoriable = product
      assert inventory_item.product?
      assert subject.product_presenter.is_a?(ProductPresenter)
    end
  end

  describe '#zones' do
    it 'gets all zones ordered' do
      zones = Object.new
      Zone.expects(:ordered).once.returns(zones)
      assert_equal zones, subject.zones
    end
  end

  describe '#units' do
    it 'gets all units' do
      units = Object.new
      Unit.expects(:all).once.returns(units)
      assert_equal units, subject.units
    end
  end

  describe '#inventory_item_type' do
    it 'is actually inventory item nature' do
      inventory_item = InventoryItem.new
      inventory_item.stubs(:nature).returns('some_nature')

      presenter = described_class.new inventory_item
      assert_equal 'some_nature', presenter.inventory_item_type
    end
  end

  describe '#current_type?' do
    it 'is true when passed inventory item type equal to current' do
      subject.stubs(:inventory_item_type).returns('Material')
      assert subject.current_type?('Material')
      refute subject.current_type?('Product')
    end
  end

  describe '#to_cover_cases' do
    subject { described_class.new build(:inventory_item) }
    before { subject.stubs(:to_cover).returns(449) }

    describe "for product inventory items" do
      before do
        subject.stubs(:product?).returns(true)
        subject.stubs(:product).returns(stub(items_per_case: 150))
      end

      it 'converts #to_cover number to cases' do
        assert_equal 3, subject.to_cover_cases
      end
    end

    describe "for material inventory items" do
      before { subject.stubs(:product?).returns(false) }

      it 'returns nil' do
        assert_nil subject.to_cover_cases
      end
    end
  end

  describe '#to_cover_in_main_units' do
    subject { described_class.new build(:inventory_item) }
    before do
      subject.stubs(:to_cover).returns(5_555)
      subject.stubs(:to_cover_cases).returns(3)
    end

    it 'for materials, returns #to_cover and does not  depend on the zone' do
      subject.stubs(:product?).returns(false)
      subject.stubs(:in_pack_zone?).returns(false)
      assert_equal "5,555", subject.to_cover_in_main_units

      subject.stubs(:in_pack_zone?).returns(true)
      assert_equal "5,555", subject.to_cover_in_main_units
    end

    it 'for products in pack zone, returns #to_cover_cases' do
      subject.stubs(:product?).returns(true)
      subject.stubs(:in_pack_zone?).returns(true)
      assert_equal "3", subject.to_cover_in_main_units
    end

    it 'for products in other zones, returns #to_cover' do
      subject.stubs(:product?).returns(true)
      subject.stubs(:in_pack_zone?).returns(false)
      assert_equal "5,555", subject.to_cover_in_main_units
    end
  end
end
