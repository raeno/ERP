# frozen_string_literal: true
require 'test_helper'

class InvoiceChartPresenterTest < ActiveSupport::TestCase
  def build_invoice(date, status)
    invoice = build :invoice, created_at: date, total_price: 100
    invoice.stubs("#{status}?").returns(true)
    invoice
  end

  describe '#chart_data' do
    it 'groups all invoices by month' do
      invoices = [1, 1, 2, 3, 4, 4, 5].map do |n|
        build_invoice n.months.ago, 'paid'
      end
      presenter = described_class.new invoices
      chart_data = presenter.chart_data
      refute_nil chart_data.months
      assert_equal 5, chart_data.months.size
    end

    it 'prepares total spending by each status as chart series' do
      invoices = [[1, :paid], [1, :received], [2, :paid], [3, :paid]].map do |n, status|
        build_invoice n.months.ago, status
      end
      presenter = described_class.new invoices
      chart_data = presenter.chart_data
      refute_nil chart_data.series
      assert_equal 3, chart_data.series.count
      expected = [
        { name: :pending, data: [0, 0, 0] },
        { name: :unpaid, data: [100, 0, 0] },
        { name: :paid, data: [100, 100, 100] }
      ]
      assert_equal expected, chart_data.series
    end
  end
end
