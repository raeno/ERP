require 'test_helper'

class MaterialCostChartPresenterTest < ActiveSupport::TestCase

  let(:material) { create :material }
  subject { described_class.new material }

  def create_container_with_deps(date, price, amount)
    container = create :material_container, amount: amount, price: price
    create :invoice_item, material_container: container, received_date: date
    container
  end

  setup do
    container_1 = create_container_with_deps Date.new(2017,9,1), 100, 70
    container_2 = create_container_with_deps Date.new(2017,9, 15), 1000, 500
    container_3 = create_container_with_deps Date.new(2017,9, 25), 2000, 1500
    @containers = [container_1, container_2, container_3]

    containers_query = stub(already_received_ordered: @containers)
    subject.stubs(:containers_query).returns(containers_query)
  end

  it "provides days as categories" do 
    data = subject.chart_data

    expected_days = ["09/01/17", "09/15/17", "09/25/17"]
    assert_equal expected_days, data.days
  end

  it "aggregates unit price of each container in series" do
    data = subject.chart_data

    aggregated_costs = [1.429, 2.0, 1.333]

    serie = { name: material.name, data: aggregated_costs }
    assert_equal [serie], data.series
  end
end
