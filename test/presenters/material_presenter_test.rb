# frozen_string_literal: true
require 'test_helper'

class MaterialPresenterTest < ActiveSupport::TestCase
  let(:material) { build :material }
  subject { described_class.new(material) }

  describe '#vendors_for_selection' do
    it 'gets all vendors' do
      vendors = Object.new
      Vendor.expects(:ordered).once.returns(vendors)
      assert_equal vendors, subject.vendors_for_selection
    end
  end

  describe '#units_for_selection' do
    it 'gets all units' do
      units = Object.new
      Unit.expects(:all).once.returns(units)
      assert_equal units, subject.units_for_selection
    end
  end

  describe '#vendors_comma_separated' do
    it 'is a comma-separate list of all material vendors' do
      vendor1 = build :vendor, name: "ABC"
      vendor2 = build :vendor, name: "DEF"
      material.vendors = [vendor1, vendor2]
      material.save!
      assert_equal "ABC, DEF", subject.vendors_comma_separated
    end
  end

  describe "#default_unit_conversion_rate" do
    it 'returns system default conversion rate' do
      stub_unit_conversion(material.unit, material.ingredient_unit, from_amount: 10, to_amount: 20)
      assert_equal 2.0, subject.default_unit_conversion_rate
    end

    it 'returns nil unless material main unit is specified' do
      material.unit = nil
      assert_nil subject.default_unit_conversion_rate
    end

    it 'returns nil unless material ingredient unit is specified' do
      material.ingredient_unit = nil
      assert_nil subject.default_unit_conversion_rate
    end
  end

  describe "#current_unit_conversion_rate" do
    it 'returns the material main-to-ingredient unit conversion rate' do
      stub_material_unit_conversion(material, rate: 5.5)
      assert_equal 5.5, subject.current_unit_conversion_rate
    end
  end

  describe "#pending_invoice?" do
    let(:container) { create :material_container, material: material, unit: material.unit }
    let(:invoice_item) { create :invoice_item, material_container: container }

    before do
      material.save!
      invoice_item
    end

    it 'returns true if more material is pending (there is a pending invoice item)' do
      assert subject.pending_invoice?
    end

    it 'returns false otherwise' do
      invoice_item.update_column(:received_date, 5.days.ago)
      refute subject.pending_invoice?
    end
  end
end
