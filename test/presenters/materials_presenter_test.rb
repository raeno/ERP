# frozen_string_literal: true
require 'test_helper'

class MaterialsPresenterTest < ActiveSupport::TestCase
  describe '#active_category_css_class' do
    it "returns 'active' if zone passed matches presenter's zone, and blank otherwise" do
      category = stub(id: 5)
      another_category = stub(id: 6)
      presenter = described_class.new([], category.id, nil)

      assert_equal 'active', presenter.active_category_css_class(category)
      assert presenter.active_category_css_class(another_category).blank?
    end
  end

  describe '#active_zone_css_class' do
    it "returns 'active' if zone passed matches presenter's zone, and blank otherwise" do
      zone = stub(id: 5)
      another_zone = stub(id: 6)
      presenter = described_class.new([], nil, zone.id)

      assert_equal 'active', presenter.active_zone_css_class(zone)
      assert presenter.active_zone_css_class(another_zone).blank?
    end
  end
end
