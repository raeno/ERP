# frozen_string_literal: true
require 'test_helper'

class ProductInventoryPresenterTest < ActiveSupport::TestCase
  describe "#inventory_item_presenters" do
    it "returns inventory item presenters ordered by zone" do
      # intentionally not creating in conventional order
      create :zone, :ship
      create :zone, :make
      create :zone, :pack

      product = build :product
      Zone.find_each { |zone| create :inventory_item, inventoriable: product, zone: zone }

      product_presenter = described_class.new(product)
      ii_presenters = product_presenter.inventory_item_presenters
      assert_equal Zone.make_zone, ii_presenters[0].inventory_item.zone
      assert_equal Zone.pack_zone, ii_presenters[1].inventory_item.zone
      assert_equal Zone.ship_zone, ii_presenters[2].inventory_item.zone
    end
  end
end
