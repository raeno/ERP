# frozen_string_literal: true
require 'test_helper'

class ProductStatsPresenterTest < ActiveSupport::TestCase
  let(:product) { build :product }
  subject { described_class.new(product) }

  describe "#days_of_cover_hint" do
    it "explains how days_of_cover is calculated" do
      subject.stubs(:inbound_shipped).returns(100)
      subject.stubs(:fulfillable).returns(20)
      subject.stubs(:reserved).returns(30)
      assert_equal "= 120 / 30".gsub(' ', '&nbsp;'), subject.days_of_cover_hint
    end
  end
end
