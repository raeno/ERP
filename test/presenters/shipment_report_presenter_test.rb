# frozen_string_literal: true
require 'test_helper'

class ShipmentReportPresenterTest < ActiveSupport::TestCase
  let(:shipment_report) { build :shipment_report }

  describe '#items_per_case' do
    it 'wraps report items per case' do
      shipment_report = stub(items_per_case: 30)
      subject = described_class.new shipment_report
      assert_equal '30&nbsp;ppc', subject.items_per_case
    end
  end

  describe '#allocate_items?' do
    it 'allows to allocate when priority higher certain limit' do
      shipment_report = stub(items_shipment_priority: 10_000)
      subject = described_class.new shipment_report
      assert subject.allocate_items?
    end

    it 'forbids to allocate when priority is lower that limit' do
      shipment_report = stub(items_shipment_priority: 1_000)
      subject = described_class.new shipment_report
      refute subject.allocate_items?
    end
  end

  describe '#allocate_items_class' do
    it 'is empty when can allocate items' do
      subject = described_class.new shipment_report
      subject.stubs(:allocate_items?).returns(true)
      assert_equal '', subject.allocate_items_class
    end

    it 'hides element when cannot allocate items' do
      subject = described_class.new shipment_report
      subject.stubs(:allocate_items?).returns(false)
      assert_equal 'hidden', subject.allocate_items_class
    end
  end

  describe '#allocate_cases_class' do
    it 'is empty when can allocate cases' do
      subject = described_class.new shipment_report
      subject.stubs(:allocate_cases?).returns(true)
      assert_equal '', subject.allocate_cases_class
    end

    it 'hides element when cannot allocate items' do
      subject = described_class.new shipment_report
      subject.stubs(:allocate_cases?).returns(false)
      assert_equal 'hidden', subject.allocate_cases_class
    end
  end

  describe '#allocate_cases?' do
    it 'allows to allocate when priority higher certain limit' do
      shipment_report = stub(cases_shipment_priority: 10_000)
      subject = described_class.new shipment_report
      assert subject.allocate_cases?
    end

    it 'forbids to allocate when priority is lower that limit' do
      shipment_report = stub(cases_shipment_priority: 1_000)
      subject = described_class.new shipment_report
      refute subject.allocate_cases?
    end
  end
end
