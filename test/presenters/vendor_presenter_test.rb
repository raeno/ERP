# frozen_string_literal: true
require 'test_helper'

class VendorPresenterTest < ActiveSupport::TestCase
  let(:vendor) { build :vendor }
  subject { described_class.new(vendor) }

  describe '#name_with_lead_time' do
    it 'formats name and lead time together' do
      vendor.name = 'ABC'
      vendor.lead_time_days = 15
      assert_equal "ABC - 15 days", subject.name_with_lead_time
    end
  end
end
