# frozen_string_literal: true
require 'test_helper'

module MaterialContainers
  class SameMaterialQueryTest < ActiveSupport::TestCase
    let(:unit) { create :unit }
    let(:material_1) { create :material, unit: unit }
    let(:material_2) { create :material, unit: unit }
    subject { described_class.new material_1 }

    before do
      @active = create :material_container, status: 'active', material: material_1, unit: unit, lot_number: "M1_1"
      @pending_without_invoice = create :material_container, status: 'pending', material: material_1, unit: unit, lot_number: "M1_2"
      create :material_container, status: 'pending', material: material_2, unit: unit, lot_number: 'M2_1'
    end

    describe '#all' do
      it 'fetches all containers with same material' do
        assert_equal 2, subject.all.size
      end
    end

    describe '#active' do
      it 'fetches first active container of same material' do
        assert_equal @active, subject.active
      end
    end

    describe '#latest' do
      let(:invoice_1) { create :invoice, date: 1.year.ago }
      let(:invoice_2) { create :invoice, date: 10.days.ago }
      before do
        @most_recent = create :material_container, material: material_1, unit: unit, lot_number: 'M1_3'
        create :invoice_item, invoice: invoice_2, material_container: @most_recent
        old_one = create :material_container, material: material_1, unit: unit, lot_number: 'M1_4'
        create :invoice_item, invoice: invoice_1, material_container: old_one
      end

      it 'fetches most recent container based on invoice date' do
        assert_equal @most_recent, subject.latest
      end
    end

    describe '#next_to_open' do
      let(:invoice_1) { create :invoice, date: 1.year.ago }
      let(:invoice_2) { create :invoice, date: 1.week.ago }

      before do
        @pending_old = create :material_container, material: material_1, unit: unit, lot_number: 'M1_5', status: 'pending'
        create :invoice_item, invoice: invoice_1, material_container: @pending_old
        @pending_new = create :material_container, material: material_1, unit: unit, lot_number: 'M1_6', status: 'pending'
        create :invoice_item, invoice: invoice_2, material_container: @pending_new
      end

      it 'fetches next container to open' do
        assert_equal @pending_old, subject.next_to_open
      end
    end
  end
end
