# frozen_string_literal: true
require 'test_helper'

class AmazonFbaAllocationsFetcherTest < ActiveSupport::TestCase
  let(:success_result) { stub(success?: true) }
  let(:failure_result) { stub(success?: false) }
  let(:ship_zone) { create :zone, name: 'Ship' }
  let(:update_allocation_action) { stub(call: success_result) }
  let(:ship_from_address) { stub }
  let(:products) { create_list :product, 2 }
  subject { described_class.new update_allocation_action }

  before do
    products.each do |p|
      inventory_item = build :inventory_item, product: p, zone: ship_zone
      p.stubs(:need_to_allocate?).returns(true)
      p.stubs(:ship_inventory_item).returns(inventory_item)
    end
  end

  describe '.build' do
    it 'builds an instance of fetcher using default dependencies' do
      fetcher = described_class.build(ship_from_address)
      assert_kind_of described_class, fetcher
    end
  end

  describe '#run' do
    before do
      Product.stubs(:active).returns(products)
    end

    it 'update allocations for every active product for cases and for items shipping types' do
      update_allocation_action.expects(:call).times(4).returns(failure_result)
      subject.run
    end

    describe 'when all updates are successfully' do
      it 'is successful' do
        result = subject.run
        assert result.success?
      end
    end

    describe 'when some updates failed' do
      it 'provides failure result' do
        update_allocation_action.expects(:call).returns(failure_result)
        result = subject.run
        assert result.failure?
      end
    end
  end
end
