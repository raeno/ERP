# frozen_string_literal: true
require 'test_helper'

class AmazonInboundShipmentProcessorTest < ActiveSupport::TestCase
  let(:inbound_shipment_api) { stub(get_prep_instructions_for_sku: {}) }
  let(:prep_instructions_parser) { stub(parse: {})}
  let(:shipment_plans_parser) { stub(parse: {}) }
  let(:sku) { 123 }
  let(:item) { OpenStruct.new(seller_sku: '123', quantity: 50) }
  let(:inbound_shipment_plans) { [OpenStruct.new(destination_fulfillment_center_id: 'test', items: [item])] }
  let(:shipment_plans_result) { ShipmentPlansResult.new(inbound_shipment_plans, true, []) }
  let(:product) { create :product, seller_sku: sku }
  let(:products_to_ship) { [OpenStruct.new(product: product, quantity: 100)] }

  subject { described_class.new inbound_shipment_api, prep_instructions_parser, shipment_plans_parser }


  describe '.build' do
    it 'builds an instance of processor using default dependencies' do
      processor = described_class.build(anything)
      assert_kind_of described_class, processor
    end
  end

  describe '#create_shipment_plan' do

    it 'return shipment plans result successfully' do
      inbound_shipment_api.expects(:create_shipment_plan).with(anything).returns(anything)
      shipment_plans_parser.expects(:parse).with(anything).returns(shipment_plans_result)
      processor = described_class.new inbound_shipment_api, prep_instructions_parser, shipment_plans_parser
      processor.create_shipment_plan(products_to_ship)
    end
  end

  describe '#create_shipment' do
    let(:shipment_header_hash) { { shipment_name: 'TEST', are_cases_required: true } }
    let(:shipment_header) { stub(to_h: shipment_header_hash) }
    let(:api_response) { Object.new }
    let(:shipment_info) { build :shipment_info, header: shipment_header, shipment_id: '123' }

    before do
      inbound_shipment_api.stubs(:create_shipment).returns(api_response)
    end

    it 'adds preparation details info to each item before shipping' do
      items = [{ seller_sku: 'SKU_1' }, { seller_sku: 'SKU_2' }]
      instructions_per_sku = { 'SKU_1' => Object.new, 'SKU_2' => Object.new }

      # we expect processor to add preparation instructions to each item
      inbound_shipment_api.expects(:create_shipment).once.with do |_, _, items|
        items.all? do |item|
        item_sku = item[:seller_sku]
          item[:prep_details_list] = instructions_per_sku[item_sku]
        end
      end

      processor = described_class.new inbound_shipment_api, prep_instructions_parser, shipment_plans_parser
      processor.stubs(:fetch_instructions_for_skus)
               .returns(instructions_per_sku)

      shipment_info = OpenStruct.new id: '123', header: shipment_header, items: items
      processor.create_shipment shipment_info
    end

    it 'quieries shipment api with proper paramaters' do
      inbound_shipment_api.expects(:create_shipment).with('123', shipment_header_hash, []).once
      processor = described_class.new inbound_shipment_api, prep_instructions_parser, shipment_plans_parser
      processor.create_shipment shipment_info
    end

    it 'returns api response as result' do
      response = subject.create_shipment shipment_info
      assert_equal api_response, response
    end
  end
end
