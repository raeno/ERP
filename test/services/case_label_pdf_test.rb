# frozen_string_literal: true

require 'test_helper'

class CaseLabelPdfTest < ActiveSupport::TestCase
  let(:sku) { 'KIS-10ML-LAV' }
  let(:title) { 'test title' }
  let(:size) { '10ml' }
  let(:items_per_case) { 20 }
  let(:product) { create :product, seller_sku: sku, items_per_case: items_per_case, title: title, size: size }

  it 'renders necessary product properties to pdf' do
    pdf = described_class.new(product).render
    pdf_text = PDF::Inspector::Text.analyze(pdf).strings

    assert_includes pdf_text, title
    assert_includes pdf_text, "SKU: #{sku}"
    assert_includes pdf_text, "SIZE: #{size}"
    assert_includes pdf_text, "CASE: #{items_per_case}"
  end

  it 'supports long skus' do
    long_sku = 'A' * 25
    product = create :product, seller_sku: long_sku, items_per_case: items_per_case, title: title, size: size

    pdf = described_class.new(product).render
    pdf_text = PDF::Inspector::Text.analyze(pdf).strings

    assert_includes pdf_text, "SKU: #{long_sku}"
  end
end
