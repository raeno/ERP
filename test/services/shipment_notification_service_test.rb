# frozen_string_literal: true
require 'test_helper'

class ShipmentNotificationServiceTest < ActiveSupport::TestCase
  let(:user) { create :user, id: 1000 }
  subject { described_class.new }

  describe '#call' do
    let(:expected_channel_name) { 'shipment_channel_1000' }
    let(:shipment_name) { "SHIPMENT_#1" }
    let(:shipment_data) { { message: 'some_message', status: 'working' } }

    it 'broadcasts passed data to notification server' do
      notification_server = mock
      notification_server.expects(:broadcast).with(expected_channel_name, any_parameters).once
      subject.stubs(:notification_server).returns(notification_server)
      subject.call user, shipment_name, shipment_data
    end
  end
end
