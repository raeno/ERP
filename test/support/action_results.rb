# frozen_string_literal: true
module ActionResults
  def self.included(klass)

    def successful_action
      stub(call: success_result)
    end

    def faulty_action
      stub(call: failure_result)
    end

    klass.class_eval do
      let(:success_result) do
        stub(success?: true, failure?: false, entity: Object.new,
          entities: [Object.new], errors: [])
      end

      let(:failure_result) do
        stub(success?: false, failure?: true, errors: [],
          	 entity: nil, error_message: 'Something went wrong')
      end
    end
  end
end

class ActiveSupport::TestCase
  include ActionResults
end
