# frozen_string_literal: true
module AssertInclude
  # Asserts that enumerable includes all the elements
  def assert_include(enumerable, *elements)
    elements.each do |element|
      assert enumerable.include?(element), "#{element} was not included"
    end
  end

  # Asserts that enumerable excludes all the elements
  def refute_include(enumerable, *elements)
    elements.each do |element|
      refute enumerable.include?(element), "#{element} was included"
    end
  end
end

class ActiveSupport::TestCase
  include AssertInclude
end
