# frozen_string_literal: true
module AssertUntouched
  # Usage example: assert_untouched(product, inventory) do ... end
  def assert_untouched(*models)
    old_dates = updated_at_dates(models)
    yield
    new_dates = updated_at_dates(models)
    assert_equal old_dates, new_dates, "Some models have been touched"
  end

  def updated_at_dates(models)
    models.map { |model| model.reload.updated_at }
  end
end

class ActiveSupport::TestCase
  include AssertUntouched
end
