# frozen_string_literal: true

module DurationEstimateHelpers
  def stub_duration_estimate(inventory_item, quantity, duration)
    duration_estimate = stub
    duration_estimate.expects(:duration_for).with(quantity).returns(duration)
    ProductionEstimate.expects(:new).with(inventory_item).returns(duration_estimate)
  end
end

class ActiveSupport::TestCase
  include DurationEstimateHelpers
end
