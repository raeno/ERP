# frozen_string_literal: true
def create_ingredient_in_zone(zone, ingredient_attrs = {})
  material = create :material
  inventory_item = create :inventory_item, production_zone: zone, inventoriable: material
  create :ingredient, ingredient_attrs.merge(inventory_item: inventory_item)
end

def build_ingredient(inventoriable, quantity: 1)
  inventory_item = build :inventory_item, inventoriable: inventoriable
  build :ingredient, inventory_item: inventory_item, quantity: quantity
end

def include_material_to_product(material:, product:, production_zone:, ingredient_quantity: 1)
  inventory_item = find_or_create_material_inventory_item material,
    production_zone: production_zone

  ingredient = include_inventory_item_to_product(
    inventory_item: inventory_item, product: product, ingredient_quantity: ingredient_quantity
  )
  product.reload

  ingredient
end

def include_product_to_giftbox(product:, giftbox_product:, passed_zone:,
    production_zone: nil, ingredient_quantity: 1)
  inventory_item = find_or_create_product_inventory_item product,
    production_zone: production_zone, passed_zone: passed_zone

  ingredient = include_inventory_item_to_product(
    inventory_item: inventory_item, product: giftbox_product, ingredient_quantity: ingredient_quantity
  )
  giftbox_product.reload

  ingredient
end

def include_inventory_item_to_product(inventory_item:, product:, ingredient_quantity: 1)
  ingredient = build :ingredient,
    product: product, inventory_item: inventory_item, quantity_in_small_units: ingredient_quantity
  assert_difference -> { inventory_item.ingredients.count }, 1 do
    ingredient.save!
  end

  inventory_item.reload
  product.reload
  ingredient
end
