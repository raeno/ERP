# frozen_string_literal: true

def create_inventory(product, zone, quantity: 0)
  inventory_item = create :inventory_item, inventoriable: product, zone: zone
  create :inventory, inventory_item: inventory_item, quantity: quantity
end

def find_or_create_inventory(inventory_item, quantity: 0)
  inventory = inventory_item.inventory
  inventory ||= create :inventory, inventory_item: inventory_item
  inventory.quantity = quantity
  inventory.save!
  inventory_item.reload

  inventory
end

def find_inventory_item(inventoriable, passed_zone)
  inventoriable.inventory_items.in_zone(passed_zone).first
end

def find_or_create_product_inventory_item(product, passed_zone:, production_zone: nil, quantity: 0, with_report: false)
  inventory_item = product.inventory_items.for_zone(production_zone).in_zone(passed_zone).first
  inventory_item ||= create :inventory_item,
    inventoriable: product, production_zone: production_zone, zone: passed_zone

  find_or_create_inventory(inventory_item, quantity: quantity)
  find_or_create_shipment_report(product) if with_report
  find_or_create_production_report(inventory_item) if with_report

  inventory_item
end

def find_or_create_material_inventory_item(material, production_zone: create(:zone), quantity: 0, with_report: false)
  inventory_item = material.inventory_item
  inventory_item ||= create :inventory_item,
    inventoriable: material, production_zone: production_zone, zone: production_zone

  find_or_create_inventory(inventory_item, quantity: quantity)
  find_or_create_production_report(inventory_item) if with_report

  material.reload
  inventory_item
end

