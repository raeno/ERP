# frozen_string_literal: true

def create_invoice_item_with_date(date, item_attrs = {})
  invoice = create :invoice, date: date
  material_container = item_attrs.delete(:material_container) || create(:material_container)
  create :invoice_item, item_attrs.merge(invoice: invoice, material_container: material_container)
end

def create_invoice_item_with_ordering_size(ordering_size, item_attrs = {})
  material = ordering_size.material
  container = create :material_container, ordering_size: ordering_size, material: material, unit: ordering_size.unit
  create :invoice_item, item_attrs.merge(material_container: container, material: material)
end
