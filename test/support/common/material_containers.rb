# frozen_string_literal: true
module MaterialContainerTestHelpers
  def create_container_with_invoice_item(options = {})
    invoice_date = options.delete(:date) || Time.zone.now
    create(:material_container, options).tap do |container|
      create_invoice_item_with_date invoice_date, material: container.material, material_container: container
    end
  end
end

class ActiveSupport::TestCase
  include MaterialContainerTestHelpers
end
