# frozen_string_literal: true

def stub_product_to_cover_plan(product:, in_zone:, to_cover:)
  inventory_item = find_inventory_item(product, in_zone)
  stub_to_cover_plan(inventory_item, to_cover)
end

def stub_to_cover_plan(inventory_item, to_cover)
  plan = stub(to_cover: to_cover)
  ProductionPlan::Plan.stubs(:build).with(inventory_item).returns(plan)
end
