# frozen_string_literal: true

module Common
  module Products
    def create_product_in_zones(start_zone, attrs = {})
      product = create(:product, :active, attrs)

      zones = Zone.starting_from(start_zone)
      product.zones += zones
      product.save!

      zones.each do |zone|
        find_or_create_product_inventory_item(product, passed_zone: zone)
      end

      product.reload
    end

    # create_product_with_data(
    #   ingredient: { inventory_item: material_inventory_item, quantity: 10 },
    #   to_cover: { in_zone: Zone.make_zone, quantity: 55 }
    # )
    def create_product_with_data(ingredient: nil, to_cover: nil)
      # Create product
      product = create_product_in_zones Zone.make_zone

      # Add ingredient
      if ingredient
        include_inventory_item_to_product(
          inventory_item: ingredient[:inventory_item],
          product: product,
          ingredient_quantity: ingredient[:quantity]
        )
      end

      # Stub product demand
      if to_cover
        stub_product_to_cover_plan(product: product, in_zone: to_cover[:in_zone], to_cover: to_cover[:quantity])
      end

      product
    end
  end
end

class ActiveSupport::TestCase
  include Common::Products
end
