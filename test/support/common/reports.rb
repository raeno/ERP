# frozen_string_literal: true

def find_or_create_production_report(inventory_item)
  inventory_item.production_report ||=
    create :production_report, inventory_item: inventory_item
end

def find_or_create_shipment_report(product)
  product.shipment_report ||=
    create :shipment_report, product: product
end
