# frozen_string_literal: true
module UnitConversionTestHelpers
  def stub_unit_conversion(from_unit, to_unit, from_amount: 1, to_amount: 1)
    conversion = stub(valid?: true)
    conversion.stubs(:rate).returns(to_amount.to_f / from_amount)
    conversion.stubs(:convert).with(from_amount).returns(to_amount)
    UnitConversion.stubs(:new).with(from_unit, to_unit).returns(conversion)
  end

  def stub_material_unit_conversion(material, rate: 1)
    material.unit_conversion_rate = rate
  end
end

class ActiveSupport::TestCase
  include UnitConversionTestHelpers
end
