# frozen_string_literal: true

module ActionStubbingTestHelpers
  # Normally you should provide :with argument to expect proper action arguments.
  def expect_success_once(klass, with: nil, result_entity: nil)
    action_result = stub(success?: true, entity: result_entity)
    expect_action_once klass, action_result, with: with
  end

  def expect_failure_once(klass, with: nil, result_entity: nil, error_message: 'Error')
    action_result = stub(success?: false, entity: result_entity, error_message: error_message)
    expect_action_once klass, action_result, with: with
  end

  private

  def expect_action_once(action_class, action_result, with: nil)
    expectation = action_class.any_instance.expects(:call).once.returns(action_result)
    expectation.with(*Array.wrap(with)) if with
  end
end

class ActionController::TestCase
  include ActionStubbingTestHelpers
end
