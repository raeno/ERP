# frozen_string_literal: true

module Integration
  module Navigation
    def visit_ship_page
      visit ship_product_reports_path
    end

    def visit_make_page
      visit make_tasks_path
    end

    def visit_pack_page
      visit pack_tasks_path
    end
  end
end

class ActionDispatch::IntegrationTest
  include Integration::Navigation
end
